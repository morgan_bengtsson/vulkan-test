#include "modelsystem.h"

#include "componenttypes.h"

#include <vulkanBufferCopier.h>
#include <vulkanBarrier.h>

ModelSystem::ModelSystem(vulkanutils::VulkanContext& context, SceneType scene)
	:System<ModelSystem>(context)
	, mCommandPool(context.device(), context.device().getComputeQueue(), VK_COMMAND_POOL_CREATE_RESET_COMMAND_BUFFER_BIT)
	, mCommandBuffers(getVulkanContext().device(), mCommandPool, 1)
{
}

#pragma optimize ("", off)

void ModelSystem::runImpl(SceneType scene)
{
	auto& cc = scene.getComponentCollection();
	auto& gpuCc = scene.getGpuComponentCollection();

	if (!mSetup)
		setup(scene);
	for (ecs::Entity* entity : scene.getEntities<component::Position>())
	{
		if (!entity->hasGpuComponents<vulkanutils::UploadBuffer, component::Transform>(gpuCc)
			|| entity->hasComponents<component::CameraProjection>(gpuCc))
			continue;

		std::span<const component::Position> positions = entity->getComponentArray<component::Position>(cc);
		const component::Position& position = (!positions.empty()) ? positions[0] : Eigen::Vector3f::Zero();
		std::span<const component::Orientation> orientations = entity->getComponentArray<component::Orientation>(cc);
		const component::Orientation& orientation = (!orientations.empty()) ? orientations[0] : Eigen::Matrix3f::Identity();
		std::span<const component::Scale> scales = entity->getComponentArray<component::Scale>(cc);
		const component::Scale& scale = (!scales.empty()) ? scales[0] : Eigen::Vector3f::Ones();

		Eigen::Affine3f affineTransform; affineTransform.fromPositionOrientationScale(position, orientation, scale);
		auto hostData = entity->getHostGpuComponentArray<component::Transform>(gpuCc, 0);
		Eigen::Map<Eigen::Matrix4f> transform(hostData.getData()[0].matrix);
		transform = affineTransform.matrix();

		const std::vector<ecs::Entity*>& aabBoxEntities = scene.getRefEntities<component::MeshAABoundingBox>(entity, ecs::EntityRef::Type::Child);
		auto aabBox = entity->getComponent<component::MeshAABoundingBox>(cc);
		auto aabbBoxHostData = entity->getHostGpuComponentArray<component::MeshAABoundingBox>(gpuCc);

		for (size_t i = 0; i < aabBoxEntities.size(); i++)
		{
			const component::MeshAABoundingBox& meshBox = aabBoxEntities[i]->getComponent<component::MeshAABoundingBox>(cc);
			auto transformedMeshAABB = meshBox.aabb.transformed(affineTransform);
			aabBox.aabb.extend(transformedMeshAABB);
		}
		aabbBoxHostData.getData()[0].aabb = aabBox.aabb;
	}
	getVulkanContext().compute({ mCommandBuffers[0] });
}

#pragma optimize ("", on)

void ModelSystem::setup(SceneType scene)
{
	auto& cc = scene.getComponentCollection();
	auto& gpuCc = scene.getGpuComponentCollection();

	vulkanutils::CommandBufferRecorder cbRecorder(mCommandBuffers[0], VK_COMMAND_BUFFER_USAGE_SIMULTANEOUS_USE_BIT);
	vulkanutils::BufferCopier bufferCopier(mCommandBuffers[0]);
	for (auto& entity : scene.getEntities<component::Position>())
	{
		if (entity->hasComponents<component::CameraProjection>(gpuCc))
			continue;

		if (!entity->hasGpuComponents<vulkanutils::ShaderStorageBuffer, component::Transform>(gpuCc))
		{
			entity->addGpuComponentArray<vulkanutils::ShaderStorageBuffer, component::Transform>(gpuCc, 1);
			entity->addGpuComponentArray<vulkanutils::UniformBuffer, component::Transform>(gpuCc, 1);
			entity->addHostGpuComponentArray<component::Transform>(gpuCc, 1);
		}
		{
			auto hostData = entity->getHostGpuComponentArray<component::Transform>(gpuCc, 0);
			auto& deviceData = entity->getGpuComponentArray<vulkanutils::ShaderStorageBuffer, component::Transform>(gpuCc);
			auto& deviceDataUniform = entity->getGpuComponentArray<vulkanutils::UniformBuffer, component::Transform>(gpuCc);
			bufferCopier.queue(hostData, deviceData);
			bufferCopier.queue(hostData, deviceDataUniform);
		}
		if (!entity->hasComponents<component::MeshAABoundingBox>(cc))
		{
			entity->addComponent<component::MeshAABoundingBox>(cc, {});
			entity->addHostGpuComponentArray<component::MeshAABoundingBox>(gpuCc, 1);
			entity->addGpuComponentArray<vulkanutils::ShaderStorageBuffer, component::MeshAABoundingBox>(gpuCc, 1);
		}
		{
			auto hostData = entity->getHostGpuComponentArray<component::MeshAABoundingBox>(gpuCc);
			auto& deviceData = entity->getGpuComponentArray<vulkanutils::ShaderStorageBuffer, component::MeshAABoundingBox>(gpuCc);
			bufferCopier.queue(hostData, deviceData);
		}
	}
	mSetup = true;
}
