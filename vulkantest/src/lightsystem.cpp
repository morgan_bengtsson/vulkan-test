#include "lightsystem.h"
#include "componenttypes.h"
#include "camera.h"

#include <vulkanBufferCopier.h>
#include <vulkanBarrier.h>

#include <ranges>


LightSystem::LightSystem(vulkanutils::VulkanContext& context, SceneType scene)
	: System<LightSystem>(context)
	, mCommandPool(context.device(), context.device().getGraphicsQueue(), VK_COMMAND_POOL_CREATE_RESET_COMMAND_BUFFER_BIT)
	, mUploadCommandBuffers(getVulkanContext().device(), mCommandPool, 3)
	, mLastSignalledValue(3, 0)
	, mCmdBufferIdx(0)
{}


void LightSystem::runImpl(SceneType scene)
{
	if (!getVulkanContext().device().getGraphicsQueue().wait(getVulkanContext().device(), mLastSignalledValue[mCmdBufferIdx], 0))
		return;

	auto& cc = scene.getComponentCollection();
	auto& gpuCc = scene.getGpuComponentCollection();
	auto& commandBuffer = mUploadCommandBuffers[mCmdBufferIdx];
	{
		vulkanutils::CommandBufferRecorder cbRecorder(commandBuffer, VK_COMMAND_BUFFER_USAGE_ONE_TIME_SUBMIT_BIT);
		vulkanutils::Barrier bufferBarrier(commandBuffer);
		vulkanutils::BufferCopier bufferCopier(commandBuffer);

		auto lsceneEntities = scene.getEntities<component::LightScene>();
		for (ecs::Entity* lsceneEntity : lsceneEntities)
		{
			std::vector<ecs::Entity*> dirLightEntities = scene.getRefEntities<component::Orientation, component::DirectionalLight>(lsceneEntity, ecs::EntityRef::Type::Child);
			std::vector<ecs::Entity*> posLightEntities = scene.getRefEntities<component::Position, component::PositionalLight>(lsceneEntity, ecs::EntityRef::Type::Child);
			

			if (!lsceneEntity->hasGpuComponents<vulkanutils::ShaderStorageBuffer, component::DirectionalLightGpu, component::PositionalLightGpu>(gpuCc))
			{
				lsceneEntity->addGpuComponentArray<vulkanutils::ShaderStorageBuffer, component::DirectionalLightGpu>(gpuCc, dirLightEntities.size());
				lsceneEntity->addGpuComponentArray<vulkanutils::ShaderStorageBuffer, component::PositionalLightGpu>(gpuCc, posLightEntities.size());
				lsceneEntity->addHostGpuComponentArray<component::DirectionalLightGpu>(gpuCc, dirLightEntities.size());
				lsceneEntity->addHostGpuComponentArray<component::PositionalLightGpu>(gpuCc, posLightEntities.size());
			}

			auto dirLightHostData = lsceneEntity->getHostGpuComponentArray<component::DirectionalLightGpu>(gpuCc);
			auto& dirLightDeviceData = lsceneEntity->getGpuComponentArray<vulkanutils::ShaderStorageBuffer, component::DirectionalLightGpu>(gpuCc);
			for (size_t i = 0; i < dirLightEntities.size(); i++)
			{
				ecs::Entity* entity = dirLightEntities[i];
				if (!entity->hasImageComponents<component::ShadowDistance>(gpuCc))
				{
					auto& shadowImageData = entity->addImage<component::ShadowDistance>(gpuCc
						, 1024, 1024, VK_FORMAT_D32_SFLOAT, 1 
						, VK_IMAGE_USAGE_SAMPLED_BIT | VK_IMAGE_USAGE_TRANSFER_SRC_BIT | VK_IMAGE_USAGE_TRANSFER_DST_BIT | VK_IMAGE_USAGE_DEPTH_STENCIL_ATTACHMENT_BIT);

					component::MaterialTextureView* textureView = entity->addComponentArray<component::MaterialTextureView>(cc, 1);

					VkSamplerAddressMode samplerAddressModes[] = { VK_SAMPLER_ADDRESS_MODE_REPEAT, VK_SAMPLER_ADDRESS_MODE_REPEAT, VK_SAMPLER_ADDRESS_MODE_REPEAT };
					textureView[0].componentType = scene.getGpuComponentCollection().getComponentTypeIndex<component::ShadowDistance>();
					textureView[0].imageSampler = std::move(vulkanutils::ImageSampler(getVulkanContext().device(), VK_FILTER_LINEAR, VK_FILTER_LINEAR
							, samplerAddressModes, 4.0f, VK_BORDER_COLOR_FLOAT_OPAQUE_BLACK));
					textureView[0].imageView = vulkanutils::ImageView(getVulkanContext().device(), *shadowImageData.getImage(), VK_FORMAT_D32_SFLOAT, VK_IMAGE_ASPECT_DEPTH_BIT);

					RenderSubPass& renderPass = entity->addComponent<RenderSubPass>(cc, { 1024, 1024 } );
					renderPass.addAttachmentCompType<component::ShadowDistance>(scene, 0, VK_FORMAT_D32_SFLOAT, { .depthStencil = { .depth = 1.0f } });
				

					// Light Index
					
					auto hostData = entity->addHostGpuComponentArray<component::LightIndex>(gpuCc, 1);
					hostData.getData()[0].lightIndex = i;
					auto& deviceData = entity->addGpuComponentArray<vulkanutils::UniformBuffer, component::LightIndex>(gpuCc, 1);
					bufferCopier.queue(hostData, deviceData);
					bufferCopier.execute();
				}

				component::DirectionalLightGpu& directionalLightGpu = dirLightHostData.getData()[i];
				component::DirectionalLight& directionalLight = entity->getComponent<component::DirectionalLight>(cc);
				memcpy(directionalLightGpu.color, directionalLight.color, sizeof(directionalLightGpu.color));
				Eigen::Matrix3f orientation = entity->getComponent<component::Orientation>(cc);
				directionalLightGpu.direction[0] = orientation(2, 0);
				directionalLightGpu.direction[1] = orientation(2, 1);
				directionalLightGpu.direction[2] = orientation(2, 2);
				directionalLightGpu.direction[3] = 1.0f;
				directionalLightGpu.view = Eigen::Matrix4f::Identity();
				directionalLightGpu.view.topLeftCorner<3, 3>() = orientation;
				directionalLightGpu.view.topRightCorner<3, 1>() = Eigen::Vector3f::Zero();

				const component::LightScene& ls = entity->getComponent<component::LightScene>(cc);

				orthographicTransform(directionalLightGpu.projection.data()
					, directionalLight.shadowBoundingBox[0], directionalLight.shadowBoundingBox[1]
					, directionalLight.shadowBoundingBox[2], directionalLight.shadowBoundingBox[3]
					, directionalLight.shadowBoundingBox[4], directionalLight.shadowBoundingBox[5]);
				directionalLightGpu.projectionView = directionalLightGpu.projection * directionalLightGpu.view;
			}


			if (!dirLightEntities.empty())
			{
				bufferCopier.queue(dirLightHostData, dirLightDeviceData);
				bufferBarrier.addBuffer(dirLightDeviceData);
			}
			/*
			// Some more stuff to do to make positional lights to work
			auto posLightHostData = lsceneEntity->getHostGpuComponentArray<component::PositionalLightGpu>(gpuCc);
			auto& posLightDeviceData = lsceneEntity->getGpuComponentArray<vulkanutils::ShaderStorageBuffer, component::PositionalLightGpu>(gpuCc);
			for (size_t i = 0; i < posLightEntities.size(); i++)
			{
				const ecs::Entity* e = posLightEntities[i];
				component::PositionalLightGpu& dl = posLightHostData.getData()[i];
				memcpy(dl.color, e->getComponent<component::LightColor>(cc).color, sizeof(dl.color));
				Eigen::Map<Eigen::Vector4f> pos(dl.position);
				pos << e->getComponent<component::Position>(cc), 1.0f;
			}
			if (!posLightEntities.empty())
			{
				bufferCopier.queue(posLightHostData, posLightDeviceData);
				bufferBarrier.addBuffer(posLightDeviceData);
			}
			*/
		}
		bufferCopier.execute();
		bufferBarrier.dependsOnCopy().doBeforeVertexShader().execute();
	}
	getVulkanContext().device().getGraphicsQueue().submit({ commandBuffer }, nullptr);
	mLastSignalledValue[mCmdBufferIdx] = getVulkanContext().device().getGraphicsQueue().getSignalValue();
	mCmdBufferIdx = (mCmdBufferIdx + 1) % 3;
}
