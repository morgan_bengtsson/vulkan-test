#include "renderpass.h"

RenderSubPass::RenderSubPass()
	: mWidth()
	, mHeight()
	, mScreenBuffers()
	, mAttachments()
{}


RenderSubPass::RenderSubPass(uint32_t width, uint32_t height)
	: mWidth(width)
	, mHeight(height)
	, mScreenBuffers()
	, mAttachments()
{}

uint32_t RenderSubPass::width() const
{
	return mWidth;
}

uint32_t RenderSubPass::height() const
{
	return mHeight;
}

std::vector<RenderSubPass::Attachment>& RenderSubPass::getAttachmentCompTypeIds()
{
	return mAttachments;
}


const std::vector<RenderSubPass::Attachment>& RenderSubPass::getAttachmentCompTypeIds() const
{
	return mAttachments;
}

bool RenderSubPass::compatible(const component::Pipeline& pipeline) const
{
	for (const Attachment& attachment : mAttachments)
	{
		bool hasMatchingOutput = false;
		for (const component::Pipeline::Attachment& setAttachment : pipeline.renderPassAttachments)
		{
			if (setAttachment.componentType == attachment.componentType
				&& setAttachment.bindingNr == attachment.bindingNr)
			{
				hasMatchingOutput = true;
				break;
			}
		}
		if (!hasMatchingOutput)
			return false;
	}
	return true;
}

void RenderSubPass::enableForScreenBuffer(size_t idx)
{
	mScreenBuffers |= (uint32_t(1) << idx);
}

bool RenderSubPass::isEnabledForScreenBuffer(size_t idx) const
{
	return (mScreenBuffers & (uint32_t(1) << idx)) != 0;
}

std::vector<VkClearValue> RenderSubPass::getClearColorValues() const
{
	std::vector<VkClearValue> clearValues;
	for (const Attachment& a : mAttachments)
		clearValues.push_back(a.clearColor);
	return clearValues;

}