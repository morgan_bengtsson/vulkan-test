#include "grasssystem.h"
#include "systemproperties.h"
#include "camera.h"
#include "material.h"
#include "mesh.h"
#include "model.h"
#include "light.h"
#include "fileload.h"

#include <vulkanContext.h>
#include <vulkanBufferCopier.h>
#include <vulkanBarrier.h>


GrassSystem::GrassSystem(vulkanutils::VulkanContext& context, SceneType scene)
	: ecs::System<GrassSystem>(context)
	, ComputeSystem(context, "./shaders")
	, mCommandPool(context.device(), context.device().getComputeQueue(), VK_COMMAND_POOL_CREATE_RESET_COMMAND_BUFFER_BIT)
	, mUploadCommandBuffers(getVulkanContext().device(), mCommandPool, 1)
	, mDescriptorPool(context.device(), 8)
{
	vulkanutils::Device* device = &getVulkanContext().device();
	mDrawPipelineEntity = ecs::Entity("grassPipeline");
	{
		using namespace vulkanutils;

		component::Pipeline& pipeline = mDrawPipelineEntity.addComponent<component::Pipeline>(scene.getComponentCollection(), component::Pipeline("grass"));
		pipeline.sets.emplace_back(systemPropertiesSet(device, scene, DescriptorSetSlot::SLOT_1 ));
		pipeline.sets.emplace_back(cameraSet(device, scene, DescriptorSetSlot::SLOT_2 ));
		pipeline.sets.emplace_back(materialSet(device, scene, DescriptorSetSlot::SLOT_4, 1 ));
		pipeline.sets.emplace_back(meshSet(device, scene, VertexInputSlot::SLOT_0, 0, false));
		pipeline.sets.emplace_back(GrassModelSet(device, scene, DescriptorSetSlot::SLOT_5 ));
		pipeline.sets.emplace_back(LightWithShadowSet(device, scene, DescriptorSetSlot::SLOT_3 ));
		pipeline.sets.emplace_back(GrassHeightMapSet(device, scene, DescriptorSetSlot::SLOT_6, VertexInputSlot::NONE ));

		pipeline.addRenderPassAttachment<component::Color>(scene, 0);
		pipeline.addRenderPassAttachment<component::Depth>(scene, 1);

		scene.addEntity(mDrawPipelineEntity);
	}

	mShadowPipelineEntity = ecs::Entity("grassShadowPipeline");
	{
		using namespace vulkanutils;

		component::Pipeline& pipeline = mShadowPipelineEntity.addComponent<component::Pipeline>(scene.getComponentCollection(), component::Pipeline("grassShadow"));
		pipeline.sets.emplace_back(cameraSet(device, scene, DescriptorSetSlot::SLOT_1 ));
		pipeline.sets.emplace_back(meshSet(device, scene, VertexInputSlot::SLOT_0, 0, false));
		pipeline.sets.emplace_back(GrassModelSet(device, scene, DescriptorSetSlot::SLOT_4 ));
		pipeline.sets.emplace_back(LightShadowSet(device, scene, DescriptorSetSlot::SLOT_2 ));
		pipeline.sets.emplace_back(GrassHeightMapSet(device, scene, DescriptorSetSlot::SLOT_5, VertexInputSlot::NONE ));
		pipeline.cullMode = VK_CULL_MODE_FRONT_BIT;
		pipeline.addRenderPassAttachment<component::ShadowDistance>(scene, 0);
		scene.addEntity(mShadowPipelineEntity);
	}

	auto [ grassMeshEntities, grassMaterialEntities ] = loadFile("./models/grass.dae", scene.getComponentCollection(), device);
	for (auto& entity : grassMeshEntities)
		scene.addEntity(entity);
	for (auto& entity : grassMaterialEntities)
		scene.addEntity(entity);
	mGrassMeshEntities = std::move(grassMeshEntities);
	mGrassMaterialEntities = std::move(grassMaterialEntities);

	std::vector<EntityResourceSet> sets;
	sets.emplace_back(std::move(GrassCameraComputeSet(&context.device(), scene, vulkanutils::DescriptorSetSlot::SLOT_0)));
	sets.emplace_back(std::move(GrassComputeSet(&context.device(), scene, vulkanutils::DescriptorSetSlot::SLOT_1)));
	mComputePipeline = std::move(createComputePipeline(
		"grass",
		std::move(sets))
	);
}


void GrassSystem::runImpl(SceneType scene)
{
	setup(scene);

	// Execute all command buffers (one per grass field)
	std::vector<ecs::Entity*> grassEntities = scene.getEntities<GrassField>();
	std::vector<VkCommandBuffer> commandBuffers;
	commandBuffers.reserve(grassEntities.size());
	for (ecs::Entity* grassEntity : grassEntities)
		commandBuffers.push_back(mGrassComputeCommandBuffers[grassEntity->getId()][0]);
	getVulkanContext().compute(commandBuffers);
}

void GrassSystem::setup(SceneType scene)
{
	auto& cc = scene.getComponentCollection();
	auto& gpuCc = scene.getGpuComponentCollection();

	ecs::Entity* cameraEntity = scene.getEntities<component::CameraProjection>()[0];

	size_t numNewGrassEntities = 0;
	{
		vulkanutils::CommandBufferRecorder cbRecorder(mUploadCommandBuffers[0], VK_COMMAND_BUFFER_USAGE_ONE_TIME_SUBMIT_BIT);
		vulkanutils::BufferCopier bufferCopier(mUploadCommandBuffers[0]);
		for (ecs::Entity* grassEntity : scene.getEntities<GrassField>())
		{
			if (grassEntity->hasGpuComponents<vulkanutils::IndirectDrawBuffer, VkDrawIndexedIndirectCommand>(gpuCc))
				continue;
			++numNewGrassEntities;

			grassEntity->addComponents<ecs::EntityRef/*, ecs::EntityRef*/>(cc,
					{ mDrawPipelineEntity.getId(), ecs::EntityRef::Type::Link }
					//, { mShadowPipelineEntity.getId(), ecs::EntityRef::Type::Link }
				);
			ecs::EntityRef* meshes = grassEntity->addComponentArray<ecs::EntityRef>(cc, mGrassMeshEntities.size());
			for (size_t i = 0; i < mGrassMeshEntities.size(); i++)
				meshes[i] = { mGrassMeshEntities[i].getId(), ecs::EntityRef::Type::Child };
			{
				const GrassField& field = grassEntity->getComponent<GrassField>(cc);
				size_t numCells = field.gridSizeX * field.gridSizeY;
				auto& indirectDrawDeviceData = grassEntity->addGpuComponentArray<vulkanutils::IndirectDrawBuffer, VkDrawIndexedIndirectCommand>(gpuCc, numCells);
				auto& indirectDrawDeviceData2 = grassEntity->addGpuComponentArray<vulkanutils::ShaderStorageBuffer, VkDrawIndexedIndirectCommand>(gpuCc, numCells);
				auto indirectDrawHostData = grassEntity->addHostGpuComponentArray<VkDrawIndexedIndirectCommand>(gpuCc, numCells);
				auto& grassFieldDeviceData = grassEntity->addGpuComponentArray<vulkanutils::UniformBuffer, GrassField>(gpuCc, 1);
				auto grassFieldHostData = grassEntity->addHostGpuComponentArray<GrassField>(gpuCc, 1);

				for (VkDrawIndexedIndirectCommand& drawCommand : indirectDrawHostData.getData())
				{
					drawCommand.firstIndex = 0;
					drawCommand.firstInstance = 0;
					drawCommand.indexCount = 3;
					drawCommand.instanceCount = 0; // set by shader
					drawCommand.vertexOffset = 1;
				}
				grassFieldHostData.getData()[0] = field;

				bufferCopier.queue(indirectDrawHostData, indirectDrawDeviceData);
				bufferCopier.queue(indirectDrawHostData, indirectDrawDeviceData2);
				bufferCopier.queue(grassFieldHostData, grassFieldDeviceData);
			}
			mGrassComputeCommandBuffers[grassEntity->getId()] = vulkanutils::CommandBuffers(getVulkanContext().device(), mCommandPool, 1);
			auto& commandBuffer = mGrassComputeCommandBuffers[grassEntity->getId()][0];

			const ecs::Entity* terrainEntity = scene.getRefEntities<component::Surface<float>>(grassEntity, ecs::EntityRef::Type::Link)[0];
			GrassField& grassField = grassEntity->getComponent<GrassField>(scene.getComponentCollection());
			{
				vulkanutils::CommandBufferRecorder cbRecorder(commandBuffer, VK_COMMAND_BUFFER_USAGE_SIMULTANEOUS_USE_BIT);
				auto& descriptorSets = mComputePipeline.createDescriptorSets(grassEntity->getId(), mDescriptorPool);
				mComputePipeline.getComputeSets()[0].bindEntityToDescriptors(scene, descriptorSets[0], *cameraEntity);
				mComputePipeline.getComputeSets()[1].bindEntitiesToDescriptors(scene, descriptorSets[1], { *terrainEntity, *grassEntity });
				auto imageBindings = mComputePipeline.getComputeSets()[1].getEntityImageBindings(scene, *terrainEntity);
				for (auto& imageBinding : imageBindings)
				{
					imageBinding.bind(descriptorSets[1]);
				}
				//mComputePipeline.getComputeSets()[1].bindImages(descriptorSets[1], { imageBindings });
				mComputePipeline.execute(
					commandBuffer
					, grassEntity->getId()
				, { grassField.gridSizeX, grassField.gridSizeY, 1 } );

				{	// Copy result from compute shader to indirect draw buffer
					auto& deviceData = grassEntity->getGpuComponentArray<vulkanutils::ShaderStorageBuffer, VkDrawIndexedIndirectCommand>(scene.getGpuComponentCollection());
					auto& deviceData2 = grassEntity->getGpuComponentArray<vulkanutils::IndirectDrawBuffer, VkDrawIndexedIndirectCommand>(scene.getGpuComponentCollection());
					vulkanutils::Barrier(commandBuffer).dependsOnCompute().doBeforeCopy().addBuffer(deviceData);
					vulkanutils::BufferCopier(commandBuffer).queue(deviceData, deviceData2);
				}
			}
		}
	}
	if (numNewGrassEntities)
	{
		getVulkanContext().compute({ mUploadCommandBuffers[0] });
		getVulkanContext().device().getComputeQueue().wait();
	}
}


GrassHeightMapSet::GrassHeightMapSet(vulkanutils::Device* device, const GrassSystem::SceneType& scene, vulkanutils::DescriptorSetSlot descSetbp, vulkanutils::VertexInputSlot vertInputBp)
	: EntityResourceSet(device, "grassHeightMap", descSetbp, vertInputBp)
{
	setDescriptorBinding<vulkanutils::UniformBuffer, component::Transform>(scene,
		{
			VK_SHADER_STAGE_VERTEX_BIT, 0
		});

	setImageDescriptorBinding<component::Elevation>(scene,
		{
			VK_SHADER_STAGE_VERTEX_BIT, 1
		});
	getDescriptorLayout().build();
}

GrassModelSet::GrassModelSet(
	vulkanutils::Device* device,
	const GrassSystem::SceneType& scene,
	vulkanutils::DescriptorSetSlot descSetbp)
	: EntityResourceSet(device, "grassModel", descSetbp, vulkanutils::VertexInputSlot::NONE)
{
	setDescriptorBinding<vulkanutils::UniformBuffer, GrassField>(scene,
		{	// height map properties
			VK_SHADER_STAGE_VERTEX_BIT, 0
		});
	setDescriptorBinding<vulkanutils::ShaderStorageBuffer, VkDrawIndexedIndirectCommand>(scene,
		{
			VK_SHADER_STAGE_VERTEX_BIT, 1
		});
	getDescriptorLayout().build();
}


GrassCameraComputeSet::GrassCameraComputeSet(vulkanutils::Device* device, const GrassSystem::SceneType& scene, vulkanutils::DescriptorSetSlot descSetbp)
	: EntityResourceSet(device, "grassCameraCompute", descSetbp, vulkanutils::VertexInputSlot::NONE)
{
	// Camera
	setDescriptorBinding<vulkanutils::UniformBuffer, component::CameraProjection>(scene,
	{
		VK_SHADER_STAGE_COMPUTE_BIT, 0
	});
	setDescriptorBinding<vulkanutils::UniformBuffer, component::Transform>(scene,
	{
		VK_SHADER_STAGE_COMPUTE_BIT, 1
	});
	setDescriptorBinding<vulkanutils::UniformBuffer, component::Transform>(scene,
	{
		VK_SHADER_STAGE_COMPUTE_BIT, 2
	});
	getDescriptorLayout().build();
}


GrassComputeSet::GrassComputeSet(vulkanutils::Device* device, const GrassSystem::SceneType& scene, vulkanutils::DescriptorSetSlot descSetbp)
	: EntityResourceSet(device, "grassCompute", descSetbp, vulkanutils::VertexInputSlot::NONE)
{
	// Model
	setDescriptorBinding<vulkanutils::UniformBuffer, component::Transform>(scene,
		{
			VK_SHADER_STAGE_COMPUTE_BIT, 0
		});

	setImageDescriptorBinding<component::Elevation>(scene,
		{
			VK_SHADER_STAGE_COMPUTE_BIT, 1
		});

	setDescriptorBinding<vulkanutils::UniformBuffer, GrassField>(scene,
		{
			VK_SHADER_STAGE_COMPUTE_BIT, 2
		});
	// Output
	setDescriptorBinding<vulkanutils::ShaderStorageBuffer, VkDrawIndexedIndirectCommand>(scene,
		{
			VK_SHADER_STAGE_COMPUTE_BIT, 3
		});

	getDescriptorLayout().build();
}
