#include "collisionsystem.h"

#include "particle.h"

#include <vulkanBufferCopier.h>
#include <vulkanBarrier.h>

#include <Eigen/Dense>

CollisionSystem::CollisionSystem(vulkanutils::VulkanContext& context)
	:System<CollisionSystem>(context)
{
}

void CollisionSystem::runImpl(SceneType scene)
{
	auto collisionVolumeEntities = scene.getEntities<kdtree::KdTree<component::CollidableObject, 6>, ecs::EntityRef>();


	for (ecs::Entity* volumeEntity : collisionVolumeEntities)
	{
		kdtree::KdTree<component::CollidableObject, 6>& kdtree = volumeEntity->getComponent<kdtree::KdTree<component::CollidableObject, 6>>(scene.getComponentCollection());
		std::vector<ecs::Entity*> collidableEntities = scene.getRefEntities<component::CollidableObject>(volumeEntity, ecs::EntityRef::Type::Child);
		
		size_t numObjects = 0;
		for (auto collidableEntity : collidableEntities)
			numObjects += collidableEntity->getComponentArray<component::CollidableObject>(scene.getComponentCollection()).size();
		if (numObjects == 0)
			continue;

		std::vector<component::CollidableObject> collisionObjects;
		std::vector<kdtree::ClosestObject> closestObjects;
		collisionObjects.reserve(numObjects);
		for (ecs::Entity* entity : collidableEntities)
		{
			std::span<component::CollidableObject> objs = entity->getComponentArray<component::CollidableObject>(scene.getComponentCollection());
			collisionObjects.insert(collisionObjects.end(), objs.begin(), objs.end());
		}
		kdtree.setObjects({ collisionObjects });
		kdtree.rebalance();

		for (auto& obj : collisionObjects)
		{
			component::CollidableObject obj2 = kdtree.getClosestObjects<1>(obj.point)[0];
			kdtree::ClosestObject& co = closestObjects.emplace_back();
			co.entityId = obj2.entityId;
			co.objectId = obj2.objectId;
			co.distance = (Eigen::Map<Eigen::Vector3f>(obj2.point) - 
				Eigen::Map<Eigen::Vector3f>(obj.point)).norm();
		}
		for (size_t i = 0, j = 0; i < collidableEntities.size(); i++)
		{
			std::span<kdtree::ClosestObject> objs = collidableEntities[i]->getComponentArray<kdtree::ClosestObject>(scene.getComponentCollection());
			size_t nObjs = objs.size();
			std::copy(closestObjects.begin() + j, closestObjects.begin() + j + nObjs, objs.begin());
			j += nObjs;
		}


		for (size_t i = 0, j = 0; i < collisionObjects.size(); i++)
		{
			component::CollidableObject& obj1 = collisionObjects[i];
			ecs::Entity* e1 = scene.getEntity(obj1.entityId);


			kdtree::ClosestObject& co = closestObjects[i];
			ecs::Entity* e2 = scene.getEntity(co.entityId);
			component::CollidableObject& obj2 = e2->getComponentArray<component::CollidableObject>(scene.getComponentCollection())[co.objectId];
			
			collide(scene, e1, obj1, e2, obj2);
		}
	}
}


void CollisionSystem::collide(SceneType scene, ecs::Entity* e1, component::CollidableObject& coll1, ecs::Entity* e2, component::CollidableObject& coll2)
{
	Eigen::Map<Eigen::Vector3f> pos1(coll1.point);
	Eigen::Map<Eigen::Vector3f> pos2(coll2.point);

	Eigen::Vector3f diff = pos2 - pos1;
	float diffLen = diff.norm();
	Eigen::Vector3f diffNorm = diff / diffLen;

	if (diffLen > coll1.radius + coll2.radius)
		return;
	
	std::span<component::Speed> speed1 = e1->getComponentArray<component::Speed>(scene.getComponentCollection());
	std::span<Particle> particles1 = e1->getComponentArray<Particle>(scene.getComponentCollection());
	std::span<component::Speed> speed2 = e2->getComponentArray<component::Speed>(scene.getComponentCollection());
	std::span<Particle> particles2 = e2->getComponentArray<Particle>(scene.getComponentCollection());

	if (particles1.size() > 0 && particles2.size() > 0)
	{
		Particle& p1 = particles1[coll1.objectId];
		Particle& p2 = particles2[coll2.objectId];
		
		Eigen::Vector3f d =
		{
			p2.position[0] - p1.position[0],
			p2.position[1] - p1.position[1],
			p2.position[2] - p1.position[2]
		};
		float dn = d.squaredNorm();
		d /= std::sqrt(dn);
		Eigen::Map<Eigen::Vector3f> s1(p1.speed);
		Eigen::Map<Eigen::Vector3f> s2(p2.speed);
		const float dampening = 0.0f;
		if ((s2 - s1).dot(d) < 0)
		{
			s1 += (s2 * (1.0f - dampening) - s1).dot(d) * d;
			//s2 += (s1 * (1.0f - dampening) - s2).dot(d) * d;
		}
	}
}
