#include "testscene.h"

#include "entity.h"
#include "camera.h"
#include "fileload.h"
#include "mesh.h"
#include "model.h"
#include "light.h"
#include "material.h"
#include "rain.h"
#include "systemproperties.h"
#include "watersurface.h"
#include "watermeshset.h"
#include "entitybuilder.h"
#include "particle.h"

#include "animationsystem.h"
#include "watersystem.h"
#include "inputsystem.h"
#include "rainsystem.h"
#include "particlesystem.h"
#include "antsystem.h"
#include "collisionsystem.h"
#include "drawsystem.h"
#include "drawwatersystem.h"
#include "initpositionsystem.h"
#include "meshsystem.h"
#include "modelsystem.h"
#include "materialsystem.h"
#include "camerasystem.h"
#include "lightsystem.h"
#include "forestsystem.h"
#include "grasssystem.h"
#include "skysystem.h"
#include "tunnelsystem.h"
#include "charactersystem.h"


TestScene::TestScene(vulkanutils::VulkanContext& context)
	: BaseClass(&context.device())
	, mContext(context)
	, mDevice(&context.device())
	, mSystems()
{}

void TestScene::setup(QtVulkanWindow& qtWindow)
{
	auto& cc = getComponentCollection();
	auto& gpuCc = getGpuComponentCollection();

	auto* systems = new ecs::SceneSystemT(*this
		, CameraSystem(mContext, *this)
		//, AntSystem(mContext, *this)
		, SkySystem(mContext, *this)
		, ForestSystem(mContext)
		//, WaterSystem(mContext, *this)
		, InputSystem(mContext)
		//, RainSystem(mContext, *this)
		//, CharacterSystem(mContext, *this)
		//, ParticleSystem(mContext, *this)
		//, TunnelSystem(mContext, *this)
		, CollisionSystem(mContext)
		, MeshSystem(mContext, *this)
		, AnimationSystem(mContext, *this)
		, MaterialSystem(mContext, *this)
		, LightSystem(mContext, *this)
		, ModelSystem(mContext, *this)
		//, GrassSystem(mContext, *this)
		, DrawSystem(mContext, *this)
	);

	systems->get<InputSystem>().comm::Listener<KeyboardInput>::attachTo(&qtWindow);
	systems->get<InputSystem>().comm::Listener<MouseInput>::attachTo(&qtWindow);


	mSystems.reset(systems);

	// Set up tripple buffering
	ecs::Entity screenEntities[3] = {"screenBuffer0", "screenBuffer1", "screenBuffer2"};
	for (size_t i = 0; i < std::size(screenEntities); i++)
	{
		RenderSubPass& renderPass = screenEntities[i].addComponent<RenderSubPass>(cc,
			{
				uint32_t(mContext.width()),
				uint32_t(mContext.height())
			});

		auto texViews = screenEntities[i].addComponentArray<component::MaterialTextureView>(cc, 2);
		texViews[0].imageSampler = vulkanutils::ImageSampler();
		texViews[0].imageView = vulkanutils::ImageView(mContext.device(), mContext.getSwapChain().getImages()[i], VK_FORMAT_B8G8R8A8_UNORM, VK_IMAGE_ASPECT_COLOR_BIT);
		texViews[0].componentType = cc.getComponentTypeIndex<component::Color>();
		renderPass.addAttachmentCompType<component::Color>(*this, 0, VK_FORMAT_B8G8R8A8_UNORM, { .color = { 0.2, 0.2, 0.2} }, true);

		auto& depthImage = screenEntities[i].addImage<component::Depth>(gpuCc,
			mContext.width(), mContext.height()
			, VK_FORMAT_D32_SFLOAT, 1
			, VK_IMAGE_USAGE_DEPTH_STENCIL_ATTACHMENT_BIT);

		texViews[1].imageSampler = vulkanutils::ImageSampler();
		texViews[1].imageView = vulkanutils::ImageView(mContext.device(), *depthImage.getImage(), VK_FORMAT_D32_SFLOAT, VK_IMAGE_ASPECT_DEPTH_BIT);
		texViews[1].componentType = cc.getComponentTypeIndex<component::Depth>();
		renderPass.addAttachmentCompType<component::Depth>(*this, 1, VK_FORMAT_D32_SFLOAT, { .depthStencil = { 1 } }, true);
		renderPass.enableForScreenBuffer(i);
		addEntity(screenEntities[i]);
	}



	EntityBuilder entityBuilder(cc);
	auto [treeMeshEntities, treeMaterialEntities]
		= loadFile("./models/EU43_Castanea_sativa_Chestnut_3ds/Models/EU43_1.3ds", cc, mDevice);
	//for (auto& entity : treeMeshEntities)
	//	addEntity(entity);
	for (auto& entity : treeMaterialEntities)
		addEntity(entity);



	auto [particleMeshEntities, particleMaterialEntities] = loadFile("./models/particle.dae", cc, mDevice);
	for (auto& entity : particleMeshEntities)
		addEntity(entity);
	for (auto& entity : particleMaterialEntities)
		addEntity(entity);

	ecs::Entity surfacePipeline("SurfacePipeline");
	{
		using namespace vulkanutils;
		component::Pipeline& pipeline = surfacePipeline.addComponent<component::Pipeline>(cc, component::Pipeline("surface"));

		pipeline.sets.emplace_back(systemPropertiesSet(mDevice, *this, DescriptorSetSlot::SLOT_1 ));
		pipeline.sets.emplace_back(cameraSet(mDevice, *this, DescriptorSetSlot::SLOT_2 ));
		pipeline.sets.emplace_back(materialSet(mDevice, *this, DescriptorSetSlot::SLOT_4, 1 ));
		pipeline.sets.emplace_back(meshSet(mDevice, *this, VertexInputSlot::SLOT_0, 1, false));
		pipeline.sets.emplace_back(ModelSet(mDevice, *this, DescriptorSetSlot::SLOT_5, false ));
		pipeline.sets.emplace_back(LightWithShadowSet(mDevice, *this, DescriptorSetSlot::SLOT_3 ));

		pipeline.addRenderPassAttachment<component::Color>(*this, 0);
		pipeline.addRenderPassAttachment<component::Depth>(*this, 1);

		addEntity(surfacePipeline);
	}

	ecs::Entity surfaceShadowDrawPipeline("SurfaceShadowPipeline");
	{
		using namespace vulkanutils;
		component::Pipeline& pipeline = surfaceShadowDrawPipeline.addComponent<component::Pipeline>(cc, component::Pipeline("surfaceShadow"));

		pipeline.sets.emplace_back(meshSet(mDevice, *this, VertexInputSlot::SLOT_0, 1, false));
		pipeline.sets.emplace_back(ModelSet(mDevice, *this, DescriptorSetSlot::SLOT_5, false ));
		pipeline.sets.emplace_back(LightShadowSet(mDevice, *this, DescriptorSetSlot::SLOT_3 ));

		pipeline.addRenderPassAttachment<component::ShadowDistance>(*this, 0);
		pipeline.cullMode = VK_CULL_MODE_BACK_BIT;

		addEntity(surfaceShadowDrawPipeline);
	}

	ecs::Entity surfaceDebugPipeline("SurfaceDebugPipeline");
	{
		using namespace vulkanutils;
		component::Pipeline& pipeline = surfaceDebugPipeline.addComponent<component::Pipeline>(cc, component::Pipeline("surface,debug_normal.geom,debug_normal.frag"));

		pipeline.sets.emplace_back(systemPropertiesSet(mDevice, *this, DescriptorSetSlot::SLOT_1 ));
		pipeline.sets.emplace_back(cameraSet(mDevice, *this, DescriptorSetSlot::SLOT_2 ));
		pipeline.sets.emplace_back(materialSet(mDevice, *this, DescriptorSetSlot::SLOT_4, 1 ));
		pipeline.sets.emplace_back(meshSet(mDevice, *this, VertexInputSlot::SLOT_0, 1, false));
		pipeline.sets.emplace_back(ModelSet(mDevice, *this, DescriptorSetSlot::SLOT_5, false ));
		pipeline.sets.emplace_back(LightSet(mDevice, *this, DescriptorSetSlot::SLOT_3 ));

		pipeline.addRenderPassAttachment<component::Color>(*this, 0);
		pipeline.addRenderPassAttachment<component::Depth>(*this, 1);

		//addEntity(surfaceDebugPipeline);
	}


	ecs::Entity particlePipeline("ParticlePipeline");
	{
		using namespace vulkanutils;
		component::Pipeline& pipeline = particlePipeline.addComponent<component::Pipeline>(cc, component::Pipeline("particle"));

		pipeline.sets.emplace_back(systemPropertiesSet(mDevice, *this, DescriptorSetSlot::SLOT_1 ));
		pipeline.sets.emplace_back(cameraSet(mDevice, *this, DescriptorSetSlot::SLOT_2 ));
		pipeline.sets.emplace_back(materialSet(mDevice, *this, DescriptorSetSlot::SLOT_4, 0 ));
		pipeline.sets.emplace_back(ParticleMeshSet(mDevice, *this, DescriptorSetSlot::NONE, VertexInputSlot::SLOT_0 ));
		pipeline.sets.emplace_back(ParticleModelSet(mDevice, *this, DescriptorSetSlot::SLOT_5, VertexInputSlot::SLOT_3 ));
		pipeline.sets.emplace_back(LightWithShadowSet(mDevice, *this, DescriptorSetSlot::SLOT_3 ));

		pipeline.addRenderPassAttachment<component::Color>(*this, 0);
		pipeline.addRenderPassAttachment<component::Depth>(*this, 1);

		addEntity(particlePipeline);
	}

	ecs::Entity particleShadowPipeline("ParticleShadowPipeline");
	{
		using namespace vulkanutils;
		component::Pipeline& pipeline = particleShadowPipeline.addComponent<component::Pipeline>(cc, component::Pipeline("particleShadow"));

		pipeline.sets.emplace_back(ParticleMeshSet(mDevice, *this, DescriptorSetSlot::NONE, VertexInputSlot::SLOT_0 ));
		pipeline.sets.emplace_back(ParticleModelSet(mDevice, *this, DescriptorSetSlot::SLOT_5, VertexInputSlot::SLOT_3 ));
		pipeline.sets.emplace_back(LightShadowSet(mDevice, *this, DescriptorSetSlot::SLOT_3 ));

		pipeline.cullMode = VK_CULL_MODE_FRONT_BIT;
		pipeline.addRenderPassAttachment<component::ShadowDistance>(*this, 0);

		addEntity(particleShadowPipeline);
	}

	ecs::Entity particleDebugPipeline("DebugParticlePipeline");
	{
		using namespace vulkanutils;
		component::Pipeline& pipeline = particleDebugPipeline.addComponent<component::Pipeline>(cc, component::Pipeline("particle,debug_normal.geom,debug_normal.frag"));

		pipeline.sets.emplace_back(systemPropertiesSet(mDevice, *this, DescriptorSetSlot::SLOT_1 ));
		pipeline.sets.emplace_back(cameraSet(mDevice, *this, DescriptorSetSlot::SLOT_2 ));
		pipeline.sets.emplace_back(materialSet(mDevice, *this, DescriptorSetSlot::SLOT_4, 1 ));
		pipeline.sets.emplace_back(ParticleMeshSet(mDevice, *this, DescriptorSetSlot::NONE, VertexInputSlot::SLOT_0 ));
		pipeline.sets.emplace_back(ParticleModelSet(mDevice, *this, DescriptorSetSlot::SLOT_5, VertexInputSlot::SLOT_3 ));
		pipeline.sets.emplace_back(LightSet(mDevice, *this, DescriptorSetSlot::SLOT_3 ));

		pipeline.addRenderPassAttachment<component::Color>(*this, 0);
		pipeline.addRenderPassAttachment<component::Depth>(*this, 1);

		//addEntity(particleDebugPipeline);
	}

	ecs::Entity particleSystemEntity("ParticleSystem");
	{
		auto particles = particleSystemEntity.addComponentArray<Particle>(cc, 10);
		auto& particleProperties = particleSystemEntity.addComponent<ParticleProperties>(cc);
		particleProperties.mass = 10;
		particleProperties.radius = 10;
		entityBuilder.addDrawPipeline(particleSystemEntity, { particlePipeline, particleShadowPipeline });//, particleDebugPipeline });
		entityBuilder.addMesh(particleSystemEntity, particleMeshEntities);
		entityBuilder.addPosition(particleSystemEntity);
		entityBuilder.addOrientation(particleSystemEntity);
		//addEntity(particleSystemEntity);
	}


	ecs::Entity surfaceEntity("Surface");
	ecs::Entity surfaceMaterialEntity("surfaceMaterial");
	cloneComponents<component::MaterialProperties, component::MaterialTexture>
		(surfaceMaterialEntity, treeMaterialEntities[0], cc);
	Eigen::Map<Eigen::Vector4f>(surfaceMaterialEntity.getComponent<component::MaterialProperties>(cc).specular) = Eigen::Vector4f(0.3f, 0.3f, 0.4f, 20.0f);
	component::Surface<float>& surface =
		surfaceEntity.addComponent<component::Surface<float>>(cc, component::PerlinSurface(1000, 10000, 10000).toSurface(10));
	ecs::Entity surfaceMesh = createSurfaceMesh(cc, surface, 100);
	surfaceMesh.addComponent<ecs::EntityRef>(cc, { surfaceMaterialEntity.getId(), ecs::EntityRef::Type::Child });

	entityBuilder.addOrientation(surfaceEntity);
	entityBuilder.addPosition(surfaceEntity);
	entityBuilder.addDrawPipeline(surfaceEntity, { surfacePipeline, surfaceShadowDrawPipeline });//, surfaceDebugPipeline});
	entityBuilder.addMesh(surfaceEntity, { surfaceMesh });
	addEntity(surfaceMesh);
	addEntity(surfaceEntity);
	addEntity(surfaceMaterialEntity);
	
	ecs::Entity cameraEntity("Camera");
	{
		entityBuilder.addCamera(cameraEntity, 2.0f, 10000.0f, 2.0f, 2.0f, 1024, 768);
		entityBuilder.addPosition(cameraEntity);
		entityBuilder.addOrientation(cameraEntity);
	}
	addEntity(cameraEntity);

	ecs::Entity globalSystemEntity("System");
	{
		globalSystemEntity.addComponent<component::SystemProperies>(cc);
	}
	addEntity(globalSystemEntity);

	ecs::Entity skyEntity("sky");
	{
		skyEntity.addComponent<component::Sky>(cc);
		skyEntity.addComponent<component::Visibility>(cc) = { true };
	}
	addEntity(skyEntity);

	ecs::Entity playerEntity("player");
	{
		playerEntity.addComponent<component::HumanPlayerInput>(cc, {});
		playerEntity.addComponent<component::HumanPlayer>(cc, {});
	}
	addEntity(playerEntity);

	ecs::Entity runModeEntity("runMode");
	{
		runModeEntity.addComponent<component::RunMode>(cc, { component::RunMode::RUN });
	}
	addEntity(runModeEntity);

	ecs::Entity waterSurfaceEntity("WaterSurface");

	WaterSurface& ws = waterSurfaceEntity.addComponent<WaterSurface>(cc,
		WaterSurface(100.0f, 20.0f, surface.getXSize(), surface.getYSize(), 0, &surface));
	ws.setVolume(0, 0, 100000.0f, 100);
	waterSurfaceEntity.addComponent<component::Orientation>(cc, component::Orientation::Identity());
	waterSurfaceEntity.addComponent<component::Position>(cc, component::Position::Zero());
	waterSurfaceEntity.addComponent<component::Visibility>(cc, { true });

	addEntity(waterSurfaceEntity);

	for (size_t i = 0; i < 10; i++)
	{
		ecs::Entity doll(("Doll" + std::to_string(i)).c_str());
		component::Character& character = doll.addComponent<component::Character>(cc);
		strcpy(character.meshName, "./models/BaseMesh_Anim.fbx");
		addEntity(doll);
	}

	ecs::Entity rain("Rain");
	{
		entityBuilder.addPosition(rain);
		entityBuilder.addOrientation(rain);
		rain.addComponent<component::Rain>(cc, component::Rain{ 10000, 10000, 700, 0.02f, 0.002f, 0.2f });
		addEntity(rain);
	}

	ecs::Entity lightScene("lightScene");
	lightScene.addComponent<component::LightScene>(cc);
	addEntity(lightScene);

	ecs::Entity collisionVolumeEntity("collisionVolume");
	{
		collisionVolumeEntity.addComponent<kdtree::KdTree<component::CollidableObject, 6>>(cc);
		ecs::EntityRef* collidableEntityRef = collisionVolumeEntity.addComponentArray<ecs::EntityRef>(cc, 1);
		collidableEntityRef->id = particleSystemEntity.getId();
		addEntity(collisionVolumeEntity);
	}
	
	ecs::Entity grassEntity("grass");
	{
		grassEntity.addComponent<ecs::EntityRef>(cc) = { surfaceEntity.getId(), ecs::EntityRef::Type::Link };
		grassEntity.addComponent<GrassField>(cc) = { 32, 32 };
		grassEntity.addComponent<component::Visibility>(cc) = { true };
		addEntity(grassEntity);
	}
	

	InitPositionsSystem().run(*this);
	mStartTime = std::chrono::system_clock::now();
}

void TestScene::teardown()
{

}

void TestScene::update()
{
	auto& cc = getComponentCollection();
	auto& gpuCc = getGpuComponentCollection();

	bool paused = false;
	for (const ecs::Entity* entity : getEntities<component::RunMode>())
	{
		const auto& runMode = entity->getComponent<component::RunMode>(cc);
		if (runMode == component::RunMode::PAUSE)
			paused = true;
	}
	
	//if (!paused)
	{
		for (auto& entity : EntitySet::getAllEntities())
		{
			if (auto waterSurfaces = entity.getComponentArray<WaterSurface>(cc); !waterSurfaces.empty())
			{
				for (auto& ws : waterSurfaces)
				{
					ws.setVolume(0, 500, 0, 100);
				}
			}
			if (auto systemProperties = entity.getComponentArray<component::SystemProperies>(cc);
				!systemProperties.empty())
			{
				systemProperties[0].timeMs = (uint32_t) std::chrono::duration_cast<std::chrono::milliseconds>(std::chrono::system_clock::now() - mStartTime).count();
				entity.setModified();
			}
		}
	}
	mSystems->run(*this);

	for (ecs::Entity* entity : getEntities<component::RunMode>())
	{
		auto& runMode = entity->getComponent<component::RunMode>(cc);
		if (runMode == component::RunMode::STEP)
			runMode = component::RunMode::PAUSE;
	}
}
