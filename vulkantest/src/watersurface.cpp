#include "watersurface.h"

#include <algorithm>
#define _USE_MATH_DEFINES
#include <math.h>

WaterSurface::WaterSurface(float depthScale, float cellSize
	, float xSize, float ySize, float startDepth, const component::Surface<float>* bottomSurface)
	: component::Surface<WaterSurfaceCell>(xSize, ySize, cellSize)
	, mDepthScale(depthScale)
{
	for (size_t r = 0; r < getDiscrete().height(); r++)
	{
		for (size_t c = 0; c < getDiscrete().width(); c++)
		{
			float nx = getXCoord(c);
			float ny = getXCoord(r);
			WaterSurfaceCell& cell = getDiscrete()(c, r);
			cell.bd = bottomSurface->get(nx, ny, [](float h){return h;});
			cell.depth = std::max(0.0f, startDepth - cell.bd);
		}
	}
}

WaterSurface::~WaterSurface()
{}

float WaterSurface::gaussian(float volume, float width, float x, float y)
{
	float amp = volume / (float(M_PI) * 2.0f * width * width);
	auto func = [a = amp, w = width](float x, float y)
	{
		return a * std::exp(-((x * x) + (y * y)) / (2 * w * w));
	};
	return func(x, y);
}


void WaterSurface::setVolume(float x, float y, float volume, float width)
{
	float nx = getDiscXCoord(x);
	float ny = getDiscYCoord(y);

	int xCell = std::clamp<int>(nx, 0, getDiscrete().width() - 2);
	int yCell = std::clamp<int>(ny, 0, getDiscrete().height() - 2);

	float dx = nx - xCell;
	float dy = ny - yCell;

	float depth = volume / (cellSize() * cellSize());
	int wi = width / cellSize();
	for (int r = -wi; r <= wi; r++)
	{
		size_t yi = yCell + r;
		if (yi >= getDiscrete().height())
			continue;
		for (int c = -wi; c <= wi; c++)
		{
			size_t xi = xCell + c;
			if (xi >= getDiscrete().width())
				continue;
			size_t idx = xi + yi * getDiscrete().width();
			float y = r - dy;
			float x = c - dx;
			getDiscrete().data()[idx].depth = gaussian(volume, width / 2, x * cellSize(), y * cellSize());
		}
	}
}

void WaterSurface::changeVolume(float x, float y, float volume, float width)
{
	float nx = getDiscXCoord(x);
	float ny = getDiscYCoord(y);

	int xCell = std::clamp<int>(nx, 0, getDiscrete().width() - 2);
	int yCell = std::clamp<int>(ny, 0, getDiscrete().height() - 2);

	float dx = nx - xCell;
	float dy = ny - yCell;

	float depth = volume / (cellSize() * cellSize());
	int wi = width / cellSize();
	for (int r = -wi; r <= wi; r++)
	{
		size_t yi = yCell + r;
		if (yi >= getDiscrete().height())
			continue;
		for (int c = -wi; c <= wi; c++)
		{
			size_t xi = xCell + c;
			if (xi >= getDiscrete().width())
				continue;
			size_t idx = xi + yi * getDiscrete().width();
			float y = r - dy;
			float x = c - dx;
			getDiscrete().data()[idx].depth += gaussian(volume, width / 2, x * cellSize(), y * cellSize());
		}
	}
}

