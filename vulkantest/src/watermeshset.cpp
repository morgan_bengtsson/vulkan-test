#include "watermeshset.h"


WaterMeshSet::WaterMeshSet(vulkanutils::Device* device
	, SceneType scene, vulkanutils::DescriptorSetSlot descSetbp, vulkanutils::VertexInputSlot vertInputBp)
	: EntityResourceSet(device, "waterMesh", descSetbp, vertInputBp)
{
	setPerVertexAttributeBinding<component::MeshVertex>(scene, 0, VK_FORMAT_R32G32B32_SFLOAT);
	setPerVertexAttributeBinding<component::MeshNormal>(scene, 1, VK_FORMAT_R32G32B32_SFLOAT);
	//setPerVertexAttributeBinding<WaterDepth>(scene, 2, VK_FORMAT_R32_SFLOAT);


	setHasIndexBinding();
}
