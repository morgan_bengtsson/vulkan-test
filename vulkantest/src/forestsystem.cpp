#include "forestsystem.h"

#include "fileload.h"
#include "systemproperties.h"


#include <vulkanContext.h>
#include <camera.h>
#include <light.h>
#include <mesh.h>
#include <model.h>
#include <entitybuilder.h>


ForestSystem::ForestSystem(vulkanutils::VulkanContext& vc)
	: ecs::System<ForestSystem>(vc)
{}


void ForestSystem::runImpl(SceneType scene)
{
	switch (mState)
	{
	case State::SETUP:
	{
		setup(scene);
		mState = State::RUN;
		break;
	}
	case State::RUN:
	{
	
		break;
	}

	};
}

void ForestSystem::setup(SceneType scene)
{
	// Load model
	std::tie(mTreeMeshes, mTreeMaterials)
		= loadFile(mTreeModelFile, scene.getComponentCollection(), &getVulkanContext().device());
	for (auto& entity : mTreeMeshes)
		scene.addEntity(entity);
	for (auto& entity : mTreeMaterials)
		scene.addEntity(entity);

	// Create pipeline
	mTreeDrawPipeline = ecs::Entity("TreePipeline");
	{
		using namespace vulkanutils;

		component::Pipeline& pipeline = mTreeDrawPipeline.addComponent<component::Pipeline>(scene.getComponentCollection(), component::Pipeline("model"));
		vulkanutils::Device* device = &getVulkanContext().device();

		pipeline.sets.emplace_back(systemPropertiesSet(device, scene, DescriptorSetSlot::SLOT_1 ));
		pipeline.sets.emplace_back(cameraSet(device, scene, DescriptorSetSlot::SLOT_2 ));
		pipeline.sets.emplace_back(materialSet(device, scene, DescriptorSetSlot::SLOT_4, 1 ));
		pipeline.sets.emplace_back(meshSet(device, scene, VertexInputSlot::SLOT_0, 1, false));
		pipeline.sets.emplace_back(ModelSet(device, scene, DescriptorSetSlot::SLOT_5, false ));
		pipeline.sets.emplace_back(LightWithShadowSet(device, scene, DescriptorSetSlot::SLOT_3 ));
		
		pipeline.addRenderPassAttachment<component::Color>(scene, 0);
		pipeline.addRenderPassAttachment<component::Depth>(scene, 1);

		scene.addEntity(mTreeDrawPipeline);
	}

	mTreeShadowPipeline = ecs::Entity("TreeShadowPipeline");
	{
		using namespace vulkanutils;

		component::Pipeline& pipeline = mTreeShadowPipeline.addComponent<component::Pipeline>(scene.getComponentCollection(), component::Pipeline("modelShadow"));
		vulkanutils::Device* device = &getVulkanContext().device();

		pipeline.sets.emplace_back(materialSet(device, scene, DescriptorSetSlot::SLOT_4, 1 ));
		pipeline.sets.emplace_back(meshSet(device, scene, VertexInputSlot::SLOT_0, 1, false));
		pipeline.sets.emplace_back(ModelSet(device, scene, DescriptorSetSlot::SLOT_5, false ));
		pipeline.sets.emplace_back(LightShadowSet(device, scene, DescriptorSetSlot::SLOT_3 ));

		pipeline.cullMode = VK_CULL_MODE_FRONT_BIT;
		pipeline.addRenderPassAttachment<component::ShadowDistance>(scene, 0);

		scene.addEntity(mTreeShadowPipeline);
	}

	mTreeDebugDrawPipeline = ecs::Entity("TreeDebugPipeline");
	{
		using namespace vulkanutils;
		component::Pipeline& pipeline = mTreeDebugDrawPipeline.addComponent<component::Pipeline>(scene.getComponentCollection(), component::Pipeline("model,debug_normal.geom,debug_normal.frag"));
		vulkanutils::Device* device = &getVulkanContext().device();

		pipeline.sets.emplace_back(systemPropertiesSet(device, scene, DescriptorSetSlot::SLOT_1 ));
		pipeline.sets.emplace_back(cameraSet(device, scene, DescriptorSetSlot::SLOT_2 ));
		pipeline.sets.emplace_back(materialSet(device, scene, DescriptorSetSlot::SLOT_4, 1 ));
		pipeline.sets.emplace_back(meshSet(device, scene, VertexInputSlot::SLOT_0, 1, false));
		pipeline.sets.emplace_back(ModelSet(device, scene, DescriptorSetSlot::SLOT_5, false ));
		pipeline.sets.emplace_back(LightSet(device, scene, DescriptorSetSlot::SLOT_3 ));

		scene.addEntity(mTreeDebugDrawPipeline);
	}

	// Place trees on surface
	auto surfaceEntity = scene.getEntities<component::Surface<float>, component::Position, component::Orientation>()[0];
	const auto& [ parentPosition, parentOrientation ] = surfaceEntity->getComponents<component::Position, component::Orientation>(scene.getComponentCollection());

	Eigen::Affine3f parentTransform;
	parentTransform.fromPositionOrientationScale(parentPosition, parentOrientation, Eigen::Vector3f::Ones());
	auto& surface = surfaceEntity->getComponent<component::Surface<float>>(scene.getComponentCollection());
	auto surfaceHeightFunc = [](float h){return h;};

	std::uniform_real_distribution<float> distribution(-0.5f, 0.5f);
	uint64_t seed = std::chrono::system_clock::now().time_since_epoch().count();
	std::default_random_engine generator((unsigned int)seed);
	


	EntityBuilder entityBuilder(scene.getComponentCollection());
	for (size_t i = 0; i < 10; i++)
	{
		ecs::Entity tree(("Tree:" + std::to_string(i)).c_str());
		tree.addComponent<ecs::EntityRef>(scene.getComponentCollection(), ecs::EntityRef{ surfaceEntity->getId(), ecs::EntityRef::Type::Parent });
		entityBuilder.addMesh(tree, mTreeMeshes);
		entityBuilder.addPosition(tree);
		entityBuilder.addOrientation(tree);
		tree.addComponent<component::Scale>(scene.getComponentCollection()) = Eigen::Vector3f(10, 10, 10) * (distribution(generator) + 1.0f);
		entityBuilder.addDrawPipeline(tree, { mTreeDrawPipeline, mTreeShadowPipeline});//, mTreeDebugDrawPipeline });

		float surfaceX = distribution(generator) * surface.getXSize();
		float surfaceY = distribution(generator) * surface.getYSize();
		float surfaceHeight = surface.get(surfaceX, surfaceY, surfaceHeightFunc);

		Eigen::Affine3f transform = parentTransform;

		transform.translate(Eigen::Vector3f(surfaceX, surfaceHeight, surfaceY));
		transform.rotate(Eigen::AngleAxisf(-90.0f / 180.0f * 3.14f, Eigen::Vector3f::UnitX()));

		auto [position, orientation] = tree.getComponents<component::Position, component::Orientation>(scene.getComponentCollection());

		position = transform.matrix().topRightCorner<3, 1>();
		orientation *= transform.matrix().topLeftCorner<3, 3>();
		scene.addEntity(tree);
	}
}
