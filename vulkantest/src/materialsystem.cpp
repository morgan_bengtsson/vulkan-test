#include "materialsystem.h"

#include "imageloading.h"

#include <vulkanBufferCopier.h>
#include <vulkanBarrier.h>

MaterialSystem::MaterialSystem(vulkanutils::VulkanContext& context, SceneType scene)
	:System<MaterialSystem>(context)
	, mCommandPool(context.device(), context.device().getGraphicsQueue(), VK_COMMAND_POOL_CREATE_RESET_COMMAND_BUFFER_BIT)
	, mUploadCommandBuffers(getVulkanContext().device(), mCommandPool, 3)
	, mLastSignalledValue(3, 0)
	, mCmdBufferIdx(0)
{
}

void MaterialSystem::runImpl(SceneType scene)
{
	auto& commandBuffers = mUploadCommandBuffers;
	if (!getVulkanContext().device().getGraphicsQueue().wait(getVulkanContext().device(), mLastSignalledValue[mCmdBufferIdx], 0))
		return;


	auto& commandBuffer = commandBuffers[mCmdBufferIdx];
	{
		vulkanutils::CommandBufferRecorder cbRecorder(commandBuffer, VK_COMMAND_BUFFER_USAGE_ONE_TIME_SUBMIT_BIT);
		vulkanutils::Barrier bufferBarrier(commandBuffer);

		vulkanutils::BufferCopier bufferCopier(commandBuffer);
		for (auto& entity : scene.getEntities<component::MaterialProperties>())
		{
			if (!entity->isModified())
				continue;

			auto matProp = entity->getComponent<component::MaterialProperties>(scene.getComponentCollection());
			if (!entity->hasGpuComponents<vulkanutils::UniformBuffer, component::MaterialProperties>(scene.getGpuComponentCollection()))
			{
				entity->addGpuComponentArray<vulkanutils::UniformBuffer, component::MaterialProperties>(scene.getGpuComponentCollection(), 1);
				entity->addHostGpuComponentArray<component::MaterialProperties>(scene.getGpuComponentCollection(), 1);
			}
			{
				auto hostData = entity->getHostGpuComponentArray<component::MaterialProperties>(scene.getGpuComponentCollection());
				auto& deviceData = entity->getGpuComponentArray<vulkanutils::UniformBuffer, component::MaterialProperties>(scene.getGpuComponentCollection());
				hostData.getData()[0] = matProp;
				bufferCopier.queue(hostData, deviceData);
				bufferBarrier.addBuffer(deviceData);
			}

			if (std::span<component::MaterialTexture> matTextures = entity->getComponentArray<component::MaterialTexture>(scene.getComponentCollection());
				!matTextures.empty())
			{
				if (auto entityImages = entity->getImages<component::Raw>(scene.getGpuComponentCollection());
					entityImages.empty())
				{
					component::MaterialTextureView* textureViews = entity->addComponentArray<component::MaterialTextureView>(scene.getComponentCollection(), matTextures.size());
					for (size_t i = 0; i < matTextures.size(); i++)
					{
						auto& matTex = matTextures[i];
						ImageBitmap ibm(matTex.name);
						auto imageDataHost = entity->addHostGpuComponentArray<component::Raw>(scene.getGpuComponentCollection(), ibm.width() * ibm.height() * 4);
						auto& imageDataDevice = entity->addImage<component::Raw>(scene.getGpuComponentCollection()
							, ibm.width(), ibm.height(), VK_FORMAT_R8G8B8A8_UNORM, size_t(1 + std::log2(std::max(ibm.width(), ibm.height()))) 
							, VK_IMAGE_USAGE_TRANSFER_SRC_BIT | VK_IMAGE_USAGE_TRANSFER_DST_BIT | VK_IMAGE_USAGE_SAMPLED_BIT);
						std::memcpy(imageDataHost.getData().data(), ibm.data(), ibm.width() * ibm.height() * 4);
						//std::memset(imageDataHost.getData().data(), 0, ibm.width() * ibm.height() * 4);
						bufferCopier.queue<decltype(imageDataHost)>(imageDataHost, *imageDataDevice.getImage());

						VkSamplerAddressMode samplerAddressModes[] = { VK_SAMPLER_ADDRESS_MODE_REPEAT,
										VK_SAMPLER_ADDRESS_MODE_REPEAT,
										VK_SAMPLER_ADDRESS_MODE_REPEAT };

						textureViews[i].componentType = scene.getGpuComponentCollection().getComponentTypeIndex<component::Raw>();
						textureViews[i].imageSampler = std::move(vulkanutils::ImageSampler(
							getVulkanContext().device()
							, VK_FILTER_LINEAR, VK_FILTER_LINEAR
							, samplerAddressModes, 16.0f, VK_BORDER_COLOR_INT_TRANSPARENT_BLACK));
						textureViews[i].imageView = vulkanutils::ImageView(
							getVulkanContext().device()
							, *imageDataDevice.getImage()
							, VK_IMAGE_ASPECT_COLOR_BIT);
					}
				}
			}
		}
		bufferCopier.execute();
		bufferBarrier.dependsOnCopy().doBeforeVertexShader().execute();
	}

	getVulkanContext().device().getGraphicsQueue().submit({ commandBuffer }, nullptr);
	mLastSignalledValue[mCmdBufferIdx] = getVulkanContext().device().getGraphicsQueue().getSignalValue();
	mCmdBufferIdx = (mCmdBufferIdx + 1) % 3;
}
