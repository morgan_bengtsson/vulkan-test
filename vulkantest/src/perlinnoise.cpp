#include "perlinnoise.h"

#include <random>
#include <chrono>


float PerlinNoise::Gradients[4][2] = 
{
	{ 1, 0}, {0, 1},
	{-1, 0}, {0, -1}
};


float PerlinNoise::smoothFunc(float x) const
{
	return x * x * x * (x * (x * 6.0f - 15.0f) + 10.0f);
}

float PerlinNoise::smoothFuncDerrivate(float x) const
{
	return x * x * (x * (x * 30.0f - 60.0f) + 30.0f);
}


PerlinNoise::PerlinNoise(size_t seed)
	: mPermutations()
{
	if (seed == 0)
	{
		seed = std::chrono::system_clock::now().time_since_epoch().count();
	}
	std::default_random_engine generator(seed);

	for (size_t i = 0; i < std::size(mPermutations); i++)
		mPermutations[i] = i;
	std::shuffle(mPermutations, mPermutations + std::size(mPermutations), generator);
}

float PerlinNoise::get(float x, float y) const
{
	float xn = 256.0f * x;
	float yn = 256.0f * y;

	while (xn < 0) xn += 256;
	while (yn < 0) yn += 256;


	int xi = int(std::floor(xn));
	int yi = int(std::floor(yn));

	int xIdx = xi % 256;
	int yIdx = yi % 256;

	float lx = xn - xi + xIdx;
	float ly = yn - yi + yIdx;

	int idx[4] = 
	{
		mPermutations[(mPermutations[xIdx] + yIdx) & 0xff] & 0x3,
		mPermutations[(mPermutations[xIdx + 1] + yIdx) & 0xff] & 0x3,
		mPermutations[(mPermutations[xIdx] + yIdx + 1) & 0xff] & 0x3,
		mPermutations[(mPermutations[xIdx + 1] + yIdx + 1) & 0xff] & 0x3
	};


	float corners[4][2] = { { xIdx, yIdx },
		{ xIdx + 1, yIdx },
		{xIdx, yIdx + 1},
		{xIdx + 1, yIdx + 1} };

	float s[4];
	for (size_t i = 0; i < 4; i++)
	{
		s[i] = Gradients[idx[i]][0] * (lx - corners[i][0]) + Gradients[idx[i]][1] + (ly - corners[i][1]);
	}
	float sx = smoothFunc(lx - xIdx);
	float sy = smoothFunc(ly - yIdx);
	float n0 = sx * (s[1] - s[0]) + s[0];
	float n1 = sx * (s[3] - s[2]) + s[2];
	float r = sy * (n1 - n0) + n0;
	return r;
}

std::array<float, 2> PerlinNoise::getDxDy(float x, float y) const
{
	float xn = 256.0f * x;
	float yn = 256.0f * y;

	while (xn < 0) xn += 256;
	while (yn < 0) yn += 256;


	int xi = int(std::floor(xn));
	int yi = int(std::floor(yn));

	int xIdx = xi % 256;
	int yIdx = yi % 256;

	float lx = xn - xi + xIdx;
	float ly = yn - yi + yIdx;

	int idx[4] = 
	{
		mPermutations[(mPermutations[xIdx] + yIdx) & 0xff] & 0x3,
		mPermutations[(mPermutations[xIdx + 1] + yIdx) & 0xff] & 0x3,
		mPermutations[(mPermutations[xIdx] + yIdx + 1) & 0xff] & 0x3,
		mPermutations[(mPermutations[xIdx + 1] + yIdx + 1) & 0xff] & 0x3
	};


	float corners[4][2] = { { xIdx, yIdx },
		{ xIdx + 1, yIdx },
		{xIdx, yIdx + 1},
		{xIdx + 1, yIdx + 1} };

	float s[4];
	for (size_t i = 0; i < 4; i++)
	{
		s[i] = Gradients[idx[i]][0] * (lx - corners[i][0]) + Gradients[idx[i]][1] + (ly - corners[i][1]);
	}
	float sx = smoothFunc(lx - xIdx);
	float sy = smoothFunc(ly - yIdx);
	float ss = s[3] + s[0] - s[2] - s[1];
	float n01 = sx * ss + s[2] - s[0];
	float n02 = sy * ss + s[1] - s[0];

	return {
		smoothFuncDerrivate(lx - xIdx) * n02,
		smoothFuncDerrivate(ly - yIdx) * n01
	};
}
