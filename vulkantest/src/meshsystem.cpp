#include "meshsystem.h"

#include "kdtree.h"

#include <vulkanBufferCopier.h>
#include <vulkanBarrier.h>




MeshSystem::MeshSystem(vulkanutils::VulkanContext& context, SceneType scene)
	:System<MeshSystem>(context)
	, mCommandPool(context.device(), context.device().getGraphicsQueue(), VK_COMMAND_POOL_CREATE_RESET_COMMAND_BUFFER_BIT)
	, mUploadCommandBuffers(getVulkanContext().device(), mCommandPool, 3)
	, mLastSignalledValue(3, 0)
	, mCmdBufferIdx(0)
{
}

void MeshSystem::runImpl(SceneType scene)
{
	if (!getVulkanContext().device().getGraphicsQueue().wait(getVulkanContext().device(), mLastSignalledValue[mCmdBufferIdx], 0))
		return;

	auto& cc = scene.getComponentCollection();
	auto& gpuCc = scene.getGpuComponentCollection();

	auto& commandBuffer = mUploadCommandBuffers[mCmdBufferIdx];
	{
		vulkanutils::CommandBufferRecorder cbRecorder(commandBuffer, VK_COMMAND_BUFFER_USAGE_ONE_TIME_SUBMIT_BIT);
		vulkanutils::Barrier bufferBarrier(commandBuffer);

		vulkanutils::BufferCopier bufferCopier(commandBuffer);
		for (auto& entity : scene.getEntities<component::MeshIndex>())
		{
			if (!entity->isModified())
				continue;

			const auto vertices = entity->getComponentArray<component::MeshVertex>(cc);
			const auto normals = entity->getComponentArray<component::MeshNormal>(cc);
			const auto texCoords = entity->getComponentArray<component::MeshTexCoord>(cc);
			const auto vertexIndices = entity->getComponentArray<component::MeshIndex>(cc);
			const auto vertexBoneData = entity->getComponentArray<skeleton::VertexBoneData>(cc);

			if (!entity->hasGpuComponents<vulkanutils::IndexBuffer, component::MeshIndex>(gpuCc))
			{
				entity->addHostGpuComponentArray<component::MeshIndex>(gpuCc, vertexIndices.size());
				entity->addGpuComponentArray<vulkanutils::IndexBuffer, component::MeshIndex>(gpuCc, vertexIndices.size());

				if (!vertices.empty())
				{
					entity->addHostGpuComponentArray<component::MeshVertex>(gpuCc, vertices.size());
					entity->addGpuComponentArray<vulkanutils::VertexBuffer, component::MeshVertex>(gpuCc, vertices.size());

					auto getMinMax = [&](size_t dim)
					{
						auto [minIt, maxIt] = std::minmax_element(vertices.begin(), vertices.end(), [&](const component::MeshVertex& lhs, const component::MeshVertex& rhs)
							{ return lhs.point[dim] < rhs.point[dim]; });
						return std::make_tuple(minIt->point[dim], maxIt->point[dim] );
					};
					Eigen::Vector3f bbMin, bbMax;
					std::tie(bbMin.x(), bbMax.x()) = getMinMax(0);
					std::tie(bbMin.y(), bbMax.y()) = getMinMax(1);
					std::tie(bbMin.z(), bbMax.z()) = getMinMax(2);
					auto& bbox = entity->addComponent<component::MeshAABoundingBox>(cc);
					bbox.aabb = {bbMin, bbMax};

				}
				if (!normals.empty())
				{
					entity->addHostGpuComponentArray<component::MeshNormal>(gpuCc, vertices.size());
					entity->addGpuComponentArray<vulkanutils::VertexBuffer, component::MeshNormal>(gpuCc, vertices.size());
				}
				if (!texCoords.empty())
				{
					entity->addHostGpuComponentArray<component::MeshTexCoord>(gpuCc, vertices.size());
					entity->addGpuComponentArray<vulkanutils::VertexBuffer, component::MeshTexCoord>(gpuCc, vertices.size());
				}
				if (!vertexBoneData.empty())
				{
					entity->addHostGpuComponentArray<skeleton::VertexBoneData>(gpuCc, vertexBoneData.size());
					entity->addGpuComponentArray<vulkanutils::VertexBuffer, skeleton::VertexBoneData>(gpuCc, vertexBoneData.size());
				}


			}
			else
				continue;

			if (!vertices.empty())
			{
				auto hostVertexData = entity->getHostGpuComponentArray<component::MeshVertex>(gpuCc);
				auto& deviceVertexData = entity->getGpuComponentArray<vulkanutils::VertexBuffer, component::MeshVertex>(gpuCc);
				for (size_t i = 0; i < vertices.size(); i++)
					hostVertexData.getData()[i] = vertices[i];
				bufferCopier.queue(hostVertexData, deviceVertexData);
				bufferBarrier.addBuffer(deviceVertexData);
			}
				
			if (!normals.empty())
			{
				auto hostNormalData = entity->getHostGpuComponentArray<component::MeshNormal>(gpuCc);
				auto& deviceNormalData = entity->getGpuComponentArray<vulkanutils::VertexBuffer, component::MeshNormal>(gpuCc);
				for (size_t i = 0; i < vertices.size(); i++)
					hostNormalData.getData()[i] = normals[i];
				bufferCopier.queue(hostNormalData, deviceNormalData);
				bufferBarrier.addBuffer(deviceNormalData);
			}
			if (!texCoords.empty())
			{
				auto hostTexCoordData = entity->getHostGpuComponentArray<component::MeshTexCoord>(gpuCc);
				auto& deviceTexCoordData = entity->getGpuComponentArray<vulkanutils::VertexBuffer, component::MeshTexCoord>(gpuCc);
				for (size_t i = 0; i < texCoords.size(); i++)
					hostTexCoordData.getData()[i] = texCoords[i];
				bufferCopier.queue(hostTexCoordData, deviceTexCoordData);
				bufferBarrier.addBuffer(deviceTexCoordData);
			}
			if (!vertexBoneData.empty())
			{
				auto hostBoneData = entity->getHostGpuComponentArray<skeleton::VertexBoneData>(gpuCc);
				auto& deviceBoneData = entity->getGpuComponentArray<vulkanutils::VertexBuffer, skeleton::VertexBoneData>(gpuCc);
				for (size_t i = 0; i < vertexBoneData.size(); i++)
					hostBoneData.getData()[i] = vertexBoneData[i];
				bufferCopier.queue(hostBoneData, deviceBoneData);
				bufferBarrier.addBuffer(deviceBoneData);
			}
			{
				auto hostIndexData = entity->getHostGpuComponentArray<component::MeshIndex>(gpuCc);
				auto& deviceIndexData = entity->getGpuComponentArray<vulkanutils::IndexBuffer, component::MeshIndex>(gpuCc);
				for (size_t i = 0; i < vertexIndices.size(); i++)
					hostIndexData.getData()[i] = vertexIndices[i];
				bufferCopier.queue(hostIndexData, deviceIndexData);
				bufferBarrier.addBuffer(deviceIndexData);
			}
		}
	
		bufferCopier.execute();
		bufferBarrier.dependsOnCopy().doBeforeVertexShader().execute();
	}

	getVulkanContext().device().getGraphicsQueue().submit({ commandBuffer }, nullptr);
	mLastSignalledValue[mCmdBufferIdx] = getVulkanContext().device().getGraphicsQueue().getSignalValue();
	mCmdBufferIdx = (mCmdBufferIdx + 1) % 3;

}