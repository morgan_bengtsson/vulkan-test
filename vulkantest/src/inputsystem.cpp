#include "inputsystem.h"

#include "raycast.h"

#include <QEvent>

#include <iostream>
#define _USE_MATH_DEFINES
#include <math.h>

#include <Eigen/Geometry>



InputSystem::InputSystem(vulkanutils::VulkanContext& context)
	: System<InputSystem>(context)
	, comm::Listener<KeyboardInput>()
	, mDebugWindow(nullptr)
{

}

void InputSystem::setupDebugWindow()
{
	mDebugWindow = new QWidget();
	mDebugWindow->setLayout(new QVBoxLayout());
	mLogWidget = new QPlainTextEdit(mDebugWindow);
	mLogWidget->setReadOnly(true);
	mCmdWidget = new QLineEdit(mDebugWindow);
	QObject::connect(mCmdWidget, &QLineEdit::returnPressed,
		[this]()
	{
		mCommandToProcess = mCmdWidget->text().toStdString();
		mCmdWidget->clear();
	});
	mDebugWindow->layout()->addWidget(mLogWidget);
	mDebugWindow->layout()->addWidget(mCmdWidget);
	mDebugWindow->show();
}

void InputSystem::runImpl(SceneType scene)
{
	if (mDebugWindow == nullptr)
		setupDebugWindow();

	auto& cc = scene.getComponentCollection();
	auto& gpuCc = scene.getGpuComponentCollection();

	KeyboardInput::KeyModifiers modifiers = {};
	for (auto& key : mPressedKeys)
	{
		if (key.key == Qt::Key::Key_Shift)
			modifiers.shift = 1;
		if (key.key == Qt::Key::Key_Control)
			modifiers.ctrl = 1;
		if (key.key == Qt::Key::Key_Alt)
			modifiers.alt = 1;
	}
	for (auto& key : mPressedKeys)
	{
		key.modifiers = modifiers;
		for (auto& entity : scene.getEntities<component::RunMode>())
		{
			updateRunMode(*entity, scene, key);
		}
		for (auto& entity : scene.getEntities<component::CameraProjection>())
		{
			updateInput(*entity, scene, key);
		}
	}
	std::vector<KeyboardInput> pk;
	pk.reserve(mPressedKeys.size());
	for (KeyboardInput& key : mPressedKeys)
	{
		if (key.state == KeyboardInput::State::PRESSED_DOWN)
			key.state = KeyboardInput::State::DOWN;
		if (key.state != KeyboardInput::State::RELEASED)
			pk.push_back(key);
	}
	mPressedKeys = pk;
	processCommand(scene);

	// get inverse camera transform
	ecs::Entity* camEntity = scene.getEntities<component::CameraProjection>()[0];
	const component::CameraProjection& cameraProjection = camEntity->getComponent<component::CameraProjection>(cc);
	const component::Position& pos = camEntity->getComponent<component::Position>(cc);
	const component::Orientation& orientation = camEntity->getComponent<component::Orientation>(cc);
	Eigen::Matrix4f cameraViewTransform = Eigen::Matrix4f::Identity();
	cameraViewTransform.topLeftCorner<3, 3>() = orientation.transpose();
	cameraViewTransform.topRightCorner<3, 1>() = -orientation.transpose() * pos;
	Eigen::Matrix4f invCameraTransform = (cameraProjection * cameraViewTransform).inverse();


	auto humanPlayerEntities = scene.getEntities<component::HumanPlayerInput>();
	component::HumanPlayerInput& playerInput = humanPlayerEntities[0]->getComponent<component::HumanPlayerInput>(cc);
	playerInput.keyboardEvents = mPressedKeys;
	playerInput.mouseEvents = mMouseInputEvents;
	playerInput.mouseRayIntersections.clear();

	for (const MouseInput& mi : mMouseInputEvents)
	{
		if (mi.buttons & MouseInput::Buttons(MouseInput::Button::LEFT))
		{
			auto ray = raycast::getCameraRay(mi.normPos, pos, invCameraTransform);
			vulkanutils::logger(Log::Severity::INFO) << "RAY\n" << ray.origin().transpose() << "\n" << ray.direction().transpose() << std::endl;
			component::RayEntityIntersection intersectionRes = {};
			if (raycast::intersection(scene, ray, &intersectionRes))
			{
				std::cout << "INTERSECTS" << std::endl;
				playerInput.mouseRayIntersections.emplace_back(intersectionRes);
			}
			else
			{
				playerInput.mouseRayIntersections.emplace_back();
			}
		}
	}

	mMouseInputEvents.clear();
}



void InputSystem::updateRunMode(ecs::Entity& entity, SceneType& scene, const KeyboardInput& keyboardInput)
{
	auto& runMode = entity.getComponent<component::RunMode>(scene.getComponentCollection());
	if (keyboardInput.key == Qt::Key::Key_Space && keyboardInput.state == KeyboardInput::State::PRESSED_DOWN)
	{
		if (runMode == component::RunMode::RUN)
			runMode = component::RunMode::PAUSE;
		else
			runMode = component::RunMode::RUN;
	}
	if (keyboardInput.key == Qt::Key::Key_PageUp && keyboardInput.state == KeyboardInput::State::PRESSED_DOWN)
	{
		runMode = component::RunMode::STEP;
	}

}

void InputSystem::updateInput(ecs::Entity& entity, SceneType& scene, const KeyboardInput& keyboardInput)
{
	auto& pos = entity.getComponent<component::Position>(scene.getComponentCollection());
	auto& orientation = entity.getComponent<component::Orientation>(scene.getComponentCollection());

	float speed = 4.0f;
	if (keyboardInput.modifiers.shift)
		speed = 40.0f;

	switch (keyboardInput.key)
	{
	case 'W':
		pos -= speed * orientation.col(2);
		break;
	case 'S':
		pos += speed * orientation.col(2);
		break;
	case 'A':
		pos -= speed * orientation.col(0);
		break;
	case 'D':
		pos += speed * orientation.col(0);
		break;
	case 'Q':
		pos -= speed * orientation.col(1);
		break;
	case 'E':
		pos += speed * orientation.col(1);
		break;
	case Qt::Key::Key_Up:
		orientation *= Eigen::AngleAxisf(-0.01f * float(M_PI), Eigen::Vector3f::UnitX()).toRotationMatrix();
		break;
	case Qt::Key::Key_Down:
		orientation *= Eigen::AngleAxisf(0.01f * float(M_PI), Eigen::Vector3f::UnitX()).toRotationMatrix();
		break;
	case Qt::Key::Key_Left:
		orientation = Eigen::AngleAxisf(0.01f * float(M_PI), Eigen::Vector3f::UnitY()).toRotationMatrix() * orientation;
		break;
	case Qt::Key::Key_Right:
		orientation = Eigen::AngleAxisf(-0.01f * float(M_PI), Eigen::Vector3f::UnitY()).toRotationMatrix() * orientation;
		break;
	case Qt::Key::Key_Space:
		break;
	}
	entity.setModified();
}

void InputSystem::execute(const KeyboardInput& message)
{
	auto it = std::find_if(mPressedKeys.begin(), mPressedKeys.end(),
		[& message ](const KeyboardInput& ki)
	{
		return ki.key == message.key;
	});
	if (message.state == KeyboardInput::State::DOWN)
	{
		if (it == mPressedKeys.end())
		{
			auto pressedMessage = message;
			pressedMessage.state = KeyboardInput::State::PRESSED_DOWN;
			mPressedKeys.push_back(pressedMessage);
		}
		else
		{
			it->modifiers = message.modifiers;
		}
	}
	else if (message.state == KeyboardInput::State::UP)
	{
		if (it != mPressedKeys.end())
			it->state = KeyboardInput::State::RELEASED;
	}
}


void InputSystem::execute(const MouseInput& message)
{
	mMouseInputEvents.push_back(message);
}


void InputSystem::processCommand(SceneType& scene)
{
	if (mCommandToProcess.empty())
		return;

	std::stringstream ret;
	ret << "$ " << mCommandToProcess;
	std::stringstream ss(mCommandToProcess);
	std::string cmd;
	ss >> cmd;
	if (cmd == "ent")
	{
		ecs::Entity* entity = nullptr;
		std::string cmd2;
		ss.ignore();
		if (std::isdigit(ss.peek()))
		{
			size_t id; ss >> id;
			entity = scene.getEntity(id);
		}
		ss >> cmd2;
		if (cmd2 == "idx")
		{
			size_t idx; ss >> idx;
			entity = &scene.getAllEntities()[idx];
			ss >> cmd2;
		}


		if (cmd2 == "c")
		{
			ret << std::endl << std::to_string(scene.getAllEntities().size());
		}
		else if (cmd2 == "s" && entity)
		{
			ret << "\n" << entity->getName();
			ret << "\n" << entity->getId();
		}
		else if (cmd2 == "r" && entity)
		{
			scene.removeEntity(entity->getId());
		}
	}

	vulkanutils::logger(Log::Severity::INFO) << ret.str() << std::endl;
	mLogWidget->appendPlainText(QString::fromStdString(ret.str()));

	mCommandToProcess.clear();
}
