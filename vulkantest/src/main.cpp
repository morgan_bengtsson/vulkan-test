#include "main.h"

#include "qtvulkanwindow.h"
#include "outputdebugstringbuf.h"
#include "testscene.h"
#include "tracer.h"
#include "fpstimer.h"

#include <vulkanUtils.h>
#include <vulkanContext.h>
#include <QApplication>
#include <QWindow>
#include <QWidget>
#include <QMainWindow>
#include <QLayout>
#include <QVBoxLayout>
#include <QPlainTextEdit>

#include <iostream>
#include <chrono>
#include <vulkan/vulkan.h>

#include "stablevector.h"

const int WIDTH = 1600;
const int HEIGHT = 1000;


void setupLogging()
{
	// Setup loggers
	setupWindowsDebugOutput();
	vulkanutils::logger.addStream(&std::cout);
	vulkanutils::logger.setLevel(Log::Severity::INFO);
}

int main(int argc, char** argv)
{
	setupLogging();

	QApplication app(argc, argv);
	QMainWindow mainWindow;

	QtVulkanWindow window;
	QWidget* windowWidget = QWidget::createWindowContainer(&window, &mainWindow, Qt::Widget);
	windowWidget->setSizePolicy(QSizePolicy(QSizePolicy::Policy::Expanding, QSizePolicy::Policy::Expanding));
	
	
	QVBoxLayout* layout = new QVBoxLayout();
	layout->addWidget(windowWidget);
	mainWindow.setCentralWidget(new QWidget());
	mainWindow.centralWidget()->setLayout(layout);
	mainWindow.setSizePolicy(QSizePolicy(QSizePolicy::Policy::Expanding, QSizePolicy::Policy::Expanding));
	mainWindow.centralWidget()->setSizePolicy(QSizePolicy(QSizePolicy::Policy::Expanding, QSizePolicy::Policy::Expanding));
	mainWindow.resize(WIDTH, HEIGHT);
	mainWindow.show();

	vulkanutils::VulkanContext vulkanContext("vulkan-test", window.winId());
	vulkanContext.init(window.width(), window.height());
	TestScene scene(vulkanContext);
	scene.setup(window);

	FpsTimer fpsTimer;
	fpsTimer.setTargetFps(10.0f);
	while (window.isVisible())
	{
		scene.update();
		scene.postUpdate();
		fpsTimer.tickWait();
		app.processEvents();
		std::cout << fpsTimer.getFps() << std::endl;
	}

	vkDeviceWaitIdle(vulkanContext.device());
	vulkanutils::logger(Log::Severity::INFO) << std::endl << TimingTracer::GetInstance() << std::endl;

	return 0;
};
