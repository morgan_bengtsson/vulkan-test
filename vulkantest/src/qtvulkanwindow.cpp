#include "qtvulkanwindow.h"

#include <QKeyEvent>

#include <Eigen/Dense>

#include <iostream>

#define _USE_MATH_DEFINES
#include <math.h>


QtVulkanWindow::QtVulkanWindow(QWindow* parent)
	:QWindow(parent)
	, comm::Dispatcher<KeyboardInput, MouseInput>()
	, mVulkanInstance()
	, mInitialized(false)
{
	QWindow::setSurfaceType(SurfaceType::VulkanSurface);
}

void QtVulkanWindow::setVulkanInstance(VkInstance instance)
{
	mVulkanInstance.setVkInstance(instance);
}

void QtVulkanWindow::exposeEvent(QExposeEvent*)
{
	if (isExposed())
	{
		if (!mInitialized)
		{
			mVulkanInstance.create();
			QWindow::setVulkanInstance(&mVulkanInstance);
			render();

			mInitialized = true;
		}
	}
}

void QtVulkanWindow::keyPressEvent(QKeyEvent* event)
{
	KeyboardInput ki;
	ki.key = event->key();
	ki.modifiers.alt = event->modifiers().testFlag(Qt::KeyboardModifier::AltModifier);
	ki.modifiers.ctrl = event->modifiers().testFlag(Qt::KeyboardModifier::ControlModifier);
	ki.modifiers.shift = event->modifiers().testFlag(Qt::KeyboardModifier::ShiftModifier);
	ki.state = KeyboardInput::State::DOWN;
	dispatch(ki);
}

void QtVulkanWindow::keyReleaseEvent(QKeyEvent* event)
{
	if (event->isAutoRepeat())
		return;
	KeyboardInput ki;
	ki.key = event->key();
	ki.modifiers.alt = event->modifiers().testFlag(Qt::KeyboardModifier::AltModifier);
	ki.modifiers.ctrl = event->modifiers().testFlag(Qt::KeyboardModifier::ControlModifier);
	ki.modifiers.shift = event->modifiers().testFlag(Qt::KeyboardModifier::ShiftModifier);
	ki.state = KeyboardInput::State::UP;
	dispatch(ki);
}

void QtVulkanWindow::mouseDoubleClickEvent(QMouseEvent* ev)
{
	QWindow::mouseDoubleClickEvent(ev);
}

void QtVulkanWindow::mouseMoveEvent(QMouseEvent* ev)
{
	MouseInput mi = {};
	if (ev->buttons() & Qt::MouseButton::LeftButton)
		mi.buttons |= MouseInput::Buttons(MouseInput::Button::LEFT);
	if (ev->buttons() & Qt::MouseButton::RightButton)
		mi.buttons |= MouseInput::Buttons(MouseInput::Button::RIGHT);
	if (ev->buttons() & Qt::MouseButton::MiddleButton)
		mi.buttons |= MouseInput::Buttons(MouseInput::Button::MIDDLE);

	mi.pos = { ev->windowPos().x(), ev->windowPos().y() };
	mi.normPos = { ev->windowPos().x() / float(width()) * 2 - 1, ev->windowPos().y() / float(height()) * 2 - 1 };

	dispatch<MouseInput>(mi);
}

void QtVulkanWindow::mousePressEvent(QMouseEvent* ev)
{
	MouseInput mi = {};
	if (ev->buttons() & Qt::MouseButton::LeftButton)
		mi.buttons |= MouseInput::Buttons(MouseInput::Button::LEFT);
	if (ev->buttons() & Qt::MouseButton::RightButton)
		mi.buttons |= MouseInput::Buttons(MouseInput::Button::RIGHT);
	if (ev->buttons() & Qt::MouseButton::MiddleButton)
		mi.buttons |= MouseInput::Buttons(MouseInput::Button::MIDDLE);

	mi.pos = { ev->windowPos().x(), ev->windowPos().y() };
	mi.normPos = { ev->windowPos().x() / float(width()) * 2 - 1, ev->windowPos().y() / float(height()) * 2 - 1 };

	dispatch<MouseInput>(mi);
}

void QtVulkanWindow::mouseReleaseEvent(QMouseEvent* ev)
{
	MouseInput mi = {};
	if (ev->buttons() & Qt::MouseButton::LeftButton)
		mi.buttons |= MouseInput::Buttons(MouseInput::Button::LEFT);
	if (ev->buttons() & Qt::MouseButton::RightButton)
		mi.buttons |= MouseInput::Buttons(MouseInput::Button::RIGHT);
	if (ev->buttons() & Qt::MouseButton::MiddleButton)
		mi.buttons |= MouseInput::Buttons(MouseInput::Button::MIDDLE);

	mi.pos = { ev->windowPos().x(), ev->windowPos().y() };
	mi.normPos = { ev->windowPos().x() / float(width()) * 2 - 1, ev->windowPos().y() / float(height()) * 2 - 1 };

	dispatch<MouseInput>(mi);
}

bool QtVulkanWindow::event(QEvent *e)
{
	if (e->type() == QEvent::UpdateRequest)
	{
		render();
	}
	return QWindow::event(e);
}

void QtVulkanWindow::render()
{
   //requestUpdate(); // render continuously
}

void QtVulkanWindow::resizeEvent(QResizeEvent* resizeEvent)
{
}
