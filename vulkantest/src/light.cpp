#include "light.h"

#include <vulkanResourceSet.h>

LightSet::LightSet(vulkanutils::Device* device, SceneType scene, vulkanutils::DescriptorSetSlot descSetbp)
	: EntityResourceSet(device, "light", descSetbp, vulkanutils::VertexInputSlot::NONE)
{
	setDescriptorBinding<vulkanutils::ShaderStorageBuffer, component::DirectionalLightGpu>(scene,
		{
			VK_SHADER_STAGE_FRAGMENT_BIT,
			0,
			1,
		});
	setDescriptorBinding<vulkanutils::ShaderStorageBuffer, component::PositionalLightGpu>(scene,
		{
			VK_SHADER_STAGE_FRAGMENT_BIT,
			1,
			1,
		});
	getDescriptorLayout().build();
}

LightWithShadowSet::LightWithShadowSet(vulkanutils::Device* device, SceneType scene, vulkanutils::DescriptorSetSlot descSetbp)
	: EntityResourceSet(device, "light", descSetbp, vulkanutils::VertexInputSlot::NONE)
{
	setDescriptorBinding<vulkanutils::ShaderStorageBuffer, component::DirectionalLightGpu>(scene,
		{
			VK_SHADER_STAGE_FRAGMENT_BIT,
			0,
			1,
		});
	setDescriptorBinding<vulkanutils::ShaderStorageBuffer, component::PositionalLightGpu>(scene,
		{
			VK_SHADER_STAGE_FRAGMENT_BIT,
			1,
			1,
		});
	setImageDescriptorBinding<component::ShadowDistance>(scene,
		{
			VK_SHADER_STAGE_FRAGMENT_BIT,
			2,
			1,
		});
	getDescriptorLayout().build();
}



LightShadowSet::LightShadowSet(vulkanutils::Device* device, SceneType scene, vulkanutils::DescriptorSetSlot descSetbp)
	: EntityResourceSet(device, "light", descSetbp, vulkanutils::VertexInputSlot::NONE)
{
	setDescriptorBinding<vulkanutils::ShaderStorageBuffer, component::DirectionalLightGpu>(scene,
		{
			VK_SHADER_STAGE_VERTEX_BIT | VK_SHADER_STAGE_FRAGMENT_BIT | VK_SHADER_STAGE_TESSELLATION_EVALUATION_BIT,
			0,
			1,
		});
	setDescriptorBinding<vulkanutils::ShaderStorageBuffer, component::PositionalLightGpu>(scene,
		{
			VK_SHADER_STAGE_VERTEX_BIT | VK_SHADER_STAGE_FRAGMENT_BIT | VK_SHADER_STAGE_TESSELLATION_EVALUATION_BIT,
			1,
			1,
		});
	
	setDescriptorBinding<vulkanutils::UniformBuffer, component::LightIndex>(scene,
		{
			VK_SHADER_STAGE_VERTEX_BIT | VK_SHADER_STAGE_FRAGMENT_BIT | VK_SHADER_STAGE_TESSELLATION_EVALUATION_BIT,
			2,
			1,
		});
	getDescriptorLayout().build();
}
