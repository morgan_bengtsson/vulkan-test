#include "mesh.h"


EntityResourceSet meshSet(vulkanutils::Device* device
	, MeshSetSceneType scene, vulkanutils::VertexInputSlot vertInputBp, size_t numTexCoords, bool withSkeleton)
{
	EntityResourceSet ret(device, "mesh", vulkanutils::DescriptorSetSlot::NONE, vertInputBp);

	ret.setPerVertexAttributeBinding<component::MeshVertex>(scene, 0, VK_FORMAT_R32G32B32_SFLOAT);
	ret.setPerVertexAttributeBinding<component::MeshNormal>(scene, 1, VK_FORMAT_R32G32B32_SFLOAT);

	for (size_t i = 0; i < numTexCoords; i++)
		ret.setPerVertexAttributeBinding<component::MeshTexCoord>(scene, 2 + i, VK_FORMAT_R32G32_SFLOAT);
	if (withSkeleton)
	{
		ret.setPerVertexAttributeBinding<skeleton::VertexBoneData>(scene,
			{ 
				{ uint32_t(2 + numTexCoords), VK_FORMAT_R32G32B32A32_UINT, skeleton::NUM_BONE_WEIGHTS * sizeof(uint32_t) },
				{ uint32_t(3 + numTexCoords), VK_FORMAT_R32G32B32A32_SFLOAT, skeleton::NUM_BONE_WEIGHTS * sizeof(float) }
			});
	}
	ret.setHasIndexBinding();
	ret.addComponetTypes<component::MeshVertex, component::MeshNormal, component::MeshIndex>(scene);
	return ret;
}

EntityResourceSet skinnedMeshSet(vulkanutils::Device* device
		, SkinnedMeshSetSceneType scene, vulkanutils::VertexInputSlot vertInputBp, size_t numTexCoords)
{
	EntityResourceSet ret(device, "SkinnedMesh", vulkanutils::DescriptorSetSlot::NONE, vertInputBp);
	ret.setPerVertexAttributeBinding<component::SkinnedMeshVertex>(scene, 0, VK_FORMAT_R32G32B32_SFLOAT);
	ret.setPerVertexAttributeBinding<component::SkinnedMeshNormal>(scene, 1, VK_FORMAT_R32G32B32_SFLOAT);
	for (size_t i = 0; i < numTexCoords; i++)
		ret.setPerVertexAttributeBinding<component::MeshTexCoord>(scene, 2 + i, VK_FORMAT_R32G32_SFLOAT);
	ret.setHasIndexBinding();
	return ret;
}
