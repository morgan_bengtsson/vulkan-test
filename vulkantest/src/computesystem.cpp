#include "computesystem.h"



ComputeSystem::ComputeSystem(vulkanutils::VulkanContext& context, const char* shaderPath)
	: mContext(context)
	, mShaderPath(shaderPath)
{
	
}


ComputeSystem::Pipeline ComputeSystem::createComputePipeline(const char* shaderName, std::vector<EntityResourceSet> computeSets)
{
	ComputeSystem::Pipeline computePipelineData;
	computePipelineData.mShader = vulkanutils::loadComputeShader(mContext.device(), mShaderPath.c_str(), shaderName);
	computePipelineData.mComputeSets = std::move(computeSets);

	std::vector<const vulkanutils::ResourceSet*> resourceSets;
	resourceSets.reserve(computePipelineData.mComputeSets.size());
	for (auto& set : computePipelineData.mComputeSets)
		resourceSets.push_back(&set);

	computePipelineData.mComputePipeline = vulkanutils::ComputePipeline(mContext, computePipelineData.mShader, resourceSets);
	return computePipelineData;
}


std::vector<vulkanutils::DescriptorSet>& ComputeSystem::Pipeline::createDescriptorSets(size_t entityId, const vulkanutils::DescriptorPool& pool, const std::vector<size_t> variableSetSize)
{
	auto& descSets = mComputeDescSets[entityId];
	auto variableSetSizeIt = variableSetSize.begin();
	for (auto& compSet : mComputeSets)
		if (variableSetSize.end() != variableSetSizeIt)
			descSets.push_back(compSet.createDescriptorSet(pool, *variableSetSizeIt++));
		else
			descSets.push_back(compSet.createDescriptorSet(pool));
	return descSets;
}

const std::vector<vulkanutils::DescriptorSet>& ComputeSystem::Pipeline::getDescriptorSets(size_t entityId) const
{
	return mComputeDescSets.find(entityId)->second;
}

std::vector<vulkanutils::DescriptorSet>& ComputeSystem::Pipeline::getDescriptorSets(size_t entityId)
{
	return mComputeDescSets.find(entityId)->second;
}


void ComputeSystem::Pipeline::removeDescriptorSet(size_t entityId)
{
	mComputeDescSets.erase(entityId);
}

const std::vector<EntityResourceSet>& ComputeSystem::Pipeline::getComputeSets() const
{
	return mComputeSets;
}

void ComputeSystem::Pipeline::execute(VkCommandBuffer commandBuffer, size_t entityId, std::array<uint32_t, 3> groupSize)
{
	mComputePipeline.bind(commandBuffer);
	auto& descriptorSets = getDescriptorSets(entityId);
	for (size_t i = 0; i < mComputeSets.size(); i++)
		descriptorSets[i].bindToCompute(commandBuffer, mComputePipeline.getLayout(), (uint32_t) mComputeSets[i].getDescriptorSetBindpoint());
	vkCmdDispatch(commandBuffer, groupSize[0], groupSize[1], groupSize[2]);
}
