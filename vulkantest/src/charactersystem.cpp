#include "charactersystem.h"
#include "entitybuilder.h"
#include "fileload.h"

#include <vulkanContext.h>

#include <random>
#include <unsupported/Eigen/Splines>

#define _USE_MATH_DEFINES
#include <math.h>


CharacterSystem::CharacterSystem(vulkanutils::VulkanContext& context, SceneType scene)
	: ecs::System<CharacterSystem>(context)
{
	auto& cc = scene.getComponentCollection();
	auto& gpuCc = scene.getGpuComponentCollection();

	mDrawPipeline = ecs::Entity("CharacterPipeline");
	{
		using namespace vulkanutils;
		component::Pipeline& pipeline = mDrawPipeline.addComponent<component::Pipeline>(cc, component::Pipeline("doll"));

		pipeline.sets.emplace_back(systemPropertiesSet(&context.device(), scene, DescriptorSetSlot::SLOT_1 ));
		pipeline.sets.emplace_back(cameraSet(&context.device(), scene, DescriptorSetSlot::SLOT_2 ));
		pipeline.sets.emplace_back(materialSet(&context.device(), scene, DescriptorSetSlot::SLOT_4, 0 ));
		pipeline.sets.emplace_back(skinnedMeshSet(&context.device(), scene, VertexInputSlot::SLOT_0, 0));
		pipeline.sets.emplace_back(ModelSet(&context.device(), scene, DescriptorSetSlot::SLOT_5, true ));
		pipeline.sets.emplace_back(LightWithShadowSet(&context.device(), scene, DescriptorSetSlot::SLOT_3 ));

		pipeline.addRenderPassAttachment<component::Color>(scene, 0);
		pipeline.addRenderPassAttachment<component::Depth>(scene, 1);

		scene.addEntity(mDrawPipeline);
	}

	mShadowPipeline = ecs::Entity("ShadowPipeline");
	{
		using namespace vulkanutils;
		component::Pipeline& pipeline = mShadowPipeline.addComponent<component::Pipeline>(cc, component::Pipeline("dollShadow"));

		pipeline.sets.emplace_back(skinnedMeshSet(&context.device(), scene, VertexInputSlot::SLOT_0, 0));
		pipeline.sets.emplace_back(ModelSet(&context.device(), scene, DescriptorSetSlot::SLOT_5, true ));
		pipeline.sets.emplace_back(LightShadowSet(&context.device(), scene, DescriptorSetSlot::SLOT_3 ));

		pipeline.addRenderPassAttachment<component::ShadowDistance>(scene, 0);
		pipeline.cullMode = VK_CULL_MODE_BACK_BIT;

		scene.addEntity(mShadowPipeline);
	}
}


void aStar(const component::Surface<float>& surface, component::Character& character)
{
	auto heightFunc = [&](float x, float y)
	{
		auto surfaceHeightFunc = [](float h){return h;};
		return surface.get(x, y, surfaceHeightFunc);
	};

	const float inclineCost = 1.0f;
	const float declineCost = 1.0f;
	const float stepLength = 100.0f;

	const size_t gridXSize = surface.getXSize() / stepLength;
	const size_t gridYSize = surface.getYSize() / stepLength;

	auto coordToIdx = [&](float x, float y)
	{
		size_t xidx = std::round((x + surface.getXSize() / 2.0f) / stepLength);
		size_t yidx = std::round((y + surface.getYSize() / 2.0f) / stepLength);
		return yidx * gridXSize + xidx;
	};
	auto idxToCoord = [&](size_t idx)
	{
		return Eigen::Vector2f(idx % gridXSize, idx / gridXSize) * stepLength - Eigen::Vector2f(surface.getXSize(), surface.getYSize()) / 2.0f;
	};

	size_t startPosIdx = coordToIdx(character.position.x(), character.position.y());

	struct Node
	{
		float currentCost;
		float estimatedCost;
	};

	std::unordered_map<size_t, Node> nodes;
	std::unordered_map<size_t, size_t> previousNodes;
	std::deque<size_t> frontier;

	auto costEsitmateFunc = [&](const Eigen::Vector2f& pos) { return (character.goalPosition - pos).norm(); };

	nodes[startPosIdx] =
		{
			0,
			costEsitmateFunc(character.position)
		};
	frontier.push_back(startPosIdx);
	previousNodes[startPosIdx] = startPosIdx;

	while (!frontier.empty())
	{
		size_t nodeIdx = frontier.front();
		frontier.pop_front();
		Eigen::Vector2f pos = idxToCoord(nodeIdx);
		float height = heightFunc(pos.x(), pos.y());

		Node& node = nodes[nodeIdx];
		float estimatedCost = node.estimatedCost;
		if ((pos - character.goalPosition).squaredNorm() < stepLength * stepLength)
		{
			size_t numControlPoints = 1;
			for (size_t i = nodeIdx; i != startPosIdx; i = previousNodes[i])
				numControlPoints++;
			numControlPoints++;
			Eigen::Matrix2Xf controlPoints(2, numControlPoints);
			controlPoints.col(0) = character.position;
			for (size_t i = nodeIdx, col = 0; i != startPosIdx; i = previousNodes[i], col++)
			{
				controlPoints.col(numControlPoints - 2 - col) = idxToCoord(i);
			}
			controlPoints.col(numControlPoints - 1) = character.goalPosition;
			character.pathSpline = Eigen::SplineFitting<Eigen::Spline<float, 2> >::Interpolate(controlPoints, std::min<size_t>(9, controlPoints.cols() - 2));
			return;
		}

		std::vector<size_t> neighbours;
		bool left = (nodeIdx % gridXSize) == 0;
		bool right = (nodeIdx + 1) % gridXSize == 0;
		bool top = (nodeIdx < gridXSize);
		bool bottom = (nodeIdx >= (gridXSize - 1) * gridYSize);

		
		if (!left)
		{
			neighbours.push_back(nodeIdx - 1);
			if (!top) neighbours.push_back(nodeIdx - gridXSize - 1);
			if (!bottom) neighbours.push_back(nodeIdx + gridXSize - 1);
		}
		if (!right)
		{
			neighbours.push_back(nodeIdx + 1);
			if (!top) neighbours.push_back(nodeIdx - gridXSize + 1);
			if (!bottom) neighbours.push_back(nodeIdx + gridXSize + 1);
		}
		if (!top) neighbours.push_back(nodeIdx - gridXSize);
		if (!bottom) neighbours.push_back(nodeIdx + gridXSize);

		for (size_t nIdx : neighbours)
		{
			size_t pIdx = previousNodes[nodeIdx];
			if (nIdx == pIdx)
				continue;
			
			Eigen::Vector2f prevPos = idxToCoord(pIdx);
			Eigen::Vector2f nextPos = idxToCoord(nIdx);
			float turn = (nextPos - pos).normalized().dot((pos - prevPos).normalized());
			if (turn <= 0 && nodeIdx != pIdx)
				continue;

			float nextHeight = heightFunc(nextPos.x(), nextPos.y());
			float heightCost = stepLength * ((nextHeight - height) * ((nextHeight > height) ? inclineCost : -declineCost));
			float currentCost = nodes[nodeIdx].currentCost + heightCost + stepLength + (1.0f - turn) * stepLength;



			if (auto itRet = nodes.insert({ nIdx, {} }); itRet.second || currentCost < itRet.first->second.currentCost)
			{
				float estimatedRemainingCost = costEsitmateFunc(nextPos);
				float estimatedCost = currentCost + estimatedRemainingCost;
				itRet.first->second = 
				{
					currentCost,
					estimatedCost,
				};
				frontier.push_back(nIdx);
				previousNodes[nIdx] = nodeIdx;
			}
		}
		std::sort(frontier.begin(), frontier.end(), [&](size_t lhs, size_t rhs)
		{
			return nodes[lhs].estimatedCost < nodes[rhs].estimatedCost;
		});
	}
}


void CharacterSystem::updateCharacter(ecs::Entity& entity, SceneType& scene, const component::Surface<float>& surface)
{
	auto& cc = scene.getComponentCollection();
	auto& gpuCc = scene.getGpuComponentCollection();

	uint64_t seed = std::chrono::system_clock::now().time_since_epoch().count();
	std::default_random_engine generator((unsigned int)seed);
	std::uniform_real_distribution<float> distribution(-0.5f, 0.5f);

	component::Character& character = entity.getComponent<component::Character>(cc);

	if (character.splinePos < 1.0)
	{
		Eigen::Matrix2f posDerriv = character.pathSpline.derivatives<1>(character.splinePos);
		character.direction = (posDerriv.col(1).normalized());// - character.position);
		character.position = posDerriv.col(0);
		character.splinePos = std::min<float>(1.0f, character.splinePos + 10.0f / posDerriv.col(1).norm());

		for (auto& aa : entity.getComponentArray<skeleton::ActiveAnimation>(cc))
			aa.animationIndex = 2;
	}
	else if (character.splinePos >= 1.0)
	{
		for (auto& aa : entity.getComponentArray<skeleton::ActiveAnimation>(cc))
			aa.animationIndex = 0;
	}




	auto surfaceHeightFunc = [](float h){return h;};
	float surfaceHeight = surface.get(character.position.x(), character.position.y(), surfaceHeightFunc);

	float angle = std::atan2(character.direction.x(), character.direction.y());
	auto transform(Eigen::Transform<float, 3, Eigen::Affine>::Identity());
	transform.translate(Eigen::Vector3f(character.position.x(), surfaceHeight, character.position.y()));
	transform.rotate(Eigen::AngleAxisf(angle, Eigen::Vector3f::UnitY()));
	transform.rotate(Eigen::AngleAxisf(-90.0f / 180.0f * 3.14f, Eigen::Vector3f::UnitX()));


	auto& orientation = entity.getComponent<component::Orientation>(scene.getComponentCollection());
	orientation = transform.matrix().topLeftCorner<3, 3>();
	auto& position = entity.getComponent<component::Position>(scene.getComponentCollection());
	position = transform.matrix().topRightCorner<3, 1>();
}


void CharacterSystem::setupCharacter(ecs::Entity& entity, SceneType& scene, const component::Surface<float>& surface, std::vector<ecs::Entity>& newEntities)
{
	auto& cc = scene.getComponentCollection();
	auto& gpuCc = scene.getGpuComponentCollection();

	component::Character& character = entity.getComponent<component::Character>(cc);

	// randomize a position
	uint64_t seed = std::chrono::system_clock::now().time_since_epoch().count();
	std::default_random_engine generator((unsigned int)seed);
	std::uniform_real_distribution<float> distribution(-0.5f, 0.5f);

	auto surfaceHeightFunc = [](float h){return h;};
	character.position = { distribution(generator) * surface.getXSize(), distribution(generator) * surface.getYSize() };
	character.goalPosition = { distribution(generator) * surface.getXSize(), distribution(generator) * surface.getYSize() };
	character.direction = (character.goalPosition - character.position).normalized();//(1.0f + distribution(generator));
	aStar(surface, character);
	character.splinePos = 0.0f;
	float surfaceHeight = surface.get(character.position.x(), character.position.y(), surfaceHeightFunc);

	auto transform(Eigen::Transform<float, 3, Eigen::Affine>::Identity());
	transform.translate(Eigen::Vector3f(character.position.x(), surfaceHeight, character.position.y()));
	transform.rotate(Eigen::AngleAxisf(-90.0f / 180.0f * 3.14f, Eigen::Vector3f::UnitX()));

	auto& orientation = entity.addComponent<component::Orientation>(scene.getComponentCollection());
	orientation = transform.matrix().topLeftCorner<3, 3>();
	auto& position = entity.addComponent<component::Position>(scene.getComponentCollection());
	position = transform.matrix().topRightCorner<3, 1>();


	// Add mesh
	auto insertRet = mMeshEntities.insert({ character.meshName, {} });
	MeshEntities& meshEntityIds = insertRet.first->second;
	if (insertRet.second)
	{
		auto [dollMeshEntities, dollMaterialEntities] = loadFile(character.meshName, cc, &getVulkanContext().device());
		meshEntityIds.meshes.reserve(dollMeshEntities.size());
		meshEntityIds.materials.reserve(dollMaterialEntities.size());
		for (auto& meshEntity : dollMeshEntities)
			insertRet.first->second.meshes.push_back(meshEntity.getId());
		for (auto& materialEntity : dollMaterialEntities)
			insertRet.first->second.meshes.push_back(materialEntity.getId());

		for (auto& entity : dollMeshEntities)
			scene.addEntity(entity);
		for (auto& entity : dollMaterialEntities)
			scene.addEntity(entity);
	}


	EntityBuilder entityBuilder(cc);
	std::vector<ecs::Entity> meshEntities;
	for (size_t meshEntityId : meshEntityIds.meshes)
		meshEntities.push_back(*scene.getEntity(meshEntityId));
	entityBuilder.addMesh(entity, meshEntities);
	entity.addComponents<ecs::EntityRef, ecs::EntityRef>(cc, 
		{ mDrawPipeline.getId(), ecs::EntityRef::Type::Link },
		{ mShadowPipeline.getId(), ecs::EntityRef::Type::Link } );
}

void CharacterSystem::runImpl(SceneType scene)
{
	auto& cc = scene.getComponentCollection();
	auto& gpuCc = scene.getGpuComponentCollection();

	auto surfaceEntities = scene.getEntities<component::Surface<float>>();
	const component::Surface<float>& surface = surfaceEntities.front()->getComponent<component::Surface<float>>(cc);
	const component::DiscreteSurface<float>& dsurface = surface.getDiscrete();


	auto playerEntities = scene.getEntities<component::HumanPlayer, component::HumanPlayerInput>();
	for (ecs::Entity* playerEntity : playerEntities)
	{
		const component::HumanPlayerInput& input = playerEntity->getComponent<component::HumanPlayerInput>(cc);
		component::HumanPlayer& player = playerEntity->getComponent<component::HumanPlayer>(cc);
		for (const component::RayEntityIntersection& intersection : input.mouseRayIntersections)
		{
			if (intersection.distance > 0.0f)
			{
				ecs::Entity* entity = scene.getEntity(intersection.entity);
				if (entity->hasComponents<component::Character>(cc))
				{
					player.controlledEntity = intersection.entity;
				}
				else if (entity->getId() == surfaceEntities.front()->getId())
				{
					if (ecs::Entity* controlledCharacterEntity = scene.getEntity(player.controlledEntity))
					{
						component::Character& character = controlledCharacterEntity->getComponent<component::Character>(cc);
						character.goalPosition = { intersection.position.x(), intersection.position.z() };
						aStar(surface, character);
						character.splinePos = 0.0f;
					}
				}
			}
		}
	}



	std::vector<ecs::Entity> newEntities;
	for (auto& ce : scene.getEntities<component::Character>())
	{
		if (scene.getRefEntities<component::MeshVertex>(ce, ecs::EntityRef::Type::Child).empty())
			setupCharacter(*ce, scene, surface, newEntities);
		else
			updateCharacter(*ce, scene, surface);
	}
	for (auto& e : newEntities)
		scene.addEntity(e);
}
