#include <drawcall.h>



void DrawCall::sort()
{
	for (auto& [set, setBindings] : bindings)
	{
		std::sort(std::begin(setBindings.uniform), std::end(setBindings.uniform));
		std::sort(std::begin(setBindings.shaderStorage), std::end(setBindings.shaderStorage));
		std::sort(std::begin(setBindings.image), std::end(setBindings.image));
		std::sort(std::begin(setBindings.vertexBuffer), std::end(setBindings.vertexBuffer));
	}
	for (auto& [set, setBindings] : cullBindings)
	{
		std::sort(std::begin(setBindings.uniform), std::end(setBindings.uniform));
		std::sort(std::begin(setBindings.shaderStorage), std::end(setBindings.shaderStorage));
		std::sort(std::begin(setBindings.image), std::end(setBindings.image));
		std::sort(std::begin(setBindings.vertexBuffer), std::end(setBindings.vertexBuffer));
	}
}

std::vector<CombinedDrawCall> CombinedDrawCall::build(DrawSystem::SceneType scene, DrawSystem& ds, size_t renderPassEntityId, std::vector<DrawCall> drawCalls)
{
	std::vector<CombinedDrawCall> combinedDrawCalls;
	std::sort(std::begin(drawCalls), std::end(drawCalls), DrawCallDescriptorSetOrder());

	auto startIt = std::begin(drawCalls);
	auto endIt = startIt;
	while (endIt != std::end(drawCalls))
	{
		startIt = endIt;
		DrawCall& firstDc = *startIt;
		endIt = std::find_if(startIt + 1, std::end(drawCalls), [&firstDc](const DrawCall& dc)
			{ return !DrawCallBufferEqual()(firstDc, dc); });
		combinedDrawCalls.emplace_back(scene, ds, renderPassEntityId, std::span<DrawCall>(startIt, endIt));
	}
	return std::move(combinedDrawCalls);
}

#pragma optimize ("", off)

void CombinedDrawCall::uploadComponentOffsetBuffers(VkCommandBuffer commandBuffer, std::span<CombinedDrawCall> combinedDrawCalls)
{
	vulkanutils::BufferCopier copier(commandBuffer, combinedDrawCalls.size());
	for (CombinedDrawCall& cdc : combinedDrawCalls)
	{
		copier.queue(cdc.mCullComponentOffsetHostBuffer, cdc.mCullComponentOffsetDeviceBuffer);
		copier.queue(cdc.mComponentOffsetHostBuffer, cdc.mComponentOffsetDeviceBuffer);
		copier.queue(cdc.mDrawCommandHostBuffer, cdc.mDrawCommandDeviceBuffer);
	}

}

CombinedDrawCall::CombinedDrawCall(DrawSystem::SceneType scene, DrawSystem& ds, size_t renderPassEntityId, std::span<DrawCall> drawCalls)
	: mPipelineEntity(drawCalls.front().pipelineEntity)
	, mRenderPassEntity(renderPassEntityId)
{
	auto& cc = scene.getComponentCollection();
	auto& gpuCc = scene.getGpuComponentCollection();

	auto& graphicsPipelines = ds.getPipeline(mPipelineEntity, renderPassEntityId);
	ecs::Entity* pipelineEntity = scene.getEntity(mPipelineEntity);
	const component::Pipeline& pipeline = pipelineEntity->getComponent<component::Pipeline>(cc);

	for (auto& [set, dcRsb] : drawCalls.front().bindings)
	{
		ResourceSetBinding& rsb = mBindings[set];
		for (const auto& uniformBinding : dcRsb.uniform)
			rsb.uniformBindings.push_back(uniformBinding);
		for (const auto& shaderStorage : dcRsb.shaderStorage)
			rsb.shaderStorageBindings.push_back(BufferBinding{ shaderStorage.bindingNr, *shaderStorage.buffer->getBuffer() });
		for (const auto& image : dcRsb.image)
			rsb.imageBindings.push_back(ImageBinding{ image.bindingNr, image.elementNr, *image.imageView, *image.imageSampler });
		rsb.vertexAttributeBindings = dcRsb.vertexBuffer;

		std::sort(rsb.uniformBindings.begin(), rsb.uniformBindings.end(), [](auto& lhs, auto& rhs)
			{ return lhs.bindingNr < rhs.bindingNr; });
		std::sort(rsb.shaderStorageBindings.begin(), rsb.shaderStorageBindings.end(), [](auto& lhs, auto& rhs)
			{ return lhs.bindingNr < rhs.bindingNr; });
		std::sort(rsb.vertexAttributeBindings.begin(), rsb.vertexAttributeBindings.end(), [](auto& lhs, auto& rhs)
			{ return lhs.bindingNr < rhs.bindingNr; });
		std::sort(rsb.imageBindings.begin(), rsb.imageBindings.end(), [](auto& lhs, auto& rhs)
			{ return lhs.bindingNr < rhs.bindingNr; });
	}

	for (auto& [set, dcRsb] : drawCalls.front().cullBindings)
	{
		ResourceSetBinding& rsb = mBindings[set];
		for (const auto& uniformBinding : dcRsb.uniform)
			rsb.uniformBindings.push_back(uniformBinding);
		for (const auto& shaderStorage : dcRsb.shaderStorage)
			rsb.shaderStorageBindings.push_back(BufferBinding{ shaderStorage.bindingNr, *shaderStorage.buffer->getBuffer() });
		for (const auto& image : dcRsb.image)
			rsb.imageBindings.push_back(ImageBinding{ image.bindingNr, image.elementNr, *image.imageView, *image.imageSampler });
		rsb.vertexAttributeBindings = dcRsb.vertexBuffer;

		std::sort(rsb.uniformBindings.begin(), rsb.uniformBindings.end(), [](auto& lhs, auto& rhs)
			{ return lhs.bindingNr < rhs.bindingNr; });
		std::sort(rsb.shaderStorageBindings.begin(), rsb.shaderStorageBindings.end(), [](auto& lhs, auto& rhs)
			{ return lhs.bindingNr < rhs.bindingNr; });
		std::sort(rsb.vertexAttributeBindings.begin(), rsb.vertexAttributeBindings.end(), [](auto& lhs, auto& rhs)
			{ return lhs.bindingNr < rhs.bindingNr; });
		std::sort(rsb.imageBindings.begin(), rsb.imageBindings.end(), [](auto& lhs, auto& rhs)
			{ return lhs.bindingNr < rhs.bindingNr; });
	}


	mIndexBuffer = drawCalls.front().indexBuffer;
	mIndirectDrawBuffer = drawCalls.front().indirectDrawBuffer;

	// TODO: use this as shader variation parameter
	mMaxNumComponents = graphicsPipelines[0].getMaxNumComponents();
	uint32_t maxSetNumber = graphicsPipelines[0].getNumSets();

	size_t componentOffsetBufferSize = mMaxNumComponents * maxSetNumber * drawCalls.size();
	mComponentOffsetHostBuffer = ds.mUploadBufferAllocator.allocate(componentOffsetBufferSize * sizeof(uint32_t));
	mComponentOffsetDeviceBuffer = ds.mShaderStorageBufferAllocator.allocate(componentOffsetBufferSize * sizeof(uint32_t), false);

	mCullComponentOffsetHostBuffer = ds.mUploadBufferAllocator.allocate(drawCalls.size() * sizeof(uint32_t));
	mCullComponentOffsetDeviceBuffer = ds.mShaderStorageBufferAllocator.allocate(drawCalls.size() * sizeof(uint32_t), false);


	mClippingBuffer = ds.mShaderStorageBufferAllocator.allocate(drawCalls.size() * sizeof(uint32_t), false);

	mDrawCommandHostBuffer = ds.mUploadBufferAllocator.allocate(sizeof(VkDrawIndexedIndirectCommand));
	mDrawCommandDeviceBuffer = ds.mShaderStorageBufferAllocator.allocate(sizeof(VkDrawIndexedIndirectCommand), false);
	mDrawCommandDeviceIdBuffer = ds.mIndirectDrawBufferAllocator.allocate(sizeof(VkDrawIndexedIndirectCommand));


	VkDrawIndexedIndirectCommand& drawCommand = mDrawCommandHostBuffer.getData<VkDrawIndexedIndirectCommand>()[0];
	drawCommand.firstIndex = 0;
	drawCommand.firstInstance = 0;
	drawCommand.indexCount = mIndexBuffer->getMemRange().size / sizeof(uint32_t);
	drawCommand.instanceCount = 0;
	drawCommand.vertexOffset = 0;


	std::span<uint32_t> cullComponentOffsetBuffer = mCullComponentOffsetHostBuffer.getData<uint32_t>();
	std::span<uint32_t> componentOffsetBuffer = mComponentOffsetHostBuffer.getData<uint32_t>();
	for (size_t i = 0; i < drawCalls.size(); i++)
	{
		const DrawCall& dc = drawCalls[i];
		for (auto& [set, bufferBindings] : dc.bindings)
		{
			for (auto& bufferBinding : bufferBindings.shaderStorage)
			{
				uint32_t idx = (i * maxSetNumber + uint32_t(set->getDescriptorSetBindpoint())) * mMaxNumComponents + bufferBinding.bindingNr;
				componentOffsetBuffer[idx] = uint32_t(bufferBinding.buffer->getMemRange().offset);
			}
		}
		for (auto& [set, bufferBindings] : dc.cullBindings)
		{
			if (bufferBindings.shaderStorage.size() > 1)
				__debugbreak(); // increasing this means the shader needs the same constants as the graphics pipeline
			for (auto& bufferBinding : bufferBindings.shaderStorage)
			{
				cullComponentOffsetBuffer[i] = uint32_t(bufferBinding.buffer->getMemRange().offset);
			}
		}
	}
	mNumInstances = drawCalls.size();
}

bool DrawCallBufferEqual::operator()(const EntityResourceSet::VertexAttributeBinding& lhs, const EntityResourceSet::VertexAttributeBinding& rhs) const
{
	return lhs.bindingNr == rhs.bindingNr && lhs.buffer == rhs.buffer;
}

bool DrawCallBufferEqual::operator()(const DrawCall::ResourceSetBinding& lhs, const DrawCall::ResourceSetBinding& rhs) const
{
	if (!std::equal(lhs.uniform.begin(), lhs.uniform.end(), rhs.uniform.begin(), rhs.uniform.end(), *this))
		return false;
	if (!std::equal(lhs.shaderStorage.begin(), lhs.shaderStorage.end(), rhs.shaderStorage.begin(), rhs.shaderStorage.end(), *this))
		return false;
	if (!std::equal(lhs.image.begin(), lhs.image.end(), rhs.image.begin(), rhs.image.end()))
		return false;
	if (!std::equal(lhs.vertexBuffer.begin(), lhs.vertexBuffer.end(), rhs.vertexBuffer.begin(), rhs.vertexBuffer.end(), *this))
		return false;
	return true;
}

bool DrawCallBufferEqual::operator()(const std::tuple<const vulkanutils::ResourceSet*, DrawCall::ResourceSetBinding>& lhs, std::tuple<const vulkanutils::ResourceSet*, DrawCall::ResourceSetBinding> rhs) const
{
	return std::get<0>(lhs)->getDescriptorSetBindpoint() == std::get<0>(rhs)->getDescriptorSetBindpoint() &&
		operator()(std::get<1>(lhs), std::get<1>(rhs));
}

bool DrawCallBufferEqual::operator()(const DrawCall& lhs, const DrawCall& rhs)
{
	if (lhs.pipelineEntity != rhs.pipelineEntity)
		return false;
	if (lhs.indexBuffer != rhs.indexBuffer)
		return false;
	if (!std::equal(lhs.bindings.begin(), lhs.bindings.end(),
		rhs.bindings.begin(), rhs.bindings.end(), *this))
		return false;
	if (!std::equal(lhs.cullBindings.begin(), lhs.cullBindings.end(),
		rhs.cullBindings.begin(), rhs.cullBindings.end(), *this))
		return false;
	return true;
}


template<>
bool DrawCallBufferEqual::operator()(const EntityResourceSet::BufferBinding<vulkanutils::ShaderStorageBuffer>& lhs, const EntityResourceSet::BufferBinding<vulkanutils::ShaderStorageBuffer>& rhs) const
{
	return lhs.bindingNr == rhs.bindingNr && *lhs.buffer->getBuffer() == *rhs.buffer->getBuffer();
}

bool DrawCallDescriptorSetOrder::operator()(const DrawCall::ResourceSetBinding& lhs, const DrawCall::ResourceSetBinding& rhs) const
{
	return std::make_tuple(lhs.uniform, lhs.shaderStorage, lhs.image, lhs.vertexBuffer) <
		std::make_tuple(rhs.uniform, rhs.shaderStorage, rhs.image, rhs.vertexBuffer);
}

bool DrawCallDescriptorSetOrder::operator()(const std::tuple<const vulkanutils::ResourceSet*, DrawCall::ResourceSetBinding>& lhs, std::tuple<const vulkanutils::ResourceSet*, DrawCall::ResourceSetBinding> rhs) const
{
	if (std::get<0>(lhs)->getDescriptorSetBindpoint() != std::get<0>(rhs)->getDescriptorSetBindpoint())
		return std::get<0>(lhs)->getDescriptorSetBindpoint() < std::get<0>(rhs)->getDescriptorSetBindpoint();
	return operator()(std::get<1>(lhs), std::get<1>(rhs));
}

bool DrawCallDescriptorSetOrder::operator()(const DrawCall& lhs, const DrawCall& rhs) const
{
	if (lhs.pipelineEntity != rhs.pipelineEntity)
		return lhs.pipelineEntity < rhs.pipelineEntity;
	if (std::lexicographical_compare(lhs.bindings.begin(), lhs.bindings.end(),
		rhs.bindings.begin(), rhs.bindings.end(), *this))
		return true;
	if (std::lexicographical_compare(rhs.bindings.begin(), rhs.bindings.end(),
		lhs.bindings.begin(), lhs.bindings.end(), *this))
		return false;

	if (std::lexicographical_compare(lhs.cullBindings.begin(), lhs.cullBindings.end(),
		rhs.cullBindings.begin(), rhs.cullBindings.end(), *this))
		return true;
	if (std::lexicographical_compare(rhs.cullBindings.begin(), rhs.cullBindings.end(),
		lhs.cullBindings.begin(), lhs.cullBindings.end(), *this))
		return false;

	if (lhs.indexBuffer != rhs.indexBuffer)
		return lhs.indexBuffer < rhs.indexBuffer;
	return false;
}
