#include "model.h"


ModelSet::ModelSet(vulkanutils::Device* device, SceneType scene, vulkanutils::DescriptorSetSlot descSetbp, bool withSkeleton)
	: EntityResourceSet(device, "model", descSetbp, vulkanutils::VertexInputSlot::NONE)
{
	setDescriptorBinding<vulkanutils::ShaderStorageBuffer, component::Transform>(scene, 
		{ VK_SHADER_STAGE_VERTEX_BIT, 0 });
	if (withSkeleton)
	{
		setDescriptorBinding<vulkanutils::ShaderStorageBuffer, skeleton::Bone>(scene, 
			{ VK_SHADER_STAGE_VERTEX_BIT, 1 });
	}
	getDescriptorLayout().build();
}
