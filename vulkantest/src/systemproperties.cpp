#include "systemproperties.h"
#include "entity.h"


EntityResourceSet systemPropertiesSet(vulkanutils::Device* device, SystemPropertiesSetSceneType scene, vulkanutils::DescriptorSetSlot descSetbp)
{
	EntityResourceSet ret(device, "system", descSetbp, vulkanutils::VertexInputSlot::NONE);
	ret.setDescriptorBinding<vulkanutils::UniformBuffer, component::SystemProperies>(scene, 
		{ VK_SHADER_STAGE_VERTEX_BIT | VK_SHADER_STAGE_FRAGMENT_BIT | VK_SHADER_STAGE_TESSELLATION_EVALUATION_BIT, 0 });
	ret.getDescriptorLayout().build();
	return ret;
}
