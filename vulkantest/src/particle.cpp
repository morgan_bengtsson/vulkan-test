#include "particle.h"

ParticleModelSet::ParticleModelSet(vulkanutils::Device* device, SceneType scene, vulkanutils::DescriptorSetSlot descSetbp, vulkanutils::VertexInputSlot vertInputBp)
	: EntityResourceSet(device, "particleModel", descSetbp, vertInputBp)
{
	setDescriptorBinding<vulkanutils::UniformBuffer, component::Transform>(scene,
		{ VK_SHADER_STAGE_VERTEX_BIT, 0 });
	setDescriptorBinding<vulkanutils::UniformBuffer, ParticleProperties>(scene,
		{ VK_SHADER_STAGE_VERTEX_BIT, 1 });
	setDescriptorBinding<vulkanutils::ShaderStorageBuffer, component::Position>(scene,
		{ VK_SHADER_STAGE_VERTEX_BIT, 2 });
	getDescriptorLayout().build();
}


ParticleMeshSet::ParticleMeshSet(vulkanutils::Device* device
	, SceneType scene, vulkanutils::DescriptorSetSlot descSetbp, vulkanutils::VertexInputSlot vertInputBp)
	: EntityResourceSet(device, "particleMesh", descSetbp, vertInputBp)
{
	setPerVertexAttributeBinding<component::MeshVertex>(scene, 0, VK_FORMAT_R32G32B32_SFLOAT);
	setPerVertexAttributeBinding<component::MeshNormal>(scene, 1, VK_FORMAT_R32G32B32_SFLOAT);
	addComponetTypes<component::MeshVertex, component::MeshNormal, component::MeshIndex>(scene);
	setHasIndexBinding();
}
