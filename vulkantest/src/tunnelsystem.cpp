#include "tunnelsystem.h"

#include <math.h>

#include <tetgen.h>

TunnelSystem::TunnelSystem(vulkanutils::VulkanContext& context, SceneType scene)
	: ecs::System<TunnelSystem>(context)
{

}


struct TetGenData
{
	tetgenio tetGenIO;
	std::vector<double> points;
	std::vector<double> pointAttributes;
	std::vector<tetgenio::facet> faces;
	std::vector<tetgenio::polygon> polygons;
	std::vector<int> vertexIndices;
};


TetGenData createTetGenInput(const component::Surface<float>& surface)
{
	const component::DiscreteSurface<float>& dsurface = surface.getDiscrete();
	auto [minHeight, maxHeight] = std::minmax(dsurface.data().begin(), dsurface.data().end());


	TetGenData tgend = {};
	size_t nPoints = dsurface.width() * dsurface.height() + 4;
	tgend.points.resize(nPoints * 3);



}



void TunnelSystem::runImpl(SceneType scene)
{
	auto& cc = scene.getComponentCollection();
	auto& gpuCc = scene.getGpuComponentCollection();

	auto surfaceEntities = scene.getEntities<component::Surface<float>>();
	const component::Surface<float>& surface = surfaceEntities.front()->getComponent<component::Surface<float>>(cc);
	const component::DiscreteSurface<float>& dsurface = surface.getDiscrete();

	auto [minHeight, maxHeight] = std::minmax(dsurface.data().begin(), dsurface.data().end());


	tetgenbehavior behavior = {};
	tetgenio input = {};


}
