#include "tracer.h"

#include <chrono>

TimingStats::TimingStats()
{}

Tracer::Tracer(const char* tag)
	: mStart(std::chrono::system_clock::now())
	, mTag(tag)
{}

Tracer::~Tracer()
{
	stop();
}

void Tracer::stop()
{
	uint64_t endNs = std::chrono::duration_cast<std::chrono::nanoseconds>(std::chrono::system_clock::now().time_since_epoch()).count();
	uint64_t startNs = std::chrono::duration_cast<std::chrono::nanoseconds>(mStart.time_since_epoch()).count();
	TimingTracer::GetInstance().queue(mTag, startNs, endNs);
}


TimingTracer& TimingTracer::GetInstance()
{
	static TimingTracer instance;
	return instance;
}


void TimingStats::queue(const char* tag, uint64_t start, uint64_t end)
{
	Stats& stats = mStats[tag];
	uint64_t duration = end - start;
	if (stats.minNs == 0 || duration != 0 && duration < stats.minNs)
		stats.minNs = duration;
	if (duration > stats.maxNs)
		stats.maxNs = duration;
	stats.avgNs = (stats.num * stats.avgNs + duration) / (stats.num + 1);
	++stats.num;
}


void TimingTracer::queue(const char* tag, uint64_t start, uint64_t end)
{
	std::unique_lock lock(mLock);
	auto& ptr = mStats[std::hash<std::thread::id>{}(std::this_thread::get_id())];
	if (!ptr)
		ptr = std::make_unique<TimingStats>();
	ptr->queue(tag, start, end);
}
