#include "material.h"

EntityResourceSet materialSet(vulkanutils::Device* device, MaterialSetSceneType scene, vulkanutils::DescriptorSetSlot descSetbp, size_t numImages)
{
	EntityResourceSet ret(device, "material", descSetbp, vulkanutils::VertexInputSlot::NONE);
	ret.setDescriptorBinding<vulkanutils::UniformBuffer, component::MaterialProperties>(scene,
		{ VK_SHADER_STAGE_FRAGMENT_BIT, 0 });

	if (numImages > 0)
	{
		ret.setImageDescriptorBinding<component::Raw>(scene,
			{ VK_SHADER_STAGE_FRAGMENT_BIT, 1, (uint32_t) numImages });
	}
	ret.getDescriptorLayout().build();
	return ret;
}
