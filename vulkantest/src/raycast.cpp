#include "raycast.h"


#include <entity.h>


#include <Eigen/Dense>

#include <span>


namespace raycast
{

bool intersects(const component::MeshAABoundingBox& bbox, const Eigen::ParametrizedLine<float, 3> line)
{
	Eigen::Vector3f tminV = (bbox.aabb.min() - line.origin()).array() / line.direction().array();
	Eigen::Vector3f tmaxV = (bbox.aabb.max() - line.origin()).array() / line.direction().array();

	if (tminV.x() > tmaxV.x()) std::swap(tminV.x(), tmaxV.x()); 
	if (tminV.y() > tmaxV.y()) std::swap(tminV.y(), tmaxV.y()); 
	if (tminV.z() > tmaxV.z()) std::swap(tminV.z(), tmaxV.z()); 

	float tmin = tminV.x();
	float tmax = tmaxV.x();

	if (tmin > tmaxV.y() || tminV.y() > tmax)
		return false;
 
	if (tminV.y() > tmin)
		tmin = tminV.y();
 
	if (tmaxV.y() < tmax)
		tmax = tmaxV.y();
 
	if ((tmin > tmaxV.z()) || (tminV.z() > tmax)) 
		return false;
 
	if (tminV.z() > tmin)
		tmin = tminV.z();
 
	if (tmaxV.z() < tmax)
		tmax = tmaxV.z();

	return true;
}


float intersection(const std::span<const component::MeshIndex> faces, const component::MeshVertex* vertices, const Eigen::ParametrizedLine<float, 3> line)
{
	float minDist = std::numeric_limits<float>::max();
	for (size_t i = 0; i < faces.size(); i += 3)
	{
		Eigen::Map<const Eigen::Vector3f> v0(vertices[faces[i].idx].point);
		Eigen::Map<const Eigen::Vector3f> v1(vertices[faces[i + 1].idx].point);
		Eigen::Map<const Eigen::Vector3f> v2(vertices[faces[i + 2].idx].point);

		Eigen::Vector3f e0 = v1 - v0, e1 = v2 - v1, e2 = v0 - v2;
		Eigen::Vector3f n = e0.cross(e1);
		float d = v0.dot(n);

		float div = line.direction().dot(n);
		if (std::abs(div) < 0.00001)
			continue;

		float t = (d - line.origin().dot(n)) / div;
		if (t <= 0 || t >= minDist)
			continue;
		Eigen::Vector3f p = line.pointAt(t);

		if (e0.cross(p - v0).dot(n) >= 0 &&
			e1.cross(p - v1).dot(n) >= 0 &&
			e2.cross(p - v2).dot(n) >= 0)
		{
			minDist = t;
		}
	}
	return minDist;
}


bool intersection(const RaycastSceneType& scene, const Eigen::ParametrizedLine<float, 3>& ray, component::RayEntityIntersection* intersectionRes)
{
	auto& cc = scene.getComponentCollection();
	auto& gpuCc = scene.getGpuComponentCollection();

	component::RayEntityIntersection res = {};
	res.distance = std::numeric_limits<float>::max();
	auto bbEntities = scene.getEntities<component::MeshAABoundingBox, component::Position>();
	for (auto& entity : bbEntities)
	{
		const component::MeshAABoundingBox& bbox = entity->getComponent<component::MeshAABoundingBox>(cc);
		auto meshEntities = scene.getRefEntities<component::MeshVertex, component::MeshIndex>(entity, ecs::EntityRef::Type::Child);

		if (intersects(bbox, ray))
		{
			for (const ecs::Entity* meshEntity : meshEntities)
			{
				std::span<const component::MeshVertex> meshVertices = meshEntity->getComponentArray<component::MeshVertex>(cc);
				std::span<const component::MeshIndex> meshIndices = meshEntity->getComponentArray<component::MeshIndex>(cc);

				std::span<const component::Position> positions = entity->getComponentArray<component::Position>(cc);
				const component::Position& position = (!positions.empty()) ? positions[0] : Eigen::Vector3f::Zero();
				std::span<const component::Orientation> orientations = entity->getComponentArray<component::Orientation>(cc);
				const component::Orientation& orientation = (!orientations.empty()) ? orientations[0] : Eigen::Matrix3f::Identity();
				std::span<const component::Scale> scales = entity->getComponentArray<component::Scale>(cc);
				const component::Scale& scale = (!scales.empty()) ? scales[0] : Eigen::Vector3f::Ones();
				Eigen::Affine3f transform; transform.fromPositionOrientationScale(position, orientation, scale);

				if (float intDist = intersection(meshIndices, meshVertices.data(), Eigen::ParametrizedLine<float, 3>(ray).transform(transform.inverse()))
					; intDist < res.distance)
				{
					res.distance = intDist;
					res.entity = entity->getId();
					res.position = ray.pointAt(intDist);
					break;
				}
			}
		}
	}
	if (res.distance < std::numeric_limits<float>::max())
	{
		*intersectionRes = res;
		return true;
	}
	return false;
}

Eigen::ParametrizedLine<float, 3> getCameraRay(const Eigen::Vector2f& normScreenPos, const Eigen::Vector3f& camPos, const Eigen::Matrix4f& invCamTransform)
{
	Eigen::Vector4f virtMousePos = invCamTransform * Eigen::Vector4f(normScreenPos.x(), normScreenPos.y(), 0.0f, 1.0f);
	Eigen::Vector3f mousePos3d = virtMousePos.head<3>() / virtMousePos.w();
	Eigen::ParametrizedLine<float, 3> ray(camPos, (mousePos3d - camPos).normalized());
	return ray;
}


};