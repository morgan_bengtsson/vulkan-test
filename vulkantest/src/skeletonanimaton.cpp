#include "skeletonanimaton.h"

namespace skeleton
{

void Animation::BoneAnimationSequence::getBoneTransform(float timePoint, SkeletonPose::BoneTransform& transform) const
{
	const auto& sequence = *this;
	size_t seqLength = sequence.size();
	if (seqLength == 1)
	{
		transform = sequence[0].transform;
		return;
	}

	
	timePoint = std::fmodf(timePoint - sequence.front().time, sequence.back().time - sequence.front().time);
	auto it = std::lower_bound(std::begin(sequence), std::end(sequence), timePoint, [](const BoneAnimationKeyFrame& kf, float tp)
	{
		return kf.time < tp;
	});
	size_t idx = it - std::begin(sequence);

	if (idx >= seqLength - 1) idx = seqLength - 2;
	float t1 = sequence[idx].time;
	float t2 = sequence[idx + 1].time;
	float factor = (timePoint - t1) / (t2 - t1);
	transform.setInterpolate(sequence[idx].transform, sequence[idx + 1].transform, factor);

}

Animation::Animation(const char* name, size_t numBones)
	:mName(name)
	, mBoneSequences(numBones)
{
}

const std::vector<Animation::BoneAnimationSequence>& Animation::getBoneSequences() const
{
	return mBoneSequences;
}

std::vector<SkeletonPose::BoneTransform> Animation::getSkeletonPose(float timepoint) const
{
	const size_t nSequences = mBoneSequences.size();
	std::vector<SkeletonPose::BoneTransform> ret;
	ret.resize(nSequences);
	for (size_t i = 0; i < nSequences; i++)
	{
		mBoneSequences[i].getBoneTransform(timepoint, ret[i]);
	}
	return ret;
}

const char* Animation::getName() const
{
	return mName.c_str();
}

void Animation::setBoneSequence(size_t boneIdx, const BoneAnimationSequence& sequence)
{
	mBoneSequences[boneIdx] = sequence;
}

}
