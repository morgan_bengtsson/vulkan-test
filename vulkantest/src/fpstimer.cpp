#include "fpstimer.h"


FpsTimer::FpsTimer()
	: mTargetFps(0)
{}

void FpsTimer::setTargetFps(float fps)
{
	mTargetFps = fps;
}

float FpsTimer::getTargetFps() const
{
	return mTargetFps;
}

float FpsTimer::getFps() const
{
	return mFps;
}

void FpsTimer::tick()
{
	std::chrono::time_point now = std::chrono::system_clock::now();
	if (mTick)
	{
		size_t duration = std::chrono::duration_cast<std::chrono::milliseconds>(now - mLastTime).count();
		mFps = 1000.0f / duration;
	}
	else
	{
		mStartTime = now;
		mFps = mTargetFps;
	}

	mLastTime = now;
	mTick++;
}

void FpsTimer::tickWait()
{
	tick();
	if (mFps >= mTargetFps)
	{
		float sleepTime = 1.0f / mFps - 1.0f / mTargetFps;
		//std::this_thread::sleep_for(std::chrono::microseconds(int(sleepTime * 1000000.0f)));
	}
}


std::chrono::milliseconds FpsTimer::duration() const
{
	std::chrono::time_point now = std::chrono::system_clock::now();
	return std::chrono::duration_cast<std::chrono::milliseconds>(now - mLastTime);
}