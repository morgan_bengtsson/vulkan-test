#include "resourcesetbinding.h"

#include "entity.h"

EntityResourceSet::EntityResourceSet(vulkanutils::Device* device, const char* name
	, vulkanutils::DescriptorSetSlot descSetbp
	, vulkanutils::VertexInputSlot vertInputBp
)
	: vulkanutils::ResourceSet(device, descSetbp, vertInputBp)
	, mName(name)
	, mRequiredComponentTypeIds(0)
	, mRequiredImageComponentTypeIds()
	, mHasIndexBinding(false)
{}

const char* EntityResourceSet::getName() const
{
	return mName.c_str();
}

size_t EntityResourceSet::getRequiredCompTypeIds() const
{
	return mRequiredComponentTypeIds;
}

size_t EntityResourceSet::getRequiredImageCompTypeIds() const
{
	size_t requiredImageComponentTypeIds = 0;
	for (size_t compType : mRequiredImageComponentTypeIds)
		requiredImageComponentTypeIds = (requiredImageComponentTypeIds | compType);
	return requiredImageComponentTypeIds;
}

bool EntityResourceSet::compatible(const ecs::Entity& entity) const
{
	size_t requiredComponents = getRequiredCompTypeIds();
	if (requiredComponents != 0)
		return entity.hasComponentIds(requiredComponents);
	return true;
}

bool EntityResourceSet::compatibleImage(const ecs::Entity& entity) const
{
	size_t requiredComponents = getRequiredImageCompTypeIds();
	if (requiredComponents != 0)
		return entity.hasImageComponentIds(requiredComponents);
	return true;

}
/*
void EntityResourceSet::bindImages(vulkanutils::DescriptorSet& descriptorSet, std::span<ImageBinding> imageBindings) const
{
	for (ImageBinding& imageBinding : imageBindings)
	{
		descriptorSet.bindImage(*imageBinding.imageView,
			*imageBinding.imageSampler, imageBinding.bindingNr, imageBinding.elementNr);
	}
}

void EntityResourceSet::bindVertexBuffer(VkCommandBuffer commandBuffer, std::span<VertexAttributeBinding> bufferBindings) const
{
	for (VertexAttributeBinding& bb : bufferBindings)
	{
		VkBuffer buffer = *bb.buffer->getBuffer();
		VkDeviceSize offset = bb.buffer->getMemRange().offset;
		vkCmdBindVertexBuffers(commandBuffer, uint32_t(bb.bindingNr), 1, &buffer, &offset);
	}
}
*/
size_t EntityResourceSet::bindIndexBuffer(VkCommandBuffer commandBuffer, const vulkanutils::BufferAllocation<vulkanutils::IndexBuffer>& indexBuffer) const
{
	VkBuffer buffer = *indexBuffer.getBuffer();
	VkDeviceSize offset = indexBuffer.getMemRange().offset;
	vkCmdBindIndexBuffer(commandBuffer, buffer, offset, VK_INDEX_TYPE_UINT32);
	return indexBuffer.getMemRange().size / sizeof(uint32_t);
}

std::vector<size_t>& EntityResourceSet::getImageComponentTypeIds()
{
	return mRequiredImageComponentTypeIds;
}

const std::vector<size_t>& EntityResourceSet::getImageComponentTypeIds() const
{
	return mRequiredImageComponentTypeIds;
}

bool EntityResourceSet::getHasIndexBinding() const
{
	return mHasIndexBinding;
}

void EntityResourceSet::setHasIndexBinding()
{
	mHasIndexBinding = true;
}

uint32_t EntityResourceSet::getNextBindPoint() const
{
	uint32_t binding = (uint32_t) getVertexInputBindpoint();
	if (std::vector<VkVertexInputBindingDescription> prevBindings = mVertexAttributeLayout.getVertexBindingDescriptions();
		!prevBindings.empty())
	{
		binding = prevBindings.back().binding + 1;
	}
	return binding;
}
