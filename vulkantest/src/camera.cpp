#include "camera.h"
#include "entity.h"

#include <vulkanDescriptorSet.h>
#include <vulkanDescriptorLayout.h>
#include <vulkanResourceSet.h>

#include <Eigen/Dense>
#include <iostream>

EntityResourceSet cameraSet(vulkanutils::Device* device, CameraSetSceneType scene, vulkanutils::DescriptorSetSlot descSetbp)
{
	EntityResourceSet ret(device, "camera", descSetbp, vulkanutils::VertexInputSlot::NONE);
	ret.setDescriptorBinding<vulkanutils::UniformBuffer, component::CameraProjection>(scene,
		{
			VK_SHADER_STAGE_VERTEX_BIT | VK_SHADER_STAGE_FRAGMENT_BIT | VK_SHADER_STAGE_TESSELLATION_EVALUATION_BIT | VK_SHADER_STAGE_GEOMETRY_BIT, // Shader flags
			0
		});
	ret.setDescriptorBinding<vulkanutils::UniformBuffer, component::Transform>(scene,
		{
			VK_SHADER_STAGE_VERTEX_BIT | VK_SHADER_STAGE_FRAGMENT_BIT | VK_SHADER_STAGE_TESSELLATION_EVALUATION_BIT | VK_SHADER_STAGE_GEOMETRY_BIT, // Shader flags
			1
		});
	ret.setDescriptorBinding<vulkanutils::UniformBuffer, component::Transform>(scene,
		{
			VK_SHADER_STAGE_VERTEX_BIT | VK_SHADER_STAGE_FRAGMENT_BIT | VK_SHADER_STAGE_TESSELLATION_EVALUATION_BIT | VK_SHADER_STAGE_GEOMETRY_BIT, // Shader flags
			2
		});
	ret.getDescriptorLayout().build();
	ret.addComponetTypes<component::CameraProjection>(scene);
	return ret;
}

void perspectiveTransform(float matrix[16],
	float near, float far,
	float left, float right,
	float up, float down, int width, int height)
{

	matrix[0] = 2.0f * near / (right - left) * float(height) / float(width);
	matrix[1] = 0.0f;
	matrix[2] = 0.0f;
	matrix[3] = 0.0f;
	
	matrix[4] = 0.0f;
	matrix[5] = 2.0f * near / (up - down);
	matrix[6] = 0.0f;
	matrix[7] = 0.0f;
	
	matrix[8] = (right + left) / (right - left) * float(width) / float(height);
	matrix[9] = (up + down) / (up - down);
	matrix[10] = -far / (far - near);
	matrix[11] = -1;

	matrix[12] = 0.0f;
	matrix[13] = 0.0f;
	matrix[14] = -far * near / (far - near);
	matrix[15] = 0.0f;
}

void orthographicTransform(float matrix[16],
	float near, float far,
	float left, float right,
	float up, float down)
{

	matrix[0] = 2.0f / (right - left);
	matrix[1] = 0.0f;
	matrix[2] = 0.0f;
	matrix[3] = 0.0f;
	
	matrix[4] = 0.0f;
	matrix[5] = 2.0f / (up - down);
	matrix[6] = 0.0f;
	matrix[7] = 0.0f;
	
	matrix[8] = 0.0f;
	matrix[9] = 0.0f;
	matrix[10] = -2.0f / (far - near);
	matrix[11] = 0.0f;

	matrix[12] = -(right + left) / (right - left);
	matrix[13] = -(up + down) / (up - down);
	matrix[14] = -near / (far - near);
	matrix[15] = 1.0f;
}
