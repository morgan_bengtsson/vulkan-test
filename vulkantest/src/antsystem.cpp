#include "antsystem.h"
#include "entitybuilder.h"
#include "fileload.h"
#include "material.h"
#include "systemproperties.h"
#include "antpheromoneset.h"
#include "surface.h"
#include "surfaceOp.h"
#include "camera.h"
#include "mesh.h"
#include "model.h"

#include <ranges>
#include <numbers>
#include <chrono>


#include <vulkanContext.h>
#include <vulkanBufferCopier.h>
#include <vulkanBarrier.h>



static EntityResourceSet antComputeSet(vulkanutils::Device* device, const AntSystem::SceneType& scene, vulkanutils::DescriptorSetSlot descSetbp)
{
	EntityResourceSet ret(device, "antCompute", descSetbp, vulkanutils::VertexInputSlot::NONE);
	ret.setDescriptorBinding<vulkanutils::ShaderStorageBuffer, Ant>(scene,
		{	// ant (in)
			VK_SHADER_STAGE_COMPUTE_BIT, 0
		});
	ret.setDescriptorBinding<vulkanutils::ShaderStorageBuffer, AntHill>(scene,
		{	// ant hill (in)
			VK_SHADER_STAGE_COMPUTE_BIT, 1
		});
	ret.setDescriptorBinding<vulkanutils::ShaderStorageBuffer, PheromoneData>(scene,
		{	// pheromones (in)
			VK_SHADER_STAGE_COMPUTE_BIT, 2
		});
	ret.setDescriptorBinding<vulkanutils::ShaderStorageBuffer, Ant>(scene,
		{	// ant (out)
			VK_SHADER_STAGE_COMPUTE_BIT, 3
		});
	ret.setDescriptorBinding<vulkanutils::ShaderStorageBuffer, AntHill>(scene,
		{	// ant hill (out)
			VK_SHADER_STAGE_COMPUTE_BIT, 4
		});
	ret.setDescriptorBinding<vulkanutils::ShaderStorageBuffer, PheromoneData>(scene,
		{	// pheromones (out)
			VK_SHADER_STAGE_COMPUTE_BIT, 5
		});
	ret.setDescriptorBinding<vulkanutils::UniformBuffer, component::SurfaceProperties>(scene,
		{	// pheromone map properties (in)
			VK_SHADER_STAGE_COMPUTE_BIT, 6
		});
	ret.getDescriptorLayout().build();
	return ret;
}

static EntityResourceSet antPheromoneMapComputeSet(vulkanutils::Device* device, const AntSystem::SceneType& scene, vulkanutils::DescriptorSetSlot descSetbp)
{
	EntityResourceSet ret(device, "AntPheromoneMapCompute", descSetbp, vulkanutils::VertexInputSlot::NONE);
	ret.setDescriptorBinding<vulkanutils::ShaderStorageBuffer, PheromoneData>(scene,	// pheromones (in)
		{ VK_SHADER_STAGE_COMPUTE_BIT, 0 });
	ret.setDescriptorBinding<vulkanutils::ShaderStorageBuffer, PheromoneData>(scene,	// pheromones (out)
		{ VK_SHADER_STAGE_COMPUTE_BIT, 1 });
	ret.setDescriptorBinding<vulkanutils::UniformBuffer, component::SurfaceProperties>(scene,	// pheromone map properties (in)
		{ VK_SHADER_STAGE_COMPUTE_BIT, 2 });
	ret.getDescriptorLayout().build();
	return ret;
}

static EntityResourceSet antHeightMapSet(vulkanutils::Device* device, const AntSystem::SceneType& scene, vulkanutils::DescriptorSetSlot descSetbp)
{
	EntityResourceSet ret(device, "antHeightMap", descSetbp, vulkanutils::VertexInputSlot::NONE);
	ret.setDescriptorBinding<vulkanutils::UniformBuffer, component::SurfaceProperties>(scene,
		{ VK_SHADER_STAGE_VERTEX_BIT | VK_SHADER_STAGE_FRAGMENT_BIT, 0 });
	ret.setImageDescriptorBinding<component::Elevation>(scene,
		{ VK_SHADER_STAGE_FRAGMENT_BIT | VK_SHADER_STAGE_VERTEX_BIT, 1 });
	ret.getDescriptorLayout().build();
	return ret;
}

static EntityResourceSet antModelSet(vulkanutils::Device* device, const AntSystem::SceneType& scene, vulkanutils::DescriptorSetSlot descSetbp)
{
	EntityResourceSet ret(device, "model", descSetbp, vulkanutils::VertexInputSlot::NONE);
	ret.setDescriptorBinding<vulkanutils::ShaderStorageBuffer, skeleton::Bone>(scene,
		{ VK_SHADER_STAGE_VERTEX_BIT, 0 });
	ret.getDescriptorLayout().build();
	return ret;
}

static EntityResourceSet antSet(vulkanutils::Device* device, const AntSystem::SceneType& scene, vulkanutils::DescriptorSetSlot descSetbp)
{
	EntityResourceSet ret(device, "antModel", descSetbp, vulkanutils::VertexInputSlot::NONE);
	ret.setDescriptorBinding<vulkanutils::UniformBuffer, Ant>(scene,
		{ VK_SHADER_STAGE_VERTEX_BIT | VK_SHADER_STAGE_FRAGMENT_BIT, 0 });
	ret.getDescriptorLayout().build();
	return ret;
}

AntPheromoneMap::AntPheromoneMap(float xSize, float ySize, float cellSize)
	:component::Surface<PheromoneData>(xSize, ySize, cellSize)
{}

AntSystem::AntSystem(vulkanutils::VulkanContext& context, SceneType scene)
	:System<AntSystem>(context)
	, ComputeSystem(context, "./shaders")
	, mRandomGenerator((unsigned int)std::chrono::system_clock::now().time_since_epoch().count())
	, mDescriptorPool(context.device(), 8)
	, mCommandPool(context.device(), context.device().getComputeQueue(), VK_COMMAND_POOL_CREATE_RESET_COMMAND_BUFFER_BIT)
	, mNumPlayers(1)
	, mNumAntsPerPlayer(100)
	, mAntSpeed(2.0f)
	, mTurnRate(0.2f)
	, mMaxTrailStrength(10.0f)
{

	vulkanutils::Device* device = &getVulkanContext().device();

	auto [antMeshes, antMaterials]
		= loadFile("./models/Ant_anim_fbx/Ant_anim_fbx.fbx", scene.getComponentCollection(), device);
	
	for (auto& entity : antMeshes)
		scene.addEntity(entity);
	for (auto& entity : antMaterials)
		scene.addEntity(entity);
	mAntMeshes = std::move(antMeshes);
	mAntMaterials = std::move(antMaterials);
	
	auto [antHillMeshes, antHillMaterials]
		= loadFile("./models/AntHill.3DS", scene.getComponentCollection(), device);
	for (auto& entity : antHillMeshes)
		scene.addEntity(entity);
	for (auto& entity : antHillMaterials)
		scene.addEntity(entity);
	mAntHillMeshes = std::move(antHillMeshes);
	mAntHillMaterials = std::move(antHillMaterials);

	
	mAntDrawPipelineEntity = ecs::Entity("antPipeline");
	{
		using namespace vulkanutils;

		component::Pipeline& pipeline = mAntDrawPipelineEntity.addComponent<component::Pipeline>(scene.getComponentCollection(), component::Pipeline("ant"));
		pipeline.sets.emplace_back(systemPropertiesSet(device, scene, DescriptorSetSlot::SLOT_1 ));
		pipeline.sets.emplace_back(cameraSet(device, scene, DescriptorSetSlot::SLOT_2 ));
		pipeline.sets.emplace_back(materialSet(device, scene, DescriptorSetSlot::SLOT_4, 1 ));
		pipeline.sets.emplace_back(meshSet(device, scene, VertexInputSlot::SLOT_0, 1, true));
		pipeline.sets.emplace_back(antModelSet(device, scene, DescriptorSetSlot::SLOT_5 ));
		pipeline.sets.emplace_back(LightSet(device, scene, DescriptorSetSlot::SLOT_3 ));
		pipeline.sets.emplace_back(antSet(device, scene, DescriptorSetSlot::SLOT_6 ));
		pipeline.sets.emplace_back(antHeightMapSet(device, scene, DescriptorSetSlot::SLOT_7 ));

		pipeline.addRenderPassAttachment<component::Color>(scene, 0);
		pipeline.addRenderPassAttachment<component::Depth>(scene, 1);

		scene.addEntity(mAntDrawPipelineEntity);
	}
	
	mAntHillDrawPipelineEntity = ecs::Entity("antHillPipeline");
	{
		using namespace vulkanutils;

		component::Pipeline& pipeline = mAntHillDrawPipelineEntity.addComponent<component::Pipeline>(scene.getComponentCollection(), component::Pipeline("antHill"));
		pipeline.sets.emplace_back(systemPropertiesSet(device, scene, DescriptorSetSlot::SLOT_1 ));
		pipeline.sets.emplace_back(cameraSet(device, scene, DescriptorSetSlot::SLOT_2 ));
		pipeline.sets.emplace_back(materialSet(device, scene, DescriptorSetSlot::SLOT_4, 0 ));
		pipeline.sets.emplace_back(meshSet(device, scene, VertexInputSlot::SLOT_0, 0, false));
		pipeline.sets.emplace_back(ModelSet(device, scene, DescriptorSetSlot::SLOT_5, false ));
		pipeline.sets.emplace_back(LightSet(device, scene, DescriptorSetSlot::SLOT_3 ));

		pipeline.addRenderPassAttachment<component::Color>(scene, 0);
		pipeline.addRenderPassAttachment<component::Depth>(scene, 1);

		scene.addEntity(mAntHillDrawPipelineEntity);
	}

	mAntPheromoneDrawPipelineEntity = ecs::Entity("PheromoneMapPipeline");
	{
		using namespace vulkanutils;
		component::Pipeline& pipeline = mAntPheromoneDrawPipelineEntity.addComponent<component::Pipeline>(scene.getComponentCollection(), component::Pipeline("antPheromone"));
		pipeline.sets.emplace_back(systemPropertiesSet(device, scene, DescriptorSetSlot::SLOT_1 ));
		pipeline.sets.emplace_back(cameraSet(device, scene, DescriptorSetSlot::SLOT_2 ));
		pipeline.sets.emplace_back(AntPheromoneSet(device, scene, DescriptorSetSlot::SLOT_4, VertexInputSlot::NONE ));
		pipeline.sets.emplace_back(meshSet(device, scene, VertexInputSlot::SLOT_0, 1, false));
		pipeline.sets.emplace_back(ModelSet(device, scene, DescriptorSetSlot::SLOT_5, false ));
		pipeline.sets.emplace_back(LightSet(device, scene, DescriptorSetSlot::SLOT_3 ));

		pipeline.addRenderPassAttachment<component::Color>(scene, 0);
		pipeline.addRenderPassAttachment<component::Depth>(scene, 1);

		scene.addEntity(mAntPheromoneDrawPipelineEntity);
	}

	{
		std::vector<EntityResourceSet> sets;
		sets.emplace_back(antComputeSet(&context.device(), scene, vulkanutils::DescriptorSetSlot::SLOT_0));
		mAntComputePipeline = std::move(createComputePipeline("ant", std::move(sets)));
	}

	{
		std::vector<EntityResourceSet> sets;
		sets.emplace_back(antPheromoneMapComputeSet(&context.device(), scene, vulkanutils::DescriptorSetSlot::SLOT_0));
		mAntPheromoneComputePipeline = std::move(createComputePipeline("antPheromone", std::move(sets)));
	}

}


void AntSystem::runImpl(AntSystem::SceneType scene)
{
	if (scene.getEntities<AntWorld>().empty())
		setup(scene);
	else
		update(scene);
}

void AntSystem::update(AntSystem::SceneType scene)
{
	std::vector<ecs::Entity*> antWorldEntities = scene.getEntities<AntWorld>();

	// Execute all command buffers (one per world)
	std::vector<VkCommandBuffer> commandBuffers;
	for (ecs::Entity* antWorldEntity : antWorldEntities)
		commandBuffers.push_back(mAntWorldCommandBuffers[antWorldEntity->getId()][0]);
	getVulkanContext().compute(commandBuffers);
}

ecs::Entity AntSystem::createAntWorld(AntSystem::SceneType scene)
{	
	auto& cc = scene.getComponentCollection();
	auto& gpuCc = scene.getGpuComponentCollection();

	ecs::Entity antWorldEntity("antWorld");

	ecs::Entity* terrainEntity = scene.getEntities<component::Surface<float>>()[0];
	// Add pheromone map
	ecs::Entity pheromoneMapEntity = createPheromoneMap(scene, *terrainEntity);
	AntWorld& antWorld = antWorldEntity.addComponent<AntWorld>(scene.getComponentCollection());
	antWorld.terrainEntityId = terrainEntity->getId();
	antWorld.pheromoneEntityId = pheromoneMapEntity.getId();
	scene.addEntity(pheromoneMapEntity);

	// Add ant hills
	for (size_t i = 0; i < mNumPlayers; i++)
	{
		terrainEntity = scene.getEntities<component::Surface<float>>()[0];
		ecs::Entity antHillEntity = createAntHill(scene, *terrainEntity);
		antWorld.antHillEntityIds.push_back(antHillEntity.getId());
		scene.addEntity(antHillEntity);
	}
	mAntWorldCommandBuffers[antWorldEntity.getId()] = vulkanutils::CommandBuffers(getVulkanContext().device(), mCommandPool, 1);

	return antWorldEntity;
}

ecs::Entity AntSystem::createPheromoneMap(AntSystem::SceneType scene, ecs::Entity& terrainEntity)
{
	auto& cc = scene.getComponentCollection();
	auto& gpuCc = scene.getGpuComponentCollection();

	ecs::Entity pheromoneMapEntity("pheromoneMap");

	vulkanutils::CommandPool tempCommandPool(getVulkanContext().device(), getVulkanContext().device().getGraphicsQueue(), VK_COMMAND_POOL_CREATE_RESET_COMMAND_BUFFER_BIT);

	// ---------One-time uploads--------------
	auto commandBuffers = vulkanutils::CommandBuffers(getVulkanContext().device(), tempCommandPool, 1);
	{
		vulkanutils::CommandBufferRecorder cbRecorder(commandBuffers[0], VK_COMMAND_BUFFER_USAGE_ONE_TIME_SUBMIT_BIT);

		// upload terrain if not already done
		component::Surface<float> terrain = terrainEntity.getComponent<component::Surface<float>>(cc);
		if (!terrainEntity.hasGpuComponents<vulkanutils::UniformBuffer, component::SurfaceProperties>(gpuCc))
		{
			component::SurfaceProperties props = terrain.getProperties();
			auto surfacePropHost = terrainEntity.addHostGpuComponentArray<component::SurfaceProperties>(gpuCc, 1);
			auto& surfacePropDevice = terrainEntity.addGpuComponentArray<vulkanutils::UniformBuffer, component::SurfaceProperties>(gpuCc, 1);

			component::MaterialTextureView* textureViews = terrainEntity.addComponentArray<component::MaterialTextureView>(scene.getComponentCollection(), 1);
			auto terrainElevationImageDataHost = terrainEntity.addHostGpuComponentArray<component::Elevation>(scene.getGpuComponentCollection(), props.width * props.height);
			auto& terrainElevationImageDataDevice = terrainEntity.addImage<component::Elevation>(scene.getGpuComponentCollection()
				, props.width, props.height, VK_FORMAT_R32_SFLOAT, 1, VK_IMAGE_USAGE_TRANSFER_SRC_BIT | VK_IMAGE_USAGE_TRANSFER_DST_BIT | VK_IMAGE_USAGE_SAMPLED_BIT);

			surfacePropHost.getData()[0] = props;
			for (size_t i = 0; i < props.height * props.height; i++)
				terrainElevationImageDataHost.getData()[i].elevation = terrain.getDiscrete().data()[i];

			vulkanutils::BufferCopier copier(commandBuffers[0], 2);
			copier.queue(surfacePropHost, surfacePropDevice);
			copier.queue(terrainElevationImageDataHost, *terrainElevationImageDataDevice.getImage());

			VkSamplerAddressMode samplerAddressModes[] = { VK_SAMPLER_ADDRESS_MODE_CLAMP_TO_EDGE,
				VK_SAMPLER_ADDRESS_MODE_CLAMP_TO_EDGE,
				VK_SAMPLER_ADDRESS_MODE_CLAMP_TO_EDGE };

			textureViews[0].componentType = scene.getGpuComponentCollection().getComponentTypeIndex<component::Elevation>();
			textureViews[0].imageSampler = std::move(vulkanutils::ImageSampler(
				getVulkanContext().device()
				, VK_FILTER_LINEAR, VK_FILTER_LINEAR
				, samplerAddressModes, 0.0f, VK_BORDER_COLOR_FLOAT_TRANSPARENT_BLACK ));
			textureViews[0].imageView = vulkanutils::ImageView(
				getVulkanContext().device()
				, *terrainElevationImageDataDevice.getImage()
				, VK_IMAGE_ASPECT_COLOR_BIT);
		}

		// Create entities for meshes, materials and pheromone map
		ecs::Entity antPheromoneMaterialEntity("antPhMaterial");
		antPheromoneMaterialEntity.addComponent<component::MaterialProperties>(cc) =
		{
			{0.1f, 0.1f, 0.2f, 1.0f},
			{0.3f, 0.4f, 0.8f, 1.0f},
			{0.0f, 0.0f, 0.0f, 1.0f},
			{0.3f, 0.3f, 0.4f, 20.0f},
		};


		ecs::Entity antPheromoneMeshEntity = createSurfaceMesh(cc, terrain, 5, true, true);
		antPheromoneMeshEntity.setName("PheromoneMapMesh");
		antPheromoneMeshEntity.addComponent<ecs::EntityRef>(cc, { antPheromoneMaterialEntity.getId(), ecs::EntityRef::Type::Child });

		pheromoneMapEntity.addComponents<ecs::EntityRef, component::Position, component::Orientation, ecs::EntityRef, component::Visibility>
			(cc,
				{ antPheromoneMeshEntity.getId(), ecs::EntityRef::Type::Child },
				Eigen::Vector3f(0.0f, 1.0f, 0.0f),
				component::Orientation::Identity(),
				{ mAntPheromoneDrawPipelineEntity.getId(), ecs::EntityRef::Type::Link },
				{ true }  );


		auto pheromoneMapPropHost = pheromoneMapEntity.addHostGpuComponentArray<component::SurfaceProperties>(gpuCc, 1);

		const float cellSize = 2.0f;
		pheromoneMapPropHost.getData()[0] = {size_t(terrain.getXSize() / cellSize), size_t(terrain.getYSize() / cellSize), cellSize};

		auto& pheromoneMapPropDevice = pheromoneMapEntity.addGpuComponentArray<vulkanutils::UniformBuffer, component::SurfaceProperties>(gpuCc, 1);

		size_t pmapSize = pheromoneMapPropHost.getData()[0].height * pheromoneMapPropHost.getData()[0].width;
		auto pheromoneMapHost = pheromoneMapEntity.addHostGpuComponentArray<PheromoneData>(gpuCc, pmapSize); // input
		auto pheromoneMapDevice = pheromoneMapEntity.addGpuComponentArrays<vulkanutils::ShaderStorageBuffer, PheromoneData>(gpuCc,
			{ 
				pmapSize, pmapSize
			}); // output

		std::uniform_real_distribution<float> distribution(0.0f, 1.0f);
		for (int i = 0; i < 100; i++)
		{
			pheromoneMapHost.getData()[distribution(mRandomGenerator) * pheromoneMapHost.getData().size()].foodStore = 100.0f;
		}

		vulkanutils::BufferCopier copier(commandBuffers[0], 2);
		copier.queue(pheromoneMapHost, *pheromoneMapDevice[0]);
		copier.queue(pheromoneMapPropHost, pheromoneMapPropDevice);

		mAntPheromoneComputePipeline.createDescriptorSets(pheromoneMapEntity.getId(), mDescriptorPool);

		scene.addEntity(antPheromoneMaterialEntity);
		scene.addEntity(antPheromoneMeshEntity);
	}

	getVulkanContext().device().getGraphicsQueue().submit( { commandBuffers[0] }, nullptr );
	//getVulkanContext().compute({ commandBuffers[0] });
	getVulkanContext().device().getGraphicsQueue().wait();
	// -----------------------------------------


	return pheromoneMapEntity;
}


void AntSystem::recordAntWorld(SceneType scene, ecs::Entity& antWorldEntity)
{
	// Record all commands to run on the GPU (including copying up/down)
	auto& cc = scene.getComponentCollection();
	auto& gpuCc = scene.getGpuComponentCollection();

	AntWorld& antWorld = antWorldEntity.getComponent<AntWorld>(cc);

	std::vector<ecs::Entity*> antHillEntities;
	for (size_t id : antWorld.antHillEntityIds)
		antHillEntities.push_back(scene.getEntity(id));
	ecs::Entity* pheromoneMapEntity = scene.getEntity(antWorld.pheromoneEntityId);
	auto pheromoneMapDevice = pheromoneMapEntity->getGpuComponentArrays<vulkanutils::ShaderStorageBuffer, PheromoneData>(gpuCc);

	auto& commandBuffer = mAntWorldCommandBuffers[antWorldEntity.getId()][0];
	vulkanutils::CommandBufferRecorder cbRecorder(commandBuffer, VK_COMMAND_BUFFER_USAGE_SIMULTANEOUS_USE_BIT);
	// Upload ants and ant hills

	{
		std::vector<ecs::Entity*> antEntities = scene.getGpuEntities<vulkanutils::UniformBuffer, Ant>();
		vulkanutils::BufferCopier copier(commandBuffer, antEntities.size());
		ecs::Entity* prevAntHillEntity = nullptr;
		for (size_t antIdx = 0, antSubIdx; antIdx < antEntities.size(); antIdx++, antSubIdx++)
		{
			ecs::Entity& antEntity = *antEntities[antIdx];
			auto antHillEntity = scene.getRefEntities(&antEntity, ecs::EntityRef::Type::Parent)[0];

			if (prevAntHillEntity == nullptr || prevAntHillEntity->getId() != antHillEntity->getId())
			{
				prevAntHillEntity = antHillEntity;
				antSubIdx = 0;
			}
			auto antBufferDevices = antHillEntity->getGpuComponentArrays<vulkanutils::ShaderStorageBuffer, Ant>(scene.getGpuComponentCollection());
			auto& antDataDevice   = antEntity.getGpuComponentArray<vulkanutils::UniformBuffer, Ant>(scene.getGpuComponentCollection());
			copier.queue(
				*antDataDevice.getBuffer(), antDataDevice.getMemRange()
				, *antBufferDevices[0]->getBuffer(), vulkanutils::MemRange(antBufferDevices[0]->getMemRange().offset + antSubIdx * sizeof(Ant), sizeof(Ant)));
		}
	}

	{
		vulkanutils::Barrier barrier(commandBuffer);
		barrier.dependsOnCopy().doBeforeCompute();
		for (ecs::Entity* antHillEntity : antHillEntities)
		{
			auto antBufferDevice = antHillEntity->getGpuComponentArrays<vulkanutils::ShaderStorageBuffer, Ant>(gpuCc);
			auto antHillBufferDevice = antHillEntity->getGpuComponentArrays<vulkanutils::ShaderStorageBuffer, AntHill>(gpuCc);
			barrier.addBuffer(*antBufferDevice[0]);
			barrier.addBuffer(*antHillBufferDevice[0]);
		}
		barrier.addBuffer(*pheromoneMapDevice[0]);
	}

	// Pheromone map compute shader
	auto& descriptorSets = mAntPheromoneComputePipeline.getDescriptorSets(pheromoneMapEntity->getId());
	mAntPheromoneComputePipeline.getComputeSets()[0].bindEntityToDescriptors(scene, descriptorSets[0], *pheromoneMapEntity);

	auto pheromoneMapProperties = pheromoneMapEntity->getHostGpuComponentArray<component::SurfaceProperties>(gpuCc).getData()[0];
	mAntPheromoneComputePipeline.execute(
		commandBuffer
		, pheromoneMapEntity->getId()
		, { pheromoneMapProperties.width, pheromoneMapProperties.height, 1 }
	);

	vulkanutils::Barrier(commandBuffer).dependsOnCompute().doBeforeCopy().addBuffer(*pheromoneMapDevice[1]);
	vulkanutils::BufferCopier(commandBuffer).queue(*pheromoneMapDevice[1], *pheromoneMapDevice[0]);
	vulkanutils::Barrier(commandBuffer).dependsOnCopy().doBeforeCompute().addBuffer(*pheromoneMapDevice[0]);

	// run compute shader, once per ant hill
	{
		vulkanutils::Barrier copyBackBarrier(commandBuffer);
		copyBackBarrier.dependsOnCompute().doBeforeCopy();
		for (ecs::Entity* antHillEntity : antHillEntities)
		{
			auto antBufferDevice		= antHillEntity->getGpuComponentArrays<vulkanutils::ShaderStorageBuffer, Ant>(gpuCc);
			auto antHillBufferDevice	= antHillEntity->getGpuComponentArrays<vulkanutils::ShaderStorageBuffer, AntHill>(gpuCc);
			
			auto& descriptorSets = mAntComputePipeline.createDescriptorSets(antHillEntity->getId(), mDescriptorPool);
			mAntComputePipeline.getComputeSets()[0].bindEntitiesToDescriptors(scene, descriptorSets[0], { *antHillEntity, *pheromoneMapEntity });
			mAntComputePipeline.execute(commandBuffer, antHillEntity->getId(), { uint32_t(antBufferDevice[0]->getMemRange().size / sizeof(Ant)), 1, 1 } );

			copyBackBarrier.addBuffer(*antBufferDevice[1]).addBuffer(*antHillBufferDevice[1]);
		}
		copyBackBarrier.addBuffer(*pheromoneMapDevice[1]);
	}

	{
		vulkanutils::BufferCopier(commandBuffer).queue(*pheromoneMapDevice[1], *pheromoneMapDevice[0]);
		for (ecs::Entity* antHillEntity : antHillEntities)
		{
			auto antHillBufferDevice = antHillEntity->getGpuComponentArrays<vulkanutils::ShaderStorageBuffer, AntHill>(gpuCc);
			vulkanutils::BufferCopier(commandBuffer).queue(*antHillBufferDevice[1], *antHillBufferDevice[0]);
		}
	}
	{
		std::vector<ecs::Entity*> antEntities = scene.getGpuEntities<vulkanutils::UniformBuffer, Ant>();
		vulkanutils::BufferCopier copier(commandBuffer, antEntities.size());
		ecs::Entity* prevAntHillEntity = nullptr;
		for (size_t antIdx = 0, antSubIdx; antIdx < antEntities.size(); antIdx++, antSubIdx++)
		{
			ecs::Entity& antEntity = *antEntities[antIdx];
			auto antHillEntity = scene.getRefEntities(&antEntity, ecs::EntityRef::Type::Parent)[0];

			if (prevAntHillEntity == nullptr || prevAntHillEntity->getId() != antHillEntity->getId())
			{
				prevAntHillEntity = antHillEntity;
				antSubIdx = 0;
			}
			auto antBufferDevices = antHillEntity->getGpuComponentArrays<vulkanutils::ShaderStorageBuffer, Ant>(scene.getGpuComponentCollection());
			auto& antDataDevice   = antEntity.getGpuComponentArray<vulkanutils::UniformBuffer, Ant>(scene.getGpuComponentCollection());
			copier.queue(
				*antBufferDevices[1]->getBuffer(), vulkanutils::MemRange(antBufferDevices[1]->getMemRange().offset + antSubIdx * sizeof(Ant), sizeof(Ant)),
				*antDataDevice.getBuffer(), antDataDevice.getMemRange());
		}
	}
}


void AntSystem::setup(AntSystem::SceneType scene)
{
	ecs::Entity antWorldEntity = createAntWorld(scene);
	scene.addEntity(antWorldEntity);
	recordAntWorld(scene, antWorldEntity);
}


ecs::Entity AntSystem::createAntHill(AntSystem::SceneType scene, ecs::Entity& terrainEntity)
{
	auto& cc = scene.getComponentCollection();
	auto& gpuCc = scene.getGpuComponentCollection();

	std::uniform_real_distribution<float> distribution(-0.1f, 0.1f);
	EntityBuilder entityBuilder(scene.getComponentCollection());

	ecs::Entity antHill("Ant hill");
	component::Surface<float>& surface = terrainEntity.getComponent<component::Surface<float>>(cc);
	antHill.addComponent<ecs::EntityRef>(cc, {terrainEntity.getId(), ecs::EntityRef::Type::Link });
	entityBuilder.addMesh(antHill, mAntHillMeshes);
	entityBuilder.addPosition(antHill);
	entityBuilder.addOrientation(antHill);
	antHill.getComponent<component::Orientation>(cc);
	entityBuilder.addDrawPipeline(antHill, { mAntHillDrawPipelineEntity });

	auto antHillBufferHost = antHill.addHostGpuComponentArray<AntHill>(gpuCc, 1);
	auto antHillBufferDevice = antHill.addGpuComponentArrays<vulkanutils::ShaderStorageBuffer, AntHill>(gpuCc, {1, 1});

	AntHill& antHillData = antHillBufferHost.getData()[0];
	antHillData.position[0] = surface.getXSize() * distribution(mRandomGenerator);
	antHillData.position[1] = surface.getYSize() * distribution(mRandomGenerator);
	antHillData.size = 30.0f;
	antHillData.numAnts = mNumAntsPerPlayer;
	component::Scale& scale = antHill.addComponent<component::Scale>(cc);
	scale = Eigen::Vector3f(0.5f, 0.5f, 1.0f);

	auto antBufferDevice = antHill.addGpuComponentArrays<vulkanutils::ShaderStorageBuffer, Ant>(gpuCc, { antHillData.numAnts, antHillData.numAnts} );


	// ---------One-time uploads-------------- (increadibly inefficient, move out)
	auto commandBuffers = vulkanutils::CommandBuffers(getVulkanContext().device(), mCommandPool, 1);
	{
		vulkanutils::CommandBufferRecorder cbRecorder(commandBuffers[0], VK_COMMAND_BUFFER_USAGE_ONE_TIME_SUBMIT_BIT);
		vulkanutils::BufferCopier copier(commandBuffers[0], 1);
		copier.queue(antHillBufferHost, *antHillBufferDevice[0]);
	}
	getVulkanContext().compute({ commandBuffers[0] });
	getVulkanContext().device().getComputeQueue().wait();
	// -----------------------------------------

	std::vector<ecs::Entity> ants;
	for (size_t i = 0; i < antHillData.numAnts; i++)
		ants.push_back(createAnt(scene, antHill, terrainEntity));
	for (auto& ant : ants)
		scene.addEntity(ant);

	return antHill;
}

ecs::Entity AntSystem::createAnt(SceneType scene, ecs::Entity& antHill, ecs::Entity& terrainEntity)
{
	std::uniform_real_distribution<float> distribution(-0.5f, 0.5f);

	EntityBuilder entityBuilder(scene.getComponentCollection());
	ecs::Entity ant("ant");
	ecs::EntityRef* childs = ant.addComponentArray<ecs::EntityRef>(scene.getComponentCollection(), mAntMeshes.size() + 2);
	{
		ant.addComponentArray<component::Visibility>(scene.getComponentCollection(), true);
		
		size_t numSkeletons = 0;
		for (size_t i = 0; i < mAntMeshes.size(); i++)
		{
			const ecs::Entity& meshEntity = mAntMeshes[i];
			childs[i].id = meshEntity.getId();
			childs[i].type = ecs::EntityRef::Type::Child;
			if (meshEntity.hasComponents<skeleton::Skeleton>(scene.getComponentCollection()))
				++numSkeletons;
		}
		if (numSkeletons > 0)
		{
			ant.addComponent<skeleton::ActiveAnimation>(scene.getComponentCollection()).animationIndex = 0;
		}
	}

	childs[mAntMeshes.size() + 0] = ecs::EntityRef {antHill.getId(), ecs::EntityRef::Type::Parent };
	childs[mAntMeshes.size() + 1] = ecs::EntityRef {terrainEntity.getId(), ecs::EntityRef::Type::Link };

	entityBuilder.addPosition(ant);
	entityBuilder.addOrientation(ant);
	entityBuilder.addDrawPipeline(ant, { mAntDrawPipelineEntity });
	ant.addComponent<component::Scale>(scene.getComponentCollection());

	auto antDataHost = ant.addHostGpuComponentArray<Ant>(scene.getGpuComponentCollection(), 1);
	auto& antDataDevice = ant.addGpuComponentArray<vulkanutils::UniformBuffer, Ant>(scene.getGpuComponentCollection(), 1);
	Ant& antData = antDataHost.getData()[0];

	auto antHillDataHost = antHill.getHostGpuComponentArray<AntHill>(scene.getGpuComponentCollection());
	const AntHill& antHillData = antHillDataHost.getData()[0];

	antData.position[0] = antHillData.position[0];
	antData.position[1] = antHillData.position[1];
	antData.size = 0.005f;
	antData.speed = 0.0f;
	antData.directionAngle = distribution(mRandomGenerator) * 2.0f * std::numbers::pi_v<float>;
	antData.maxStamina = 100000.0f;
	antData.stamina = antData.maxStamina * 2.0f * distribution(mRandomGenerator);
	antData.state = Ant::State::REST;
	
	// ---------One-time uploads-------------- (increadibly inefficient, move out)
	auto commandBuffers = vulkanutils::CommandBuffers(getVulkanContext().device(), mCommandPool, 1);
	{
		vulkanutils::CommandBufferRecorder cbRecorder(commandBuffers[0], VK_COMMAND_BUFFER_USAGE_ONE_TIME_SUBMIT_BIT);
		vulkanutils::BufferCopier copier(commandBuffers[0], 1);
		copier.queue(antDataHost, antDataDevice);
	}
	getVulkanContext().compute({ commandBuffers[0] });
	getVulkanContext().device().getComputeQueue().wait();
	// -----------------------------------------

	return ant;
}

