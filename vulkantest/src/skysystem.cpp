#include "skysystem.h"

#include "componenttypes.h"
#include "systemproperties.h"
#include "camera.h"
#include "light.h"

#include <vulkanContext.h>
#include <vulkanCommandPool.h>
#include <vulkanBufferCopier.h>


template<typename SceneType>
EntityResourceSet skyMeshSet(vulkanutils::Device* device, SceneType scene, vulkanutils::DescriptorSetSlot descSetbp)
{
	EntityResourceSet ret(device, "sky", descSetbp, vulkanutils::VertexInputSlot::NONE);
	ret.setHasIndexBinding();
	ret.addComponetTypes<component::MeshIndex>(scene);
	return ret;
}

template<typename SceneType>
EntityResourceSet skyLightSet(vulkanutils::Device* device, SceneType scene, vulkanutils::DescriptorSetSlot descSetbp)
{
	EntityResourceSet ret(device, "skyLight", descSetbp, vulkanutils::VertexInputSlot::NONE);
	ret.setDescriptorBinding<vulkanutils::UniformBuffer, component::Sky>(scene,
		{
			VK_SHADER_STAGE_FRAGMENT_BIT, 0,
		});
	ret.getDescriptorLayout().build();
	return ret;
}


SkySystem::SkySystem(vulkanutils::VulkanContext& context, SceneType scene)
	: ecs::System<SkySystem>(context)
	, mCommandPool(context.device(), context.device().getComputeQueue(), VK_COMMAND_POOL_CREATE_RESET_COMMAND_BUFFER_BIT)
	, mCommandBuffers(context.device(), mCommandPool, 3)
	, mLastSignalledValue(3, 0)
	, mCmdBufferIdx(0)
{
	setupSkyDomeMesh(scene);
}

void SkySystem::setupSkyDomeMesh(SceneType scene)
{
	auto& cc = scene.getComponentCollection();
	auto& gpuCc = scene.getGpuComponentCollection();
	vulkanutils::Device* device = &getVulkanContext().device();

	// Make a square
	mSkyDomeScreenMeshEntity = ecs::Entity("SkyDomeMesh");
	component::MeshIndex* meshIndices = mSkyDomeScreenMeshEntity.addComponentArray<component::MeshIndex>(cc, 6);
	meshIndices[0].idx = 0;
	meshIndices[1].idx = 1;
	meshIndices[2].idx = 2;
	meshIndices[3].idx = 1;
	meshIndices[4].idx = 3;
	meshIndices[5].idx = 2;

	{
		using namespace vulkanutils;
		mSkyDomePipelineEntity = ecs::Entity("SkyDomePipeline");
		component::Pipeline& pipeline = mSkyDomePipelineEntity.addComponent<component::Pipeline>(scene.getComponentCollection(), component::Pipeline("skydome"));
		pipeline.sets.emplace_back(systemPropertiesSet(device, scene, DescriptorSetSlot::SLOT_1 ));
		pipeline.sets.emplace_back(cameraSet(device, scene, DescriptorSetSlot::SLOT_2 ));
		pipeline.sets.emplace_back(skyMeshSet(device, scene, DescriptorSetSlot::NONE ));
		pipeline.sets.emplace_back(skyLightSet(device, scene, DescriptorSetSlot::SLOT_3 ));
		pipeline.addRenderPassAttachment<component::Color>(scene, 0);
		pipeline.addRenderPassAttachment<component::Depth>(scene, 1);
		pipeline.cullMode = VK_CULL_MODE_NONE;
	}
	
	scene.addEntity(mSkyDomeScreenMeshEntity);
	scene.addEntity(mSkyDomePipelineEntity);
}

void SkySystem::runImpl(SceneType scene)
{
	auto& cc = scene.getComponentCollection();
	auto& gpuCc = scene.getGpuComponentCollection();

	auto& commandBuffer = mCommandBuffers[mCmdBufferIdx];

	if (!getVulkanContext().device().getComputeQueue().wait(getVulkanContext().device(), mLastSignalledValue[mCmdBufferIdx], 0))
		return;

	{
		vulkanutils::CommandBufferRecorder cbRecorder(commandBuffer, VK_COMMAND_BUFFER_USAGE_ONE_TIME_SUBMIT_BIT);
		vulkanutils::BufferCopier bufferCopier(commandBuffer);

		std::vector<ecs::Entity> newEntities;
		for (ecs::Entity* entity : scene.getEntities<component::Sky>())
		{
			if (!scene.getRefEntities<component::MeshIndex>(entity, ecs::EntityRef::Type::Child).empty())
				continue;

			ecs::Entity sun("sun");
			entity->addComponents<ecs::EntityRef, ecs::EntityRef, ecs::EntityRef>(cc
				, { mSkyDomeScreenMeshEntity.getId(), ecs::EntityRef::Type::Child }
				, { sun.getId(), ecs::EntityRef::Type::Child }
				, { mSkyDomePipelineEntity.getId(), ecs::EntityRef::Type::Link });
			entity->addHostGpuComponentArray<component::Sky>(gpuCc, 1);
			entity->addGpuComponentArray<vulkanutils::UniformBuffer, component::Sky>(gpuCc, 1);
			sun.addComponent<component::Orientation>(cc, Eigen::Matrix3f::Identity());
			component::DirectionalLight& dl = sun.addComponent<component::DirectionalLight>(cc);
			dl.color[0] = 1.0f; dl.color[1] = 1.0f; dl.color[2] = 1.0f; dl.color[3] = 1.0f;
			dl.shadowBoundingBox[0] = -6000;
			dl.shadowBoundingBox[1] = 6000;
			dl.shadowBoundingBox[2] = -6000;
			dl.shadowBoundingBox[3] = 6000;
			dl.shadowBoundingBox[4] = -6000;
			dl.shadowBoundingBox[5] = 6000;
			for (auto ls : scene.getEntities<component::LightScene>())
				ls->addComponent<ecs::EntityRef>(cc, { sun.getId(), ecs::EntityRef::Type::Child });
			newEntities.push_back(sun);
		}
		for (auto& entity : newEntities)
			scene.addEntity(entity);

		for (ecs::Entity* entity : scene.getEntities<component::Sky>())
		{
			auto skyLightEntities = scene.getRefEntities<component::DirectionalLight>(entity, ecs::EntityRef::Type::Child);
			auto sun = skyLightEntities.front();

			auto skyDataHost = entity->getHostGpuComponentArray<component::Sky>(gpuCc);
			auto& skyDataDevice = entity->getGpuComponentArray<vulkanutils::UniformBuffer, component::Sky>(gpuCc);
			component::Sky& sky = entity->getComponent<component::Sky>(cc);
			skyDataHost.getData()[0] = sky;
			auto& lightDir = sun->getComponent<component::Orientation>(cc);
			lightDir = -(Eigen::AngleAxisf(sky.sunPos[1], Eigen::Vector3f::UnitY()) * Eigen::AngleAxisf(sky.sunPos[0], Eigen::Vector3f::UnitX())).toRotationMatrix();
			bufferCopier.queue(skyDataHost, skyDataDevice);
			sky.sunPos[0] = 4.0f;//+= 0.01f;
		}
	}
	getVulkanContext().device().getComputeQueue().submit({ commandBuffer }, nullptr);

	mLastSignalledValue[mCmdBufferIdx] = getVulkanContext().device().getComputeQueue().getSignalValue();
	mCmdBufferIdx = (mCmdBufferIdx + 1) % 3;
}



static Eigen::Vector3f calculate_scattering(
	const Eigen::Vector3f& startIn, 				// the start of the ray (the camera position)
    const Eigen::Vector3f& dir, 					// the direction of the ray (the camera vector)
    float max_dist, 			// the maximum distance the ray can travel (because something is in the way, like an object)
    const Eigen::Vector3f& scene_color,			// the color of the scene
    const Eigen::Vector3f& light_dir, 			// the direction of the light
    const Eigen::Vector3f& light_intensity,		// how bright the light is, affects the brightness of the atmosphere
    const Eigen::Vector3f& planet_position, 		// the position of the planet
    float planet_radius, 		// the radius of the planet
    float atmo_radius, 			// the radius of the atmosphere
    const Eigen::Vector3f& beta_ray, 				// the amount rayleigh scattering scatters the colors (for earth: causes the blue atmosphere)
    const Eigen::Vector3f& beta_mie, 				// the amount mie scattering scatters colors
    const Eigen::Vector3f& beta_absorption,   	// how much air is absorbed
    const Eigen::Vector3f& beta_ambient,			// the amount of scattering that always occurs, cna help make the back side of the atmosphere a bit brighter
    float g, 					// the direction mie scatters the light in (like a cone). closer to -1 means more towards a single direction
    float height_ray, 			// how high do you have to go before there is no rayleigh scattering?
    float height_mie, 			// the same, but for mie
    float height_absorption,	// the height at which the most absorption happens
    float absorption_falloff,	// how fast the absorption falls off from the absorption height
    int steps_i, 				// the amount of steps along the 'primary' ray, more looks better but slower
    int steps_l 				// the amount of steps along the light ray, more looks better but slower
	)
{
	Eigen::Vector3f start = startIn - planet_position;
    float b = 2.0f * dir.dot(start);
    float c = start.dot(start) - (atmo_radius * atmo_radius);
    float d = (b * b) - 4.0f * c;
    if (d < 0.0f) return scene_color;
    
    Eigen::Vector2f ray_length = {
        std::max<float>((-b - sqrt(d)) / 2.0f, 0.0f),
        std::min<float>((-b + sqrt(d)) / 2.0f, max_dist)
	};
    
    if (ray_length.x() > ray_length.y()) return scene_color;
    bool allow_mie = max_dist > ray_length.y();
    ray_length.y() = std::min<float>(ray_length.y(), max_dist);
    ray_length.x() = std::max<float>(ray_length.x(), 0.0f);
    float step_size_i = (ray_length.y() - ray_length.x()) / float(steps_i);
    
    float ray_pos_i = ray_length.x() + step_size_i * 0.5f;
    Eigen::Vector3f total_ray = Eigen::Vector3f::Zero(); // for rayleigh
    Eigen::Vector3f total_mie = Eigen::Vector3f::Zero(); // for mie
    
    Eigen::Vector3f opt_i = Eigen::Vector3f::Zero();
    Eigen::Vector3f prev_density = Eigen::Vector3f::Zero();
    Eigen::Vector2f scale_height(height_ray, height_mie);
    
    float mu = dir.dot(light_dir);
    float mumu = mu * mu;
    float gg = g * g;
    float phase_ray = 3.0f / (50.2654824574f /* (16 * pi) */) * (1.0f + mumu);
    float phase_mie = allow_mie ? 3.0f / (25.1327412287f /* (8 * pi) */) * ((1.0f - gg) * (mumu + 1.0f)) / (pow(1.0f + gg - 2.0f * mu * g, 1.5f) * (2.0f + gg)) : 0.0f;
    
    for (int i = 0; i < steps_i; ++i) {
        Eigen::Vector3f pos_i = start + dir * ray_pos_i;
        float height_i = pos_i.norm() - planet_radius;
		if (height_i < 0)
			break;
        Eigen::Vector3f density(std::exp(-height_i / scale_height.x()), std::exp(-height_i / scale_height.y()), 0.0f);
        float denom = (height_absorption - height_i) / absorption_falloff;
        density.z() = (1.0f / (denom * denom + 1.0f)) * density.x();
        density *= step_size_i;
        opt_i += (density + (prev_density - density) * 0.5f).array().max(0.0f).matrix();
        prev_density = density;
        b = 2.0f * light_dir.dot(pos_i);
        c = pos_i.dot(pos_i) - (atmo_radius * atmo_radius);
        d = (b * b) - 4.0f * c;
        float step_size_l = (-b + sqrt(d)) / (2.0f * float(steps_l));
        float ray_pos_l = step_size_l * 0.5f;
        Eigen::Vector3f opt_l = Eigen::Vector3f::Zero();

        Eigen::Vector3f prev_density_l = Eigen::Vector3f::Zero();
        for (int l = 0; l < steps_l; ++l)
        {
            Eigen::Vector3f pos_l = pos_i + light_dir * ray_pos_l;
            float height_l = pos_l.norm() - planet_radius;
			if (height_l < 0)
				break;
            Eigen::Vector3f density_l(std::exp(-height_l / scale_height.x()), std::exp(-height_l / scale_height.y()), 0.0f);
            float denom = (height_absorption - height_l) / absorption_falloff;
            density_l.z() = (1.0f / (denom * denom + 1.0f)) * density_l.x();
            density_l *= step_size_l;
            opt_l += (density_l + (prev_density_l - density_l) * 0.5).array().max(0.0f).matrix();
            prev_density_l = density_l;
            ray_pos_l += step_size_l;
        }
        Eigen::Vector3f attn = (-beta_ray * (opt_i.x() + opt_l.x()) - beta_mie * (opt_i.y() + opt_l.y()) - beta_absorption * (opt_i.z() + opt_l.z())).array().exp();
        total_ray += density.x() * attn;
        total_mie += density.y() * attn;
        ray_pos_i += step_size_i;
    	
    }
    Eigen::Vector3f opacity = (-(beta_mie * opt_i.y() + beta_ray * opt_i.x() + beta_absorption * opt_i.z())).array().exp();
	return (
        	phase_ray * beta_ray.array() * total_ray.array() // rayleigh color
       		+ phase_mie * beta_mie.array() * total_mie.array() // mie
            + opt_i.x() * beta_ambient.array() // and ambient
    ) * light_intensity.array() + scene_color.array() * opacity.array(); // now make sure the background is rendered correctly
}