#include "watersystem.h"

#include "entity.h"
#include "systemproperties.h"
#include "camera.h"
#include "watermeshset.h"
#include "model.h"
#include "mesh.h"
#include "entitybuilder.h"

#include <vulkanContext.h>

#include <vector>

typedef ecs::ComponentParamPack<WaterSurface>
	WaterSystemComponents;

WaterSystem::WaterSystem(vulkanutils::VulkanContext& context, SceneType scene)
	: System<WaterSystem>(context)
{
	auto& cc = scene.getComponentCollection();
	auto& gpuCc = scene.getGpuComponentCollection();

	mWaterSurfacePipeline = ecs::Entity("WaterPipeline");
	{
		using namespace vulkanutils;
		component::Pipeline& pipeline = mWaterSurfacePipeline.addComponent<component::Pipeline>(cc, component::Pipeline("water"));

		pipeline.sets.emplace_back(systemPropertiesSet(&context.device(), scene, DescriptorSetSlot::SLOT_1 ));
		pipeline.sets.emplace_back(cameraSet(&context.device(), scene, DescriptorSetSlot::SLOT_2 ));
		pipeline.sets.emplace_back(materialSet(&context.device(), scene, DescriptorSetSlot::SLOT_4, 1 ));
		pipeline.sets.emplace_back(WaterMeshSet(&context.device(), scene, DescriptorSetSlot::NONE, VertexInputSlot::SLOT_0));
		pipeline.sets.emplace_back(ModelSet(&context.device(), scene, DescriptorSetSlot::SLOT_5, false ));
		pipeline.sets.emplace_back(LightSet(&context.device(), scene, DescriptorSetSlot::SLOT_3 ));

		pipeline.addRenderPassAttachment<component::Color>(scene, 0);
		pipeline.addRenderPassAttachment<component::Depth>(scene, 1);

		scene.addEntity(mWaterSurfacePipeline);
	}

	mWaterMaterialEntity = ecs::Entity("waterMaterialEntity");
	component::MaterialProperties& waterMaterial = mWaterMaterialEntity.addComponent<component::MaterialProperties>(cc);
	Eigen::Map<Eigen::Vector4f>(waterMaterial.ambient) = Eigen::Vector4f(0.1f, 0.1f, 0.2f, 1.0f );
	Eigen::Map<Eigen::Vector4f>(waterMaterial.diffuse) = Eigen::Vector4f(0.3f, 0.4f, 0.8f, 1.0f);
	Eigen::Map<Eigen::Vector4f>(waterMaterial.emissive) = Eigen::Vector4f(0.0f, 0.0f, 0.0f, 1.0f);
	Eigen::Map<Eigen::Vector4f>(waterMaterial.specular) = Eigen::Vector4f(0.3f, 0.3f, 0.4f, 20.0f);
	scene.addEntity(mWaterMaterialEntity);
}

void WaterSystem::updateWater(ecs::Entity& entity, SceneType& scene) const
{
	WaterSurface& ws = entity.getComponent<WaterSurface>(scene.getComponentCollection());
	component::DiscreteSurface<WaterSurfaceCell>& ds = ws.getDiscrete();

	const float g = 0.1f * 9.82f;
	const float dt = 0.75f;
	const float dl = ws.cellSize();
	const float dlSq = ws.cellSize() * ws.cellSize();
	const float attenuation = 0.0001f;
	const float f = g * dt / dlSq * 0.1f;

	// remove boundary processing outside loops to remove branching
	/*
	for (int i = 0; i < 4; i++)
	{
		auto ds2 = ds;
		for (size_t r = 0; r < ds.height(); r++)
		{
			for (size_t c = 0; c < ds.width(); c++)
			{
				auto& tcell = ds(c, r);
				auto& cell = ds2(c, r);
				{
					float dhdt = 0;
					if (c > 0) dhdt += ds2(c - 1, r).uRight;
					if (r > 0) dhdt += ds2(c, r - 1).vDown;

					if (c < ds.width() - 1)
					{
						dhdt -= cell.uRight;

						const WaterSurfaceCell& cellRight = ds2(c + 1, r);
						float dU = -f * (cellRight.depth + cellRight.bd - cell.depth - cell.bd);
						tcell.uRight = std::clamp(cell.uRight + dU, -std::max(0.0f, cellRight.depth) * dlSq, std::max(0.0f, cell.depth) * dlSq);
					}
					if (r < ds.height() - 1)
					{
						dhdt -= cell.vDown;

						const WaterSurfaceCell& cellDown = ds2(c, r + 1);
						float dV = -f * (cellDown.depth + cellDown.bd - cell.depth - cell.bd);
						tcell.vDown = std::clamp(cell.vDown + dV, -std::max(0.0f, cellDown.depth) * dlSq, std::max(0.0f, cell.depth) * dlSq);
					}
					if (cell.depth > 0)
					{
						//tcell.uRight *= tcell.depth / cell.depth;
						//tcell.vDown *= tcell.depth / cell.depth;
					}
					tcell.uRight -= tcell.uRight * attenuation;
					tcell.vDown -= tcell.vDown * attenuation;

					tcell.depth += dhdt * dt / dlSq;
				}
			}
		}

		ds2 = ds;
		for (size_t r = 1; r < ds.height() - 1; r++)
		{
			for (size_t c = 1; c < ds.width() - 1; c++)
			{
				auto& cell = ds2(c, r);
				float dh = 0;
				unsigned int n = 0;
				
				//if (c > 0)
				{
					dh += ds2(c - 1, r).uRight;
					n++;
				}
				//if (c < ds.width() - 1)
				{
					dh -= cell.uRight;
					n++;
				}

				//if (r > 0)
				{
					dh += ds2(c, r - 1).vDown;
					n++;
				}
				//if (r < ds.height() - 1)
				{
					dh -= cell.vDown;
					n++;
				}

				if (dh * dt < -cell.depth * dlSq)
				{
					float diff = -(dh * dt / dlSq + cell.depth) / n;
					//if (c > 0)
					{
						ds(c - 1, r).uRight += diff;
					}
					//if (c < ds.width() - 1)
					{
						ds(c, r).uRight -= diff;
					}

					//if (r > 0)
					{
						ds(c, r - 1).vDown += diff;
					}
					//if (r < ds.height() - 1)
					{
						ds(c, r).vDown -= diff;
					}
				}
			}
		}
	}
	*/


	// Do work directly against upload buffers
	// Add copying to gpu
	/*
	ecs::EntityRef meshEntityId = entity.getComponent<ecs::EntityRef>(scene.getComponentCollection());
	ecs::Entity* meshEntity = scene.getEntity(meshEntityId.id);

	std::span<component::MeshVertex> vertices
		= meshEntity->getComponentArray<component::MeshVertex>(scene.getComponentCollection());
	std::span<WaterDepth> waterDepths
		= meshEntity->getComponentArray<WaterDepth>(scene.getComponentCollection());

	for (size_t i = 0; i < vertices.size(); i++)
	{
		component::MeshVertex& vertex = vertices[i];
		WaterDepth& wd = waterDepths[i];
		float x = vertex.point[0] / ws.cellSize() + ds.width() / 2;
		float z = vertex.point[2] / ws.cellSize() + ds.height() / 2;
		if (x >= 0 && x < ds.width() && z >= 0 && z < ds.height())
		{
			WaterSurfaceCell& cell = ds(x, z);
			float y = cell.depth + cell.bd;
			wd.depth = cell.depth;
			vertex.point[1] = y;
		}
	}
	calcSurfaceNormals(scene.getComponentCollection(), *meshEntity);
	*/
	//meshEntity->setModified();
}

void WaterSystem::setupWater(ecs::Entity& entity, SceneType& scene) const
{
	auto& cc = scene.getComponentCollection();
	auto& gpuCc = scene.getGpuComponentCollection();

	WaterSurface& ws = entity.getComponent<WaterSurface>(scene.getComponentCollection());

	ecs::Entity waterSurfaceMeshEntity =
		createFlatSurfaceMesh(cc,
		ws.getXSize(), ws.getYSize(),
		20);
	waterSurfaceMeshEntity.setName("waterSurfaceMeshEntity");
	waterSurfaceMeshEntity.addComponent<ecs::EntityRef>(cc, { mWaterMaterialEntity.getId(), ecs::EntityRef::Type::Child });
	size_t numVertices = waterSurfaceMeshEntity.getComponentArray<component::MeshVertex>(cc).size();
	waterSurfaceMeshEntity.addComponentArray<WaterDepth>(cc, numVertices);
	scene.addEntity(waterSurfaceMeshEntity);
	
	entity.addComponent<ecs::EntityRef>(cc, { mWaterSurfacePipeline.getId(), ecs::EntityRef::Type::Link });
	entity.addComponent<ecs::EntityRef>(cc, { waterSurfaceMeshEntity.getId(), ecs::EntityRef::Type::Child });
}


void WaterSystem::runImpl(SceneType scene)
{
	for (auto& entity : scene.getEntities<WaterSurface>())
	{
		if (scene.getRefEntities<component::MeshVertex>(entity, ecs::EntityRef::Type::Child).empty())
			setupWater(*entity, scene);
		else
		{
			component::RunMode& runMode = entity->getComponent<component::RunMode>(scene.getComponentCollection());
			//if (runMode != component::RunMode::PAUSE)
			{
				updateWater(*entity, scene);
			}
		}
	}
}
