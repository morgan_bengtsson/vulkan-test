#include "animationsystem.h"

#include <vulkanBufferCopier.h>
#include <vulkanBarrier.h>

#include <map>


template<typename SceneType>
static EntityResourceSet animationMeshComputeSet(vulkanutils::Device* device, SceneType scene, vulkanutils::DescriptorSetSlot descSetbp)
{
	EntityResourceSet ret(device, "AnimationMeshCompute", descSetbp, vulkanutils::VertexInputSlot::NONE);
	ret.setDescriptorBinding<vulkanutils::ShaderStorageBuffer, skeleton::Joint>(scene,
		{ VK_SHADER_STAGE_COMPUTE_BIT, 0 });
	ret.setDescriptorBinding<vulkanutils::ShaderStorageBuffer, skeleton::Animation::BoneAnimationKeyFrame>(scene,
		{ VK_SHADER_STAGE_COMPUTE_BIT, 1 });
	ret.setDescriptorBinding<vulkanutils::ShaderStorageBuffer, skeleton::AnimationKeyFrameRange>(scene,
		{ VK_SHADER_STAGE_COMPUTE_BIT, 2 });
	ret.getDescriptorLayout().build();
	return ret;
}

template<typename SceneType>
static EntityResourceSet animationModelInputComputeSet(vulkanutils::Device* device, SceneType scene, vulkanutils::DescriptorSetSlot descSetbp)
{
	EntityResourceSet ret(device, "AnimationModelInputComputeSet", descSetbp, vulkanutils::VertexInputSlot::NONE);
	// Max 1000 different buffers bound to this array
	ret.setDescriptorBinding<vulkanutils::ShaderStorageBuffer, skeleton::ActiveAnimation>(scene,
		{ VK_SHADER_STAGE_COMPUTE_BIT, 0, 1000, true });
	ret.getDescriptorLayout().build();
	return ret;
}

template<typename SceneType>
static EntityResourceSet animationModelOutputComputeSet(vulkanutils::Device* device, SceneType scene, vulkanutils::DescriptorSetSlot descSetbp)
{
	EntityResourceSet ret(device, "AnimationModelOutputComputeSet", descSetbp, vulkanutils::VertexInputSlot::NONE);
	// Max 1000 different buffers bound to this array
	ret.setDescriptorBinding<vulkanutils::ShaderStorageBuffer, skeleton::Bone>(scene,
		{ VK_SHADER_STAGE_COMPUTE_BIT, 0, 1000, true });
	ret.getDescriptorLayout().build();
	return ret;
}

template<typename SceneType>
static EntityResourceSet skinningMeshComputeSet(vulkanutils::Device* device, SceneType scene, vulkanutils::DescriptorSetSlot descSetbp)
{
	EntityResourceSet ret(device, "SkinningMeshComputeSet", descSetbp, vulkanutils::VertexInputSlot::NONE);
	ret.setDescriptorBinding<vulkanutils::ShaderStorageBuffer, component::MeshVertex>(scene,
		{ VK_SHADER_STAGE_COMPUTE_BIT, 0 });
	ret.setDescriptorBinding<vulkanutils::ShaderStorageBuffer, component::MeshNormal>(scene,
		{ VK_SHADER_STAGE_COMPUTE_BIT, 1 });
	ret.setDescriptorBinding<vulkanutils::ShaderStorageBuffer, skeleton::VertexBoneData>(scene,
		{ VK_SHADER_STAGE_COMPUTE_BIT, 2 });
	ret.getDescriptorLayout().build();
	return ret;
}


template<typename SceneType>
static EntityResourceSet skinningModelInputComputeSet(vulkanutils::Device* device, SceneType scene, vulkanutils::DescriptorSetSlot descSetbp)
{
	EntityResourceSet ret(device, "SkinningModelInputComputeSet", descSetbp, vulkanutils::VertexInputSlot::NONE);
	ret.setDescriptorBinding<vulkanutils::ShaderStorageBuffer, skeleton::Bone>(scene,
		{ VK_SHADER_STAGE_COMPUTE_BIT, 0 });
	ret.getDescriptorLayout().build();
	return ret;
}

template<typename SceneType>
static EntityResourceSet skinningModelOutputComputeSet(vulkanutils::Device* device, SceneType scene, vulkanutils::DescriptorSetSlot descSetbp)
{
	EntityResourceSet ret(device, "SkinningModelOutputComputeSet", descSetbp, vulkanutils::VertexInputSlot::NONE);
	ret.setDescriptorBinding<vulkanutils::ShaderStorageBuffer, component::SkinnedMeshVertex>(scene,
		{ VK_SHADER_STAGE_COMPUTE_BIT, 0 });
	ret.setDescriptorBinding<vulkanutils::ShaderStorageBuffer, component::SkinnedMeshNormal>(scene,
		{ VK_SHADER_STAGE_COMPUTE_BIT, 1 });
	ret.getDescriptorLayout().build();
	return ret;
}


AnimationSystem::AnimationSystem(vulkanutils::VulkanContext& context, SceneType scene)
	: System<AnimationSystem>(context)
	, ComputeSystem(context, "./shaders")
	, mAnimationPipeline()
	, mSkinningPipeline()
	, mCommandPool(context.device(), context.device().getComputeQueue(), VK_COMMAND_POOL_CREATE_RESET_COMMAND_BUFFER_BIT)
	, mCommandBuffers(context.device(), mCommandPool, 3)
	, mLastSignalledValue(3, 0)
	, mCmdBufferIdx(0)
	, mDescriptorPool(context.device(), 1024)
{
	{
		std::vector<EntityResourceSet> sets;
		sets.emplace_back(animationMeshComputeSet(&getVulkanContext().device(), scene, vulkanutils::DescriptorSetSlot::SLOT_0));
		sets.emplace_back(animationModelInputComputeSet(&getVulkanContext().device(), scene, vulkanutils::DescriptorSetSlot::SLOT_1));
		sets.emplace_back(animationModelOutputComputeSet(&getVulkanContext().device(), scene, vulkanutils::DescriptorSetSlot::SLOT_2));
		mAnimationPipeline = std::move(createComputePipeline("animation", std::move(sets)));
	}
	{
		std::vector<EntityResourceSet> sets;
		sets.emplace_back(skinningMeshComputeSet(&getVulkanContext().device(), scene, vulkanutils::DescriptorSetSlot::SLOT_0));
		sets.emplace_back(skinningModelInputComputeSet(&getVulkanContext().device(), scene, vulkanutils::DescriptorSetSlot::SLOT_1));
		sets.emplace_back(skinningModelOutputComputeSet(&getVulkanContext().device(), scene, vulkanutils::DescriptorSetSlot::SLOT_2));
		mSkinningPipeline = std::move(createComputePipeline("skinning", std::move(sets)));
	}
}

void AnimationSystem::runImpl(SceneType scene)
{
	auto& cc = scene.getComponentCollection();
	auto& gpuCc = scene.getGpuComponentCollection();

	auto& commandBuffer = mCommandBuffers[mCmdBufferIdx];
	if (!mSetup)
		setup(scene);

	for (ecs::Entity* entity : scene.getEntities<skeleton::ActiveAnimation>())
	{
		auto activeAnimations = entity->getComponentArray<skeleton::ActiveAnimation>(cc);
		for (auto& activeAnimation : activeAnimations)
			activeAnimation.progress += 0.1f;

		auto activeAnimationsGpu = entity->getHostGpuComponentArray<skeleton::ActiveAnimation>(gpuCc);
		std::copy(std::begin(activeAnimations), std::end(activeAnimations), std::begin(activeAnimationsGpu.getData()));
	}

	if (!getVulkanContext().device().getComputeQueue().wait(getVulkanContext().device(), mLastSignalledValue[mCmdBufferIdx], 0))
		return;


	getVulkanContext().device().getComputeQueue().submit({ commandBuffer }, &getVulkanContext().device().getGraphicsQueue(), {}, {});
	mLastSignalledValue[mCmdBufferIdx] = getVulkanContext().device().getComputeQueue().getSignalValue();
	mCmdBufferIdx = (mCmdBufferIdx + 1) % 3;
}

void AnimationSystem::setup(SceneType scene)
{
	auto& cc = scene.getComponentCollection();
	auto& gpuCc = scene.getGpuComponentCollection();

	struct EntityComp
	{
		bool operator()(const ecs::Entity* lhs, const ecs::Entity* rhs) const
		{
			return lhs->getId() < rhs->getId();
		}
	};

	auto entityRequiresSkinning = [&](const ecs::Entity* entity)
	{
		auto pipelines = scene.getRefEntities<component::Pipeline>(entity, ecs::EntityRef::Type::Link);
		for (ecs::Entity* pipelineEntity : pipelines)
		{
			for (auto& pipeline : pipelineEntity->getComponentArray<component::Pipeline>(cc))
			{
				for (auto& set : pipeline.sets)
				{
					if (set.getRequiredCompTypeIds<vulkanutils::VertexBuffer>() & (size_t(1) << gpuCc.getComponentTypeIndex<component::SkinnedMeshVertex>()))
						return true;
				}
			}
		}
		return false;
	};

	std::map<ecs::Entity*, std::vector<ecs::Entity*>, EntityComp> meshInstances;
	auto& commandBuffer = mCommandBuffers[0];
	{
		vulkanutils::CommandBufferRecorder cbRecorder(commandBuffer, VK_COMMAND_BUFFER_USAGE_ONE_TIME_SUBMIT_BIT);
		vulkanutils::BufferCopier bufferCopier(commandBuffer);
		auto entities = scene.getEntities<skeleton::ActiveAnimation>();
		for (ecs::Entity* entity : entities)
		{
			auto meshSkeletonEntities = scene.getRefEntities<skeleton::Skeleton>(entity, ecs::EntityRef::Type::Child);
			for (size_t i = 0; i < meshSkeletonEntities.size(); i++)
			{
				ecs::Entity* meshEntity = meshSkeletonEntities[i];
				meshInstances[meshEntity].push_back(entity);
			}
		}
		for (ecs::Entity* entity : scene.getEntities<skeleton::ActiveAnimation>())
		{
			if (entity->hasGpuComponents<vulkanutils::ShaderStorageBuffer, skeleton::Bone>(gpuCc))
				continue;
			const auto& meshEnts = scene.getRefEntities<skeleton::Skeleton>(entity, ecs::EntityRef::Type::Child);
			for (auto& meshEnt : meshEnts)
			{
				size_t numBones = meshEnt->getComponent<skeleton::Skeleton>(cc).numBones();
				entity->addGpuComponentArray<vulkanutils::ShaderStorageBuffer, skeleton::Bone>(gpuCc, numBones);
			}
			std::span<skeleton::ActiveAnimation> activeAnimations = entity->getComponentArray<skeleton::ActiveAnimation>(cc);
			entity->addHostGpuComponentArray<skeleton::ActiveAnimation>(gpuCc, activeAnimations.size());
			entity->addGpuComponentArray<vulkanutils::ShaderStorageBuffer, skeleton::ActiveAnimation>(gpuCc, activeAnimations.size());
		}

		for (auto& [meshEntity, skeletonEntities] : meshInstances)
		{
			// One shader call per mesh
			const skeleton::Skeleton& meshSkeleton = meshEntity->getComponent<skeleton::Skeleton>(cc);
			if (meshEntity->hasGpuComponents<vulkanutils::ShaderStorageBuffer, skeleton::Joint>(gpuCc))
				continue;

			// Joints
			const std::vector<skeleton::Joint>& joints = meshSkeleton.getJoints();
			{
				auto hostData = meshEntity->addHostGpuComponentArray<skeleton::Joint>(gpuCc, joints.size());
				auto& deviceData = meshEntity->addGpuComponentArray<vulkanutils::ShaderStorageBuffer, skeleton::Joint>(gpuCc, joints.size());
				for (size_t j = 0; j < joints.size(); j++)
					hostData.getData()[j] = joints[j];
				bufferCopier.queue(hostData, deviceData);
			}

			std::vector<skeleton::AnimationKeyFrameRange> animationKeyFrameRanges;
			std::vector<skeleton::Animation::BoneAnimationKeyFrame> keyFrames;
			const std::vector<skeleton::Animation>& animations = meshSkeleton.getAnimations();
			// Per animation
			for (const skeleton::Animation& animation : animations)
			{
				// Per bone
				for (const skeleton::Animation::BoneAnimationSequence& animationSeq : animation.getBoneSequences())
				{
					// Per pose of this bone
					animationKeyFrameRanges.emplace_back(skeleton::AnimationKeyFrameRange{ uint32_t(keyFrames.size()), 0 });
					for (const skeleton::Animation::BoneAnimationKeyFrame& kf : animationSeq)
					{
						keyFrames.push_back(kf);
						animationKeyFrameRanges.back().numKeyFrames++;
					}
				}
			}
			{
				auto hostData = meshEntity->addHostGpuComponentArray<skeleton::Animation::BoneAnimationKeyFrame>(gpuCc, keyFrames.size());
				auto& deviceData = meshEntity->addGpuComponentArray<vulkanutils::ShaderStorageBuffer, skeleton::Animation::BoneAnimationKeyFrame>(gpuCc, keyFrames.size());
				for (size_t j = 0; j < keyFrames.size(); j++)
					hostData.getData()[j] = keyFrames[j];
				bufferCopier.queue(hostData, deviceData);
			}

			{
				auto hostData = meshEntity->addHostGpuComponentArray<skeleton::AnimationKeyFrameRange>(gpuCc, keyFrames.size());
				auto& deviceData = meshEntity->addGpuComponentArray<vulkanutils::ShaderStorageBuffer, skeleton::AnimationKeyFrameRange>(gpuCc, keyFrames.size());
				for (size_t j = 0; j < animationKeyFrameRanges.size(); j++)
					hostData.getData()[j] = animationKeyFrameRanges[j];
				bufferCopier.queue(hostData, deviceData);
			}

			{
				// Constant source data
				auto hostVertexData = meshEntity->getHostGpuComponentArray<component::MeshVertex>(gpuCc);
				auto hostNormalData = meshEntity->getHostGpuComponentArray<component::MeshNormal>(gpuCc);
				auto hostVertexBoneData = meshEntity->getHostGpuComponentArray<skeleton::VertexBoneData>(gpuCc);

				// Write output to skinned arrays, then copy to vertex buffers
				bool doSkinning = false;
				for (auto& skeletonEntity : skeletonEntities)
				{
					if (!entityRequiresSkinning(skeletonEntity))
						continue;
					doSkinning = true;
					skeletonEntity->addGpuComponentArray<vulkanutils::ShaderStorageBuffer, component::SkinnedMeshVertex>(gpuCc, hostVertexData.getData().size());
					skeletonEntity->addGpuComponentArray<vulkanutils::ShaderStorageBuffer, component::SkinnedMeshNormal>(gpuCc, hostNormalData.getData().size());
					skeletonEntity->addGpuComponentArray<vulkanutils::VertexBuffer, component::SkinnedMeshVertex>(gpuCc, hostVertexData.getData().size());
					skeletonEntity->addGpuComponentArray<vulkanutils::VertexBuffer, component::SkinnedMeshNormal>(gpuCc, hostNormalData.getData().size());
				}
				
				if (doSkinning)
				{
					// Write one original copy here
					auto& deviceVertexData = meshEntity->addGpuComponentArray<vulkanutils::ShaderStorageBuffer, component::MeshVertex>(gpuCc, hostVertexData.getData().size());
					auto& deviceNormalData = meshEntity->addGpuComponentArray<vulkanutils::ShaderStorageBuffer, component::MeshNormal>(gpuCc, hostNormalData.getData().size());
					auto& deviceVertexBoneData = meshEntity->addGpuComponentArray<vulkanutils::ShaderStorageBuffer, skeleton::VertexBoneData>(gpuCc, hostVertexBoneData.getData().size());
					bufferCopier.queue(hostVertexData, deviceVertexData);
					bufferCopier.queue(hostNormalData, deviceNormalData);
					bufferCopier.queue(hostVertexBoneData, deviceVertexBoneData);
				}
			}
		}
	}
	getVulkanContext().device().getComputeQueue().submit({ commandBuffer }, nullptr);
	getVulkanContext().device().getComputeQueue().wait();

	for (size_t i = 0; i < 3; i++)
	{
		auto& commandBuffer = mCommandBuffers[i];
		vulkanutils::CommandBufferRecorder cbRecorder(commandBuffer, VK_COMMAND_BUFFER_USAGE_SIMULTANEOUS_USE_BIT);
		{
			vulkanutils::Barrier barrier(commandBuffer);
			vulkanutils::BufferCopier bufferCopier(commandBuffer);
			barrier.dependsOnCopy().doBeforeCompute();
			for (ecs::Entity* entity : scene.getEntities<skeleton::ActiveAnimation>())
			{
				auto meshEntities = scene.getRefEntities<skeleton::Skeleton>(entity, ecs::EntityRef::Type::Child);
				auto hostData = entity->getHostGpuComponentArray<skeleton::ActiveAnimation>(gpuCc);
				auto& deviceData = entity->getGpuComponentArray<vulkanutils::ShaderStorageBuffer, skeleton::ActiveAnimation>(gpuCc);
				std::span<skeleton::ActiveAnimation> activeAnimations = entity->getComponentArray<skeleton::ActiveAnimation>(cc);
				for (size_t i = 0; i < activeAnimations.size(); i++)
					hostData.getData()[i] = activeAnimations[i];
				bufferCopier.queue(hostData, deviceData);
				barrier.addBuffer(deviceData);
			}
		}
		
		for (auto& [meshEntity, skeletonEntities] : meshInstances)
		{
			const skeleton::Skeleton& meshSkeleton = meshEntity->getComponent<skeleton::Skeleton>(cc);
			auto& descriptorSets = mAnimationPipeline.createDescriptorSets(meshEntity->getId(), mDescriptorPool, {0, skeletonEntities.size(), skeletonEntities.size() });

			mAnimationPipeline.getComputeSets()[0].bindEntityToDescriptors(scene, descriptorSets[i * 3 + 0], *meshEntity);
			std::vector<const vulkanutils::BufferAllocation<vulkanutils::ShaderStorageBuffer>*> modelInputBuffers;
			std::vector<const vulkanutils::BufferAllocation<vulkanutils::ShaderStorageBuffer>*> modelOutputBuffers;
			for (ecs::Entity* skeletonEntity : skeletonEntities)
			{
				auto& activeAnimationsDevice = skeletonEntity->getGpuComponentArray<vulkanutils::ShaderStorageBuffer, skeleton::ActiveAnimation>(gpuCc);
				auto meshEntities = scene.getRefEntities<skeleton::Skeleton>(skeletonEntity, ecs::EntityRef::Type::Child);
				size_t meshIdx = std::find(meshEntities.begin(), meshEntities.end(), meshEntity) - meshEntities.begin();
				auto& boneTransformDevice = skeletonEntity->getGpuComponentArray<vulkanutils::ShaderStorageBuffer, skeleton::Bone>(gpuCc, meshIdx);
				modelInputBuffers.emplace_back(&activeAnimationsDevice);
				modelOutputBuffers.emplace_back(&boneTransformDevice);
			}
			
			descriptorSets[i * 3 + 1].bindBuffers(modelInputBuffers, 0, 0);
			descriptorSets[i * 3 + 2].bindBuffers(modelOutputBuffers, 0, 0);
			mAnimationPipeline.execute(commandBuffer, meshEntity->getId(), { uint32_t(skeletonEntities.size()), 1, 1 });
		}

		for (ecs::Entity* entity : scene.getEntities<skeleton::ActiveAnimation>())
		{
			if (!entityRequiresSkinning(entity))
				continue;

			auto meshSkeletonEntities = scene.getRefEntities<skeleton::Skeleton>(entity, ecs::EntityRef::Type::Child);
			for (size_t j = 0; j < meshSkeletonEntities.size(); j++)
			{
				vulkanutils::BufferCopier bufferCopier(commandBuffer);
				vulkanutils::Barrier barrier(commandBuffer);
				barrier.dependsOnCompute().doBeforeCopy();

				std::vector<const vulkanutils::BufferAllocation<vulkanutils::ShaderStorageBuffer>*> meshBuffers;
				std::vector<const vulkanutils::BufferAllocation<vulkanutils::ShaderStorageBuffer>*> modelInputBuffers;
				std::vector<const vulkanutils::BufferAllocation<vulkanutils::ShaderStorageBuffer>*> modelOutputBuffers;

				ecs::Entity* meshEntity = meshSkeletonEntities[j];
				std::vector<vulkanutils::DescriptorSet>& descriptorSets = mSkinningDescSets[{entity->getId(), meshEntity->getId()}];
				for (auto& compSet : mSkinningPipeline.getComputeSets())
					descriptorSets.push_back(compSet.createDescriptorSet(mDescriptorPool));

				const auto& vertices = meshEntity->getComponentArray<component::MeshVertex>(cc);
				if (!meshEntity->hasGpuComponents<vulkanutils::ShaderStorageBuffer, component::MeshVertex, component::MeshNormal, skeleton::VertexBoneData>(gpuCc))
					continue;




				auto& deviceVertexData = meshEntity->getGpuComponentArray<vulkanutils::ShaderStorageBuffer, component::MeshVertex>(gpuCc);
				auto& deviceNormalData = meshEntity->getGpuComponentArray<vulkanutils::ShaderStorageBuffer, component::MeshNormal>(gpuCc);
				auto& deviceVertexBoneData = meshEntity->getGpuComponentArray<vulkanutils::ShaderStorageBuffer, skeleton::VertexBoneData>(gpuCc);
				meshBuffers.push_back(&deviceVertexData);
				meshBuffers.push_back(&deviceNormalData);
				meshBuffers.push_back(&deviceVertexBoneData);

				auto& deviceBoneData = entity->getGpuComponentArray<vulkanutils::ShaderStorageBuffer, skeleton::Bone>(gpuCc, j);
				auto& deviceSkinnedVertexData = entity->getGpuComponentArray<vulkanutils::ShaderStorageBuffer, component::SkinnedMeshVertex>(gpuCc, j);
				auto& deviceSkinnedNormalData = entity->getGpuComponentArray<vulkanutils::ShaderStorageBuffer, component::SkinnedMeshNormal>(gpuCc, j);

				modelInputBuffers.push_back(&deviceBoneData);
				modelOutputBuffers.push_back(&deviceSkinnedVertexData);
				modelOutputBuffers.push_back(&deviceSkinnedNormalData);

				descriptorSets[i * 3 + 0].bindBuffers(meshBuffers, 0, 0);
				descriptorSets[i * 3 + 1].bindBuffers(modelInputBuffers, 0, 0);
				descriptorSets[i * 3 + 2].bindBuffers(modelOutputBuffers, 0, 0);

				mSkinningPipeline.mComputePipeline.bind(commandBuffer);
				for (size_t i = 0; i < mSkinningPipeline.mComputeSets.size(); i++)
					descriptorSets[i].bindToCompute(commandBuffer, mSkinningPipeline.mComputePipeline.getLayout(), (uint32_t) mSkinningPipeline.mComputeSets[i].getDescriptorSetBindpoint());
				vkCmdDispatch(commandBuffer, uint32_t(vertices.size()), 1, 1);

				
				barrier.addBuffer(deviceSkinnedVertexData).addBuffer(deviceSkinnedVertexData);
				barrier.execute();
				auto& deviceSkinnedVertexData2 = entity->getGpuComponentArray<vulkanutils::VertexBuffer, component::SkinnedMeshVertex>(gpuCc, j);
				auto& deviceSkinnedNormalData2 = entity->getGpuComponentArray<vulkanutils::VertexBuffer, component::SkinnedMeshNormal>(gpuCc, j);


				bufferCopier.queue(deviceSkinnedVertexData, deviceSkinnedVertexData2);
				bufferCopier.queue(deviceSkinnedNormalData, deviceSkinnedNormalData2);

			}
		}
	}

	mSetup = true;
}
