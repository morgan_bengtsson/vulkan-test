#include "drawsystem.h"


#include "drawcall.h"
#include "fileload.h"
#include "component.h"
#include "resourcesetbinding.h"
#include "imageloading.h"
#include "idrawsystemextension.h"
#include "renderpass.h"


#include <vulkanRenderPassRecorder.h>
#include <vulkanBarrier.h>
#include <vulkanContext.h>

#include <chrono>
#include <iostream>
#include <type_traits>
#include <algorithm>


#pragma optimize ("", off)

struct DrawSystemExtensionSet
{
	std::vector<std::tuple<size_t, std::unique_ptr<IDrawSystemExtension> > > extensions;
};




typedef ecs::ComponentParamPack<component::Pipeline>
	DrawPipelineComponents;
typedef ecs::ComponentParamPack<component::SystemProperies>
	SystemPropertiesComponents;


static VkSamplerAddressMode samplerAddressModes[] = { VK_SAMPLER_ADDRESS_MODE_REPEAT,
										VK_SAMPLER_ADDRESS_MODE_REPEAT,
										VK_SAMPLER_ADDRESS_MODE_REPEAT };

EntityResourceSet drawSet(vulkanutils::Device* device, DrawSystem::SceneType scene, vulkanutils::DescriptorSetSlot slot)
{
	EntityResourceSet ret(device, "draw", slot, vulkanutils::VertexInputSlot::NONE);
	const vulkanutils::DescriptorBinding<vulkanutils::ShaderStorageBuffer> descBindings[] =
	{
		{ VK_SHADER_STAGE_VERTEX_BIT | VK_SHADER_STAGE_FRAGMENT_BIT, 0 }, // component offset buffer
		{ VK_SHADER_STAGE_VERTEX_BIT | VK_SHADER_STAGE_FRAGMENT_BIT, 1 }, // clipping buffer
	};
	ret.getDescriptorLayout().addDescriptorBindings<vulkanutils::ShaderStorageBuffer>( { descBindings, std::size(descBindings)});
	ret.getDescriptorLayout().build();
	return ret;
}

EntityResourceSet cullComputeCameraSet(vulkanutils::Device* device, DrawSystem::SceneType scene, vulkanutils::DescriptorSetSlot slot)
{
	EntityResourceSet ret(device, "cullcamera", slot, vulkanutils::VertexInputSlot::NONE);
	ret.setDescriptorBinding<vulkanutils::UniformBuffer, component::CameraProjection>(scene,
		{ VK_SHADER_STAGE_COMPUTE_BIT, 0 });
	ret.setDescriptorBinding<vulkanutils::UniformBuffer, component::Transform>(scene,
		{ VK_SHADER_STAGE_COMPUTE_BIT, 1 });
	ret.setDescriptorBinding<vulkanutils::UniformBuffer, component::Transform>(scene,
		{ VK_SHADER_STAGE_COMPUTE_BIT, 2 });

	ret.getDescriptorLayout().build();
	return ret;
}

// WIP: only keep AABB on gpu for mesh, do transform on GPU since I dont have a way now to
// connect correct entry in each list of AABB per model to the correct mesh
EntityResourceSet cullComputeSet(vulkanutils::Device* device, vulkanutils::DescriptorSetSlot slot)
{
	EntityResourceSet ret(device, "cull", slot, vulkanutils::VertexInputSlot::NONE);
	const vulkanutils::DescriptorBinding<vulkanutils::ShaderStorageBuffer> descBindings[] =
	{
		{ VK_SHADER_STAGE_COMPUTE_BIT, 0 }, // component offset buffer
		{ VK_SHADER_STAGE_COMPUTE_BIT, 1 }, // clipping buffer
		{ VK_SHADER_STAGE_COMPUTE_BIT, 2 }, // draw command buffer
	};

	ret.getDescriptorLayout().addDescriptorBindings<vulkanutils::ShaderStorageBuffer>({ descBindings, std::size(descBindings) });
	ret.getDescriptorLayout().build();
	return ret;
}

EntityResourceSet cullComputeAABBSet(vulkanutils::Device* device, DrawSystem::SceneType scene, vulkanutils::DescriptorSetSlot slot)
{
	EntityResourceSet ret(device, "cullAABB", slot, vulkanutils::VertexInputSlot::NONE);
	ret.setDescriptorBinding<vulkanutils::ShaderStorageBuffer, component::MeshAABoundingBox>(scene,
		{ VK_SHADER_STAGE_COMPUTE_BIT, 0 });

	ret.getDescriptorLayout().build();
	return ret;
}

DrawSystem::DrawSystem(vulkanutils::VulkanContext& context, SceneType scene)
	: System<DrawSystem>(context)
	, mDevice(&context.device())
	, mDescriptorPool(context.device(), 8192 * 4)
	, mDrawSet(drawSet(&context.device(), scene, vulkanutils::DescriptorSetSlot::SLOT_0))
	, mSystemState(State::SETUP)
	, mFrameCounter(0)
	, mPendingImageIndex()
	, mSubmittedImageIndex(2)
	, mStartTime()
	, mRenderCommandPool(context.device(), context.device().getGraphicsQueue())
	, mRenderCommandBuffers(context.device(), mRenderCommandPool, vulkanutils::VulkanContext::NUM_SWAP_IMAGES)
	, mExtraRenderCommandBuffers()
	, mUploadCommandPool(context.device(), context.device().getGraphicsQueue(), VK_COMMAND_POOL_CREATE_TRANSIENT_BIT | VK_COMMAND_POOL_CREATE_RESET_COMMAND_BUFFER_BIT)
	, mUploadCommandBuffers(context.device(), mUploadCommandPool, vulkanutils::VulkanContext::NUM_SWAP_IMAGES)
	, mComputeCommandPool(context.device(), context.device().getComputeQueue(), VK_COMMAND_POOL_CREATE_RESET_COMMAND_BUFFER_BIT)
	, mComputeCommandBuffers(getVulkanContext().device(), mComputeCommandPool, 1)
	, mExtensionSet(new DrawSystemExtensionSet())
	, mQuitRenderThread(false)
	, mRenderThread()
	, mRenderScene()
	, mFpsTimer()
	, mShaderStorageBufferAllocator(&context.device(), 65536)
	, mIndirectDrawBufferAllocator(&context.device(), 65536)
	, mUploadBufferAllocator(&context.device(), 65536)
	, mCullComputeSystem(context, "./shaders")
	, mCullPipeline()
{
	mFpsTimer.setTargetFps(60);

}

DrawSystem::~DrawSystem()
{
	if (mAsyncSubmit && mRenderThread.joinable())
	{
		mQuitRenderThread = true;
		mRenderThread.join();
	}
}

DrawSystem::DrawSystem(DrawSystem&& rhs)
	: ecs::System<DrawSystem>(std::move(rhs))
	, mActiveDrawCalls(std::move(rhs.mActiveDrawCalls))
	, mDevice(rhs.mDevice)
	, mDescriptorPool(std::move(rhs.mDescriptorPool))
	, mDrawSet(std::move(rhs.mDrawSet))
	, mShaders(std::move(rhs.mShaders))
	, mPipelines(std::move(rhs.mPipelines))
	, mRenderPasses(std::move(rhs.mRenderPasses))
	, mFrameBuffers(std::move(rhs.mFrameBuffers))
	, mExtensionSet(std::move(rhs.mExtensionSet))
	, mSystemState(rhs.mSystemState)
	, mFrameCounter(rhs.mFrameCounter)
	, mPendingImageIndex(rhs.mPendingImageIndex.load())
	, mSubmittedImageIndex(rhs.mSubmittedImageIndex.load())
	, mStartTime(rhs.mStartTime)
	, mRenderCommandPool(std::move(rhs.mRenderCommandPool))
	, mRenderCommandBuffers(std::move(rhs.mRenderCommandBuffers))
	, mExtraRenderCommandBuffers(std::move(rhs.mExtraRenderCommandBuffers))
	, mUploadCommandPool(std::move(rhs.mUploadCommandPool))
	, mUploadCommandBuffers(std::move(rhs.mUploadCommandBuffers))
	, mComputeCommandPool(std::move(rhs.mComputeCommandPool))
	, mComputeCommandBuffers(std::move(rhs.mComputeCommandBuffers))
	, mQuitRenderThread()
	, mRenderMutex()
	, mRenderThread()
	, mRenderScene(std::move(rhs.mRenderScene))
	, mShaderStorageBufferAllocator(std::move(rhs.mShaderStorageBufferAllocator))
	, mIndirectDrawBufferAllocator(std::move(rhs.mIndirectDrawBufferAllocator))
	, mUploadBufferAllocator(std::move(rhs.mUploadBufferAllocator))
	, mCullComputeSystem(std::move(rhs.mCullComputeSystem))
	, mCullPipeline(std::move(rhs.mCullPipeline))
{
	if (mAsyncSubmit && rhs.mRenderThread.joinable())
	{
		rhs.mQuitRenderThread = true;
		rhs.mRenderThread.join();
		mRenderThread = std::thread([this]{ renderThreadFunc(); });
	}
}


void DrawSystem::addExtension(size_t compIds, std::unique_ptr<IDrawSystemExtension>&& extension)
{
	mExtensionSet->extensions.emplace_back(std::make_tuple(compIds, std::move(extension)));
}


vulkanutils::ShaderSet& DrawSystem::getOrCreateShaderSet(const char* shaderName)
{
	auto shaderIt = mShaders.find(shaderName);
	if (shaderIt != mShaders.end())
	{
		return shaderIt->second;
	}
	return mShaders[shaderName] = vulkanutils::loadShaderSet(*mDevice, "./shaders", shaderName);
}

void DrawSystem::buildRenderPass(const ecs::Entity& entity, const SceneType& scene)
{
	if (getRenderPass(entity.getId()))
		return;

	const RenderSubPass& rsp = entity.getComponent<RenderSubPass>(scene.getComponentCollection());

	std::vector<vulkanutils::RenderPass::Attachement> attachments;
	std::vector<vulkanutils::RenderPass::RenderSubPass> subPasses(1);

	for (const RenderSubPass::Attachment& att : rsp.getAttachmentCompTypeIds())
	{
		if (att.bindingNr >= subPasses[0].attachments.size())
			subPasses[0].attachments.resize(att.bindingNr + 1, VK_ATTACHMENT_UNUSED );
		
		subPasses[0].attachments[att.bindingNr] = attachments.size();
		attachments.emplace_back(att.format, att.presentation);
	}
	
	vulkanutils::RenderPass renderPass(
		getVulkanContext().device(), rsp.width(), rsp.height(), attachments, subPasses);

	// TODO: Check order of attachments
	std::vector<RenderSubPass::ImageBinding> imageBindings = rsp.getEntityBindings(scene, entity);
	std::vector<VkImageView> vkImageViews;
	vkImageViews.reserve(imageBindings.size());
	for (auto& ib : imageBindings)
		vkImageViews.push_back(*ib.imageView);
	getFrameBuffer(entity.getId()) = vulkanutils::FrameBuffer(&getVulkanContext().device(), renderPass, 
		vkImageViews.data(), vkImageViews.size(),
		rsp.width(), rsp.height());
	getRenderPass(entity.getId()) = std::move(renderPass);
}


void DrawSystem::buildPipeline(const ecs::Entity& entity, const ecs::Entity& renderPassEntity, const SceneType& scene)
{
	vulkanutils::RenderPass& renderPass = getRenderPass(renderPassEntity.getId());
	const RenderSubPass& renderSubPass = renderPassEntity.getComponent<RenderSubPass>(scene.getComponentCollection());

	const auto& pipelines = entity.getComponentArray<component::Pipeline>(scene.getComponentCollection());
	for (const component::Pipeline& drawPipeline : pipelines)
	{
		if (!renderSubPass.compatible(drawPipeline))
			continue;
		vulkanutils::ShaderSet& shaderSet = getOrCreateShaderSet(drawPipeline.shaderName);
		std::vector<const vulkanutils::ResourceSet*> resourceSets;
		resourceSets.reserve(drawPipeline.sets.size());
		resourceSets.push_back(&mDrawSet);
		for (auto& set : drawPipeline.sets)
			resourceSets.push_back(&set);

		std::sort(std::begin(resourceSets), std::end(resourceSets), []
			(const vulkanutils::ResourceSet* rs1, const vulkanutils::ResourceSet* rs2)
			{
				return rs1->getDescriptorSetBindpoint() < rs2->getDescriptorSetBindpoint();
			});
		vulkanutils::GraphicsPipeline pipeline(getVulkanContext().device(), renderPass, 0, shaderSet, resourceSets, drawPipeline.cullMode);
		getPipeline(entity.getId(), renderPassEntity.getId()).emplace_back(std::move(pipeline));
	}
}


std::vector<vulkanutils::GraphicsPipeline>& DrawSystem::getPipeline(size_t pipelineEntityId, size_t renderPassEntityId)
{
	return mPipelines[{ pipelineEntityId, renderPassEntityId }];
}

const std::vector<vulkanutils::GraphicsPipeline>& DrawSystem::getPipeline(size_t pipelineEntityId, size_t renderPassEntityId) const
{
	return mPipelines.find({ pipelineEntityId, renderPassEntityId })->second;
}

vulkanutils::RenderPass& DrawSystem::getRenderPass(size_t entityId)
{
	return mRenderPasses[entityId];
}

const vulkanutils::RenderPass& DrawSystem::getRenderPass(size_t entityId) const
{
	return mRenderPasses.find(entityId)->second;
}

vulkanutils::FrameBuffer& DrawSystem::getFrameBuffer(size_t entityId)
{
	return mFrameBuffers[entityId];
}

const vulkanutils::FrameBuffer& DrawSystem::getFrameBuffer(size_t entityId) const
{
	return mFrameBuffers.find(entityId)->second;
}


void DrawSystem::updateSystemProperties(ecs::Entity& entity, SceneType& scene, vulkanutils::BufferCopier& bufferCopier)
{
	std::span<const component::SystemProperies> systemProperties = entity.getComponentArray<component::SystemProperies>(scene.getComponentCollection());

	if (!entity.hasGpuComponents<vulkanutils::UniformBuffer, component::SystemProperies>(scene.getGpuComponentCollection()))
	{
		entity.addGpuComponentArray<vulkanutils::UniformBuffer, component::SystemProperies>(scene.getGpuComponentCollection(), 2);
		entity.addGpuComponentArray<vulkanutils::UploadBuffer, component::SystemProperies>(scene.getGpuComponentCollection(), 2);
	}
	{
		auto hostData = entity.getHostGpuComponentArray<component::SystemProperies>(scene.getGpuComponentCollection());
		auto& deviceData = entity.getGpuComponentArray<vulkanutils::UniformBuffer, component::SystemProperies>(scene.getGpuComponentCollection());
		memcpy(hostData.getData().data(), systemProperties.data(), sizeof(component::SystemProperies));
		bufferCopier.queue(hostData, deviceData);
	}
}

void DrawSystem::upload(SceneType scene, size_t swapImageIdx)
{
	vulkanutils::CommandBufferRecorder cbRecorder(mUploadCommandBuffers[swapImageIdx], VK_COMMAND_BUFFER_USAGE_ONE_TIME_SUBMIT_BIT);
		
	vulkanutils::Barrier(mUploadCommandBuffers[swapImageIdx]).dependsOnVertexShader().doBeforeCopy().globalBarrier();
	auto hasComponent = [&scene]<typename... Ts>(ecs::Entity & entity)
	{
		return entity.hasComponents<Ts...>(scene.getComponentCollection());
	};

	// Update host buffers
	vulkanutils::BufferCopier bufferCopier(mUploadCommandBuffers[swapImageIdx]);

	for (auto& entity : scene.getEntities<RenderSubPass>())
		buildRenderPass(*entity, scene);

	for (auto& entity : scene.getAllEntities())
	{
		bool updated = false;
		bool handledByExtension = false;
		for (auto& extIt : mExtensionSet->extensions)
		{
			if (entity.hasComponentIds(std::get<0>(extIt)) && entity.isModified())
			{
				std::get<1>(extIt)->update(*this, entity, scene, bufferCopier);
				handledByExtension = true;
			}
		}
		if (handledByExtension)
			continue;

		if (SystemPropertiesComponents::apply(hasComponent, entity) && entity.isModified())
		{
			updateSystemProperties(entity, scene, bufferCopier);
			updated = true;
		}
		if (DrawPipelineComponents::apply(hasComponent, entity) && entity.isModified())
		{
			for (auto& [renderPassEntityId, renderPass] : mRenderPasses)
				buildPipeline(entity, *scene.getEntity(renderPassEntityId), scene);
			updated = true;
		}
		if (updated) entity.setUnmodified();
	}
	bufferCopier.execute();
	vulkanutils::Barrier(mUploadCommandBuffers[swapImageIdx]).dependsOnCopy().doBeforeVertexShader().globalBarrier();
}

void DrawSystem::setup(SceneType scene)
{
	std::vector<EntityResourceSet> sets;
	sets.emplace_back(cullComputeCameraSet(mDevice, scene, vulkanutils::DescriptorSetSlot::SLOT_0));
	sets.emplace_back(cullComputeSet(mDevice, vulkanutils::DescriptorSetSlot::SLOT_1));
	sets.emplace_back(cullComputeAABBSet(mDevice, scene, vulkanutils::DescriptorSetSlot::SLOT_2));
	mCullPipeline = mCullComputeSystem.createComputePipeline("cull", std::move(sets));

	upload(scene, 0);
	getVulkanContext().device().getGraphicsQueue().submit(
		{ mUploadCommandBuffers[mPendingImageIndex] }
		, &getVulkanContext().device().getComputeQueue()
	);

	auto subPassEntities = scene.getEntities<RenderSubPass>();
	for (auto& entity : subPassEntities)
	{
		const RenderSubPass& renderSubPass = entity->getComponent<RenderSubPass>(scene.getComponentCollection());
		if (!renderSubPass.isEnabledForAnyScreenBuffer())
			mExtraRenderCommandBuffers[entity->getId()] = vulkanutils::CommandBuffers(getVulkanContext().device(), mRenderCommandPool, vulkanutils::VulkanContext::NUM_SWAP_IMAGES);
	}

	recordCull(scene);
	for (size_t i = 0; i < vulkanutils::VulkanContext::NUM_SWAP_IMAGES; i++)
		recordDraw(scene, i);
}

void DrawSystem::draw(SceneType scene)
{
	if (!mRenderScene)
	{
		std::lock_guard<std::mutex> lock(mRenderMutex);
		mRenderScene.reset(new SceneType(scene));
		mPendingImageIndex = mSubmittedImageIndex.load();
	}
	if (mAsyncSubmit)
	{
		while (mSubmittedImageIndex == mPendingImageIndex)
			std::this_thread::sleep_for(std::chrono::microseconds(10));
	}
	else
	{
		uint32_t nextIdx = (mSubmittedImageIndex + 1) % 3;
		upload(*mRenderScene, nextIdx);
		uint32_t imageIdx = 0;
		while (!getVulkanContext().waitForNextImage(&imageIdx) && !mQuitRenderThread)
		{
			std::this_thread::sleep_for(std::chrono::microseconds(10));
		}
		if (!mQuitRenderThread)
		{
			submitFrame(imageIdx);
			mSubmittedImageIndex = imageIdx;
		}
	}
	mPendingImageIndex = (mPendingImageIndex + 1) % 3;
}

void DrawSystem::renderThreadFunc()
{
	while (!mQuitRenderThread)
	{
		std::this_thread::sleep_for(std::chrono::microseconds(10));
		if (!mRenderScene)
			continue;
		
		uint32_t imageIdx = 0;
		uint32_t nextIdx = (mSubmittedImageIndex + 1) % 3;
		if (nextIdx != mPendingImageIndex)
		{
			upload(*mRenderScene, nextIdx);
			while (!getVulkanContext().waitForNextImage(&imageIdx) && !mQuitRenderThread)
			{
				std::this_thread::sleep_for(std::chrono::microseconds(10));
			}
			if (!mQuitRenderThread)
			{
				mFpsTimer.tickWait();
				std::cout << "DRAW FPS: " << mFpsTimer.getFps() << std::endl;

				std::lock_guard<std::mutex> lock(mRenderMutex);
				submitFrame(imageIdx);
				mSubmittedImageIndex = imageIdx;
			}
		}
	}
}

void DrawSystem::submitFrame(uint32_t imageIdx)
{
	getVulkanContext().compute({ mComputeCommandBuffers[0] });

	std::vector<VkCommandBuffer> commandBuffers;
	for (auto& [_, cbufs] : mExtraRenderCommandBuffers)
		commandBuffers.push_back(cbufs[imageIdx]);

	commandBuffers.push_back(mRenderCommandBuffers[imageIdx]);
	commandBuffers.push_back(mUploadCommandBuffers[imageIdx]);
	getVulkanContext().render(imageIdx, commandBuffers);
}


void DrawSystem::runImpl(SceneType scene)
{
	switch (mSystemState)
	{
	case State::SETUP:
		{
			setup(scene);
			mSystemState = State::UPLOAD;
			break;
		}
	case State::UPLOAD:
		{
			getVulkanContext().device().getGraphicsQueue().wait();
			if (mAsyncSubmit)
				mRenderThread = std::thread([this]{ renderThreadFunc(); });
			mSystemState = State::RENDER;
			break;
		}
	case State::RENDER:
		{
			draw(scene);
			break;
		}
	default:
		break;
	};
}

void DrawSystem::recordCull(SceneType scene)
{
	auto& cc = scene.getComponentCollection();
	auto& gpuCc = scene.getGpuComponentCollection();

	std::vector<ecs::Entity*> modelEntities = scene.getEntities<component::Visibility, component::MeshAABoundingBox>();
	mActiveDrawCalls.clear();

	auto& commandBuffer = mComputeCommandBuffers[0];
	vulkanutils::CommandBufferRecorder cbRecorder(commandBuffer, VK_COMMAND_BUFFER_USAGE_SIMULTANEOUS_USE_BIT);
	for (ecs::Entity* entity : scene.getEntities<RenderSubPass>())
	{
		const RenderSubPass& renderSubPass = entity->getComponent<RenderSubPass>(cc);
		auto&& cdc = buildCombinedDrawCalls(modelEntities, entity->getId(), scene);
		CombinedDrawCall::uploadComponentOffsetBuffers(commandBuffer, cdc);
		mActiveDrawCalls[entity->getId()] = std::move(cdc);
	}

	for (auto& [renderSubPass, drawCalls] : mActiveDrawCalls)
	{
		for (CombinedDrawCall& drawCall : drawCalls)
		{
			if (drawCall.mBindings.find(&mCullPipeline.getComputeSets()[0]) == drawCall.mBindings.end())
				continue;
			if (drawCall.mBindings.find(&mCullPipeline.getComputeSets()[2]) == drawCall.mBindings.end())
				continue;

			for (const EntityResourceSet& set : mCullPipeline.getComputeSets())
			{
				vulkanutils::logger(Log::Severity::INFO) << "  Set \"" << set.getName() << "\" at  bind point " << (int)set.getDescriptorSetBindpoint() << std::endl;
				CombinedDrawCall::ResourceSetBinding& drawCallBindings = drawCall.mBindings[&set];
				if (vulkanutils::DescriptorSet& descriptorSet = drawCall.mDescriptorSets[&set]; !descriptorSet)
				{
					descriptorSet = set.createDescriptorSet(mDescriptorPool);
					for (auto& bb : drawCallBindings.uniformBindings)
						descriptorSet.bindBuffer<vulkanutils::UniformBuffer>(*bb.buffer, bb.bindingNr);
					for (CombinedDrawCall::BufferBinding& bb : drawCallBindings.shaderStorageBindings)
						descriptorSet.bindBuffer<vulkanutils::ShaderStorageBuffer>(bb.buffer, 0, VK_WHOLE_SIZE, bb.bindingNr);
				}
			}

			vulkanutils::DescriptorSet& descriptorSet = drawCall.mDescriptorSets[&mCullPipeline.getComputeSets()[1]];
			descriptorSet.bindBuffer(drawCall.mCullComponentOffsetDeviceBuffer, 0);
			descriptorSet.bindBuffer(drawCall.mClippingBuffer, 1);
			descriptorSet.bindBuffer(drawCall.mDrawCommandDeviceBuffer, 2);

			mCullPipeline.mComputePipeline.bind(commandBuffer);
			for (const EntityResourceSet& set : mCullPipeline.getComputeSets())
			{
				auto& descriptorSet = drawCall.mDescriptorSets[&set];
				descriptorSet.bindToCompute(commandBuffer, mCullPipeline.mComputePipeline.getLayout(), (uint32_t)set.getDescriptorSetBindpoint());
			}
			vkCmdDispatch(commandBuffer, drawCall.mNumInstances, 1, 1);

			vulkanutils::Barrier(commandBuffer).dependsOnCompute().doBeforeCopy().addBuffer(drawCall.mDrawCommandDeviceBuffer);
			vulkanutils::BufferCopier(commandBuffer).queue(drawCall.mDrawCommandDeviceBuffer, drawCall.mDrawCommandDeviceIdBuffer);
			//vulkanutils::Barrier(commandBuffer).dependsOnCopy().doBeforeVertexShader().addBuffer(drawCall.mDrawCommandDeviceIdBuffer);
		}
	}
}


void DrawSystem::recordDraw(SceneType scene, size_t swapImageIdx)
{
	std::vector<ecs::Entity*> modelEntities = scene.getEntities<component::Visibility, component::MeshAABoundingBox>();

	for (const ecs::Entity* entity : scene.getEntities<RenderSubPass>())
	{
		const RenderSubPass& renderSubPass = entity->getComponent<RenderSubPass>(scene.getComponentCollection());
		VkCommandBuffer commandBuffer = VK_NULL_HANDLE;
		if (renderSubPass.isEnabledForScreenBuffer(swapImageIdx)) 
			commandBuffer = mRenderCommandBuffers[swapImageIdx];
		else if (!renderSubPass.isEnabledForAnyScreenBuffer())
			commandBuffer = mExtraRenderCommandBuffers[entity->getId()][swapImageIdx];

		if (commandBuffer)
		{
			vulkanutils::CommandBufferRecorder cbRecorder(commandBuffer);
			recordDrawCalls(scene, commandBuffer, entity->getId(), mActiveDrawCalls[entity->getId()]);
		}
	}
}

void DrawSystem::setCullBinding(
	const SceneType& scene, const EntityResourceSet& set, const ecs::Entity* e, DrawCall& drawCall)
{
	auto& cc = scene.getComponentCollection();
	auto& gpuCc = scene.getGpuComponentCollection();

	vulkanutils::logger(Log::Severity::DEBUG) << "SET: " << set.getName() << std::endl;
	DrawCall::ResourceSetBinding& drawCallBindings = drawCall.cullBindings[&set];

	if (set.compatible<vulkanutils::UniformBuffer>(*e))
	{
		if (auto bufferBindings = set.getEntityBufferDescriptorBindings<vulkanutils::UniformBuffer>(scene, *e); !bufferBindings.empty())
		{
			vulkanutils::logger(Log::Severity::DEBUG) << "    Uniform Entity: " << e->getName() << std::endl;
			std::copy(bufferBindings.begin(), bufferBindings.end(), std::back_inserter(drawCallBindings.uniform));
		}
	}
	if (set.compatible<vulkanutils::ShaderStorageBuffer>(*e))
	{
		if (auto bufferBindings = set.getEntityBufferDescriptorBindings<vulkanutils::ShaderStorageBuffer>(scene, *e); !bufferBindings.empty())
		{
			vulkanutils::logger(Log::Severity::DEBUG) << "    Shader Storage Entity: " << e->getName() << std::endl;
			std::copy(bufferBindings.begin(), bufferBindings.end(), std::back_inserter(drawCallBindings.shaderStorage));
		}
	}
	if (set.compatible<vulkanutils::VertexBuffer>(*e))
	{
		if (auto bufferBindings = set.getEntityBufferVertexAttributeBindings(scene, *e); !bufferBindings.empty())
		{
			vulkanutils::logger(Log::Severity::DEBUG) << "    Vertex Entity: " << e->getName() << std::endl;
			std::copy(bufferBindings.begin(), bufferBindings.end(), std::back_inserter(drawCallBindings.vertexBuffer));
		}
	}
	if (set.compatibleImage(*e) && e->hasComponents<component::MaterialTextureView>(cc))
	{
		if (auto imageBindings = set.getEntityImageBindings(scene, *e); !imageBindings.empty())
		{
			vulkanutils::logger(Log::Severity::DEBUG) << "    Image Entity: " << e->getName() << std::endl;
			std::copy(imageBindings.begin(), imageBindings.end(), std::back_inserter(drawCallBindings.image));
		}
	}
}

void DrawSystem::setDrawCallBinding(
	const SceneType& scene, const EntityResourceSet& set, const ecs::Entity* e, DrawCall& drawCall)
{
	auto& cc = scene.getComponentCollection();
	auto& gpuCc = scene.getGpuComponentCollection();

	vulkanutils::logger(Log::Severity::DEBUG) << "SET: " << set.getName() << std::endl;
	DrawCall::ResourceSetBinding& drawCallBindings = drawCall.bindings[&set];

	if (set.compatible<vulkanutils::UniformBuffer>(*e))
	{
		if (auto bufferBindings = set.getEntityBufferDescriptorBindings<vulkanutils::UniformBuffer>(scene, *e); !bufferBindings.empty())
		{
			vulkanutils::logger(Log::Severity::DEBUG) << "    Uniform Entity: " << e->getName() << std::endl;
			std::copy(bufferBindings.begin(), bufferBindings.end(), std::back_inserter(drawCallBindings.uniform));
		}
	}
	if (set.compatible<vulkanutils::ShaderStorageBuffer>(*e))
	{
		if (auto bufferBindings = set.getEntityBufferDescriptorBindings<vulkanutils::ShaderStorageBuffer>(scene, *e); !bufferBindings.empty())
		{
			vulkanutils::logger(Log::Severity::DEBUG) << "    Shader Storage Entity: " << e->getName() << std::endl;
			std::copy(bufferBindings.begin(), bufferBindings.end(), std::back_inserter(drawCallBindings.shaderStorage));
		}
	}
	if (set.compatible<vulkanutils::VertexBuffer>(*e))
	{
		if (auto bufferBindings = set.getEntityBufferVertexAttributeBindings(scene, *e); !bufferBindings.empty())
		{
			vulkanutils::logger(Log::Severity::DEBUG) << "    Vertex Entity: " << e->getName() << std::endl;
			std::copy(bufferBindings.begin(), bufferBindings.end(), std::back_inserter(drawCallBindings.vertexBuffer));
		}
	}
	if (set.compatibleImage(*e) && e->hasComponents<component::MaterialTextureView>(cc))
	{
		if (auto imageBindings = set.getEntityImageBindings(scene, *e); !imageBindings.empty())
		{
			vulkanutils::logger(Log::Severity::DEBUG) << "    Image Entity: " << e->getName() << std::endl;
			std::copy(imageBindings.begin(), imageBindings.end(), std::back_inserter(drawCallBindings.image));
		}
	}
	if (set.getHasIndexBinding() && !drawCall.indexBuffer && e->hasGpuComponents<vulkanutils::IndexBuffer, component::MeshIndex>(gpuCc))
	{
		vulkanutils::logger(Log::Severity::DEBUG) << "    Index Entity: " << e->getName() << std::endl;
		drawCall.indexBuffer = &e->getGpuComponentArray<vulkanutils::IndexBuffer, component::MeshIndex>(gpuCc);
	}
	if (!drawCall.indirectDrawBuffer && e->hasGpuComponents<vulkanutils::IndirectDrawBuffer, VkDrawIndexedIndirectCommand>(gpuCc))
	{
		vulkanutils::logger(Log::Severity::DEBUG) << "    Indirect Draw Entity: " << e->getName() << std::endl;
		drawCall.indirectDrawBuffer = &e->getGpuComponentArray<vulkanutils::IndirectDrawBuffer, VkDrawIndexedIndirectCommand>(gpuCc);
	}
}

void DrawSystem::setDrawCallBindings(
	const SceneType& scene, const std::vector<EntityResourceSet>& sets, const ecs::Entity* e, DrawCall& drawCall)
{
	for (const EntityResourceSet& set : sets)
		setDrawCallBinding(scene, set, e, drawCall);
}

void DrawSystem::buildBaseDrawCall(
	const SceneType& scene, const component::Pipeline& pipeline, const ecs::Entity* e, DrawCall& drawCall)
{
	auto& cc = scene.getComponentCollection();
	auto& gpuCc = scene.getGpuComponentCollection();

	// Add bindings for this entity
	setDrawCallBindings(scene, pipeline.sets, e, drawCall);

	// Add bindings for child entities, up until (but not including) meshes
	auto childEntities = scene.getRefEntities(e, ecs::EntityRef::Type::Child);
	auto childEntities2 = scene.getRefEntities(e, ecs::EntityRef::Type::Link);
	std::copy(std::begin(childEntities2), std::end(childEntities2), std::back_inserter(childEntities));

	for (auto childEntity : childEntities)
		if (!childEntity->hasGpuComponents<vulkanutils::IndexBuffer, component::MeshIndex>(gpuCc))
			buildBaseDrawCall(scene, pipeline, childEntity, drawCall);
};

std::vector<DrawCall> DrawSystem::buildDrawCalls(
	const SceneType& scene, const component::Pipeline& pipeline, const ecs::Entity* e, const DrawCall& drawCall)
{
	std::vector<DrawCall> fullDrawCalls;
	auto& gpuCc = scene.getGpuComponentCollection();

	auto childEntities = scene.getRefEntities(e, ecs::EntityRef::Type::Child);
	for (auto ce : childEntities)
	{
		if (!ce->hasGpuComponents<vulkanutils::IndexBuffer, component::MeshIndex>(gpuCc))
		{
			auto newDrawCalls = buildDrawCalls(scene, pipeline, ce, drawCall);
			std::copy(std::begin(newDrawCalls), std::end(newDrawCalls), std::back_inserter(fullDrawCalls));
		}
		else
		{
			fullDrawCalls.emplace_back(drawCall);
			buildBaseDrawCall(scene, pipeline, ce, fullDrawCalls.back());
			if (!fullDrawCalls.back().indexBuffer)
				__debugbreak();
		}
	}
	return fullDrawCalls;
};

std::vector<CombinedDrawCall> DrawSystem::buildCombinedDrawCalls(const std::vector<ecs::Entity*>& drawableEntities, size_t renderPassEntityId, const SceneType& scene)
{
	auto& cc = scene.getComponentCollection();
	auto& gpuCc = scene.getGpuComponentCollection();

	const ecs::Entity* cameraEntity = nullptr;
	const ecs::Entity* systemEntity = nullptr;
	{
		std::vector<const ecs::Entity*> cameraEntities = scene.getEntities<component::CameraProjection, component::Position, component::Orientation>();
		if (cameraEntities.size() != 1)
			vulkanutils::logger(Log::Severity::WARNING) << "There must be exactly 1 camera to draw to ( " << cameraEntities.size() << " )" << std::endl;
		else
			cameraEntity = cameraEntities.front();

		std::vector<const ecs::Entity*> systemProperyEntities = scene.getEntities<component::SystemProperies>();
		if (systemProperyEntities.size() != 1)
			vulkanutils::logger(Log::Severity::WARNING) << "There must be exactly 1 systemProperty entity ( " << systemProperyEntities.size() << " )" << std::endl;
		else
			systemEntity = systemProperyEntities.front();
	}

	if (!cameraEntity || !systemEntity)
		return {};
	std::vector<const ecs::Entity*> lightScenes = scene.getEntities<component::LightScene>();

	const ecs::Entity* renderPassEntity = scene.getEntity(renderPassEntityId);
	std::vector<DrawCall> drawCalls;
	for (const ecs::Entity* entity : drawableEntities)
	{
		std::span<const component::MeshAABoundingBox> bboxes = entity->getComponentArray<component::MeshAABoundingBox>(scene.getComponentCollection());

		const auto& drawPipelineEntities = scene.getRefEntities<component::Pipeline>(entity, ecs::EntityRef::Type::Link);
		for (auto drawPipelineEntity : drawPipelineEntities)
		{
			if (getPipeline(drawPipelineEntity->getId(), renderPassEntityId).empty())
				continue;
			vulkanutils::logger(Log::Severity::DEBUG) << "\nPipeline " << drawPipelineEntity->getName() << std::endl;
			
			auto pipelines = drawPipelineEntity->getComponentArray<component::Pipeline>(cc);
			for (size_t i = 0; i < pipelines.size(); i++)
			{
				DrawCall baseDrawCall;
				baseDrawCall.pipelineEntity = drawPipelineEntity->getId();
				baseDrawCall.drawableEntity = entity->getId();
				baseDrawCall.renderPassEntity = renderPassEntityId;
				setDrawCallBindings(scene, pipelines[i].sets, systemEntity, baseDrawCall);
				setDrawCallBindings(scene, pipelines[i].sets, cameraEntity, baseDrawCall);
				buildBaseDrawCall(scene, pipelines[i], lightScenes.front(), baseDrawCall);
				setDrawCallBindings(scene, pipelines[i].sets, renderPassEntity, baseDrawCall); // pass render pass Entity also as input

				
				buildBaseDrawCall(scene, pipelines[i], entity, baseDrawCall);
				setCullBinding(scene, mCullPipeline.getComputeSets()[0], cameraEntity, baseDrawCall);
				setCullBinding(scene, mCullPipeline.getComputeSets()[2], entity, baseDrawCall);
				std::vector<DrawCall> dc = buildDrawCalls(scene, pipelines[i], entity, baseDrawCall);
				std::copy(std::begin(dc), std::end(dc), std::back_inserter(drawCalls));
			}
		}
	}

	for (DrawCall& drawCall : drawCalls)
		drawCall.sort();
	std::sort(std::begin(drawCalls), std::end(drawCalls), DrawCallDescriptorSetOrder());
	return std::move(CombinedDrawCall::build(scene, *this, renderPassEntityId, drawCalls));
}

void DrawSystem::recordDrawCalls(const SceneType& scene
	, VkCommandBuffer commandBuffer
	, size_t renderPassEntityId
	, std::vector<CombinedDrawCall>& drawCalls) 
{
	auto& cc = scene.getComponentCollection();
	auto& gpuCc = scene.getGpuComponentCollection();

	const ecs::Entity* renderPassEntity = scene.getEntity(renderPassEntityId);
	const RenderSubPass& renderSubPass = renderPassEntity->getComponent<RenderSubPass>(cc);
	
	// Bind Render Pass
	vulkanutils::RenderPass& vkRenderPass = getRenderPass(renderPassEntityId);
	vulkanutils::FrameBuffer& frameBuffer = getFrameBuffer(renderPassEntityId);
	vulkanutils::RenderPassRecorder rpRecorder(commandBuffer, vkRenderPass, frameBuffer, renderSubPass.getClearColorValues());
	vulkanutils::DescriptorSet* prevDescSet[(int)vulkanutils::DescriptorSetSlot::NUM_SLOTS] = {};

	const vulkanutils::GraphicsPipeline* prevGraphicsPipeline = nullptr;
	CombinedDrawCall* prevDrawCall = nullptr;
	for (size_t i = 0; i < drawCalls.size(); i++)
	{
		CombinedDrawCall& drawCall = drawCalls[i];

		const std::vector<vulkanutils::GraphicsPipeline>& graphicsPipelines = getPipeline(drawCall.mPipelineEntity, renderPassEntityId);
		const ecs::Entity* pipelineEntity = scene.getEntity(drawCall.mPipelineEntity);
		auto pipelines = pipelineEntity->getComponentArray<component::Pipeline>(cc);
		
		vulkanutils::logger(Log::Severity::INFO) << "\nBind pipeline " << pipelineEntity->getName() << std::endl;
		for (size_t j = 0; j < pipelines.size(); j++)
		{
			const vulkanutils::GraphicsPipeline& graphicsPipeline = graphicsPipelines[j];
			const component::Pipeline& pipeline = pipelines[j];
			if (!renderSubPass.compatible(pipeline))
				continue;
			if (!prevGraphicsPipeline || prevGraphicsPipeline != &graphicsPipeline)
				graphicsPipeline.bind(commandBuffer);

			{
				vulkanutils::DescriptorSet& descriptorSet = drawCall.mDescriptorSets[&mDrawSet];
				if (!descriptorSet)
				{
					descriptorSet = mDrawSet.createDescriptorSet(mDescriptorPool);
					descriptorSet.bindBuffer(drawCall.mComponentOffsetDeviceBuffer, 0);
					descriptorSet.bindBuffer(drawCall.mClippingBuffer, 1);
				}

				if (descriptorSet && prevDescSet[(uint32_t)mDrawSet.getDescriptorSetBindpoint()] != &descriptorSet)
				{
					descriptorSet.bindToGraphics(commandBuffer, graphicsPipeline.getLayout(), (uint32_t)mDrawSet.getDescriptorSetBindpoint());
					prevDescSet[(uint32_t)mDrawSet.getDescriptorSetBindpoint()] = &descriptorSet;
				}
			}

			size_t numIndices = 0;
			for (const EntityResourceSet& set : pipeline.sets)
			{
				vulkanutils::logger(Log::Severity::INFO) << "  Set \"" << set.getName() << "\" at  bind point " << (int) set.getDescriptorSetBindpoint() << std::endl;
				CombinedDrawCall::ResourceSetBinding* prevDrawCallBindings = (prevDrawCall ? &prevDrawCall->mBindings[&set] : nullptr);
				CombinedDrawCall::ResourceSetBinding& drawCallBindings = drawCall.mBindings[&set];
				vulkanutils::DescriptorSet& descriptorSet = drawCall.mDescriptorSets[&set];

				if (set.getDescriptorSetBindpoint() != vulkanutils::DescriptorSetSlot::NONE && !descriptorSet)
				{
					descriptorSet = set.createDescriptorSet(mDescriptorPool);

					//if (!prevDrawCallBindings || prevDrawCallBindings->bufferBindings != bufferBindingsList)
					{
						for (auto& bb : drawCallBindings.uniformBindings)
							descriptorSet.bindBuffer<vulkanutils::UniformBuffer>(*bb.buffer, bb.bindingNr);
					}
					//if (!prevDrawCallBindings || prevDrawCallBindings->bufferBindings != bufferBindingsList)
					{
						for (CombinedDrawCall::BufferBinding& bb : drawCallBindings.shaderStorageBindings)
							descriptorSet.bindBuffer<vulkanutils::ShaderStorageBuffer>(bb.buffer, 0, VK_WHOLE_SIZE, bb.bindingNr);
					}
					//if (!prevDrawCallBindings || prevDrawCallBindings->imageBindings != imageBindingList)
					{
						for (CombinedDrawCall::ImageBinding& ib : drawCallBindings.imageBindings)
							descriptorSet.bindImage(ib.imageView, ib.imageSampler, ib.bindingNr, ib.elementNr);
					}

					vulkanutils::logger(Log::Severity::INFO) << "    Uniform Buffers: " << drawCallBindings.uniformBindings.size() << " of "
						<< set.getDescriptorLayout().getDescriptorBindings<vulkanutils::UniformBuffer>().size() << std::endl;
					vulkanutils::logger(Log::Severity::INFO) << "    Shader Storage Buffers: " << drawCallBindings.shaderStorageBindings.size() << " of "
						<< set.getDescriptorLayout().getDescriptorBindings<vulkanutils::ShaderStorageBuffer>().size() << std::endl;
					vulkanutils::logger(Log::Severity::INFO) << "    Images: " << drawCallBindings.imageBindings.size() << " of "
						<< set.getDescriptorLayout().getDescriptorImageBindings().size() << std::endl;
				}

				std::vector<EntityResourceSet::VertexAttributeBinding>& vertexBindingList = drawCallBindings.vertexAttributeBindings;
				if (!prevDrawCallBindings || prevDrawCallBindings->vertexAttributeBindings != vertexBindingList)
					for (EntityResourceSet::VertexAttributeBinding& vab : vertexBindingList)
						vab.bind(commandBuffer, descriptorSet);
				vulkanutils::logger(Log::Severity::INFO) << "    VertexBuffer: " << vertexBindingList.size() << " of "
					<< set.getAttributeLayout().getVertexBindingDescriptions().size() << std::endl;

				if (set.getHasIndexBinding())
				{
					numIndices = set.bindIndexBuffer(commandBuffer, *drawCall.mIndexBuffer);
					vulkanutils::logger(Log::Severity::INFO) << "    Indices: " << numIndices << std::endl;
				}
				if (descriptorSet && prevDescSet[(uint32_t) set.getDescriptorSetBindpoint()] != &descriptorSet)
				{
					descriptorSet.bindToGraphics(commandBuffer, graphicsPipeline.getLayout(), (uint32_t) set.getDescriptorSetBindpoint());
					prevDescSet[(uint32_t) set.getDescriptorSetBindpoint()] = &descriptorSet;
				}
			}
			vulkanutils::logger(Log::Severity::INFO) << "Num indices: " << numIndices << std::endl;
			//vulkanutils::logger(Log::Severity::INFO) << "Num instances: " << numInstances << std::endl;

			
			if (true) //drawCall.mIndirectDrawBuffer)
			{
				vkCmdDrawIndexedIndirect(commandBuffer,
					*drawCall.mDrawCommandDeviceIdBuffer.getBuffer(),
					drawCall.mDrawCommandDeviceIdBuffer.getMemRange().offset,
					1,
					sizeof(VkDrawIndexedIndirectCommand));
			}
			else
			{
				vkCmdDrawIndexed(commandBuffer,
					uint32_t(numIndices),
					drawCall.mNumInstances,
					0, 0, 0);
			}

			prevGraphicsPipeline = &graphicsPipeline;
			prevDrawCall = &drawCall;
		}
	}
}

void DrawSystem::cull(SceneType& scene)
{
	auto& cc = scene.getComponentCollection();
	auto& gpuCc = scene.getGpuComponentCollection();

	ecs::Entity* cullingEntity = scene.getEntity(mCullingEntity);
	auto aabbEntities = scene.getEntities<component::MeshAABoundingBox, component::Position>();
	for (ecs::Entity* aabbEntity : aabbEntities)
	{
		std::span<const component::Position> positions = aabbEntity->getComponentArray<component::Position>(cc);
		const component::Position& position = (!positions.empty()) ? positions[0] : Eigen::Vector3f::Zero();
		std::span<const component::Orientation> orientations = aabbEntity->getComponentArray<component::Orientation>(cc);
		const component::Orientation& orientation = (!orientations.empty()) ? orientations[0] : Eigen::Matrix3f::Identity();
		std::span<const component::Scale> scales = aabbEntity->getComponentArray<component::Scale>(cc);
		const component::Scale& scale = (!scales.empty()) ? scales[0] : Eigen::Vector3f::Ones();
		Eigen::Affine3f transform; transform.fromPositionOrientationScale(position, orientation, scale);

		std::span<const component::MeshAABoundingBox> bboxes = aabbEntity->getComponentArray<component::MeshAABoundingBox>(cc);
		if (!aabbEntity->hasGpuComponents<vulkanutils::ShaderStorageBuffer, component::MeshAABoundingBox>(gpuCc))
		{
			aabbEntity->addHostGpuComponentArray<component::MeshAABoundingBox>(gpuCc, bboxes.size());
			aabbEntity->addGpuComponentArray<vulkanutils::ShaderStorageBuffer, component::MeshAABoundingBox>(gpuCc, bboxes.size());
		}
		if (!aabbEntity->hasGpuComponents<vulkanutils::ShaderStorageBuffer, component::Transform>(gpuCc))
		{
			aabbEntity->addHostGpuComponentArray<component::Transform>(gpuCc, 1);
			aabbEntity->addGpuComponentArray<vulkanutils::ShaderStorageBuffer, component::Transform>(gpuCc, 1);
		}
	}
}
