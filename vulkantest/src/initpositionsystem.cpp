#include "initpositionsystem.h"

#include <chrono>
#include <random>

InitPositionsSystem::InitPositionsSystem()
{
}

void InitPositionsSystem::run(SceneType scene)
{
	uint64_t seed = std::chrono::system_clock::now().time_since_epoch().count();
	std::default_random_engine generator((unsigned int)seed);
	std::uniform_real_distribution<float> distribution(-0.5f, 0.5f);

	for (auto& entity : scene.getAllEntities())
	{
		bool hasRain = entity.hasComponents<component::Rain>(scene.getComponentCollection());
		bool hasPosition = entity.hasComponents<component::Position>(scene.getComponentCollection());
		bool hasOrientation = entity.hasComponents<component::Orientation>(scene.getComponentCollection());

		auto parentEntities = scene.getRefEntities<component::Position, component::Orientation>(&entity, ecs::EntityRef::Type::Parent);

		if (!parentEntities.empty())
		{
			ecs::Entity* parentEntity = parentEntities.front();
			const auto& [ parentPosition, parentOrientation ] = parentEntity->getComponents<component::Position, component::Orientation>(scene.getComponentCollection());

			auto parentTransform(Eigen::Transform<float, 3, Eigen::Affine>::Identity());
			parentTransform.translate(parentPosition);
			parentTransform.rotate(parentOrientation);
				
			if (parentEntity->hasComponents<component::Surface<float>>(scene.getComponentCollection()))
			{
				auto& surface = parentEntity->getComponent<component::Surface<float>>(scene.getComponentCollection());
				if (hasRain)
				{
					auto& rain = entity.getComponent<component::Rain>(scene.getComponentCollection());
					rain.width = surface.getXSize();
					rain.depth = surface.getYSize();
					if (hasPosition)
					{
						auto& position = entity.getComponent<component::Position>(scene.getComponentCollection());
						position.y() = 0;
					}
				}
				else
				{
					auto surfaceHeightFunc = [](float h){return h;};
					float surfaceX = distribution(generator) * surface.getXSize();
					float surfaceY = distribution(generator) * surface.getYSize();
					float surfaceHeight = surface.get(surfaceX, surfaceY, surfaceHeightFunc);
					parentTransform.translate(Eigen::Vector3f(surfaceX, surfaceHeight, surfaceY));
					parentTransform.rotate(Eigen::AngleAxisf(-90.0f / 180.0f * 3.14f, Eigen::Vector3f::UnitX()));

				}
			}
				

			if (hasOrientation)
			{
				auto& orientation = entity.getComponent<component::Orientation>(scene.getComponentCollection());
				orientation *= parentTransform.matrix().topLeftCorner<3, 3>();
			}
			if (hasPosition)
			{
				auto& position = entity.getComponent<component::Position>(scene.getComponentCollection());
				position = parentTransform.matrix().topRightCorner<3, 1>();
			}
		}
		entity.setModified();
	}
}

