#include "drawwatersystem.h"
#include "watersurface.h"


typedef ecs::ComponentParamPack<component::MeshVertex, component::MeshNormal, WaterDepth, component::MeshIndex>
	WaterMeshComponents;


DrawWaterSystem::DrawWaterSystem()
{
}

size_t DrawWaterSystem::requiredComponents(const DrawSystem::SceneType& scene) const
{
	return WaterMeshComponents::componentTypes(scene.getComponentCollection());
}

void DrawWaterSystem::update(
	DrawSystem& ds,
	ecs::Entity& entity,
	DrawSystem::SceneType& scene,
	vulkanutils::BufferCopier& bufferCopier)
{
	ResourceAccessor dsAccessor(ds);

	const auto vertices = entity.getComponentArray<component::MeshVertex>(scene.getComponentCollection());
	const auto normals = entity.getComponentArray<component::MeshNormal>(scene.getComponentCollection());
	const auto waterDepths = entity.getComponentArray<WaterDepth>(scene.getComponentCollection());
	const auto vertexIndices = entity.getComponentArray<component::MeshIndex>(scene.getComponentCollection());

	const size_t bytesPerVertex = sizeof(component::MeshVertex) + sizeof(component::MeshNormal) 
		+ sizeof(WaterDepth);
	const size_t vertexBufferSize = vertices.size() * bytesPerVertex;
	const size_t vertexIndexBufferSize = vertexIndices.size() * sizeof(component::MeshIndex);

	if (!entity.hasGpuComponents<vulkanutils::IndexBuffer, component::MeshIndex>(scene.getGpuComponentCollection()))
	{
		entity.addGpuComponentArray<vulkanutils::UploadBuffer, component::Raw>(scene.getGpuComponentCollection(), vertexBufferSize);
		entity.addGpuComponentArray<vulkanutils::VertexBuffer, component::Raw>(scene.getGpuComponentCollection(), vertexBufferSize);

		entity.addGpuComponentArray<vulkanutils::UploadBuffer, component::MeshIndex>(scene.getGpuComponentCollection(), vertexIndices.size());
		entity.addGpuComponentArray<vulkanutils::IndexBuffer, component::MeshIndex>(scene.getGpuComponentCollection(), vertexIndices.size());
	}
	{
		auto hostData = entity.getHostGpuComponentArray<component::Raw>(scene.getGpuComponentCollection());
		auto& deviceData = entity.getGpuComponentArray<vulkanutils::VertexBuffer, component::Raw>(scene.getGpuComponentCollection());
		std::span<component::Raw> vertexBufferPtr = hostData.getData();
		for (size_t i = 0; i < vertices.size(); i++)
		{
			size_t byteOffset = 1;
			float* vertexPtr = (float*)(&vertexBufferPtr[i * bytesPerVertex + byteOffset]);
			vertexPtr[0] = vertices[i].point[0];
			vertexPtr[1] = vertices[i].point[1];
			vertexPtr[2] = vertices[i].point[2];
			byteOffset += 3 * sizeof(float);
			float* normalPtr = (float*)(&vertexBufferPtr[i * bytesPerVertex + byteOffset]);
			normalPtr[0] = normals[i].normal[0];
			normalPtr[1] = normals[i].normal[1];
			normalPtr[2] = normals[i].normal[2];
			byteOffset += 3 * sizeof(float);
			float* depthPtr = (float*)(&vertexBufferPtr[i * bytesPerVertex + byteOffset]);
			depthPtr[0] = waterDepths[i].depth;
			byteOffset += 1 * sizeof(float);
		}
		bufferCopier.queue(hostData, deviceData);
	}
	{
		auto hostData = entity.getHostGpuComponentArray<component::MeshIndex>(scene.getGpuComponentCollection());
		auto& deviceData = entity.getGpuComponentArray<vulkanutils::IndexBuffer, component::MeshIndex>(scene.getGpuComponentCollection());
		memcpy(hostData.getData().data(), vertexIndices.data(), vertexIndices.size_bytes());
		bufferCopier.queue(hostData, deviceData);
	}
}


void DrawWaterSystem::draw(
	DrawSystem& ds,
	VkCommandBuffer commandBuffer,
	const std::vector<ecs::Entity>& waterEntities,
	const DrawSystem::SceneType& scene)
{
}
