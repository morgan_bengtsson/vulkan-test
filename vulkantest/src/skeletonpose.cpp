#include "skeletonpose.h"

namespace skeleton
{



SkeletonPose::BoneTransform::BoneTransform(const Eigen::Matrix4f& matrix)
	: BoneTransform()
{
	Eigen::Affine3f affineTransform(matrix);
	Eigen::Map<Eigen::Vector3f, Eigen::Aligned16> pos(position);
	pos = affineTransform.translation().head<3>();

	Eigen::Matrix3f scaling;
	Eigen::Map<Eigen::Quaternionf, Eigen::Aligned16> quatMap(quaternion);
	affineTransform.computeRotationScaling(&quatMap, &scaling);
	Eigen::Map<Eigen::Vector3f, Eigen::Aligned16> sc(scale);
	sc = scaling.diagonal();
}

SkeletonPose::BoneTransform::BoneTransform(float position_[4], float quaternion_[4], float scale_[3])
{
	std::memcpy(quaternion, quaternion_, sizeof(quaternion));
	std::memcpy(position, position_, sizeof(position));
	std::memcpy(scale, scale_, sizeof(scale));
}

SkeletonPose::BoneTransform::BoneTransform(const BoneTransform& prev, const BoneTransform& next, float progress)
{
	interpolateRot<Eigen::Aligned16>(prev.quaternion, next.quaternion, progress, quaternion);
	interpolatePos<Eigen::Aligned16>(prev.position, next.position, progress, position);
	interpolateScale<Eigen::Aligned16>(prev.scale, next.scale, progress, scale);
}

void SkeletonPose::BoneTransform::setInterpolate(const BoneTransform& prev, const BoneTransform& next, float progress)
{
	interpolateRot<Eigen::Aligned16>(prev.quaternion, next.quaternion, progress, quaternion);
	interpolatePos<Eigen::Aligned16>(prev.position, next.position, progress, position);
	interpolateScale<Eigen::Aligned16>(prev.scale, next.scale, progress, scale);
}

SkeletonPose::BoneTransform::BoneTransform()
	:quaternion()
	, position()
	, scale()
{}


Eigen::Matrix4f SkeletonPose::BoneTransform::getMatrix() const
{
	Eigen::Matrix4f transform(Eigen::Matrix4f::Identity());
	transform.topLeftCorner<3, 3>().noalias()
		= Eigen::Map<const Eigen::Quaternionf, Eigen::Aligned16>(quaternion).toRotationMatrix()
			* Eigen::Map<const Eigen::Vector3f, Eigen::Aligned16>(scale).asDiagonal();
	transform.topRightCorner<3, 1>() = Eigen::Map<const Eigen::Vector3f, Eigen::Aligned16>(position);
	return transform;
};

};
