#include "camerasystem.h"
#include "componenttypes.h"

#include <vulkanBufferCopier.h>
#include <vulkanBarrier.h>

#include <ranges>
#include <utility>

CameraSystem::CameraSystem(vulkanutils::VulkanContext& context, SceneType scene)
	: System<CameraSystem>(context)
	, mCommandPool(context.device(), context.device().getGraphicsQueue(), VK_COMMAND_POOL_CREATE_RESET_COMMAND_BUFFER_BIT)
	, mUploadCommandBuffers(getVulkanContext().device(), mCommandPool, 3)
	, mLastSignalledValue(3, 0)
	, mCmdBufferIdx(0)
{}

void CameraSystem::setup(SceneType scene)
{
	auto& cc = scene.getComponentCollection();
	auto& gpuCc = scene.getGpuComponentCollection();

	auto entities = scene.getEntities<component::CameraProjection, component::Orientation, component::Position>();

	for (size_t i = 0; i < 3; i++)
	{
		auto& commandBuffer = mUploadCommandBuffers[i];

		vulkanutils::CommandBufferRecorder cbRecorder(commandBuffer, VK_COMMAND_BUFFER_USAGE_SIMULTANEOUS_USE_BIT);
		vulkanutils::Barrier bufferBarrier(commandBuffer);
		bufferBarrier.dependsOnCopy().doBeforeVertexShader();
		vulkanutils::BufferCopier bufferCopier(commandBuffer);

		for (auto entity : entities)
		{
			if (!entity->hasGpuComponents<vulkanutils::UniformBuffer, component::Transform, component::CameraProjection>(gpuCc))
			{
				entity->addGpuComponentArrays<vulkanutils::UniformBuffer, component::Transform>(gpuCc, {1, 1});
				entity->addHostGpuComponentArrays<component::Transform>(gpuCc, {1, 1});

				entity->addGpuComponentArray<vulkanutils::UniformBuffer, component::CameraProjection>(gpuCc, 1);
				entity->addHostGpuComponentArray<component::CameraProjection>(gpuCc, 1);
			}

			{
				auto hostProjectionTransform = entity->getHostGpuComponentArray<component::CameraProjection>(gpuCc);
				auto& deviceProjectionTransform = entity->getGpuComponentArray<vulkanutils::UniformBuffer, component::CameraProjection>(gpuCc);
				bufferCopier.queue(hostProjectionTransform, deviceProjectionTransform);
				bufferBarrier.addBuffer(deviceProjectionTransform);

				auto hostViewTransform = entity->getHostGpuComponentArray<component::Transform>(gpuCc, 0);
				auto& deviceViewTransform = entity->getGpuComponentArray<vulkanutils::UniformBuffer, component::Transform>(gpuCc, 0);
				bufferCopier.queue(hostViewTransform, deviceViewTransform);
				bufferBarrier.addBuffer(deviceViewTransform);

				auto hostProjectionViewTransform = entity->getHostGpuComponentArray<component::Transform>(gpuCc, 1);
				auto& deviceProjectionViewTransform = entity->getGpuComponentArray<vulkanutils::UniformBuffer, component::Transform>(gpuCc, 1);
				bufferCopier.queue(hostProjectionViewTransform, deviceProjectionViewTransform);
				bufferBarrier.addBuffer(deviceProjectionViewTransform);

			}
		}
	}
	mSetup = true;
}

void CameraSystem::runImpl(SceneType scene)
{
	auto& cc = scene.getComponentCollection();
	auto& gpuCc = scene.getGpuComponentCollection();

	auto& commandBuffer = mUploadCommandBuffers[mCmdBufferIdx];
	if (!mSetup)
		setup(scene);

	if (!getVulkanContext().device().getGraphicsQueue().wait(getVulkanContext().device(), mLastSignalledValue[mCmdBufferIdx], 0))
		return;
	getVulkanContext().device().getGraphicsQueue().submit({ commandBuffer }, nullptr);

	mLastSignalledValue[mCmdBufferIdx] = getVulkanContext().device().getGraphicsQueue().getSignalValue();
	mCmdBufferIdx = (mCmdBufferIdx + 1) % 3;

	auto entities = scene.getEntities<component::CameraProjection, component::Orientation, component::Position>();
	for (auto entity : entities)
	{
		//if (entity->isModified())
		//	continue;
		const component::Position& position = entity->getComponent<component::Position>(cc);
		const component::Orientation& orientation = entity->getComponent<component::Orientation>(cc);
		const component::CameraProjection& projection = entity->getComponent<component::CameraProjection>(cc);
		
		auto hostProjectionTransform = entity->getHostGpuComponentArray<component::CameraProjection>(gpuCc, 0);
		hostProjectionTransform.getData()[0] = projection;

		auto hostViewTransform = entity->getHostGpuComponentArray<component::Transform>(gpuCc);
		Eigen::Map<Eigen::Matrix4f> viewTransform(hostViewTransform.getData()[0].matrix);
		viewTransform.setIdentity();
		viewTransform.topLeftCorner<3, 3>() = orientation.transpose();
		viewTransform.topRightCorner<3, 1>() = -orientation.transpose() * position;

		auto hostProjectionViewTransform = entity->getHostGpuComponentArray<component::Transform>(gpuCc, 1);
		Eigen::Map<Eigen::Matrix4f> viewProjectionTransform(hostProjectionViewTransform.getData()[0].matrix);
		viewProjectionTransform = projection * viewTransform;
	}
}
