#include "surface.h"
#include "perlinnoise.h"

namespace component
{

float PerlinSurface::getHeight(float x, float y) const
{
	float px = x / mXSize + 0.5f;
	float py = y / mYSize + 0.5f;
	return getHeightNormalized(px, py);
}

std::array<float, 2> PerlinSurface::dxdy(float x, float y) const
{
	float px = x / mXSize + 0.5f;
	float py = y / mYSize + 0.5f;
	auto res = getDxDyNormalized(px, py);
	res[0] /= mXSize;
	res[1] /= mYSize;
	return res;
}


float PerlinSurface::getXSize() const
{
	return mXSize;
}

float PerlinSurface::getYSize() const
{
	return mYSize;
}

std::array<float, 2> PerlinSurface::getDxDyNormalized(float xNorm, float yNorm) const
{
	float px = xNorm * NOISE_RESOLUTION;
	float py = yNorm * NOISE_RESOLUTION;

	std::array<float, 2> res = { 0.0f, 0.0f };
	float sum = 0;
	float d = 1.0f;
	for (size_t pnIter = 0; pnIter < NOISE_LEVELS; pnIter++)
	{
		std::array<float, 2> dxdy = mNoise.getDxDy(px * d, py * d);
		res[0] += dxdy[0] / d;
		res[1] += dxdy[1] / d;
		
		sum += 1.0f / d;
		d *= 2.0f;
	}
	return { res[0] * mHeightScale / sum, res[1] * mHeightScale / sum };
}


float PerlinSurface::getHeightNormalized(float xNorm, float yNorm) const
{
	float px = xNorm * NOISE_RESOLUTION;
	float py = yNorm * NOISE_RESOLUTION;

	float h = 0;
	float sum = 0;
	float d = 1.0f;
	for (size_t pnIter = 0; pnIter < NOISE_LEVELS; pnIter++)
	{
		h += mNoise.get(px * d, py * d) / d;
		sum += 1.0f / d;
		d *= 2.0f;
	}
	return h * mHeightScale / sum;
}

PerlinSurface::PerlinSurface(float heightScale
	, float xSize, float ySize)
	: mXSize(xSize)
	, mYSize(ySize)
	, mNoise()
	, mHeightScale(heightScale)
{
}

Surface<float> PerlinSurface::toSurface(float cellSize) const
{
	Surface<float> ds(getXSize(), getYSize(), cellSize);
	std::span<float> data = ds.getDiscrete().data();
	
	for (size_t r = 0; r < ds.getDiscrete().height(); r++)
	{
		for (size_t c = 0; c < ds.getDiscrete().width(); c++)
		{
			ds.getDiscrete()(c, r) = getHeight(ds.getXCoord(c), ds.getYCoord(r));
		}
	}
	return ds;
}

}


void calcSurfaceNormals(CalcSurfaceNormalsCCType components, ecs::Entity& meshEntity)
{
	std::span<component::MeshVertex> vertices
		= meshEntity.getComponentArray<component::MeshVertex>(components);
	std::span<component::MeshNormal> normals
		= meshEntity.getComponentArray<component::MeshNormal>(components);
	std::span<component::MeshIndex> vertexIndices
		= meshEntity.getComponentArray<component::MeshIndex>(components);

	for (auto& normal : normals)
	{
		normal.normal[0] = 0;
		normal.normal[1] = 0;
		normal.normal[2] = 0;
	}

	const size_t numFaces = vertexIndices.size() / 3;
	for (uint32_t i = 0; i < numFaces; i++)
	{
		uint32_t idx[3] =
		{
			vertexIndices[i * 3].idx,
			vertexIndices[i * 3 + 1].idx,
			vertexIndices[i * 3 + 2].idx
		};
		Eigen::Map<Eigen::Vector3f> point1(vertices[idx[0]].point);
		Eigen::Map<Eigen::Vector3f> point2(vertices[idx[1]].point);
		Eigen::Map<Eigen::Vector3f> point3(vertices[idx[2]].point);

		Eigen::Vector3f n = (point1 - point2).cross(point3 - point1).normalized();
		Eigen::Map<Eigen::Vector3f> n1(normals[idx[0]].normal);
		Eigen::Map<Eigen::Vector3f> n2(normals[idx[1]].normal);
		Eigen::Map<Eigen::Vector3f> n3(normals[idx[2]].normal);
		if (n.y() < 0) n = -n;

		n1 += n; n2 += n; n3 += n;
	}
	for (uint32_t i = 0; i < vertices.size(); i++)
	{
		Eigen::Map<Eigen::Vector3f> n(normals[i].normal);
		float nNorm = n.norm();
		if (nNorm > 0.001)
			n.normalize();
		else
			n = Eigen::Vector3f(0, 1, 0);
	}
}

ecs::Entity createFlatSurfaceMesh(
	CreateFlatSurfaceMeshCCType components,
	float xSize, float ySize,
	float gridScale)
{
	ecs::Entity meshEntity("SurfaceMesh");

	float xoffset = gridScale / std::sqrt(3.0f);
	float xdiff = 2.0f * xoffset;
	float zdiff = gridScale;

	uint32_t resHeight = size_t(ySize / zdiff);
	uint32_t resWidth = size_t(xSize / xdiff);
	uint32_t numPoints = (resHeight + 1) * (resWidth + 1);
	uint32_t numFaces = (resHeight * resWidth) * 2;

	// Set vertices
	component::MeshVertex* vertices
		= meshEntity.addComponentArray<component::MeshVertex>(components, numPoints);
	component::MeshNormal* normals
		= meshEntity.addComponentArray<component::MeshNormal>(components, numPoints);
	component::MeshIndex* vertexIndices
		= meshEntity.addComponentArray<component::MeshIndex>(components, numFaces * 3);

	for (uint32_t j = 0; j <= resHeight; j++)
	{
		uint32_t pointIdx = j * (resWidth + 1);
		float z = (float(j) - resHeight / 2) * zdiff;
		for (uint32_t i = 0; i <= resWidth; i++)
		{
			float x = (float(i) - resWidth / 2) * xdiff + (j & 0x01) * xoffset;
			vertices[pointIdx + i] = { x, 0, z };
			normals[pointIdx + i] = { 0, 0, 0 };
		}
	}
	for (uint32_t j = 0; j < resHeight; j++)
	{
		uint32_t pointIdx = j * (resWidth + 1);
		uint32_t triangleIdx = j * (resWidth);
		for (uint32_t i = 0; i < resWidth; i++)
		{
			if ((j & 0x01) == 0)
			{
				vertexIndices[(triangleIdx + i) * 6 + 0].idx = pointIdx + i;
				vertexIndices[(triangleIdx + i) * 6 + 1].idx = pointIdx + i + 1;
				vertexIndices[(triangleIdx + i) * 6 + 2].idx = pointIdx + i + resWidth + 1;

				vertexIndices[(triangleIdx + i) * 6 + 3].idx = pointIdx + i + resWidth + 2;
				vertexIndices[(triangleIdx + i) * 6 + 4].idx = pointIdx + i + resWidth + 1;
				vertexIndices[(triangleIdx + i) * 6 + 5].idx = pointIdx + i + 1;
			}
			else
			{
				vertexIndices[(triangleIdx + i) * 6 + 0].idx = pointIdx + i;
				vertexIndices[(triangleIdx + i) * 6 + 1].idx = pointIdx + i + resWidth + 2;
				vertexIndices[(triangleIdx + i) * 6 + 2].idx = pointIdx + i + resWidth + 1;

				vertexIndices[(triangleIdx + i) * 6 + 3].idx = pointIdx + i + 1;
				vertexIndices[(triangleIdx + i) * 6 + 4].idx = pointIdx + i + resWidth + 2;
				vertexIndices[(triangleIdx + i) * 6 + 5].idx = pointIdx + i;
			}
		}
	}
	calcSurfaceNormals(components, meshEntity);
	return meshEntity;
}

// WIP Extend for component::Surface
ecs::Entity createSurfaceMesh(
	CreateSurfaceMeshCCType components,
	const component::Surface<float>& surface,
	float gridScale, bool hasTexCoords, bool normalizedTexCoords)
{
	ecs::Entity meshEntity = createFlatSurfaceMesh(components, surface.getXSize(), surface.getYSize(), gridScale);

	std::span<component::MeshVertex> vertices
		= meshEntity.getComponentArray<component::MeshVertex>(components);
	std::span<component::MeshNormal> normals
		= meshEntity.getComponentArray<component::MeshNormal>(components);
	std::span<component::MeshIndex> vertexIndices
		= meshEntity.getComponentArray<component::MeshIndex>(components);

	component::MeshTexCoord* texCoords = nullptr;
	if (hasTexCoords)
		texCoords = meshEntity.addComponentArray<component::MeshTexCoord>(components, vertices.size());

	float xoffset = gridScale / std::sqrt(3.0f);
	float xdiff = 2.0f * xoffset;
	float zdiff = gridScale;

	uint32_t resHeight = size_t(surface.getYSize() / zdiff);
	uint32_t resWidth = size_t(surface.getXSize() / xdiff);

	for (uint32_t j = 0; j <= resHeight; j++)
	{
		uint32_t pointIdx = j * (resWidth + 1);
		for (uint32_t i = 0; i <= resWidth; i++)
		{
			float x = vertices[pointIdx + i].point[0];
			float z = vertices[pointIdx + i].point[2];
			float y = surface.get(x, z, [](float h){return h;});
			vertices[pointIdx + i].point[1] = y;
			if (hasTexCoords)
			{
				if (normalizedTexCoords)
					texCoords[pointIdx + i] = { x / surface.getXSize() + 0.5f, z / surface.getYSize() + 0.5f };
				else
					texCoords[pointIdx + i] = { x / xdiff, z / zdiff };
			}
		}
	}
	calcSurfaceNormals(components, meshEntity);
	return meshEntity;
}

