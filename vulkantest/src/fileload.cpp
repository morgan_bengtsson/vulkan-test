#include "fileload.h"

#include "skeleton.h"

#include <assimp/Importer.hpp>
#include <assimp/scene.h>
#include <assimp/postprocess.h>

#include <filesystem>


std::tuple<std::vector<ecs::Entity>, std::vector<ecs::Entity> > loadFile(
	const char* file,
	FileLoadComponentCollection collection,
	vulkanutils::Device* dev)
{
	std::filesystem::path filePath(std::filesystem::canonical(std::filesystem::path(file)));
	Assimp::Importer fileImporter;
	const aiScene* scene = fileImporter.ReadFile(filePath.string().c_str(), aiProcess_Triangulate);
	if (!scene)
	{
		vulkanutils::logger(Log::Severity::ERROR) << "Failed to load file " << filePath << std::endl;
		return {};
	}
	vulkanutils::logger(Log::Severity::INFO) << "Loaded file " << filePath << std::endl;

	size_t numTextures = 0;
	size_t numMaterials = scene->mNumMaterials;
	std::vector<ecs::Entity> materialEntities(numMaterials);
	for (size_t i = 0; i < numMaterials; i++)
	{
		materialEntities[i] = ecs::Entity(("Material:" + filePath.filename().string() + ":" + std::to_string(i)).c_str());
		component::MaterialProperties& mp = materialEntities[i].addComponent<component::MaterialProperties>(collection);
		
		const aiMaterial& material = *scene->mMaterials[i];
		aiColor4D aiColor;
		material.Get(AI_MATKEY_COLOR_EMISSIVE, aiColor);
		for (size_t i = 0; i < 4; i++) mp.emissive[i] = aiColor[(uint32_t) i];
		material.Get(AI_MATKEY_COLOR_AMBIENT, aiColor);
		for (size_t i = 0; i < 4; i++) mp.ambient[i] = aiColor[(uint32_t) i];
		material.Get(AI_MATKEY_COLOR_DIFFUSE, aiColor);
		for (size_t i = 0; i < 4; i++) mp.diffuse[i] = aiColor[(uint32_t) i];
		material.Get(AI_MATKEY_COLOR_SPECULAR, aiColor);
		for (size_t i = 0; i < 3; i++) mp.specular[i] = aiColor[(uint32_t) i];
		material.Get(AI_MATKEY_SHININESS, mp.specular[3]);


		aiString textureFilePath;
		material.GetTexture(aiTextureType::aiTextureType_DIFFUSE, 0, &textureFilePath);
		if (textureFilePath.length > 0)
		{
			auto fullFilePath = filePath.parent_path() / textureFilePath.C_Str();
			if (std::filesystem::is_regular_file(fullFilePath))
			{
				component::MaterialTexture& mt = materialEntities[i].addComponent<component::MaterialTexture>(collection);
				std::string texturePath = (fullFilePath).string();
				std::strncpy(mt.name, texturePath.c_str(), sizeof(mt.name));
				numTextures = 1;
			}
		}
	}

	size_t numMeshes = scene->mNumMeshes;
	std::vector<ecs::Entity> meshEntities(numMeshes);
	for (size_t i = 0; i < numMeshes; i++)
	{
		ecs::Entity& meshEntity = meshEntities[i];
		aiMesh& mesh = *scene->mMeshes[i];

		// Mesh Entity
		meshEntity = ecs::Entity(("Mesh:" + filePath.filename().string() + ":" + std::to_string(i)).c_str());

		// Set Material
		meshEntity.addComponent<ecs::EntityRef>(collection, {materialEntities[mesh.mMaterialIndex].getId(), ecs::EntityRef::Type::Child });

		// Set vertices
		component::MeshVertex* vertices
			= meshEntity.addComponentArray<component::MeshVertex>(collection, mesh.mNumVertices);
		component::MeshNormal* normals
			= meshEntity.addComponentArray<component::MeshNormal>(collection, mesh.mNumVertices);


		component::MeshTexCoord* texCoords = nullptr;
		if (mesh.mNumUVComponents[0] > 0 && numTextures > 0)
		{
			texCoords
				= meshEntity.addComponentArray<component::MeshTexCoord>(collection, mesh.mNumVertices);
		}
		for (size_t i = 0; i < mesh.mNumVertices; i++)
		{
			vertices[i].point[0] = mesh.mVertices[i].x;
			vertices[i].point[1] = mesh.mVertices[i].y;
			vertices[i].point[2] = mesh.mVertices[i].z;

			normals[i].normal[0] = mesh.mNormals[i].x;
			normals[i].normal[1] = mesh.mNormals[i].y;
			normals[i].normal[2] = mesh.mNormals[i].z;
			if (texCoords)
			{
				texCoords[i].coord[0] = mesh.mTextureCoords[0][i].x;
				texCoords[i].coord[1] = mesh.mTextureCoords[0][i].y;
			}
		}

		component::MeshIndex* vertexIndices
			= meshEntity.addComponentArray<component::MeshIndex>(collection, size_t(mesh.mNumFaces) * 3);

		for (size_t i = 0; i < mesh.mNumFaces; i++)
		{
			vertexIndices[3 * i + 0].idx = mesh.mFaces[i].mIndices[0];
			vertexIndices[3 * i + 1].idx = mesh.mFaces[i].mIndices[1];
			vertexIndices[3 * i + 2].idx = mesh.mFaces[i].mIndices[2];
		}

		if (mesh.mNumBones > 0)
		{
			meshEntity.addComponent<skeleton::Skeleton>(collection, skeleton::Skeleton(*scene, mesh));
			skeleton::VertexBoneData* vertexBoneData
				= meshEntity.addComponentArray<skeleton::VertexBoneData>(collection, mesh.mNumVertices);
			skeleton::getVertexBoneData(mesh, vertexBoneData);
		}

	}
	return { meshEntities, materialEntities };
}
