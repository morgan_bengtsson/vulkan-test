#include "skeleton.h"

#include <assimp/scene.h>

#include <Eigen/Dense>
#include <Eigen/Geometry>

#include <map>


namespace skeleton
{

const std::vector<Joint>& Skeleton::getJoints() const
{
	return mJoints;
}

void getVertexBoneData(const aiMesh& mesh, skeleton::VertexBoneData* vertexBoneData)
{
	for (size_t i = 0; i < mesh.mNumVertices; i++)
	{
		for (size_t j = 0; j < skeleton::NUM_BONE_WEIGHTS; j++)
		{
			vertexBoneData[i].boneWeight[j] = 0;
			vertexBoneData[i].boneIdx[j] = 0;
		}
	}

	for (size_t i = 0; i < mesh.mNumBones; i++)
	{
		auto& bone = mesh.mBones[i];
		for (size_t j = 0; j < bone->mNumWeights; j++)
		{
			auto& boneWeight = bone->mWeights[j];
			uint32_t vertexIdx = boneWeight.mVertexId;
			skeleton::VertexBoneData& vbd = vertexBoneData[vertexIdx];
			for (size_t k = 0; k < skeleton::NUM_BONE_WEIGHTS; k++)
			{
				if (vbd.boneWeight[k] == 0.0f)
				{
					vbd.boneIdx[k] = uint16_t(i);
					vbd.boneWeight[k] = boneWeight.mWeight;
					break;
				}
			}
		}
	}
	for (size_t i = 0; i < mesh.mNumVertices; i++)
	{
		float weightSum = 0.0f;
		for (size_t j = 0; j < skeleton::NUM_BONE_WEIGHTS; j++)
		{
			weightSum += vertexBoneData[i].boneWeight[j];
		}
		if (weightSum > 0.0f)
		{
			for (size_t j = 0; j < skeleton::NUM_BONE_WEIGHTS; j++)
			{
				vertexBoneData[i].boneWeight[j] /= weightSum;
			}
		}
	}
}

Skeleton::Skeleton(const aiScene& scene, const aiMesh& mesh)
	: mJoints{{}}
	, mAnimations()
	, mNodeNameToBoneIdx()
{
	for (size_t i = 0; i < mesh.mNumBones; i++)
	{
		auto& bone = mesh.mBones[i];
		mNodeNameToBoneIdx[bone->mName.C_Str()] = i;
	}

	addNodeToSkeleton(0, *scene.mRootNode, mesh.mBones, mNodeNameToBoneIdx);
	trimSkeleton();
	initAnimations(scene);
}

std::optional<size_t> Skeleton::getBoneIdx(const char* node)
{
	auto it = mNodeNameToBoneIdx.find(node);
	if (it == mNodeNameToBoneIdx.end())
		return {};
	return it->second;
}

const std::vector<Animation>& Skeleton::getAnimations() const
{
	return mAnimations;
}

void Skeleton::initAnimations(const aiScene& scene)
{
	auto getPosition = [](aiNodeAnim* node, float tp, float ret[4])
	{
		size_t numKeys = node->mNumPositionKeys;
		if (numKeys == 1) memcpy(ret, &node->mPositionKeys[0].mValue.x, sizeof(float) * 3);
		else
		{
			size_t idx = 0;
			while (idx < numKeys - 2 && tp > node->mPositionKeys[idx + 1].mTime) idx++;
			float t1 = node->mPositionKeys[idx].mTime, t2 = node->mPositionKeys[idx + 1].mTime;
			float factor = (tp - t1) / (t2 - t1);
			skeleton::interpolatePos(&node->mPositionKeys[idx].mValue.x, &node->mPositionKeys[idx + 1].mValue.x, factor, ret);
		}
		ret[3] = 1.0f;
	};

	auto getRotation = [](aiNodeAnim* node, float tp, float ret[4])
	{
		size_t numKeys = node->mNumRotationKeys;
		if (numKeys == 1) memcpy(ret, &node->mRotationKeys[0].mValue.w, sizeof(float) * 4);
		else
		{
			size_t idx = 0;
			while (idx < numKeys - 2 && tp > node->mRotationKeys[idx + 1].mTime) idx++;
			float t1 = node->mRotationKeys[idx].mTime, t2 = node->mRotationKeys[idx + 1].mTime;
			float factor = (tp - t1) / (t2 - t1);
			float quat1[4] = { node->mRotationKeys[idx].mValue.x, node->mRotationKeys[idx].mValue.y, node->mRotationKeys[idx].mValue.z, node->mRotationKeys[idx].mValue.w };
			float quat2[4] = { node->mRotationKeys[idx + 1].mValue.x, node->mRotationKeys[idx + 1].mValue.y, node->mRotationKeys[idx + 1].mValue.z, node->mRotationKeys[idx + 1].mValue.w };
			skeleton::interpolateRot(quat1, quat2, factor, ret);
		}
	};

	auto getScale = [](aiNodeAnim* node, float tp, float ret[3])
	{
		size_t numKeys = node->mNumScalingKeys;
		if (numKeys == 1) memcpy(ret, &node->mScalingKeys[0].mValue.x, sizeof(float) * 3);
		else
		{
			size_t idx = 0;
			while (idx < numKeys - 2 && tp > node->mScalingKeys[idx + 1].mTime) idx++;
			float t1 = node->mScalingKeys[idx].mTime, t2 = node->mScalingKeys[idx + 1].mTime;
			float factor = (tp - t1) / (t2 - t1);
			skeleton::interpolateScale(&node->mScalingKeys[idx].mValue.x, &node->mScalingKeys[idx + 1].mValue.x, factor, ret);
		}
	};

	mAnimations.reserve(scene.mNumAnimations);
	for (size_t i = 0; i < scene.mNumAnimations; i++)
	{
		aiAnimation* srcAnim = scene.mAnimations[i];
		Animation& anim = mAnimations.emplace_back(srcAnim->mName.C_Str(), numBones());
		for (size_t j = 0; j < srcAnim->mNumChannels; j++)
		{
			aiNodeAnim* nodeAnimation = srcAnim->mChannels[j];
			auto boneIdx = getBoneIdx(nodeAnimation->mNodeName.C_Str());
			if (boneIdx.has_value())
			{
				std::vector<float> keyFrameTp;
				for (size_t posIdx = 0, rotIdx = 0, scaleIdx = 0; ; )
				{
					constexpr float floatMax = std::numeric_limits<float>::max();
					const float posTime = (posIdx < nodeAnimation->mNumPositionKeys) ? nodeAnimation->mPositionKeys[posIdx].mTime : floatMax;
					const float rotTime = (rotIdx < nodeAnimation->mNumRotationKeys) ? nodeAnimation->mRotationKeys[rotIdx].mTime : floatMax;
					const float scaleTime = (scaleIdx < nodeAnimation->mNumScalingKeys) ? nodeAnimation->mScalingKeys[scaleIdx].mTime : floatMax;

					const float time = std::min({ posTime, rotTime, scaleTime });
					if (time == floatMax)
						break;

					keyFrameTp.push_back(time);
					if (posTime <= time) ++posIdx;
					if (rotTime <= time) ++rotIdx;
					if (scaleTime <= time) ++scaleIdx;
				}

				Animation::BoneAnimationSequence boneAnimSeq;
				boneAnimSeq.resize(keyFrameTp.size());
				for (size_t i = 0; i < keyFrameTp.size(); i++)
				{
					float pos[4], rot[4], scale[3];
					getPosition(nodeAnimation, keyFrameTp[i], pos);
					getRotation(nodeAnimation, keyFrameTp[i], rot);
					getScale(nodeAnimation, keyFrameTp[i], scale);

					boneAnimSeq[i].transform = SkeletonPose::BoneTransform(pos, rot, scale);
					boneAnimSeq[i].time = keyFrameTp[i] / srcAnim->mTicksPerSecond;
				}
				anim.setBoneSequence(boneIdx.value(), boneAnimSeq);
			}
		}
	}
}

size_t Skeleton::numBones() const
{
	return mNodeNameToBoneIdx.size();
}

void Skeleton::getBoneTransforms(size_t animationIdx, float timeMs, Bone* bones) const
{
	const Animation& animation = mAnimations[animationIdx];
	std::vector<SkeletonPose::BoneTransform> pose = animation.getSkeletonPose(timeMs / 1000.0f);
	std::vector<Eigen::Matrix4f> parentTransforms(mJoints.size());
	parentTransforms[0] = Eigen::Matrix4f::Identity();

	for (size_t i = 0; i < mJoints.size(); i++)
	{
		const Joint& joint = mJoints[i];
		Eigen::Matrix4f jointGlobalTransform;
		if (joint.boneIdx >= 0)
		{
			jointGlobalTransform.noalias() = parentTransforms[i] * pose[joint.boneIdx].getMatrix();
			Eigen::Map<Eigen::Matrix4f>(bones[joint.boneIdx].finalTransform)
				= jointGlobalTransform * Eigen::Map<const Eigen::Matrix4f>(joint.offsetTransform);
		}
		else
		{
			jointGlobalTransform.noalias() = parentTransforms[i] * Eigen::Map<const Eigen::Matrix4f>(joint.offsetTransform);
		}
		for (size_t j = joint.firstChildNode; j < joint.firstChildNode + joint.numChildNodes; j++)
		{
			parentTransforms[j] = jointGlobalTransform;
		}
	}
}


void Skeleton::addNodeToSkeleton(size_t jointIdx,
	const aiNode& node,
	aiBone** const bones,
	const std::map<std::string, uint16_t>& nodeNameToBoneIdx)
{
	Joint& joint = mJoints[jointIdx];
	if (auto it = nodeNameToBoneIdx.find(node.mName.C_Str()); it != nodeNameToBoneIdx.end())
		joint.boneIdx = it->second;
	else
		joint.boneIdx = -1;
	Eigen::Matrix4f offsetTransform = Eigen::Matrix4f::Identity();
	// Set local transform to identity matrix if its not a bone
	if (joint.boneIdx >= 0)
	{
		auto& bone = *(bones[joint.boneIdx]);
		offsetTransform.row(0) = Eigen::Map<const Eigen::Vector4f>(bone.mOffsetMatrix[0]);
		offsetTransform.row(1) = Eigen::Map<const Eigen::Vector4f>(bone.mOffsetMatrix[1]);
		offsetTransform.row(2) = Eigen::Map<const Eigen::Vector4f>(bone.mOffsetMatrix[2]);
		offsetTransform.row(3) = Eigen::Map<const Eigen::Vector4f>(bone.mOffsetMatrix[3]);
	}
	else
	{
		offsetTransform.row(0) = Eigen::Map<const Eigen::Vector4f>(node.mTransformation[0]);
		offsetTransform.row(1) = Eigen::Map<const Eigen::Vector4f>(node.mTransformation[1]);
		offsetTransform.row(2) = Eigen::Map<const Eigen::Vector4f>(node.mTransformation[2]);
		offsetTransform.row(3) = Eigen::Map<const Eigen::Vector4f>(node.mTransformation[3]);
	}
	memcpy(joint.offsetTransform, offsetTransform.data(), sizeof(joint.offsetTransform));

	joint.firstChildNode = (node.mNumChildren > 0 ? mJoints.size() : 0);
	joint.numChildNodes = node.mNumChildren;
	mJoints.resize(mJoints.size() + node.mNumChildren);
	size_t firstChildNode = joint.firstChildNode;
	for (size_t i = 0; i < node.mNumChildren; i++)
	{
		addNodeToSkeleton(firstChildNode + i, *node.mChildren[i], bones, nodeNameToBoneIdx);
	}
}


void Skeleton::trimSkeleton()
{
	bool nodeRemoved = true;
	while (nodeRemoved)
	{
		std::vector<size_t> nRemoved(mJoints.size());
		for (size_t i = 0, n = 0; i < mJoints.size(); i++)
		{
			if (mJoints[i].numChildNodes == 0 && mJoints[i].boneIdx < 0)
				++n;
			nRemoved[i] = n;
		}
		nodeRemoved = nRemoved.back();

		mJoints.erase(std::remove_if(std::begin(mJoints), std::end(mJoints), [](const Joint& joint)
		{
			return joint.numChildNodes == 0 && joint.boneIdx < 0;
		}), mJoints.end());

		for (size_t i = 0; i < mJoints.size(); i++)
		{
			Joint& joint = mJoints[i];
			if (joint.numChildNodes > 0)
			{
				size_t removedBeforeFirst = nRemoved[joint.firstChildNode - 1];
				size_t numRemovedChildren = nRemoved[joint.firstChildNode + joint.numChildNodes - 1] - removedBeforeFirst;
				joint.firstChildNode -= removedBeforeFirst;
				joint.numChildNodes -= numRemovedChildren;
			}
		}
	}
}

};
