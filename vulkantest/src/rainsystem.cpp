#include "rainsystem.h"
#include "fileload.h"
#include "tracer.h"
#include "systemproperties.h"
#include "camera.h"
#include "material.h"
#include "rain.h"
#include "entitybuilder.h"

#include <vulkanBufferCopier.h>
#include <vulkanContext.h>

#include <vulkanBarrier.h>
#include <chrono>

RainSystem::RainSystem(vulkanutils::VulkanContext& context, SceneType scene)
	: System<RainSystem>(context)
	, mRandomRainGenerator((unsigned int)(std::chrono::system_clock::now().time_since_epoch().count()))
	, mCommandPool(context.device(), context.device().getComputeQueue(), VK_COMMAND_POOL_CREATE_RESET_COMMAND_BUFFER_BIT)
	, mCommandBuffers()
{
	mCommandBuffers = vulkanutils::CommandBuffers(getVulkanContext().device(), mCommandPool, 1);

	auto& cc = scene.getComponentCollection();
	auto& gpuCc = scene.getGpuComponentCollection();
	vulkanutils::Device* device = &getVulkanContext().device();

	auto [rainMeshEntities, rainMaterialEntities] = loadFile("./models/waterdrop.dae", cc, device);
	mRainMeshes = std::move(rainMeshEntities);
	mRainMaterials = std::move(rainMaterialEntities);

	for (auto& entity : mRainMeshes)
		scene.addEntity(entity);
	for (auto& entity : mRainMaterials)
		scene.addEntity(entity);

	mRainDrawPipeline = ecs::Entity("RainPipeline");
	{
		using namespace vulkanutils;
		component::Pipeline& pipeline = mRainDrawPipeline.addComponent<component::Pipeline>(cc, component::Pipeline("rain"));

		pipeline.sets.emplace_back(systemPropertiesSet(device, scene, DescriptorSetSlot::SLOT_1 ));
		pipeline.sets.emplace_back(cameraSet(device, scene, DescriptorSetSlot::SLOT_2 ));
		pipeline.sets.emplace_back(materialSet(device, scene, DescriptorSetSlot::SLOT_4, 0 ));
		pipeline.sets.emplace_back(RainMeshSet(device, scene, DescriptorSetSlot::NONE, VertexInputSlot::SLOT_0 ));
		pipeline.sets.emplace_back(RainModelSet(device, scene, DescriptorSetSlot::SLOT_3, VertexInputSlot::SLOT_3 ));

		pipeline.addRenderPassAttachment<component::Color>(scene, 0);
		pipeline.addRenderPassAttachment<component::Depth>(scene, 1);

		scene.addEntity(mRainDrawPipeline);
	}
}


void RainSystem::runImpl(SceneType scene)
{
	std::vector<ecs::Entity*> rainEntities = scene.getEntities<component::Rain>();
	{
		if (std::end(rainEntities) != std::find_if(std::begin(rainEntities), std::end(rainEntities),
			[&](ecs::Entity* e)
			{
				return !e->hasComponents<component::RainDrop>(scene.getComponentCollection());
			}))
		{
			{
				vulkanutils::CommandBufferRecorder cbRecorder(mCommandBuffers[0], VK_COMMAND_BUFFER_USAGE_ONE_TIME_SUBMIT_BIT);
				for (ecs::Entity* e : rainEntities)
					setup(*e, scene);
			}
			getVulkanContext().compute({ mCommandBuffers[0] });
		}
	}

	return;
	/*
	std::vector<ecs::Entity*> systemPropertiesEntities = scene.getEntities<component::SystemProperies>();
	std::vector<ecs::Entity*> waterSurfaceEntities = scene.getEntities<WaterSurface>();
	if (false && !systemPropertiesEntities.empty() && !waterSurfaceEntities.empty())
	{
		// Collision, drops and water surface
		uint32_t timeMs = systemPropertiesEntities.front()->getComponent<component::SystemProperies>(scene.getComponentCollection()).timeMs;
		for (ecs::Entity* entity : rainEntities)
		{
			auto drops = entity->getComponentArray<component::RainDrop>(scene.getComponentCollection());
			for (auto& drop : drops)
			{
				float dropTime = float(timeMs % uint32_t(drop.life));
				Eigen::Vector3f pos = Eigen::Map<Eigen::Vector3f, Eigen::Aligned16>(drop.start) + Eigen::Map<Eigen::Vector3f, Eigen::Aligned16>(drop.dir) * dropTime;

				for (auto& waterSurfaceEntity : waterSurfaceEntities)
				{
					WaterSurface& waterSurface = waterSurfaceEntity->getComponent<WaterSurface>(scene.getComponentCollection());

					float depth = waterSurface.get(pos.x(), pos.z(), [](const WaterSurfaceCell& wc){return wc.depth + wc.bd; });
					float vsHeight = depth;
					if (pos.y() <= vsHeight && pos.y() > vsHeight - Eigen::Map<Eigen::Vector3f, Eigen::Aligned16>(drop.dir).norm())
					{
						waterSurface.changeVolume(pos.x(), pos.z(), 5, 10);
					}
				}
			}
		}
	}
	*/
}

void RainSystem::setup(ecs::Entity& entity, SceneType& scene)
{
	if (entity.hasComponents<component::RainDrop>(scene.getComponentCollection()))
		return;

	auto& cc = scene.getComponentCollection();
	auto& gpuCc = scene.getGpuComponentCollection();

	entity.addComponent<ecs::EntityRef>(cc,
		{ mRainDrawPipeline.getId(), ecs::EntityRef::Type::Link });
	EntityBuilder(cc).addMesh(entity, mRainMeshes);

	const component::Rain& rain = entity.getComponent<component::Rain>(cc);

	const int numDrops = int(rain.width * rain.height * rain.density);
	std::uniform_real_distribution<float> distribution(0.0f, 1.0f);
	auto* drops = entity.addComponentArray<component::RainDrop>(cc, numDrops);
	for (size_t i = 0; i < numDrops; i++)
	{
		component::RainDrop& drop = drops[i];
		drop.start[0] = (distribution(mRandomRainGenerator) - 0.5f) * rain.width;
		drop.start[1] = rain.height;
		drop.start[2] = (distribution(mRandomRainGenerator) - 0.5f) * rain.depth;
		float speed = rain.speed * (0.5f + distribution(mRandomRainGenerator));
		drop.dir[0] = 0;
		drop.dir[1] = -speed;
		drop.dir[2] = 0;
		drop.life = rain.height / speed;
		drop.size = rain.size;
	}

	entity.setModified();
	{
		vulkanutils::Barrier(mCommandBuffers[0]).dependsOnVertexShader().doBeforeCopy().globalBarrier();
		vulkanutils::BufferCopier bufferCopier(mCommandBuffers[0]);

		std::span<const component::RainDrop> rainDrops = entity.getComponentArray<component::RainDrop>(cc);
		const component::Position& position = entity.getComponent<component::Position>(cc);
		const component::Orientation& orientation = entity.getComponent<component::Orientation>(cc);
	
		if (!entity.hasGpuComponents<vulkanutils::UniformBuffer, component::Transform>(scene.getGpuComponentCollection()))
		{
			entity.addGpuComponentArray<vulkanutils::UniformBuffer, component::Transform>(gpuCc, 1);
			entity.addHostGpuComponentArray<component::Transform>(gpuCc, 1);

			entity.addGpuComponentArray<vulkanutils::VertexBuffer, component::RainDrop>(gpuCc, rainDrops.size());
			entity.addHostGpuComponentArray<component::RainDrop>(gpuCc, rainDrops.size());
			{
				auto& deviceData = entity.getGpuComponentArray<vulkanutils::UniformBuffer, component::Transform>(gpuCc);
				auto hostData = entity.getHostGpuComponentArray<component::Transform>(gpuCc);

				// Set host uniform buffer
				{
					Eigen::Map<Eigen::Matrix4f> transform(hostData.getData()[0].matrix);
					transform.topRightCorner<4, 1>() = position.homogeneous();
					transform.topLeftCorner<3, 3>() = orientation;
				}
				bufferCopier.queue(hostData, deviceData);
			}
			// Set host vertex buffer
			{
				auto hostData = entity.getHostGpuComponentArray<component::RainDrop>(gpuCc);
				auto& deviceData = entity.getGpuComponentArray<vulkanutils::VertexBuffer, component::RainDrop>(gpuCc);
				memcpy(hostData.getData().data(), rainDrops.data(), rainDrops.size_bytes());
				bufferCopier.queue(hostData, deviceData);
			}
		}
		vulkanutils::Barrier(mCommandBuffers[0]).dependsOnCopy().doBeforeVertexShader().globalBarrier();
	}
}
