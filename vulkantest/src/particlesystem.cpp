#include "particlesystem.h"

#include <vulkanBufferCopier.h>
#include <vulkanContext.h>
#include <vulkanResourceSet.h>
#include <vulkanDescriptorPool.h>
#include <vulkanBarrier.h>

#include <random>
#include <chrono>
#include <iostream>

KNNComputeSet::KNNComputeSet(vulkanutils::Device* device, SceneType scene, vulkanutils::DescriptorSetSlot descSetbp)
	: EntityResourceSet(device, "KNNCompute", descSetbp, vulkanutils::VertexInputSlot::NONE)
{
	setDescriptorBinding<vulkanutils::ShaderStorageBuffer, component::CollidableObject>(scene,
		{ VK_SHADER_STAGE_COMPUTE_BIT, 0 });
	setDescriptorBinding<vulkanutils::ShaderStorageBuffer, kdtree::ClosestObject>(scene,
		{ VK_SHADER_STAGE_COMPUTE_BIT, 1 });
	setDescriptorBinding<vulkanutils::ShaderStorageBuffer, kdtree::KdTree<component::CollidableObject, 6>>(scene,
		{ VK_SHADER_STAGE_COMPUTE_BIT, 2 });
	
	getDescriptorLayout().build();
}


ParticleSystem::ParticleSystem(vulkanutils::VulkanContext& context, SceneType scene)
	:System<ParticleSystem>(context)
	, mRandomGenerator((unsigned int)(std::chrono::system_clock::now().time_since_epoch().count()))
	, mDescriptorPool(context.device(), 8)
	, mCommandPool(context.device(), context.device().getComputeQueue(), VK_COMMAND_POOL_CREATE_RESET_COMMAND_BUFFER_BIT)
	, mParticleSystemStates()
{
	mParticlePipeline.shader = vulkanutils::loadComputeShader(context.device(), "./shaders", "particle");
	mParticlePipeline.computeSet
		= std::make_unique<ParticleComputeSet>(&context.device(), scene, vulkanutils::DescriptorSetSlot::SLOT_0);
	mParticlePipeline.computePipeline = vulkanutils::ComputePipeline(context, mParticlePipeline.shader, 
		{ mParticlePipeline.computeSet.get() });

	mCollisionPipeline.shader = vulkanutils::loadComputeShader(context.device(), "./shaders", "KNN");
	mCollisionPipeline.computeSet 
		= std::make_unique<KNNComputeSet>(&context.device(), scene, vulkanutils::DescriptorSetSlot::SLOT_0);
	mCollisionPipeline.computePipeline = vulkanutils::ComputePipeline(context, mCollisionPipeline.shader, 
		{ mCollisionPipeline.computeSet.get() });
}

void ParticleSystem::runImpl(SceneType scene)
{
	auto particleEntities = scene.getEntities<Particle, ParticleProperties>();
	auto systemEntities = scene.getEntities<component::SystemProperies>();

	for (auto& entity : particleEntities)
	{
		if (!entity->hasGpuComponents<vulkanutils::ShaderStorageBuffer, component::Position, component::Speed>(scene.getComponentCollection()))
			setupParticles(*entity, scene);
	}

	uint64_t waitValue = getVulkanContext().device().getComputeQueue().getSignalValue();


	for (auto& entity : particleEntities)
	{
		setParticleData(*entity, scene); // CPU -> GPU
	}
	for (auto& entity : particleEntities)
	{
		updateParticles(*entity, scene); // GPU calc
		waitValue++;
	}
	getVulkanContext().device().getComputeQueue().wait(getVulkanContext().device(), waitValue, 1000000000u); // Wait for GPU
	for (auto& entity : particleEntities)
	{
		getParticleData(*entity, scene); // GPU -> CPU
	}
}



void ParticleSystem::setParticleData(ecs::Entity& particeEntity, SceneType& scene)
{
	// Set input buffers
	auto hostPositionData = particeEntity.getHostGpuComponentArray<component::Position>(scene.getGpuComponentCollection());
	auto hostSpeedData =    particeEntity.getHostGpuComponentArray<component::Speed>(scene.getGpuComponentCollection());
	auto particles =        particeEntity.getComponentArray<Particle>(scene.getComponentCollection());
	for (size_t i = 0; i < particles.size(); i++)
	{
		auto& particle = particles[i];
		memcpy(hostPositionData.getData()[i].data(), particle.position, sizeof(particle.position));
		memcpy(hostSpeedData.getData()[i].data(), particle.speed, sizeof(particle.speed));
	}

	const ParticleProperties& properties = particeEntity.getComponent<ParticleProperties>(scene.getComponentCollection());
	const component::Position& position =       particeEntity.getComponent<component::Position>(scene.getComponentCollection());
	const component::Orientation& orientation = particeEntity.getComponent<component::Orientation>(scene.getComponentCollection());
	auto particleTransformHostData =           particeEntity.getHostGpuComponentArray<component::Transform>(scene.getGpuComponentCollection());
	auto particlePropertiesHostData =          particeEntity.getHostGpuComponentArray<ParticleProperties>(scene.getGpuComponentCollection());
	{
		Eigen::Map<Eigen::Matrix4f> transform(particleTransformHostData.getData()[0].matrix);
		transform.topRightCorner<4, 1>() = position.homogeneous();
		transform.topLeftCorner<3, 3>() = orientation;
	}
	particlePropertiesHostData.getData()[0] = properties;
}

void ParticleSystem::updateParticles(ecs::Entity& particeEntity, SceneType& scene)
{
	ParticleSystemState& particleSystemState = mParticleSystemStates[particeEntity.getId()];
	auto& uploadCommandBuffer = particleSystemState.computeCommandBuffers[particleSystemState.iteration & 0x01];
	particleSystemState.iteration++;

	getVulkanContext().device().getComputeQueue().submit({ uploadCommandBuffer }, &getVulkanContext().device().getGraphicsQueue(), {}, {});
}


void ParticleSystem::getParticleData(ecs::Entity& particeEntity, SceneType& scene)
{
	// Set input buffers
	auto hostPositionData = particeEntity.getHostGpuComponentArray<component::Position>(scene.getGpuComponentCollection());
	auto hostSpeedData =    particeEntity.getHostGpuComponentArray<component::Speed>(scene.getGpuComponentCollection());
	auto particles =        particeEntity.getComponentArray<Particle>(scene.getComponentCollection());
	const ParticleProperties& properties = particeEntity.getComponent<ParticleProperties>(scene.getComponentCollection());
	auto particleTransformHostData =           particeEntity.getHostGpuComponentArray<component::Transform>(scene.getGpuComponentCollection());
	auto particlePropertiesHostData =          particeEntity.getHostGpuComponentArray<ParticleProperties>(scene.getGpuComponentCollection());

	// Get result
	for (size_t i = 0; i < particles.size(); i++)
	{
		Particle& particle = particles[i];
		memcpy(particle.position, hostPositionData.getData()[i].data(), sizeof(particle.position));
		memcpy(particle.speed, hostSpeedData.getData()[i].data(), sizeof(particle.speed));
	}
	std::span<component::CollidableObject> collidableObjectData = particeEntity.getComponentArray<component::CollidableObject>(scene.getComponentCollection());
	for (uint32_t i = 0; i < collidableObjectData.size(); i++)
	{
		collidableObjectData[i].entityId = uint32_t(particeEntity.getId());
		collidableObjectData[i].objectId = i;
		collidableObjectData[i].point[0] = particles[i].position[0];
		collidableObjectData[i].point[1] = particles[i].position[1];
		collidableObjectData[i].point[2] = particles[i].position[2];
		collidableObjectData[i].radius = properties.radius;
	}

	particeEntity.setModified();
}


void ParticleSystem::setupParticles(ecs::Entity& particeEntity, SceneType& scene)
{
	auto& cc = scene.getComponentCollection();
	auto& gpuCc = scene.getGpuComponentCollection();

	// "Global" particle system data
	const ParticleProperties& properties = particeEntity.getComponent<ParticleProperties>(scene.getComponentCollection());
	const component::Position& position = particeEntity.getComponent<component::Position>(scene.getComponentCollection());
	const component::Orientation& orientation = particeEntity.getComponent<component::Orientation>(scene.getComponentCollection());

	// Create and upload ParticleProperties and Transforms
	auto& particleTransformDeviceData = particeEntity.addGpuComponentArray<vulkanutils::UniformBuffer, component::Transform>(scene.getGpuComponentCollection(), 1);
	auto& particleTransformHostData = particeEntity.addGpuComponentArray<vulkanutils::UploadBuffer, component::Transform>(scene.getGpuComponentCollection(), 1);
	auto& particlePropertiesDeviceData = particeEntity.addGpuComponentArray<vulkanutils::UniformBuffer, ParticleProperties>(scene.getGpuComponentCollection(), 1);
	auto& particlePropertiesHostData = particeEntity.addGpuComponentArray<vulkanutils::UploadBuffer, ParticleProperties>(scene.getGpuComponentCollection(), 1);

	// Create and upload particle specific data
	std::uniform_real_distribution<float> distribution(0.0f, 1.0f);

	// Init random positions
	//----------------------
	std::span<Particle> particles = particeEntity.getComponentArray<Particle>(scene.getComponentCollection());
	component::CollidableObject* collidableObjectData = particeEntity.addComponentArray<component::CollidableObject>(scene.getComponentCollection(), particles.size());
	for (size_t i = 0; i < particles.size(); i++)
	{
		auto& particle = particles[i];

		particle.position[0] = (distribution(mRandomGenerator) - 0.5f) * 1000;
		particle.position[1] = (distribution(mRandomGenerator)) * 1000;
		particle.position[2] = (distribution(mRandomGenerator) - 0.5f) * 1000;
		particle.speed[0] = particle.speed[1] = particle.speed[2] = 0;

		collidableObjectData[i].entityId = uint32_t(particeEntity.getId());
		collidableObjectData[i].objectId = uint32_t(i);
		collidableObjectData[i].point[0] = particles[i].position[0];
		collidableObjectData[i].point[1] = particles[i].position[1];
		collidableObjectData[i].point[2] = particles[i].position[2];
		collidableObjectData[i].radius = properties.radius;
	}

	auto hostPositionData =        particeEntity.addHostGpuComponentArray<component::Position>(scene.getGpuComponentCollection(), particles.size());
	auto hostSpeedData =           particeEntity.addHostGpuComponentArray<component::Speed>(scene.getGpuComponentCollection(), particles.size());
	//---------------------

	auto positionData = particeEntity.addGpuComponentArrays<vulkanutils::ShaderStorageBuffer, component::Position>(gpuCc, { particles.size(), particles.size() });
	auto speedData = particeEntity.addGpuComponentArrays<vulkanutils::ShaderStorageBuffer, component::Speed>(scene.getGpuComponentCollection(), { particles.size(), particles.size()});

	ParticleSystemState& particleSystemState = mParticleSystemStates[particeEntity.getId()];
	particleSystemState.computeCommandBuffers = vulkanutils::CommandBuffers(getVulkanContext().device(), mCommandPool, 2);

	particleSystemState.computeDescriptorSets[0] = mParticlePipeline.computeSet->createDescriptorSet(mDescriptorPool);
	particleSystemState.computeDescriptorSets[1] = mParticlePipeline.computeSet->createDescriptorSet(mDescriptorPool);


	particleSystemState.computeDescriptorSets[1].bindBuffer(*positionData[1], mParticlePipeline.computeSet->getDescriptorLayout().getDescriptorBindings<vulkanutils::ShaderStorageBuffer>()[0].binding);
	particleSystemState.computeDescriptorSets[1].bindBuffer(*speedData[1],    mParticlePipeline.computeSet->getDescriptorLayout().getDescriptorBindings<vulkanutils::ShaderStorageBuffer>()[1].binding);
	particleSystemState.computeDescriptorSets[1].bindBuffer(*positionData[0], mParticlePipeline.computeSet->getDescriptorLayout().getDescriptorBindings<vulkanutils::ShaderStorageBuffer>()[2].binding);
	particleSystemState.computeDescriptorSets[1].bindBuffer(*speedData[0],    mParticlePipeline.computeSet->getDescriptorLayout().getDescriptorBindings<vulkanutils::ShaderStorageBuffer>()[3].binding);

	mParticlePipeline.computeSet->bindEntityToDescriptors(scene, particleSystemState.computeDescriptorSets[0], particeEntity);
	mParticlePipeline.computeSet->bindEntityToDescriptors<vulkanutils::UniformBuffer>(scene, particleSystemState.computeDescriptorSets[1], particeEntity);

	// Upload particle pos and speed
	{
		vulkanutils::CommandBufferRecorder cbRecorder(particleSystemState.computeCommandBuffers[0], VK_COMMAND_BUFFER_USAGE_ONE_TIME_SUBMIT_BIT);
		
		vulkanutils::BufferCopier copier(particleSystemState.computeCommandBuffers[0], 2);
		for (size_t i = 0; i < particles.size(); i++)
		{
			auto& particle = particles[i];
			memcpy(hostPositionData.getData()[i].data(), particle.position, sizeof(particle.position));
			memcpy(hostSpeedData.getData()[i].data(), particle.speed, sizeof(particle.speed));
		}
		copier.queue(hostPositionData, *positionData[0]);
		copier.queue(hostSpeedData, *speedData[0]);
		copier.execute();

		vulkanutils::Barrier(particleSystemState.computeCommandBuffers[0])
			.dependsOnCopy().doBeforeCompute()
			.addBuffer(*speedData[0]->getBuffer(), speedData[0]->getMemRange())
			.addBuffer(*positionData[0]->getBuffer(), positionData[0]->getMemRange());
	}
	getVulkanContext().device().getComputeQueue().submit({ particleSystemState.computeCommandBuffers[0] }, nullptr);
	getVulkanContext().device().getComputeQueue().wait();

	



	// record for future usage
	for (size_t setIt = 0; setIt < 2; setIt++)
	{
		auto& commandBuffer = particleSystemState.computeCommandBuffers[setIt];

		vulkanutils::CommandBufferRecorder cbRecorder(commandBuffer
			, VK_COMMAND_BUFFER_USAGE_SIMULTANEOUS_USE_BIT);
		
		{
			vulkanutils::BufferCopier copier(commandBuffer, 4);
			copier.queue(particleTransformHostData, particleTransformDeviceData);
			copier.queue(particlePropertiesHostData, particlePropertiesDeviceData);
			// Copy from cpu (result from collision)
			copier.queue(hostPositionData, *positionData[setIt]);
			copier.queue(hostSpeedData, *speedData[setIt]);
		}
		vulkanutils::Barrier(commandBuffer)
			.dependsOnCopy().doBeforeCompute()
			.addBuffer(*speedData[setIt]->getBuffer(), speedData[setIt]->getMemRange())
			.addBuffer(*positionData[setIt]->getBuffer(), positionData[setIt]->getMemRange());

		mParticlePipeline.computePipeline.bind(commandBuffer);
		particleSystemState.computeDescriptorSets[setIt].bindToCompute(commandBuffer, mParticlePipeline.computePipeline.getLayout(), (uint32_t) mParticlePipeline.computeSet->getDescriptorSetBindpoint());
		vkCmdDispatch(commandBuffer, uint32_t(particles.size()), 1, 1);
		
		vulkanutils::Barrier(commandBuffer)
			.dependsOnCompute().doBeforeCopy()
			.addBuffer(*positionData[1 - setIt]->getBuffer(), positionData[1 - setIt]->getMemRange())
			.addBuffer(*speedData[1 - setIt]->getBuffer(), speedData[1 - setIt]->getMemRange());
		{
			vulkanutils::BufferCopier copier(commandBuffer, 3);
			// Copy result to host buffers to be able to do collisions on cpu for now
			copier.queue(*positionData[1 - setIt], hostPositionData);
			copier.queue(*speedData[1 - setIt], hostSpeedData);
		}
	}

}


ParticleComputeSet::ParticleComputeSet(vulkanutils::Device* device, SceneType scene, vulkanutils::DescriptorSetSlot descSetbp)
	: EntityResourceSet(device, "particleCompute", descSetbp, vulkanutils::VertexInputSlot::NONE)
{
	setDescriptorBinding<vulkanutils::ShaderStorageBuffer, component::Position>(scene,
		{	// pos (in)
			VK_SHADER_STAGE_COMPUTE_BIT,
			0,
			1,
		});
	setDescriptorBinding<vulkanutils::ShaderStorageBuffer, component::Speed>(scene,
		{	// speed (in)
			VK_SHADER_STAGE_COMPUTE_BIT,
			1,
			1,
		});
	setDescriptorBinding<vulkanutils::ShaderStorageBuffer, component::Position>(scene,
		{	// pos (out)
			VK_SHADER_STAGE_COMPUTE_BIT,
			2,
			1,
		});
	setDescriptorBinding<vulkanutils::ShaderStorageBuffer, component::Speed>(scene,
		{	// speed (out)
			VK_SHADER_STAGE_COMPUTE_BIT,
			3,
			1,
		});

	setDescriptorBinding<vulkanutils::UniformBuffer, component::Transform>(scene,
		{
			VK_SHADER_STAGE_COMPUTE_BIT,
			4,
			1,
		});
	setDescriptorBinding<vulkanutils::UniformBuffer, ParticleProperties>(scene,
		{
			VK_SHADER_STAGE_COMPUTE_BIT,
			5,
			1,
		});

	getDescriptorLayout().build();
}
