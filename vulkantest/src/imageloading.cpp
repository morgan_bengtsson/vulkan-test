#include <imageloading.h>

#include <map>
#include <string>
#include <QImage>

#include <iostream>


struct ImageBitmapImpl
{
	QImage image;
};


ImageBitmap::ImageBitmap()
	:mImpl(new ImageBitmapImpl())
{}

ImageBitmap::ImageBitmap(const char* filename)
	:ImageBitmap()
{
	mImpl->image = QImage(filename);
	if (mImpl->image.hasAlphaChannel())
	{
		mImpl->image = mImpl->image.convertToFormat(QImage::Format_RGBA8888);
	}
	else
	{
		QImage alpha = mImpl->image.alphaChannel();
		alpha.fill(255);
		mImpl->image.setAlphaChannel(alpha);
		mImpl->image = mImpl->image.convertToFormat(QImage::Format_RGBA8888);
	}
/*	std::cout << "Image info\n"
		<< mImpl->image.width() << std::endl
		<< mImpl->image.height() << std::endl
		<< mImpl->image.byteCount() << std::endl
		<< mImpl->image.bytesPerLine() << std::endl
		<< mImpl->image.sizeInBytes() << std::endl;
		*/
}

ImageBitmap::ImageBitmap(ImageBitmap&& b) noexcept
	:mImpl()
{
	std::swap(mImpl, b.mImpl);
}

ImageBitmap& ImageBitmap::operator=(ImageBitmap&& b) noexcept
{
	mImpl.reset(nullptr);
	std::swap(mImpl, b.mImpl);
	return *this;
}

ImageBitmap::~ImageBitmap()
{
}

size_t ImageBitmap::width() const
{
	return size_t(mImpl->image.width());
}

size_t ImageBitmap::height() const
{
	return size_t(mImpl->image.height());
}

size_t ImageBitmap::bytesPerRow() const
{
	return size_t(mImpl->image.bytesPerLine());
}

const uint32_t* ImageBitmap::data() const
{
	return reinterpret_cast<const uint32_t*>(mImpl->image.constBits());
}
