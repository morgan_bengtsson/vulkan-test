#include "rain.h"


RainModelSet::RainModelSet(vulkanutils::Device* device, SceneType scene, vulkanutils::DescriptorSetSlot descSetbp, vulkanutils::VertexInputSlot vertInputBp)
	: EntityResourceSet(device, "rainModel", descSetbp, vertInputBp)
{
	setPerInstanceAttributeBinding<component::RainDrop>(scene,
		{
			{ 4, VK_FORMAT_R32G32B32_SFLOAT, 3 * sizeof(float) },
			{ 5, VK_FORMAT_R32_SFLOAT,       1 * sizeof(float) },
			{ 6, VK_FORMAT_R32G32B32_SFLOAT, 3 * sizeof(float) },
			{ 7, VK_FORMAT_R32_SFLOAT,       1 * sizeof(float) }
		});

	setDescriptorBinding<vulkanutils::UniformBuffer, component::Transform>(scene,
		{ VK_SHADER_STAGE_VERTEX_BIT, 0	});
	getDescriptorLayout().build();
	getComponentTypeIds<vulkanutils::VertexBuffer>().addComponetTypes<component::RainDrop>(scene);
}

RainMaterialSet::RainMaterialSet(vulkanutils::Device* device, SceneType scene, vulkanutils::DescriptorSetSlot descSetbp, vulkanutils::VertexInputSlot vertInputBp)
	: EntityResourceSet(device, "rainMaterial", descSetbp, vertInputBp)
{
	setDescriptorBinding<vulkanutils::UniformBuffer, component::MaterialProperties>(scene,
		{ VK_SHADER_STAGE_FRAGMENT_BIT, 0 });
	getDescriptorLayout().build();
}

RainMeshSet::RainMeshSet(vulkanutils::Device* device, SceneType scene, vulkanutils::DescriptorSetSlot descSetbp, vulkanutils::VertexInputSlot vertInputBp)
	: EntityResourceSet(device, "rainMesh", descSetbp, vertInputBp)
{
	setPerVertexAttributeBinding<component::MeshVertex>(scene, 0, VK_FORMAT_R32G32B32_SFLOAT);
	addComponetTypes<component::MeshVertex, component::MeshNormal, component::MeshIndex>(scene);
	setHasIndexBinding();
}
