#include "idrawsystemextension.h"



IDrawSystemExtension::ResourceAccessor::ResourceAccessor(DrawSystem& ds)
	:mDs(ds)
{
}

vulkanutils::DescriptorSet IDrawSystemExtension::ResourceAccessor::createDescriptorSet(const EntityResourceSet& resourceSet) const
{
	return resourceSet.createDescriptorSet(mDs.mDescriptorPool);
}