#version 450
#extension GL_ARB_separate_shader_objects : enable
#extension GL_ARB_tessellation_shader : enable

#include "light.inc"

layout (triangles) in;

struct ControlPoints
{
	vec3 points[10];
};

layout(location = 4) patch in ControlPoints cp;


layout(set = 3, binding = 0, std430) buffer readonly directionalLightBuffer
{
	DirectionalLight directionalLights[];
};

layout(set = 3, binding = 1, std430) buffer readonly positionalLightBuffer
{
	PositionalLight positionalLights[];
};

layout(set = 3, binding = 2) uniform lightIdxBuffer
{
	uint lightIdx;
};


void main(void)
{
    float u = gl_TessCoord.x;
    float v = gl_TessCoord.y;
    float w = gl_TessCoord.z;

    float uPow2 = u * u;
    float vPow2 = v * v;
    float wPow2 = w * w;
    float uPow3 = uPow2 * u;
    float vPow3 = vPow2 * v;
    float wPow3 = wPow2 * w;

	vec3 pos = cp.points[0] * uPow3 +
				cp.points[3] * vPow3 +
				cp.points[6] * wPow3 +
				3.0 * (cp.points[1] * uPow2 * v +
						cp.points[2] * u * vPow2 +
						cp.points[4] * vPow2 * w +
						cp.points[5] * v * wPow2 +
						cp.points[7] * wPow2 * u +
						cp.points[8] * w * uPow2 +
						cp.points[9] * 2.0 * u * v * w);
	
	DirectionalLight light = directionalLights[lightIdx];
	gl_Position = light.projectionView * vec4(pos, 1.0);
}

