#version 450
#extension GL_ARB_separate_shader_objects : enable

#include "light.inc"

layout(location = 0) in vec3 position;
layout(location = 1) in vec3 normal;
layout(location = 0) out vec4 outColor;


// SET 1: camera
layout(column_major, set = 2, binding = 0) uniform CameraP
{
	mat4 camP;
};

layout(column_major, set = 2, binding = 1) uniform CameraV
{
	mat4 camV;
};



layout(set = 4, binding = 0) uniform MaterialBuffer
{
	Material material;
};



layout(set = 3, binding = 0, std430) buffer readonly directionalLightBuffer
{
	DirectionalLight directionalLights[];
};

layout(set = 3, binding = 1, std430) buffer readonly positionalLightBuffer
{
	PositionalLight positionalLights[];
};

layout(set = 3, binding = 2) uniform sampler2DShadow shadowDistance;


void main()
{
	vec3 ambLight = vec3(0.0);
	vec3 diffLight = vec3(0.0);
	vec3 specLight = vec3(0.0);	
	float shininess = max(material.specular.a, 1);
	
	vec3 camPos = -transpose(mat3(camV)) * camV[3].xyz;
	vec3 viewDir = normalize(position - camPos);

	outColor = vec4(0, 0, 0, 1);
	for (int i = 0; i < directionalLights.length(); i++)
	{
		vec4 reflection = phongShadingWithShadow(directionalLights[i], material, position, normal, viewDir, shadowDistance);
		outColor.rgb += reflection.rgb;
	}

	for (int i = 0; i < positionalLights.length(); i++)
	{
		vec3 lightColor = positionalLights[i].color.rgb;
		vec3 lightPos = positionalLights[i].position.xyz;
		vec3 lightPointDir = position - lightPos;
		vec3 lightDir = normalize(lightPointDir);

		float intensity = 1.0 / max(1.0, sqrt(dot(lightPointDir, lightPointDir)) / 1000.0);

		float normLightDot = dot(normal, -lightDir);
		ambLight += lightColor * intensity;
		diffLight += normLightDot * lightColor * intensity;
		vec3 reflection = -lightDir - 2 * normLightDot * normal;
		specLight += pow(max(0, dot(reflection, viewDir)), shininess) * lightColor * intensity;
	}
}
