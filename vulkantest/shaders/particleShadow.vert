#version 450
#extension GL_ARB_separate_shader_objects : enable

#include "light.inc"

out gl_PerVertex 
{
    vec4 gl_Position;
};

// Per-mesh
layout(location = 0) in vec3 inPosition;
layout(location = 1) in vec3 inNormal;


layout(column_major, set = 5, binding = 1) uniform ParticleProperties
{
	float radius;
	float mass;
} particleProperties;

layout(column_major, set = 5, binding = 0) uniform ParticleTransform
{
	mat4 transform;
} particleTransform;

layout(set = 5, binding = 2, std430) buffer readonly ParticlePositionBuffer
{
	vec3 particlePosition[];
};

layout(set = 3, binding = 0, std430) buffer readonly directionalLightBuffer
{
	DirectionalLight directionalLights[];
};

layout(set = 3, binding = 1, std430) buffer readonly positionalLightBuffer
{
	PositionalLight positionalLights[];
};

layout(set = 3, binding = 2) uniform lightIdxBuffer
{
	uint lightIdx;
};



void main()
{
	vec4 pos = particleTransform.transform * vec4(particlePosition[gl_InstanceIndex] + inPosition * particleProperties.radius, 1.0);
	DirectionalLight light = directionalLights[lightIdx];
	gl_Position = light.projectionView * pos;
}
