#version 450
#extension GL_ARB_separate_shader_objects : enable
#extension GL_EXT_scalar_block_layout:  require

#include "light.inc"

layout(location = 0) in vec3 position;
layout(location = 1) in vec3 normal;
layout(location = 2) in vec2 texCoord;

layout(location = 0) out vec4 outColor;

layout(column_major, set = 1, binding = 0) uniform CameraP
{
	mat4 camP;
};

layout(column_major, set = 1, binding = 1) uniform CameraV
{
	mat4 camV;
};


layout(set = 3, binding = 0, std430) buffer readonly directionalLightBuffer
{
	DirectionalLight directionalLights[];
};

layout(set = 3, binding = 1, std430) buffer readonly positionalLightBuffer
{
	PositionalLight positionalLights[];
};

layout(set = 6, binding = 0, std430) uniform AntData
{
	float position[2];
	uint state;
	float directionAngle;
	float speed;
	float size;
	float stamina;
	float maxStamina;
	float hp;
	float maxHp;
} antData;


layout(set = 4, binding = 0) uniform MaterialBuffer
{
	Material material;
};

layout(set = 4, binding = 1) uniform sampler2D texSampler;


vec3 stateColors[8] = vec3[8](
	vec3(0.2, 0.2, 0.2), // REST
	vec3(0.8, 0.8, 0.0), // SEARCH_FOOD
	vec3(0.5, 0.5, 0.0), // SEARCH_FOOD_FOLLOW_TRAIL
	vec3(0.2, 0.8, 0.2), // RETURN_FOOD
	vec3(0.0, 0.3, 0.0), // RETURN_FOOD_FOLLOW_TRAIL
	vec3(0.8, 0.2, 0.2), // RETURN
	vec3(0.5, 0.2, 0.2), // RETURN_FOLLOW_TRAIL
	vec3(0, 0, 0)		 // DEAD
);



void main()
{
	vec4 texColor = texture(texSampler, texCoord);

	vec3 ambLight = vec3(0.0);
	vec3 diffLight = vec3(0.0);
	vec3 specLight = vec3(0.0);	
	float shininess = max(material.specular.a, 1);
	
	vec3 camPos = -transpose(mat3(camV)) * camV[3].xyz;
	vec3 viewDir = normalize(position - camPos);

	Material m = material;
	m.diffuse = vec4(1);

	for (int i = 0; i < directionalLights.length(); i++)
	{
		vec4 reflection = phongShading(directionalLights[i], m, position, normal, viewDir);
		outColor.rgb += reflection.rgb;
	}
	/*
	for (int i = 0; i < positionalLights.length(); i++)
	{
		vec3 lightColor = positionalLights[i].color.rgb;
		vec3 lightPos = positionalLights[i].position.xyz;
		vec3 lightPointDir = vec3(0) - position;
		vec3 lightDir = normalize(lightPointDir);

		float intensity = 1.0 / max(1.0, dot(lightPointDir, lightPointDir));

		float normLightDot = dot(normal, -lightDir);
		ambLight += lightColor * intensity;
		diffLight += max(0.0, normLightDot) * lightColor * intensity;
		vec3 reflection = 2 * normLightDot * normal - lightDir;
		specLight += pow(max(0, dot(reflection, viewDir)), shininess) * lightColor * intensity;
	}
	*/
	outColor.rgb *= texColor.rgb;
	outColor.a = 1.0;
}

