#version 450
#extension GL_ARB_separate_shader_objects : enable
#extension GL_ARB_tessellation_shader : enable

layout (vertices = 3) out;

layout(location = 1) in vec3 inNormal[];


struct ControlPoints
{
	vec3 points[10];
};

layout(location = 4) patch out ControlPoints cp;


void setEdgePoints()
{
	int nextId = ((gl_InvocationID + 1) % 3);
	
	vec3 point1 = gl_in[gl_InvocationID].gl_Position.xyz;
	vec3 point2 = gl_in[nextId].gl_Position.xyz;
	vec3 normal1 = inNormal[gl_InvocationID];
	vec3 normal2 = inNormal[nextId];

	vec3 edge = point2 - point1;

	float w[2];
	w[0] = dot(edge, normal1);
	w[1] = -dot(edge, normal2);

	cp.points[3 * gl_InvocationID + 0] = point1;
	cp.points[3 * gl_InvocationID + 1] = (2 * point1 + point2 - w[0] * normal1) / 3;
	cp.points[3 * gl_InvocationID + 2] = (2 * point2 + point1 - w[1] * normal2) / 3;
}

void setCenterPoint()
{
	vec3 E = (cp.points[1] + cp.points[2] + 
			cp.points[4] + cp.points[5] +
			cp.points[7] + cp.points[8]) * 9 / 36;
	vec3 V = (cp.points[0] + cp.points[3] + cp.points[6]) / 6;

	cp.points[9] = E - V;
}


void main(void)
{
    gl_TessLevelInner[0] = 1.0;
    gl_TessLevelOuter[gl_InvocationID] = 1.0;

	setEdgePoints();
	barrier();
	setCenterPoint();
}
