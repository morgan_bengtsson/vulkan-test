#version 450
#extension GL_ARB_separate_shader_objects : enable

out gl_PerVertex 
{
    vec4 gl_Position;
};

// Per-vertex
layout(location = 0) in vec3 inPosition;
layout(location = 1) in vec3 inNormal;

layout(location = 0) out vec3 position;
layout(location = 1) out vec3 normal;



layout(column_major, set = 1, binding = 0) uniform CameraP
{
	mat4 camP;
};

layout(column_major, set = 1, binding = 1) uniform CameraV
{
	mat4 camV;
};

layout(column_major, set = 1, binding = 2) uniform CameraPV
{
	mat4 camPV;
};

layout(set = 5, binding = 0, std430) buffer readonly modelTransformBuffer
{
	mat4 modelTransform[];
};

void main()
{
	vec4 pos = modelTransform[gl_InstanceIndex] * vec4(inPosition, 1.0);
	position = pos.xyz;
	normal = mat3(modelTransform[gl_InstanceIndex]) * inNormal;
	gl_Position = camPV * pos;
	
}
