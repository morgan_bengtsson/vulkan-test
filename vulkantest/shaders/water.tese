#version 450
#extension GL_ARB_separate_shader_objects : enable
#extension GL_ARB_tessellation_shader : enable

layout (triangles) in;


layout(location = 2) in float inDepth[];

layout(location = 0) out vec3 outPosition;
layout(location = 1) out vec3 outNormal;
layout(location = 2) out float outDepth;


struct ControlPoints
{
	vec3 points[10];
	vec3 normals[6];
};

layout(location = 4) patch in ControlPoints cp;

layout(column_major, set = 1, binding = 0) uniform CameraP
{
	mat4 camP;
};

layout(column_major, set = 1, binding = 1) uniform CameraV
{
	mat4 camV;
};

layout(column_major, set = 1, binding = 2) uniform CameraPV
{
	mat4 camPV;
};


void main(void)
{
    float u = gl_TessCoord.x;
    float v = gl_TessCoord.y;
    float w = gl_TessCoord.z;

    float uPow2 = u * u;
    float vPow2 = v * v;
    float wPow2 = w * w;
    float uPow3 = uPow2 * u;
    float vPow3 = vPow2 * v;
    float wPow3 = wPow2 * w;

	vec3 pos = cp.points[0] * uPow3 +
				cp.points[3] * vPow3 +
				cp.points[6] * wPow3 +
				3.0 * (cp.points[1] * uPow2 * v +
						cp.points[2] * u * vPow2 +
						cp.points[4] * vPow2 * w +
						cp.points[5] * v * wPow2 +
						cp.points[7] * wPow2 * u +
						cp.points[8] * w * uPow2 +
						cp.points[9] * 2.0 * u * v * w);

	outPosition = pos;
	gl_Position = camPV * vec4(pos, 1.0);
	
	
	outNormal = normalize(cp.normals[0] * uPow2 +
				cp.normals[2] * vPow2 +
				cp.normals[4] * wPow2 +
				2.0 * (cp.normals[1] * u * v +
						cp.normals[3] * v * w +
						cp.normals[5] * w * u));


	outDepth = gl_TessCoord.x * inDepth[0] + gl_TessCoord.y * inDepth[1] + gl_TessCoord.z * inDepth[2];
}
