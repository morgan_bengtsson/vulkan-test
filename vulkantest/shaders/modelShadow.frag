#version 450
#extension GL_ARB_separate_shader_objects : enable

layout(location = 2) in vec2 texCoord;

layout(set = 4, binding = 1) uniform sampler2D texSampler;


void main()
{
	vec4 texColor = texture(texSampler, texCoord);
	if (texColor.a < 0.5) discard;
}

