
vec4 quatFromTwoVectors(vec3 u, vec3 v)
{
	vec3 w = cross(u, v);
	vec4 q = vec4(dot(u, v), w.x, w.y, w.z);
	q.w += q.length();
	return normalize(q);
}

vec4 createQuat(vec3 axis, float angle)
{
	vec4 q;
	q.xyz = axis.xyz * sin(angle / 2.0);
	q.w = cos(angle / 2.0);
	return q;
}

vec4 quatMult(vec4 lhs, vec4 rhs)
{
	vec4 ret;
	ret.xyz = cross(lhs.xyz, rhs.xyz) + lhs.w * rhs.xyz + rhs.w * lhs.xyz;
	ret.w = lhs.w * rhs.w - dot(lhs.xyz, rhs.xyz);
	return ret;
}


mat3 quatToMatrix(vec4 quat)
{

	vec3 t = 2 * quat.xyz;
	vec3 tw = t * quat.w;
	vec3 tx = t * quat.x;
	vec3 ty = t * quat.y;
	vec3 tz = t * quat.z;


	mat3 T = mat3(vec3(1 - ty.y - tz.z, tx.y + tw.z, tx.z - tw.y),
				vec3(tx.y-tw.z, 1 - tx.x - tz.z, ty.z + tw.x),
				vec3(tx.z + tw.y, ty.z - tw.x, 1 - tx.x - ty.y));
	return T;
}

vec4 slerp(vec4 q1, vec4 q2, float factor)
{
	// SLERP
	float d = dot(q1, q2);
	float absD = abs(d);
	float theta = acos(absD);
	float sinTheta = sin(theta);
	float scale0;
	float scale1;
	if (absD >= 0.99)
	{
		scale0 = 1 - factor;
		scale1 = factor;
	}
	else
	{
		scale0 = sin((1 - factor) * theta) / sinTheta;
		scale1 = sin(factor * theta) / sinTheta;
	}
	if (d < 0) scale1 = -scale1;
	return scale0 * q1 + scale1 * q2;
}
