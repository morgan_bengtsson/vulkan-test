#version 450
#extension GL_ARB_separate_shader_objects : enable
#extension GL_ARB_tessellation_shader : enable

#include "light.inc"

layout (triangles) in;

layout(location = 1) in vec3 scale[];
layout(location = 2) in vec3 inPosition[];

layout(set = 3, binding = 0, std430) buffer readonly directionalLightBuffer
{
	DirectionalLight directionalLights[];
};

layout(set = 3, binding = 1, std430) buffer readonly positionalLightBuffer
{
	PositionalLight positionalLights[];
};

layout(set = 3, binding = 2) uniform lightIdxBuffer
{
	uint lightIdx;
};


highp float rand(vec2 co)
{
    vec2 ab = vec2(12.9898, 78.233);
    highp float c = 43758.5453;
    highp float dt = dot(co.xy, ab);
    highp float sn = mod(dt, 3.14);
    return fract(sin(sn) * c);
}

vec3 relPos(mat3 P, vec3 uvw, float grassHeight)
{
	vec3 pos = P * uvw;
	vec3 center = (P[0] + P[1] + P[2]) / 3.0f;
	float bayMin = min(min(uvw[0], uvw[1]), uvw[2]);
	float radius = 3.0f * (1.0f / 3.0f - bayMin);

	float height = (1.0 - radius * radius) * grassHeight;
	vec3 relPos = normalize(P * uvw - center) * radius * (scale[0] + scale[1] + scale[2]) / 3.0f;
	relPos.y = height;


	float pi = 3.1415;
	float angle = rand(P[1].xz) * pi * 2.0f;
	float bendRadius = (rand(P[2].xz) + 1) * grassHeight;
	vec3 bentRelPos;

	bentRelPos.x = relPos.x + cos(angle) * (1 - cos(pi * height / bendRadius)) * grassHeight;
	bentRelPos.z = relPos.z + sin(angle) * (1 - cos(pi * height / bendRadius)) * grassHeight;
	bentRelPos.y = relPos.y + sin(pi * height / bendRadius) * grassHeight;

	return bentRelPos;
}


void main(void)
{
    float u = gl_TessCoord.x;
    float v = gl_TessCoord.y;
    float w = gl_TessCoord.z;

	float bayMin = min(min(u, v), w);

	mat3 P = mat3(inPosition[0], inPosition[1], inPosition[2]);
	float grassHeight = rand(inPosition[0].xz) * 10.0;
	vec3 center = (P[0] + P[1] + P[2]) / 3.0;

	vec3 rp = relPos(P, gl_TessCoord, grassHeight);
	vec3 pos = center + rp;

	DirectionalLight light = directionalLights[lightIdx];
	gl_Position = light.projectionView * vec4(pos, 1.0);
}
