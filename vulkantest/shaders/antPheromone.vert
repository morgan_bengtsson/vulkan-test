#version 450
#extension GL_ARB_separate_shader_objects : enable

out gl_PerVertex 
{
    vec4 gl_Position;
    float gl_PointSize;
    float gl_ClipDistance[];
    float gl_CullDistance[];
};

// Per-vertex
layout(location = 0) in vec3 inPosition;
layout(location = 1) in vec3 inNormal;
layout(location = 2) in vec2 inTexCoord;

layout(location = 1) out vec3 outNormal;
layout(location = 2) out vec2 outTexCoord;

struct PheromoneData
{
	float home;
	float food;
	float danger;
	float empty;
};

layout(set = 4, binding = 0, std430) buffer readonly pheromoneDataBuffer
{
	PheromoneData pheromoneData[];
};

layout(set = 5, binding = 0, std430) buffer readonly modelTransformBuffer
{
	mat4 modelTransform[];
};


void main()
{
	vec4 pos = modelTransform[gl_InstanceIndex] * vec4(inPosition, 1.0);
	outNormal = mat3(modelTransform[gl_InstanceIndex]) * inNormal;
	gl_Position = pos;
	outTexCoord = inTexCoord;
}
