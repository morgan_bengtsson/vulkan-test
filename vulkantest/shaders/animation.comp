#version 450
#extension GL_EXT_nonuniform_qualifier : enable

#include "quat.inc"

layout(local_size_x = 1, local_size_y = 1, local_size_z = 1) in;



struct Joint
{
	mat4 offsetTransform;
	int boneIdx;
	uint firstChildNode;
	uint numChildNodes;
};

struct BoneTransform
{
	vec4 quaternion;
	vec4 position;
	vec4 scale;
};

struct BoneAnimationKeyFrame
{
	BoneTransform transform;
	float time;
	float padding[3];
};

struct Animation
{
	uint firstKeyFrame;
	uint numKeyFrames;
};

struct AnimationPose
{
	uint animationIdx;
	float time;
};


// =====Input=====

// Mesh
layout(set = 0, binding = 0, std430) readonly buffer JointArray
{
	Joint skeletonJoints[];
};

// Order:
// 1. Animation (walk/run/...)
// 2. Bone
// 3. Animation keyframe

layout(set = 0, binding = 1) readonly buffer KeyFrameArray
{
	BoneAnimationKeyFrame keyFrames[];
};
layout(set = 0, binding = 2) readonly buffer AnimationArray
{
	Animation animations[];
};

// Instance
layout(set = 1, binding = 0) readonly buffer AnimationPoses
{
	AnimationPose poses[];
} animationPoses[];


// =====Output=====
layout(set = 2, binding = 0, std430) buffer boneArray
{
	mat4 transforms[];
} boneTransforms[];


mat4 getMatrix(BoneTransform bt)
{
	mat3 S = mat3(bt.scale.x, 0, 0,
					0, bt.scale.y, 0,
					0, 0, bt.scale.z);
	mat4 T = mat4(quatToMatrix(bt.quaternion) * S);
	T[3] = bt.position;
	return T;
}

BoneTransform interpolate(BoneTransform bt1, BoneTransform bt2, float factor)
{
	BoneTransform transform;
	transform.quaternion = slerp(bt1.quaternion, bt2.quaternion, factor);
	transform.position = mix(bt1.position, bt2.position, factor);
	transform.scale = mix(bt1.scale, bt2.scale, factor);
	return transform;
}


const uint MAX_NUM_BONES = 128;

void main()
{
	uint idx = gl_GlobalInvocationID.x;
	BoneTransform transforms[MAX_NUM_BONES];
	uint numBones = boneTransforms[idx].transforms.length();
	for (uint i = 0; i < numBones && i < MAX_NUM_BONES; i++)
	{
		AnimationPose pose = animationPoses[idx].poses[i];
		Animation animation = animations[pose.animationIdx * numBones + i];
		uint firstKeyFrame = animation.firstKeyFrame;
		uint numKeyFrames = animation.numKeyFrames;
		BoneAnimationKeyFrame firstKF = keyFrames[firstKeyFrame];
		BoneAnimationKeyFrame lastKF = keyFrames[firstKeyFrame + numKeyFrames - 1];

		float time = mod((pose.time - firstKF.time), lastKF.time - firstKF.time) + firstKF.time;
		uint keyFrameIdx = firstKeyFrame;
		for ( ; keyFrameIdx < firstKeyFrame + numKeyFrames - 1 && keyFrames[keyFrameIdx + 1].time < time; keyFrameIdx++)
		{
		}
		BoneAnimationKeyFrame kf1 = keyFrames[keyFrameIdx];
		BoneAnimationKeyFrame kf2 = keyFrames[keyFrameIdx + 1];
		float factor = (time - kf1.time) / (kf2.time - kf1.time);
		transforms[i] = interpolate(kf1.transform, kf2.transform, factor);
	}

	mat4 parentTransforms[MAX_NUM_BONES];
	parentTransforms[0] = mat4(1.0);
	for (uint i = 0; i < skeletonJoints.length() && i < MAX_NUM_BONES; i++)
	{
		Joint joint = skeletonJoints[i];
		mat4 jointGlobalTransform;
		if (joint.boneIdx >= 0)
		{
			jointGlobalTransform = parentTransforms[i] * getMatrix(transforms[joint.boneIdx]);
			boneTransforms[idx].transforms[joint.boneIdx] = jointGlobalTransform * joint.offsetTransform;
		}
		else
		{
			jointGlobalTransform = parentTransforms[i] * joint.offsetTransform;
		}
		for (uint j = joint.firstChildNode; j < joint.firstChildNode + joint.numChildNodes; j++)
		{
			parentTransforms[j] = jointGlobalTransform;
		}
	}
}
