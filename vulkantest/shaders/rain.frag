#version 450
#extension GL_ARB_separate_shader_objects : enable

layout(location = 0) out vec4 outColor;


// SET 2: Material
layout(set = 4, binding = 0) uniform Material
{
	vec4 emittance;
	vec4 ambient;
	vec4 diffuse;
	vec4 specular;
} material;


void main()
{
	outColor.rgba = material.diffuse.rgba;
}
