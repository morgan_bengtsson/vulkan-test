#version 450
#extension GL_ARB_separate_shader_objects : enable
#extension GL_EXT_scalar_block_layout:  require

#include "quat.inc"

out gl_PerVertex 
{
    vec4 gl_Position;
};

layout(location = 0) in vec3 inPosition;
layout(location = 1) in vec3 inNormal;
layout(location = 2) in vec2 inTexCoord;
layout(location = 3) in uvec4 boneId;
layout(location = 4) in vec4 boneWeight;

layout(location = 0) out vec3 position;
layout(location = 1) out vec3 normal;
layout(location = 2) out vec2 texCoord;

layout(column_major, set = 1, binding = 0) uniform CameraP
{
	mat4 camP;
};

layout(column_major, set = 1, binding = 1) uniform CameraV
{
	mat4 camV;
};

layout(column_major, set = 1, binding = 2) uniform CameraPV
{
	mat4 camPV;
};

// model bone poses
layout(set = 5, binding = 0, std430) buffer readonly boneArray
{
	mat4 boneTransforms[];
};


layout(set = 6, binding = 0, std430) uniform AntData
{
	vec2 position;
	uint state;
	float directionAngle;
	float speed;
	float size;
	float stamina;
	float maxStamina;
	float hp;
	float maxHp;
} antData;

struct SurfaceProperties
{
	uint width;
	uint height;
	float cellSize;
};

layout(set = 7, binding = 0) uniform SurfacePropertiesBlock { SurfaceProperties heightMapProperties; };
layout(set = 7, binding = 1) uniform sampler2D heightMap;


float surfaceHeight(vec2 pos)
{
	ivec2 texSize = textureSize(heightMap, 0);
	return texture(heightMap, pos / texSize + vec2(0.5)).x;
}

void main()
{
	mat4 boneTransform = mat4(0);
	boneTransform += boneWeight[0] * boneTransforms[boneId[0]];
	boneTransform += boneWeight[1] * boneTransforms[boneId[1]];
	boneTransform += boneWeight[2] * boneTransforms[boneId[2]];
	boneTransform += boneWeight[3] * boneTransforms[boneId[3]];

	float d = 0.1;

	float h = surfaceHeight(antData.position);
	float hx = surfaceHeight(antData.position + vec2(d, 0));
	float hy = surfaceHeight(antData.position + vec2(0, d));

	vec3 pos3d = vec3(antData.position[0], h, antData.position[1]);
	vec4 modelQuat = quatFromTwoVectors(vec3(0, 1, 0), vec3(h - hx, 1.414 * d, h - hy));

	vec4 upQuat = createQuat(vec3(0, 1, 0), -antData.directionAngle + 3.1415);
	modelQuat = quatMult(modelQuat, upQuat);

	vec4 pos = boneTransform * vec4(inPosition, 1.0);
	pos.w = 1.0f;
	pos.xyz = pos.xyz * antData.size;
	pos = quatMult(quatMult(modelQuat, pos), vec4(-modelQuat.xyz, modelQuat.w));
	pos.xyz += pos3d;

	position = pos.xyz;
	normal = mat3(boneTransform) * inNormal;
	normal = normalize(quatMult(quatMult(modelQuat, vec4(normal, 1.0)), vec4(-modelQuat.xyz, modelQuat.w)).xyz);
	gl_Position = camPV * pos;
	texCoord = inTexCoord;
}
