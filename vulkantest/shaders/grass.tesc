#version 450
#extension GL_ARB_separate_shader_objects : enable
#extension GL_ARB_tessellation_shader : enable

layout (vertices = 3) out;


layout(location = 1) in vec3 inScale[];
layout(location = 2) in float inIndexPos[];

layout(location = 1) out vec3 outScale[];
layout(location = 2) out vec3 outPosition[];
layout(location = 3) out float outIndexPos[];


void main(void)
{
    float detail = max(5.0, 31.0 * exp(-400 *  pow(inScale[gl_InvocationID].x, 4)));

    gl_TessLevelInner[0] = detail;
    gl_TessLevelOuter[gl_InvocationID] = 1.0;

	outPosition[gl_InvocationID] = gl_in[gl_InvocationID].gl_Position.xyz;
    outScale[gl_InvocationID] = inScale[gl_InvocationID];
    outIndexPos[gl_InvocationID] = inIndexPos[gl_InvocationID];
}
