#version 450
#extension GL_ARB_separate_shader_objects : enable

#include "light.inc"
#include "offsetbuffer_vert.inc"

out gl_PerVertex 
{
    vec4 gl_Position;
};

// Per-vertex
layout(location = 0) in vec3 inPosition;
layout(location = 2) in vec2 inTexCoord;

layout(location = 2) out vec2 texCoord;


layout(set = 3, binding = 0, std430) buffer readonly directionalLightBuffer
{
	DirectionalLight directionalLights[];
};
layout(set = 3, binding = 1, std430) buffer readonly positionalLightBuffer
{
	PositionalLight positionalLights[];
};
layout(set = 3, binding = 2) uniform lightIdxBuffer
{
	uint lightIdx;
};
layout(set = 5, binding = 0, std430) buffer readonly modelTransformBuffer
{
	mat4 modelTransform[];
};

void main()
{
	// SOMETHING SLIGHTLY WRONG HERE
	uint modelTransformOffset = getComponentOffset(5, 0) / 64;
	DirectionalLight light = directionalLights[lightIdx];
	gl_Position = light.projectionView * modelTransform[modelTransformOffset] * vec4(inPosition, 1.0);
	texCoord = inTexCoord;
}

