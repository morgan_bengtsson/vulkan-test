#version 450
#extension GL_ARB_separate_shader_objects : enable

#include "light.inc"

layout(location = 0) in vec3 position;
layout(location = 1) in vec3 normal;
layout(location = 2) in vec2 texCoord;


layout(location = 0) out vec4 outColor;


layout(column_major, set = 2, binding = 0) uniform CameraP
{
	mat4 camP;
};
layout(column_major, set = 2, binding = 1) uniform CameraV
{
	mat4 camV;
};


layout(set = 3, binding = 0, std430) buffer readonly directionalLightBuffer
{
	DirectionalLight directionalLights[];
};
layout(set = 3, binding = 1, std430) buffer readonly positionalLightBuffer
{
	PositionalLight positionalLights[];
};
layout(set = 3, binding = 2) uniform sampler2DShadow shadowDistance;

layout(set = 4, binding = 0) uniform MaterialBuffer
{
	Material material;
};

layout(set = 4, binding = 1) uniform sampler2D texSampler;


void main()
{
	vec4 texColor = texture(texSampler, texCoord);
	if (texColor.a < 0.5) discard;
	
	vec3 camPos = -transpose(mat3(camV)) * camV[3].xyz;
	vec3 viewDir = normalize(position - camPos);

	outColor = vec4(0, 0, 0, 1);
	for (int i = 0; i < directionalLights.length(); i++)
	{
		vec4 reflection = phongShadingWithShadow(directionalLights[i], material, position, normal, viewDir, shadowDistance);
		outColor.rgb += reflection.rgb;
	}

	outColor.rgb *= texColor.rgb;
}

