


vec3 calculate_scattering(
	vec3 start, 				// the start of the ray (the camera position)
    vec3 dir, 					// the direction of the ray (the camera vector)
    float max_dist, 			// the maximum distance the ray can travel (because something is in the way, like an object)
    vec3 scene_color,			// the color of the scene
    vec3 light_dir, 			// the direction of the light
    vec3 light_intensity,		// how bright the light is, affects the brightness of the atmosphere
    vec3 planet_position, 		// the position of the planet
    float planet_radius, 		// the radius of the planet
    float atmo_radius, 			// the radius of the atmosphere
    vec3 beta_ray, 				// the amount rayleigh scattering scatters the colors (for earth: causes the blue atmosphere)
    vec3 beta_mie, 				// the amount mie scattering scatters colors
    vec3 beta_absorption,   	// how much air is absorbed
    vec3 beta_ambient,			// the amount of scattering that always occurs, cna help make the back side of the atmosphere a bit brighter
    float g, 					// the direction mie scatters the light in (like a cone). closer to -1 means more towards a single direction
    float height_ray, 			// how high do you have to go before there is no rayleigh scattering?
    float height_mie, 			// the same, but for mie
    float height_absorption,	// the height at which the most absorption happens
    float absorption_falloff,	// how fast the absorption falls off from the absorption height
    int steps_i, 				// the amount of steps along the 'primary' ray, more looks better but slower
    int steps_l 				// the amount of steps along the light ray, more looks better but slower
) {
    start -= planet_position;
    float b = 2.0 * dot(dir, start);
    float c = dot(start, start) - (atmo_radius * atmo_radius);
    float d = (b * b) - 4.0 * c;
    if (d < 0.0) return scene_color;
    
    vec2 ray_length = vec2(
        max((-b - sqrt(d)) / 2.0, 0.0),
        min((-b + sqrt(d)) / 2.0, max_dist)
    );
    
    if (ray_length.x > ray_length.y) return scene_color;
    bool allow_mie = max_dist > ray_length.y;
    ray_length.y = min(ray_length.y, max_dist);
    ray_length.x = max(ray_length.x, 0.0);
    float step_size_i = (ray_length.y - ray_length.x) / float(steps_i);
    
    float ray_pos_i = ray_length.x + step_size_i * 0.5;
       vec3 total_ray = vec3(0.0); // for rayleigh
    vec3 total_mie = vec3(0.0); // for mie
    
    vec3 opt_i = vec3(0.0);
    vec3 prev_density = vec3(0.0);
    vec2 scale_height = vec2(height_ray, height_mie);
    
    float mu = dot(dir, light_dir);
    float mumu = mu * mu;
    float gg = g * g;
    float phase_ray = 3.0 / (50.2654824574 /* (16 * pi) */) * (1.0 + mumu);
    float phase_mie = allow_mie ? 3.0 / (25.1327412287 /* (8 * pi) */) * ((1.0 - gg) * (mumu + 1.0)) / (pow(1.0 + gg - 2.0 * mu * g, 1.5) * (2.0 + gg)) : 0.0;
    
    for (int i = 0; i < steps_i; ++i) {
        vec3 pos_i = start + dir * ray_pos_i;
        float height_i = length(pos_i) - planet_radius;
        vec3 density = vec3(exp(-height_i / scale_height), 0.0);
        float denom = (height_absorption - height_i) / absorption_falloff;
        density.z = (1.0 / (denom * denom + 1.0)) * density.x;
        density *= step_size_i;
        opt_i += max(density + (prev_density - density) * 0.5, 0.0);
        prev_density = density;
        b = 2.0 * dot(light_dir, pos_i);
        c = dot(pos_i, pos_i) - (atmo_radius * atmo_radius);
        d = (b * b) - 4.0 * c;
        float step_size_l = (-b + sqrt(d)) / (2.0 * float(steps_l));
        float ray_pos_l = step_size_l * 0.5;
        vec3 opt_l = vec3(0.0);

        vec3 prev_density_l = vec3(0.0);
        for (int l = 0; l < steps_l; ++l)
        {
            vec3 pos_l = pos_i + light_dir * ray_pos_l;
            float height_l = length(pos_l) - planet_radius;
            vec3 density_l = vec3(exp(-height_l / scale_height), 0.0);
            float denom = (height_absorption - height_l) / absorption_falloff;
            density_l.z = (1.0 / (denom * denom + 1.0)) * density_l.x;
            density_l *= step_size_l;
            opt_l += max(density_l + (prev_density_l - density_l) * 0.5, 0.0);
            prev_density_l = density_l;
            ray_pos_l += step_size_l;
        }
        vec3 attn = exp(-beta_ray * (opt_i.x + opt_l.x) - beta_mie * (opt_i.y + opt_l.y) - beta_absorption * (opt_i.z + opt_l.z));
        total_ray += density.x * attn;
        total_mie += density.y * attn;
        ray_pos_i += step_size_i;
    	
    }
    vec3 opacity = exp(-(beta_mie * opt_i.y + beta_ray * opt_i.x + beta_absorption * opt_i.z));
    return (
        	phase_ray * beta_ray * total_ray // rayleigh color
       		+ phase_mie * beta_mie * total_mie // mie
            + opt_i.x * beta_ambient // and ambient
    ) * light_intensity + scene_color * opacity; // now make sure the background is rendered correctly
}