#version 450
#extension GL_ARB_separate_shader_objects : enable

#include "offsetbuffer_vert.inc"

out gl_PerVertex 
{
    vec4 gl_Position;
};

// Per-mesh
layout(location = 0) in vec3 inPosition;
layout(location = 1) in vec3 inNormal;

layout(location = 0) out vec3 position;
layout(location = 1) out vec3 normal;


layout(column_major, set = 1, binding = 0) uniform SystemProperties
{
	uint time;
} systemProperties;


layout(column_major, set = 2, binding = 0) uniform CameraP
{
	mat4 camP;
};

layout(column_major, set = 2, binding = 1) uniform CameraV
{
	mat4 camV;
};

layout(column_major, set = 2, binding = 2) uniform CameraPV
{
	mat4 camPV;
};

layout(column_major, set = 5, binding = 0) uniform ParticleTransform
{
	mat4 transform;
} particleTransform;

layout(column_major, set = 5, binding = 1) uniform ParticleProperties
{
	float radius;
	float mass;
} particleProperties;

layout(set = 5, binding = 2, std430) buffer readonly ParticlePositionBuffer
{
	vec3 particlePosition[];
};



void main()
{
	vec4 pos = particleTransform.transform * vec4(particlePosition[gl_InstanceIndex] + inPosition * particleProperties.radius, 1.0);
	position = pos.xyz;
	normal = normalize(mat3(particleTransform.transform) * inPosition);
	gl_Position = camPV * pos;
}
