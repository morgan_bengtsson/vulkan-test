#version 450
#extension GL_ARB_separate_shader_objects : enable
#extension GL_ARB_tessellation_shader : enable

layout (vertices = 3) out;


layout(location = 1) in vec3 inScale[];

layout(location = 1) out vec3 outScale[];
layout(location = 2) out vec3 outPosition[];


void main(void)
{
    gl_TessLevelInner[0] = 7.0;
    gl_TessLevelOuter[gl_InvocationID] = 1.0;

	outPosition[gl_InvocationID] = gl_in[gl_InvocationID].gl_Position.xyz;
    outScale[gl_InvocationID] = inScale[gl_InvocationID];
}
