#version 450
#extension GL_ARB_separate_shader_objects : enable

out gl_PerVertex 
{
    vec4 gl_Position;
};

// Per-vertex
layout(location = 0) in vec3 inPosition;

// Per-instance
layout(location = 4) in vec3 start;
layout(location = 5) in float size;
layout(location = 6) in vec3 dir;
layout(location = 7) in float life;



layout(column_major, set = 1, binding = 0) uniform SystemProperties
{
	uint time;
} systemProperties;


layout(column_major, set = 2, binding = 0) uniform CameraP
{
	mat4 camP;
};

layout(column_major, set = 2, binding = 1) uniform CameraV
{
	mat4 camV;
};

layout(column_major, set = 2, binding = 2) uniform CameraPV
{
	mat4 camPV;
};

layout(column_major, set = 3, binding = 0) uniform Rain
{
	mat4 modelTransform;
} rain;


void main()
{
	float lifetime = mod(systemProperties.time * length(dir), life);
	vec3 meshpos = inPosition * size;
	vec4 pos = vec4(meshpos + start + dir * lifetime, 1.0);

	gl_Position = camPV * rain.modelTransform * pos;
}
