#version 450
#extension GL_ARB_separate_shader_objects : enable

#include "offsetbuffer_vert.inc"


out gl_PerVertex 
{
    vec4 gl_Position;
};

layout(location = 0) in vec3 inPosition;
layout(location = 1) in vec3 inNormal;


layout(location = 0) out vec3 position;
layout(location = 1) out vec3 normal;

layout(column_major, set = 2, binding = 0) uniform CameraP
{
	mat4 camP;
};
layout(column_major, set = 2, binding = 1) uniform CameraV
{
	mat4 camV;
};
layout(column_major, set = 2, binding = 2) uniform CameraPV
{
	mat4 camPV;
};

layout(set = 5, binding = 0, std430) buffer readonly modelTransformBuffer
{
	mat4 modelTransform[];
};


void main()
{
	uint modelTransformOffset = getComponentOffset(5, 0) / 64;

	vec4 pos = modelTransform[modelTransformOffset] * vec4(inPosition, 1.0);
	position = pos.xyz;
	normal = mat3(modelTransform[modelTransformOffset]) * inNormal;
	gl_Position = camPV * pos;
}
