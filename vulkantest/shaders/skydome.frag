#version 450
#extension GL_ARB_separate_shader_objects : enable
#extension GL_EXT_scalar_block_layout:  require

#include "scattering.inc"

layout(location = 0) in vec4 position;
layout(location = 0) out vec4 outColor;

layout(column_major, set = 1, binding = 0) uniform SystemProperties
{
	uint time;
} systemProperties;

layout(column_major, set = 2, binding = 0) uniform CameraP
{
	mat4 camP;
};

layout(column_major, set = 2, binding = 1) uniform CameraV
{
	mat4 camV;
};

layout(column_major, set = 2, binding = 2) uniform CameraPV
{
	mat4 camPV;
};

layout(set = 3, binding = 0, std430) uniform SkyData
{
	vec2 sunPos;
};


void main()
{
    const float planetRadius = 6371e3;
    const float atmosphereHeight = 100e3;

	vec3 camPos = -transpose(mat3(camV)) * camV[3].xyz;
	vec3 sunDir = -vec3(cos(sunPos[0]) * sin(sunPos[1]), sin(sunPos[0]), cos(sunPos[0]) * cos(sunPos[1]));
    vec3 skyDir = normalize(position.xyz / position.w - camPos);

    vec3 scatter = calculate_scattering(
        camPos,    // the start of the ray (the camera position)
        skyDir,                        // the direction of the ray (the camera vector)
        1e12,                           // the maximum distance the ray can travel (because something is in the way, like an object)
        vec3(0),                        // the color of the scene
        sunDir,                         // the direction of the light
        vec3(40),                       // how bright the light is, affects the brightness of the atmosphere
        vec3(0, -planetRadius, 0),             // the position of the planet
        planetRadius,                         // the radius of the planet
        planetRadius + atmosphereHeight,                         // the radius of the atmosphere
        vec3(5.5e-6, 13.0e-6, 22.4e-6), // the amount rayleigh scattering scatters the colors (for earth: causes the blue atmosphere)
        vec3(21e-6),                    // the amount mie scattering scatters colors
        vec3(2.04e-5, 4.97e-5, 1.95e-6),// how much air is absorbed
        vec3(0.0),                      // the amount of scattering that always occurs, cna help make the back side of the atmosphere a bit brighter
        0.7,                            // the direction mie scatters the light in (like a cone). closer to -1 means more towards a single direction
        8e3,                            // how high do you have to go before there is no rayleigh scattering?
        1.2e3,                          // the same, but for mie
        30e3,                           // the height at which the most absorption happens
        4e3,                            // how fast the absorption falls off from the absorption height
        32,                             // the amount of steps along the 'primary' ray, more looks better but slower
        16);                             // the amount of steps along the light ray, more looks better but slower

	vec3 color = 1.0 - exp(-scatter);
	outColor.rgba = vec4(scatter, 1.0);
}
