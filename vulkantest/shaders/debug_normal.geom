#version 450
#extension GL_ARB_separate_shader_objects : enable

layout(location = 0) in vec3 position[];
layout(location = 1) in vec3 normal[];

layout(triangles) in;
layout(line_strip, max_vertices=2) out;

layout(location = 0) out vec4 outColor;

layout(column_major, set = 2, binding = 0) uniform CameraP
{
	mat4 camP;
};

layout(column_major, set = 2, binding = 1) uniform CameraV
{
	mat4 camV;
};


void main()
{
	for (int i = 0; i < 3; i++)
	{
		outColor = vec4(0.0, 0.0, 1.0, 1.0);
		gl_Position = camP * camV * vec4(position[i], 1.0);
		EmitVertex();

	
		outColor = vec4(1.0, 1.0, 1.0, 1.0);
		gl_Position = camP * camV * vec4(position[i] + normal[i] * 5, 1.0);
		EmitVertex();

		EndPrimitive();
	}
}
