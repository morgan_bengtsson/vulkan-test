#version 450
#extension GL_ARB_separate_shader_objects : enable

#include "light.inc"

layout(location = 0) in vec3 position;
layout(location = 1) in vec3 normal;
layout(location = 2) in float depth;

layout(location = 0) out vec4 outColor;

layout(column_major, set = 1, binding = 0) uniform CameraP
{
	mat4 camP;
};

layout(column_major, set = 1, binding = 1) uniform CameraV
{
	mat4 camV;
};


layout(set = 3, binding = 0, std430) buffer readonly directionalLightBuffer
{
	DirectionalLight directionalLights[];
};

layout(set = 3, binding = 1, std430) buffer readonly positionalLightBuffer
{
	PositionalLight positionalLights[];
};



layout(set = 4, binding = 0) uniform MaterialBuffer
{
	Material material;
};


void main()
{
	//if (depth <= 0.5) discard;

	vec3 ambLight = vec3(0.0);
	vec3 diffLight = vec3(0.0);
	vec3 specLight = vec3(0.0);	
	float shininess = max(material.specular.a, 100);
	
	vec3 camPos = -transpose(mat3(camV)) * camV[3].xyz;
	vec3 v = normalize(camPos - position);

	for (int i = 0; i < directionalLights.length(); i++)
	{
		vec3 lightColor = directionalLights[i].color.rgb;
		vec3 lightDir = -directionalLights[i].direction.xyz;

		float normLightDot = dot(normal, lightDir);

		ambLight += lightColor;
		diffLight += max(0.0, normLightDot) * lightColor;
		vec3 reflection = 2 * normLightDot * normal - lightDir;
		specLight += pow(max(0, dot(reflection, v)), shininess) * lightColor;
	}

	vec3 emittanceColor = material.emittance.rgb;
	vec3 ambientColor = material.ambient.rgb * ambLight;
	vec3 diffuseColor = material.diffuse.rgb * diffLight;
	vec3 specularColor = material.specular.rgb * specLight * 10;

	outColor.rgb = (emittanceColor + ambientColor + diffuseColor + specularColor);
	outColor.a = 1.0f;//min(1.0, 1.0 - 1.0 / max(depth / 2, 1));
}

