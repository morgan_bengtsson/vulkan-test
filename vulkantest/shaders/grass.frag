#version 450
#extension GL_ARB_separate_shader_objects : enable

#include "light.inc"

layout(location = 0) in vec3 position;
layout(location = 1) in float inIndexPos;


layout(location = 0) out vec4 outColor;

layout(column_major, set = 1, binding = 0) uniform CameraP
{
	mat4 camP;
};

layout(column_major, set = 1, binding = 1) uniform CameraV
{
	mat4 camV;
};


layout(set = 3, binding = 0, std430) buffer readonly directionalLightBuffer
{
	DirectionalLight directionalLights[];
};

layout(set = 3, binding = 1, std430) buffer readonly positionalLightBuffer
{
	PositionalLight positionalLights[];
};

layout(set = 3, binding = 2) uniform sampler2DShadow shadowDistance;

layout(set = 4, binding = 0) uniform MaterialBuffer
{
	Material material;
};


void main()
{
	vec3 ambLight = vec3(0.0);
	vec3 diffLight = vec3(0.0);
	vec3 specLight = vec3(0.0);	
	float shininess = max(material.specular.a, 1);

	vec3 X = dFdx(position);
	vec3 Y = dFdy(position);
	vec3 normal = -normalize(cross(X,Y));

	
	vec3 camPos = -transpose(mat3(camV)) * camV[3].xyz;
	vec3 viewDir = normalize(position - camPos);

	for (int i = 0; i < directionalLights.length(); i++)
	{
		vec4 reflection = phongShadingWithShadow(directionalLights[i], material, position, normal, viewDir, shadowDistance);
		outColor.rgb += reflection.rgb / 2;
	}

	outColor.a = 1.0;// - pow(inIndexPos, 10);
}
