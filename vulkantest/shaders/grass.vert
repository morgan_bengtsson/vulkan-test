#version 450
#extension GL_ARB_separate_shader_objects : enable

out gl_PerVertex 
{
    vec4 gl_Position;
    float gl_PointSize;
    float gl_ClipDistance[];
    float gl_CullDistance[];
};

// Per-vertex
layout(location = 0) in vec3 inPosition;

layout(location = 0) out vec3 position;
layout(location = 1) out vec3 scale;
layout(location = 2) out float indexPos;



layout(column_major, set = 1, binding = 0) uniform CameraP
{
	mat4 camP;
};

layout(column_major, set = 1, binding = 1) uniform CameraV
{
	mat4 camV;
};

layout(column_major, set = 1, binding = 2) uniform CameraPV
{
	mat4 camPV;
};


// Per-instance (per grass field)
layout(column_major, set = 5, binding = 0) uniform GrassFieldTransform
{
	mat4 modelTransform;
} grass;


layout(set = 6, binding = 1) uniform sampler2D heightMap;

struct GrassField
{
	uint gridSizeX;
	uint gridSizeY;
};

struct VkDrawIndexedIndirectCommand
{
    uint    indexCount;
    uint    instanceCount;
    uint    firstIndex;
    int     vertexOffset;
    uint    firstInstance;
};

layout(set = 5, binding = 0) uniform GrassFieldBlock { GrassField grassField; };
layout(set = 5, binding = 1) buffer VkDrawIndexedIndirectCommandBlock { VkDrawIndexedIndirectCommand indirectCommand[]; };


float surfaceHeight(vec2 pos)
{
	ivec2 texSize = textureSize(heightMap, 0);
	return texture(heightMap, pos / texSize + vec2(0.5)).x;
}

highp float rand(vec2 co)
{
    vec2 ab = vec2(12.9898, 78.233);
    
	float c = 43758.5453;
    
	float dt = dot(co.xy, ab);
    
	float sn = mod(dt, 3.14);
    return fract(sin(sn) * c);
}

void main()
{
	const int nMaxStrands = 4096;
	uint drawCallIdx = gl_InstanceIndex / nMaxStrands;
	uint subIdx = gl_InstanceIndex - drawCallIdx * nMaxStrands;
	uvec2 cell = uvec2(drawCallIdx % grassField.gridSizeX, drawCallIdx / grassField.gridSizeX);

	VkDrawIndexedIndirectCommand dc = indirectCommand[drawCallIdx];

	ivec2 texSize = textureSize(heightMap, 0);

	// Pseudo random pos
	vec2 offset2d = vec2(
						((rand(vec2(drawCallIdx, subIdx + 0)) + cell.x) / grassField.gridSizeX - 0.5),
						((rand(vec2(drawCallIdx, subIdx + 0.1)) + cell.y) / grassField.gridSizeY - 0.5)
						) * texSize;
	vec3 offset3d = vec3(offset2d.x, surfaceHeight(offset2d), offset2d.y);
	scale = vec3(0.5);
	scale.xz *= sqrt(length((camV * grass.modelTransform * vec4(offset3d, 1)).xyz)) / 20;

	// scale
	gl_Position = grass.modelTransform * vec4(inPosition + offset3d, 1.0);
	position = gl_Position.xyz;
	indexPos = subIdx / dc.instanceCount;
}
