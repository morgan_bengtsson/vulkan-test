#version 450
#extension GL_ARB_separate_shader_objects : enable

out gl_PerVertex 
{
    vec4 gl_Position;
};

layout(column_major, set = 2, binding = 0) uniform CameraP
{
	mat4 camP;
};

layout(column_major, set = 2, binding = 1) uniform CameraV
{
	mat4 camV;
};

layout(column_major, set = 2, binding = 2) uniform CameraPV
{
	mat4 camPV;
};

const vec2 vertices[4] = {
    vec2(-1, -1),
    vec2(1, -1),
    vec2(-1, 1),
    vec2(1, 1)
};

layout(location = 0) out vec4 position;


void main()
{
    vec2 screenPos = vertices[gl_VertexIndex];
    gl_Position = vec4(screenPos, 1.0, 1.0);

    vec3 camPos = -transpose(mat3(camV)) * camV[3].xyz;
    vec4 pos4 = inverse(camPV) * gl_Position;
    position = pos4;
    
}
