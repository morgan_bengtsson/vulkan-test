

layout(set = 0, binding = 0) buffer readonly componentOffsetBufferBlock
{
	uint componentOffset[];
};

layout(set = 0, binding = 1) buffer readonly clippingBufferBlock
{
    uint clippingBuffer[];
};

layout (constant_id = 0) const uint NUM_SETS = 64;
layout (constant_id = 1) const uint MAX_NUM_BINDINGS = 64;

uint getComponentOffset(uint setNr, uint bindingNr)
{
	uint idx = (clippingBuffer[gl_InstanceIndex] * NUM_SETS + setNr) * MAX_NUM_BINDINGS + bindingNr;
	return componentOffset[idx];	
}
