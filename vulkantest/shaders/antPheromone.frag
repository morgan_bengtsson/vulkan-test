#version 450
#extension GL_ARB_separate_shader_objects : enable

#include "light.inc"

layout(location = 0) in vec3 position;
layout(location = 1) in vec3 normal;
layout(location = 2) in vec2 texCoord;

layout(location = 0) out vec4 outColor;

layout(column_major, set = 1, binding = 0) uniform CameraP
{
	mat4 camP;
};

layout(column_major, set = 1, binding = 1) uniform CameraV
{
	mat4 camV;
};

layout(set = 3, binding = 0, std430) buffer readonly directionalLightBuffer
{
	DirectionalLight directionalLights[];
};
layout(set = 3, binding = 1, std430) buffer readonly positionalLightBuffer
{
	PositionalLight positionalLights[];
};

struct PheromoneData
{
	float homeTrail;
	float foodTrail;
	float dangerTrail;
	float emptyTrail;
	float foodStore;
};

layout(set = 4, binding = 0, std430) buffer readonly pheromoneDataBuffer
{
	PheromoneData pheromoneData[];
};

void main()
{
	discard;
	vec3 ambLight = vec3(0.0);
	vec3 diffLight = vec3(0.0);
	vec3 specLight = vec3(0.0);	
	float shininess = max(200, 1);
	
	int mapWidth = int(sqrt(float(pheromoneData.length())));

	vec3 camPos = -transpose(mat3(camV)) * camV[3].xyz;
	vec3 viewDir = normalize(position - camPos);

	for (int i = 0; i < directionalLights.length(); i++)
	{
		vec3 lightColor = directionalLights[i].color.rgb;
		vec3 lightDir = directionalLights[i].direction.xyz;

		float normLightDot = max(0, dot(normal, -lightDir));
		ambLight += lightColor;
		diffLight += normLightDot * lightColor;
		vec3 reflection = -lightDir - 2 * normLightDot * normal;
		specLight += pow(max(0, dot(reflection, viewDir)), shininess) * lightColor;
	}

	for (int i = 0; i < positionalLights.length(); i++)
	{
		vec3 lightColor = positionalLights[i].color.rgb;
		vec3 lightPos = positionalLights[i].position.xyz;
		vec3 lightPointDir = position - lightPos;
		vec3 lightDir = normalize(lightPointDir);

		float intensity = 1.0 / max(1.0, sqrt(dot(lightPointDir, lightPointDir)) / 1000.0);

		float normLightDot = dot(normal, -lightDir);
		ambLight += lightColor * intensity;
		diffLight += normLightDot * lightColor * intensity;
		vec3 reflection = -lightDir - 2 * normLightDot * normal;
		specLight += pow(max(0, dot(reflection, viewDir)), shininess) * lightColor * intensity;
	}

	float mx = clamp(texCoord.y * mapWidth, 0, mapWidth - 2);
	float my = clamp(texCoord.x * mapWidth, 0, mapWidth - 2) ;

	PheromoneData values[4] = PheromoneData[4]( 
		pheromoneData[int(mx) * mapWidth + int(my)],
		pheromoneData[int(mx + 1) * mapWidth + int(my)],
		pheromoneData[int(mx) * mapWidth + int(my + 1)],
		pheromoneData[int(mx + 1) * mapWidth + int(my + 1)]
		);

	float homeTrail = mix(mix(values[0].homeTrail, values[1].homeTrail, mx - floor(mx)), mix(values[2].homeTrail, values[3].homeTrail, mx - floor(mx)), my - floor(my));
	float foodTrail = mix(mix(values[0].foodTrail, values[1].foodTrail, mx - floor(mx)), mix(values[2].foodTrail, values[3].foodTrail, mx - floor(mx)), my - floor(my));
	float foodStore = mix(mix(values[0].foodStore, values[1].foodStore, mx - floor(mx)), mix(values[2].foodStore, values[3].foodStore, mx - floor(mx)), my - floor(my));

	outColor.rgb = vec3(foodStore, foodTrail, homeTrail);
	outColor.a = 0.5;
}
