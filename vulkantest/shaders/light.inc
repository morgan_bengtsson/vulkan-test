
struct DirectionalLight
{
	mat4 view;
	mat4 projection;
	mat4 projectionView;
	vec4 color;
	vec4 direction;
};

struct PositionalLight
{
	mat4 view;
	mat4 projection;
	mat4 projectionView;
	vec4 color;
	vec4 position;
};

struct Material
{
	vec4 emittance;
	vec4 ambient;
	vec4 diffuse;
	vec4 specular;
};

float gaussShadowBlur(sampler2DShadow sTex, vec3 pos)
{
	float sDist = 0;

	sDist += textureOffset(sTex, pos, ivec2(-1, -1));
	sDist += 2 * textureOffset(sTex, pos, ivec2(0, -1));
	sDist += textureOffset(sTex, pos, ivec2(1, -1));
	sDist += 2 * textureOffset(sTex, pos, ivec2(-1, 0));
	sDist += 4 * textureOffset(sTex, pos, ivec2(0, 0));
	sDist += 2 * textureOffset(sTex, pos, ivec2(1, 0));
	sDist += textureOffset(sTex, pos, ivec2(-1, 1));
	sDist += 2 * textureOffset(sTex, pos, ivec2(0, 1));
	sDist += textureOffset(sTex, pos, ivec2(1, 1));


	sDist = sDist / 16.0;
	return sDist;
}

vec4 phongShading(DirectionalLight light, Material material, vec3 position, vec3 normal, vec3 viewDir)
{
	float shininess = max(material.specular.a, 1);
	
	vec3 lightColor = light.color.rgb;
	vec3 lightDir = light.direction.xyz;
	float normLightDot = dot(normal, lightDir);

	vec3 ambLight = lightColor;
	vec3 diffLight = max(0, normLightDot) * lightColor;
	vec3 reflection = lightDir - 2 * normLightDot * normal;
	vec3 specLight = pow(max(0, dot(reflection, viewDir)), shininess) * lightColor;

	vec3 emittanceColor = material.emittance.rgb;
	vec3 ambientColor = material.ambient.rgb * ambLight;
	vec3 diffuseColor = material.diffuse.rgb * diffLight;
	vec3 specularColor = material.specular.rgb * specLight;

	vec4 color = vec4(emittanceColor + ambientColor + diffuseColor + specularColor, 1.0);
	return color;
}

vec4 phongShadingWithShadow(DirectionalLight light, Material material, vec3 position, vec3 normal, vec3 viewDir, sampler2DShadow shadowTex)
{
	float shininess = max(material.specular.a, 1);
	
	vec3 lightColor = light.color.rgb;
	vec3 lightDir = light.direction.xyz;
	float normLightDot = dot(normal, lightDir);

	vec4 tPos = light.projectionView * vec4(position, 1.0);
	tPos = tPos / tPos.w;
	float shadow = 0.0;
	if (!any(greaterThan(abs(tPos.xyz), vec3(1.0))))
	{
		float bias = max(0, normLightDot) * 0.001;
		vec3 sPos = vec3(tPos.xy * 0.5 + vec2(0.5), tPos.z - bias);
		float sDist = gaussShadowBlur(shadowTex, sPos);
		shadow = 1.0 - sDist;
	}
	vec3 ambLight = lightColor;
	vec3 diffLight = (1 - shadow) * max(0, normLightDot) * lightColor;
	vec3 reflection = lightDir - 2 * normLightDot * normal;
	vec3 specLight = max(0, (1 - shadow)) * pow(max(0, dot(reflection, viewDir)), shininess) * lightColor;

	vec3 emittanceColor = material.emittance.rgb;
	vec3 ambientColor = material.ambient.rgb * ambLight;
	vec3 diffuseColor = material.diffuse.rgb * diffLight;
	vec3 specularColor = material.specular.rgb * specLight;

	vec4 color = vec4(emittanceColor + ambientColor + diffuseColor + specularColor, 1.0);
	return color;
}