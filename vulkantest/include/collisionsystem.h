#pragma once

#include "system.h"
#include "scenesubset.h"
#include "kdtree.h"
#include "particle.h"

#include <vulkanCommandBuffer.h>
#include <vulkanContext.h>
#include <vulkanComputePipeline.h>

#include <vector>

class CollisionSystem : public ecs::System<CollisionSystem>
{
public:
	typedef ecs::SceneSubSet<
		component::CollidableObject,
		kdtree::KdTree<component::CollidableObject, 6>,
		kdtree::ClosestObject,
		ecs::EntityRef,
		component::Speed,
		Particle
		> SceneType;

	CollisionSystem(vulkanutils::VulkanContext& context);
	static constexpr const char* getName()
	{
		return "CollisionSystem";
	}
	void runImpl(SceneType scene);
private:
	void collide(SceneType scene, ecs::Entity* e1, component::CollidableObject& coll1, ecs::Entity* e2, component::CollidableObject& coll2);
};

