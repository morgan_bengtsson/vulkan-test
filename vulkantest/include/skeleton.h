#pragma once

#include "skeletonanimaton.h"
#include "skeletonpose.h"

#include <vector>
#include <stdint.h>
#include <assimp/scene.h>
#include <map>
#include <optional>

namespace skeleton
{
constexpr size_t NUM_BONE_WEIGHTS = 4;

#pragma pack(push, 1)
struct VertexBoneData
{
	uint32_t boneIdx[NUM_BONE_WEIGHTS];
	float boneWeight[NUM_BONE_WEIGHTS];
};
#pragma pack(pop)

void getVertexBoneData(const aiMesh& mesh, skeleton::VertexBoneData* data);

#pragma pack(push, 1)
struct Bone
{
	float finalTransform[16];
};

struct Joint
{
	float offsetTransform[16];
	int32_t boneIdx;
	uint32_t firstChildNode;
	uint32_t numChildNodes;
	uint32_t padding;
};

struct ActiveAnimation
{
	uint32_t animationIndex;
	float progress; // 0 -> 1 (mod)
};

#pragma pack(pop)

class Skeleton
{
public:
	Skeleton() = default;
	Skeleton(const aiScene& scene, const aiMesh& mesh);
	void getBoneTransforms(size_t animationIdx, float timepoint, Bone* bones) const;

	size_t numBones() const;
	const std::vector<Animation>& getAnimations() const;
	const std::vector<Joint>& getJoints() const;

private:
	void trimSkeleton();
	
	void addNodeToSkeleton(size_t jointIdx,
		const aiNode& node,
		aiBone** const bones,
		const std::map<std::string, uint16_t>& nodeNameToBoneIdx);


	void initAnimations(const aiScene& scene);
	std::optional<size_t> getBoneIdx(const char* node);

	std::vector<Joint> mJoints;
	std::vector<Animation> mAnimations;
	std::map<std::string, uint16_t> mNodeNameToBoneIdx;
};

};
