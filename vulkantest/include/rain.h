#pragma once

#include <vulkanResourceSet.h>
#include "resourcesetbinding.h"

namespace component
{

struct Rain
{
	float width;
	float depth;
	float height;
	float density;
	float size;
	float speed;
};

#pragma pack(push, 1)
struct RainDrop
{
	float start[3];
	float size;
	float dir[3];
	float life;
};
#pragma pack (pop)

};


class RainModelSet : public EntityResourceSet
{
public:
	typedef ecs::SceneSubSet<
		component::Transform,
		component::RainDrop
	> SceneType;

	RainModelSet(vulkanutils::Device* device, SceneType scene, vulkanutils::DescriptorSetSlot descSetbp, vulkanutils::VertexInputSlot vertInputBp);
};

class RainMeshSet : public EntityResourceSet
{
public:
	typedef ecs::SceneSubSet<
		component::MeshVertex,
		component::MeshNormal,
		component::MeshIndex,
		component::Raw
	> SceneType;

	RainMeshSet(vulkanutils::Device* device, SceneType scene, vulkanutils::DescriptorSetSlot descSetbp, vulkanutils::VertexInputSlot vertInputBp);
};

class RainMaterialSet : public EntityResourceSet
{
public:
	typedef ecs::SceneSubSet<
		component::MaterialProperties
	> SceneType;

	RainMaterialSet(vulkanutils::Device* device, SceneType scene, vulkanutils::DescriptorSetSlot descSetbp, vulkanutils::VertexInputSlot vertInputBp);
};


