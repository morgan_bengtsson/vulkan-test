#pragma once

#include "drawsystem.h"
#include "resourcesetbinding.h"

#include <Entity.h>

#include <vector>


struct DrawCall
{
	void sort();
	struct ResourceSetBinding
	{
		std::vector<EntityResourceSet::BufferBinding<vulkanutils::UniformBuffer> > uniform;
		std::vector<EntityResourceSet::BufferBinding<vulkanutils::ShaderStorageBuffer> > shaderStorage;
		std::vector<EntityResourceSet::ImageBinding > image;
		std::vector<EntityResourceSet::VertexAttributeBinding > vertexBuffer;
	};

	size_t pipelineEntity;
	size_t drawableEntity;
	size_t renderPassEntity;
	std::map<const vulkanutils::ResourceSet*, ResourceSetBinding> bindings;
	std::map<const vulkanutils::ResourceSet*, ResourceSetBinding> cullBindings;

	const vulkanutils::BufferAllocation<vulkanutils::IndexBuffer>* indexBuffer = nullptr;
	const vulkanutils::BufferAllocation<vulkanutils::IndirectDrawBuffer>* indirectDrawBuffer = nullptr;
};


class CombinedDrawCall
{
public:
	static std::vector<CombinedDrawCall> build(DrawSystem::SceneType scene, DrawSystem& ds, size_t renderPassEntityId, std::vector<DrawCall> drawCalls);
	static void uploadComponentOffsetBuffers(VkCommandBuffer commandBuffer, std::span<CombinedDrawCall> combinedDrawCalls);

	CombinedDrawCall(DrawSystem::SceneType scene, DrawSystem& ds, size_t renderPassEntityId, std::span<DrawCall> drawCalls);
	CombinedDrawCall(const CombinedDrawCall&) = delete;
	CombinedDrawCall(CombinedDrawCall&& obj) = default;
	CombinedDrawCall& operator=(const CombinedDrawCall&) = delete;
	CombinedDrawCall& operator=(CombinedDrawCall&&) = default;

	struct BufferBinding
	{
		size_t bindingNr;
		VkBuffer buffer;
	};
	struct ImageBinding
	{
		size_t bindingNr;
		size_t elementNr;
		VkImageView imageView;
		VkSampler imageSampler;
	};
	struct ResourceSetBinding
	{
		bool operator==(const ResourceSetBinding&) const = default;

		std::vector<EntityResourceSet::BufferBinding<vulkanutils::UniformBuffer>> uniformBindings;
		std::vector<BufferBinding> shaderStorageBindings;
		std::vector<ImageBinding> imageBindings;
		std::vector<EntityResourceSet::VertexAttributeBinding> vertexAttributeBindings;
	};

	size_t mPipelineEntity;
	size_t mRenderPassEntity;

	std::map<const vulkanutils::ResourceSet*, ResourceSetBinding> mBindings;

	const vulkanutils::BufferAllocation<vulkanutils::IndexBuffer>* mIndexBuffer = nullptr;
	const vulkanutils::BufferAllocation<vulkanutils::IndirectDrawBuffer>* mIndirectDrawBuffer = nullptr;
	// Dim: entity (drawCall), set, component
	vulkanutils::BufferAllocation<vulkanutils::ShaderStorageBuffer> mDrawCommandDeviceBuffer;
	vulkanutils::BufferAllocation<vulkanutils::IndirectDrawBuffer> mDrawCommandDeviceIdBuffer;
	vulkanutils::BufferAllocation<vulkanutils::UploadBuffer> mDrawCommandHostBuffer;

	vulkanutils::BufferAllocation<vulkanutils::ShaderStorageBuffer> mClippingBuffer;

	vulkanutils::BufferAllocation<vulkanutils::ShaderStorageBuffer> mComponentOffsetDeviceBuffer;
	vulkanutils::BufferAllocation<vulkanutils::UploadBuffer> mComponentOffsetHostBuffer;

	vulkanutils::BufferAllocation<vulkanutils::ShaderStorageBuffer> mCullComponentOffsetDeviceBuffer;
	vulkanutils::BufferAllocation<vulkanutils::UploadBuffer> mCullComponentOffsetHostBuffer;

	std::map<const vulkanutils::ResourceSet*, vulkanutils::DescriptorSet> mDescriptorSets;
	uint32_t mMaxNumComponents = 0;
	uint32_t mNumInstances = 0;
};

class DrawCallBufferEqual
{
public:
	template<typename BufferType>
	bool operator()(const EntityResourceSet::BufferBinding<BufferType>& lhs, const EntityResourceSet::BufferBinding<BufferType>& rhs) const
	{
		return lhs.bindingNr == rhs.bindingNr && lhs.buffer == rhs.buffer;
	}


	bool operator()(const EntityResourceSet::VertexAttributeBinding& lhs, const EntityResourceSet::VertexAttributeBinding& rhs) const;
	bool operator()(const DrawCall::ResourceSetBinding& lhs, const DrawCall::ResourceSetBinding& rhs) const;
	bool operator()(const std::tuple<const vulkanutils::ResourceSet*, DrawCall::ResourceSetBinding>& lhs, std::tuple<const vulkanutils::ResourceSet*, DrawCall::ResourceSetBinding> rhs) const;
	bool operator()(const DrawCall& lhs, const DrawCall& rhs);
};

struct DrawCallDescriptorSetOrder
{
	bool operator()(const DrawCall::ResourceSetBinding& lhs, const DrawCall::ResourceSetBinding& rhs) const;
	bool operator()(const std::tuple<const vulkanutils::ResourceSet*, DrawCall::ResourceSetBinding>& lhs, std::tuple<const vulkanutils::ResourceSet*, DrawCall::ResourceSetBinding> rhs) const;
	bool operator()(const DrawCall& lhs, const DrawCall& rhs) const;
};


