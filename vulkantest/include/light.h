#pragma once

#include "resourcesetbinding.h"
#include "scenesubset.h"

#include <vulkanResourceSet.h>
#include <vulkanDevice.h>

class LightSet : public EntityResourceSet
{
public:
	typedef ecs::SceneSubSet<
		component::DirectionalLightGpu,
		component::PositionalLightGpu
		> SceneType;

	LightSet(vulkanutils::Device* device, SceneType scene, vulkanutils::DescriptorSetSlot descSetbp);
};

class LightWithShadowSet : public EntityResourceSet
{
public:
	typedef ecs::SceneSubSet<
		component::DirectionalLightGpu,
		component::PositionalLightGpu,
		component::ShadowDistance
		> SceneType;

	LightWithShadowSet(vulkanutils::Device* device, SceneType scene, vulkanutils::DescriptorSetSlot descSetbp);
};

class LightShadowSet : public EntityResourceSet
{
public:
	typedef ecs::SceneSubSet<
		component::DirectionalLightGpu,
		component::PositionalLightGpu,
		component::ShadowDistance,
		component::LightIndex
		> SceneType;

	LightShadowSet(vulkanutils::Device* device, SceneType scene, vulkanutils::DescriptorSetSlot descSetbp);
};

