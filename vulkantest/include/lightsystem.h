#pragma once

#include "system.h"
#include "scenesubset.h"
#include "light.h"
#include "renderpass.h"

#include "componenttypes.h"

#include <vulkanContext.h>
#include <vulkanCommandPool.h>



class LightSystem : public ecs::System<LightSystem>
{
public:
	typedef ecs::SceneSubSet<
			component::LightScene,
			component::PositionalLightGpu,
			component::DirectionalLightGpu,
			ecs::EntityRef,
			component::Orientation,
			component::DirectionalLight,
			component::PositionalLight,
			component::Position,
			component::ShadowDistance,
			component::MaterialTextureView,
			RenderSubPass,
			component::LightIndex,
			component::Color
		> SceneType;

	LightSystem(vulkanutils::VulkanContext& context, SceneType scene);

	static constexpr const char* getName()
	{
		return "LightSystem";
	}

	void runImpl(SceneType scene);
private:
	vulkanutils::CommandPool mCommandPool;
	vulkanutils::CommandBuffers mUploadCommandBuffers;
	std::vector<uint64_t> mLastSignalledValue;
	size_t mCmdBufferIdx;
};
