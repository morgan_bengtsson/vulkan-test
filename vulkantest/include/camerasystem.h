#pragma once

#include "system.h"
#include "scenesubset.h"
#include "camera.h"

#include "componenttypes.h"

#include <vulkanContext.h>
#include <vulkanCommandPool.h>



class CameraSystem : public ecs::System<CameraSystem>
{
public:
	typedef ecs::SceneSubSet<
			component::Position,
			component::Orientation,
			component::CameraProjection,
			component::Transform
		> SceneType;

	CameraSystem(vulkanutils::VulkanContext& context, SceneType scene);

	static constexpr const char* getName()
	{
		return "CameraSystem";
	}

	void runImpl(SceneType scene);
private:
	// WIP, remove this
	bool mSetup = false;
	void setup(SceneType scene);

	vulkanutils::CommandPool mCommandPool;
	vulkanutils::CommandBuffers mUploadCommandBuffers;
	std::vector<uint64_t> mLastSignalledValue;
	size_t mCmdBufferIdx;
};
