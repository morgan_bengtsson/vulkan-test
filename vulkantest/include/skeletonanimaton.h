#pragma once

#include "skeletonpose.h"

#include <vector>
#include <string>

namespace skeleton
{

class Animation
{
public:
	struct BoneAnimationKeyFrame
	{
		SkeletonPose::BoneTransform transform;
		float time;
		float padding[3];
	};
	class BoneAnimationSequence : public std::vector<BoneAnimationKeyFrame>
	{
	public:
		void getBoneTransform(float timePoint, SkeletonPose::BoneTransform& transform) const;
	};

	Animation(const char* name, size_t numBones);
	void setBoneSequence(size_t boneIdx, const BoneAnimationSequence& sequence);
	const std::vector<BoneAnimationSequence>& getBoneSequences() const;

	const char* getName() const;
	std::vector<SkeletonPose::BoneTransform> getSkeletonPose(float timepoint) const;

private:

	std::string mName;
	std::vector<BoneAnimationSequence> mBoneSequences;
};

};
