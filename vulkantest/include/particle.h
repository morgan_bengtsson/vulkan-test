#pragma once

#include "resourcesetbinding.h"


#pragma pack(push, 1)
struct ParticleProperties
{
	float radius;
	float mass;
};

struct Particle
{
	float position[3];
	float speed[3];
};

#pragma pack(pop)


// Set of models sharing the same descriptor layout
class ParticleModelSet : public EntityResourceSet
{
public:
	typedef ecs::SceneSubSet<
		component::Transform,
		ParticleProperties,
		component::Position
		> SceneType;

	ParticleModelSet(vulkanutils::Device* device
		, SceneType scene, vulkanutils::DescriptorSetSlot descSetbp
		, vulkanutils::VertexInputSlot vertInputBp);
};


class ParticleMeshSet : public EntityResourceSet
{
public:
	typedef ecs::SceneSubSet<
			component::MeshVertex,
			component::MeshNormal,
			component::MeshIndex,
			component::Raw
		> SceneType;

	ParticleMeshSet(vulkanutils::Device* device
		, SceneType scene, vulkanutils::DescriptorSetSlot descSetbp, vulkanutils::VertexInputSlot vertInputBp);
};
