#pragma once

#include <vector>

#include "resourcesetbinding.h"

class RenderSubPass
{
public:
	RenderSubPass();
	RenderSubPass(uint32_t width, uint32_t height);

	uint32_t width() const;
	uint32_t height() const;

	typedef EntityResourceSet::ImageBinding ImageBinding;

	template<typename ComponentType, typename SceneType>
	void addAttachmentCompType(const SceneType& scene, uint32_t bindingNr, VkFormat format, VkClearValue clearColor, bool presentation = false);

	struct Attachment
	{
		size_t componentType;
		uint32_t bindingNr;
		VkFormat format;
		VkClearValue clearColor;
		bool presentation;
	};

	std::vector<Attachment>& getAttachmentCompTypeIds();
	const std::vector<Attachment>& getAttachmentCompTypeIds() const;
	bool compatible(const component::Pipeline& pipeline) const;
	template<typename SceneType>
	std::vector<ImageBinding> getEntityBindings(const SceneType& scene, const ecs::Entity& entity) const;
	std::vector<VkClearValue> getClearColorValues() const;


	void enableForScreenBuffer(size_t idx);
	bool isEnabledForScreenBuffer(size_t idx) const;
	bool isEnabledForAnyScreenBuffer() const
	{
		return mScreenBuffers != 0;
	}

private:
	uint32_t mWidth;
	uint32_t mHeight;
	uint32_t mScreenBuffers;
	std::vector<Attachment> mAttachments;
};


#include "renderpass.tpp"
