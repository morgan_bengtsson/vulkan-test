#pragma once

#include <vulkanResourceSet.h>
#include <vulkanDescriptorSet.h>
#include <vulkanBufferAllocator.h>

#include "entity.h"

#include <vulkan/vulkan.h>


class EntityResourceSet : public vulkanutils::ResourceSet
{
public:
	EntityResourceSet(vulkanutils::Device* device, const char* name
		, vulkanutils::DescriptorSetSlot descSetbp
		, vulkanutils::VertexInputSlot vertInputBp
	);
	EntityResourceSet(EntityResourceSet&&) = default;
	EntityResourceSet& operator=(EntityResourceSet&&) = default;
	
	EntityResourceSet(const EntityResourceSet&) = delete;
	EntityResourceSet& operator=(const EntityResourceSet&) = delete;

	const char* getName() const;

	size_t getRequiredCompTypeIds() const;
	template<typename BufferType>
	size_t getRequiredCompTypeIds() const;
	size_t getRequiredImageCompTypeIds() const;

	template<typename SceneType>
	void bindEntitiesToDescriptors(const SceneType& scene, vulkanutils::DescriptorSet& descriptorSet, const std::vector<ecs::Entity>& entity) const;
	template<typename SceneType>
	void bindEntityToDescriptors(const SceneType& scene, vulkanutils::DescriptorSet& descriptorSet, const ecs::Entity& entity) const;
	template<typename BufferType, typename SceneType>
	void bindEntityToDescriptors(const SceneType& scene, vulkanutils::DescriptorSet& descriptorSet, const ecs::Entity& entity) const;


	template<typename BufferType, typename ComponentType, typename SceneType>
	void setDescriptorBinding(SceneType& scene, const vulkanutils::TypedDescriptorBinding<BufferType, ComponentType>& descBinding);
	template<typename SceneType, typename... TypedDescriptorBindings>
	void setDescriptorBindings(SceneType& scene, const TypedDescriptorBindings&... descBindings);

	template<typename ComponentType, typename SceneType>
	void setPerVertexAttributeBinding(
		SceneType& scene, uint32_t location, VkFormat format);
	template<typename ComponentType, typename SceneType>
	void setPerVertexAttributeBinding(
		SceneType& scene, const std::vector<vulkanutils::VertexAttrDescr>& attributes);

	template<typename ComponentType, typename SceneType>
	void setPerInstanceAttributeBinding(
		SceneType& scene, uint32_t location, VkFormat format);
	template<typename ComponentType, typename SceneType>
	void setPerInstanceAttributeBinding(
		SceneType& scene, const std::vector<vulkanutils::VertexAttrDescr>& attributes);



	template<typename ComponentType, typename SceneType>
	void setImageDescriptorBinding(SceneType& scene, const vulkanutils::TypedImageDescriptorBinding<ComponentType>& descBinding);

	// Structs describing input bind points of images and buffers
	struct ImageBinding
	{
		bool operator==(const ImageBinding& rhs) const = default;
		bool operator<(const ImageBinding& rhs) const
		{
			return std::make_tuple(bindingNr, elementNr, imageView, imageSampler)
				< std::make_tuple(rhs.bindingNr, elementNr, rhs.imageView, rhs.imageSampler);
		}
		
		void bind(vulkanutils::DescriptorSet& descriptorSet) const
		{
			descriptorSet.bindImage(*imageView,
				*imageSampler, bindingNr, elementNr);
		}

		size_t bindingNr = 0;
		size_t elementNr = 0;
		const vulkanutils::ImageView* imageView = nullptr;
		const vulkanutils::ImageSampler* imageSampler = nullptr;
	};

	template <typename BufferType>
	struct BufferBinding
	{
		bool operator==(const BufferBinding& rhs) const = default;
		bool operator<(const BufferBinding& rhs) const
		{
			return std::make_tuple(bindingNr, buffer->getBuffer(), buffer->getMemRange().offset)
				< std::make_tuple(rhs.bindingNr, rhs.buffer->getBuffer(), rhs.buffer->getMemRange().offset);
		}

		void bind(vulkanutils::DescriptorSet& descriptorSet) const
		{
			descriptorSet.bindBuffer(*buffer, bindingNr);
		}

		size_t bindingNr = 0;
		const vulkanutils::BufferAllocation<BufferType>* buffer = nullptr;
	};

	struct VertexAttributeBinding
	{
		bool operator==(const VertexAttributeBinding& rhs) const = default;
		bool operator<(const VertexAttributeBinding& rhs) const
		{
			return std::make_tuple(bindingNr, buffer->getBuffer(), buffer->getMemRange().offset)
				< std::make_tuple(rhs.bindingNr, rhs.buffer->getBuffer(), rhs.buffer->getMemRange().offset);
		}
		
		void bind(VkCommandBuffer commandBuffer, vulkanutils::DescriptorSet& descriptorSet)
		{
			VkBuffer buf = *buffer->getBuffer();
			VkDeviceSize offset = buffer->getMemRange().offset;
			vkCmdBindVertexBuffers(commandBuffer, uint32_t(bindingNr), 1, &buf, &offset);
		}

		VkVertexInputRate inputRate = VK_VERTEX_INPUT_RATE_VERTEX;
		size_t numInstances = 0;
		size_t bindingNr = 0;
		const vulkanutils::BufferAllocation<vulkanutils::VertexBuffer>* buffer = nullptr;
	};
	// ------------------------------

	template<typename BufferType, typename SceneType>
	std::vector<BufferBinding<BufferType>> getEntityBufferDescriptorBindings(const SceneType& scene, const ecs::Entity& entity) const;
	template<typename SceneType>
	std::vector<ImageBinding> getEntityImageBindings(const SceneType& scene, const ecs::Entity& entity) const;
	template<typename SceneType>
	std::vector<VertexAttributeBinding> getEntityBufferVertexAttributeBindings(const SceneType& scene, const ecs::Entity& entity) const;


	//template<typename BufferType>
	//void bindBuffers(vulkanutils::DescriptorSet& descriptorSet, std::span<BufferBinding<BufferType>> bufferBindings) const;
	//void bindImages(vulkanutils::DescriptorSet& descriptorSet, std::span<ImageBinding> bufferBindings) const;
	//void bindVertexBuffer(VkCommandBuffer commandBuffer, std::span<VertexAttributeBinding> bufferBindings) const;
	size_t bindIndexBuffer(VkCommandBuffer commandBuffer, const vulkanutils::BufferAllocation<vulkanutils::IndexBuffer>& indexBuffer) const;

	template<typename... BufferTypes>
	bool compatible(const ecs::Entity& entity) const;
	bool compatible(const ecs::Entity& entity) const;
	bool compatibleImage(const ecs::Entity& entity) const;
	bool getHasIndexBinding() const;

	size_t getComponentTypeIds();
	template<typename... ComponentTypes, typename SceneType>
	void addComponetTypes(const SceneType& scene);
	template<typename... ComponentTypes, typename SceneType>
	void addImageComponetTypes(const SceneType& scene);
	void setHasIndexBinding();

	template<typename BufferType>
	struct BindingCompTypeIds : public std::vector<size_t>
	{
		template<typename... ComponentTypes, typename SceneType>
		void addComponetTypes(const SceneType& scene);
	};
	template<typename BufferType>
	BindingCompTypeIds<BufferType>& getComponentTypeIds();
	template<typename BufferType>
	const BindingCompTypeIds<BufferType>& getComponentTypeIds() const;

	std::vector<size_t>& getImageComponentTypeIds();
	const std::vector<size_t>& getImageComponentTypeIds() const;

private:
	std::string mName;
	template<typename BufferType>
	uint32_t getDescrLayoutBinding(const vulkanutils::DescriptorLayout& layout, size_t idx);
	template<typename BufferType>
	bool compatibleHelp(const ecs::Entity& entity) const;
	uint32_t getNextBindPoint() const;

	size_t mRequiredComponentTypeIds;
	std::tuple<BindingCompTypeIds<vulkanutils::VertexBuffer>,
		BindingCompTypeIds<vulkanutils::IndexBuffer>,
		BindingCompTypeIds<vulkanutils::UniformBuffer>,
		BindingCompTypeIds<vulkanutils::ShaderStorageBuffer>>
		mBindingCompTypeId;
	std::vector<size_t> mRequiredImageComponentTypeIds;

	bool mHasIndexBinding;
};


#include "resourcesetbinding.tpp"
