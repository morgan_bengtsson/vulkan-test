#pragma once

#include "componenttypes.h"
#include "system.h"
#include "scenesubset.h"
#include "surface.h"
#include "skeleton.h"
#include "systemproperties.h"
#include "camera.h"
#include "model.h"
#include "mesh.h"

#include <vulkanContext.h>

class CharacterSystem : public ecs::System<CharacterSystem>
{
public:
		typedef ecs::SceneSubSet<
		component::Surface<float>,
		ecs::EntityRef,
		component::MeshVertex,
		component::MeshNormal,
		component::MeshTexCoord,
		component::MeshIndex,
		component::Color,
		component::Depth,
		component::Pipeline,
		component::MaterialProperties,
		component::SystemProperies,
		component::CameraProjection,
		component::Transform,
		component::Raw,
		skeleton::Bone,
		component::DirectionalLightGpu,
		component::PositionalLightGpu,
		component::SkinnedMeshVertex,
		component::SkinnedMeshNormal,
		component::ShadowDistance,
		component::LightIndex,
		component::Character,
		component::Orientation,
		component::Position,
		skeleton::ActiveAnimation,
		skeleton::Skeleton,
		component::Visibility,
		component::MaterialTexture,
		skeleton::VertexBoneData,
		component::HumanPlayerInput,
		component::HumanPlayer
		> SceneType;

	CharacterSystem(vulkanutils::VulkanContext& context, SceneType scene);
	static constexpr const char* getName()
	{
		return "CharacterSystem";
	}
	void runImpl(SceneType scene);

private:
	void updateCharacter(ecs::Entity& entity, SceneType& scene, const component::Surface<float>& surface);
	void setupCharacter(ecs::Entity& entity, SceneType& scene, const component::Surface<float>& surface, std::vector<ecs::Entity>& newEntities);

	ecs::Entity mDrawPipeline;
	ecs::Entity mShadowPipeline;
	struct MeshEntities
	{
		std::vector<size_t> meshes;
		std::vector<size_t> materials;
	};
	std::unordered_map<std::string, MeshEntities> mMeshEntities;
};


