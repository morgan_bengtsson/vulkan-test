#pragma once

#include "system.h"
#include "rain.h"
#include "material.h"
#include "skeleton.h"
#include "watersurface.h"
#include "particle.h"
#include "antSystem.h"
#include "grasssystem.h"
#include "renderpass.h"
#include "fpstimer.h"

#include <vulkanResourceSet.h>
#include <vulkanDevice.h>
#include <vulkanBufferAllocator.h>
#include <vulkanBufferCopier.h>
#include <vulkanBufferPool.h>
#include <vulkanCommandPool.h>
#include <vulkanCommandBuffer.h>

#include <concepts>
#include <map>
#include <chrono>
#include <set>


class DrawSystem : public ecs::System<DrawSystem>
{
public:
	typedef ecs::SceneSubSet<
		component::Raw,
		component::MeshVertex,
		component::MeshNormal,
		component::SkinnedMeshVertex,
		component::SkinnedMeshNormal,
		component::MeshTexCoord,
		component::MeshIndex,
		component::MaterialProperties,
		component::MaterialTexture,
		component::MaterialTextureView,
		component::Position,
		component::Speed,
		component::Scale,
		component::DirectionalLightGpu,
		component::PositionalLightGpu,
		component::CameraProjection,
		component::Pipeline,
		component::Orientation,
		component::DirectionalLight,
		component::PositionalLight,
		component::LightScene,
		ecs::EntityRef,
		component::Visibility,
		component::RainDrop,
		skeleton::Skeleton,
		skeleton::VertexBoneData,
		skeleton::Bone,
		component::Transform,
		WaterDepth,
		Particle,
		ParticleProperties,
		PheromoneData,
		component::SystemProperies,
		component::SurfaceProperties,
		Ant,
		component::Elevation,
		GrassField,
		VkDrawIndexedIndirectCommand,
		RenderSubPass,
		component::Color,
		component::Depth,
		component::LightIndex,
		component::ShadowDistance,
		component::Sky,
		component::MeshAABoundingBox
			> SceneType;

	DrawSystem(vulkanutils::VulkanContext& context, SceneType scene);
	DrawSystem(const DrawSystem&) = delete;
	DrawSystem(DrawSystem&&);
	~DrawSystem();

	static constexpr const char* getName()
	{
		return "DrawSystem";
	}

	template<typename T>
	void addExtension(const SceneType& scene)
	{
		std::unique_ptr<IDrawSystemExtension> extension	= std::make_unique<T>();
		addExtension(extension->requiredComponents(scene), std::move(extension));
	}
	void setup(SceneType scene);
	void upload(SceneType scene, size_t swapImageIdx);

	void recordCull(SceneType scene);

	void draw(SceneType scene);
	void recordDraw(SceneType scene, size_t swapImageIdx);
	void runImpl(SceneType scene);

private:
	friend class IDrawSystemExtension;

	void addExtension(size_t compIds, std::unique_ptr<IDrawSystemExtension>&& extension);
	void updateSystemProperties(ecs::Entity& entity, SceneType& scene, vulkanutils::BufferCopier& bufferCopier);



	vulkanutils::BufferAllocator<vulkanutils::ShaderStorageBuffer> mShaderStorageBufferAllocator;
	vulkanutils::BufferAllocator<vulkanutils::IndirectDrawBuffer> mIndirectDrawBufferAllocator;
	vulkanutils::BufferAllocator<vulkanutils::UploadBuffer> mUploadBufferAllocator;

	ComputeSystem mCullComputeSystem;
	ComputeSystem::Pipeline mCullPipeline;
	
	friend struct DrawCall;
	friend class CombinedDrawCall;



	// Record draw calls
	std::vector<CombinedDrawCall> buildCombinedDrawCalls(const std::vector<ecs::Entity*>& drawableEntities, size_t renderPassEntityId, const SceneType& scene);
	void recordDrawCalls(const SceneType& scene
		, VkCommandBuffer commandBuffer
		, size_t renderPassEntityId
		, std::vector<CombinedDrawCall>& drawCalls);

	std::map<size_t, std::vector<CombinedDrawCall>> mActiveDrawCalls;


	void setCullBinding(const SceneType& scene, const EntityResourceSet& set, const ecs::Entity* e, DrawCall& drawCall);
	void setDrawCallBinding(const SceneType& scene, const EntityResourceSet& set, const ecs::Entity* e, DrawCall& drawCall);
	void setDrawCallBindings(const SceneType& scene, const std::vector<EntityResourceSet>& sets, const ecs::Entity* e, DrawCall& drawCall);
	void buildBaseDrawCall(const SceneType& scene, const component::Pipeline& pipeline, const ecs::Entity* e, DrawCall& drawCall);
	std::vector<DrawCall> buildDrawCalls(const DrawSystem::SceneType& scene, const component::Pipeline& pipeline, const ecs::Entity* e, const DrawCall& drawCall);

	struct VertexBindInfo
	{
		size_t numInstances;
		size_t numIndices;
	};

	vulkanutils::ShaderSet& getOrCreateShaderSet(const char* shaderName);
	void buildPipeline(const ecs::Entity& entity, const ecs::Entity& renderPassEntity, const SceneType& scene);
	void buildRenderPass(const ecs::Entity& entity, const SceneType& scene);

	void cull(SceneType& scene);

	vulkanutils::RenderPass& getRenderPass(size_t entityId);
	const vulkanutils::RenderPass& getRenderPass(size_t entityId) const;
	std::vector<vulkanutils::GraphicsPipeline>& getPipeline(size_t pipelineEntityId, size_t renderPassEntityId);
	const std::vector<vulkanutils::GraphicsPipeline>& getPipeline(size_t pipelineEntityId, size_t renderPassEntityId) const;
	vulkanutils::FrameBuffer& getFrameBuffer(size_t entityId);
	const vulkanutils::FrameBuffer& getFrameBuffer(size_t entityId) const;


	vulkanutils::Device* mDevice;
	vulkanutils::DescriptorPool mDescriptorPool;
	EntityResourceSet mDrawSet;

	std::map<std::string, vulkanutils::ShaderSet> mShaders;
	std::map<std::tuple<size_t, size_t>, std::vector<vulkanutils::GraphicsPipeline> > mPipelines;
	std::map<size_t, vulkanutils::RenderPass > mRenderPasses;
	std::map<size_t, vulkanutils::FrameBuffer > mFrameBuffers;
	std::unique_ptr<struct DrawSystemExtensionSet> mExtensionSet;

	size_t mCullingEntity;

	enum class State
	{
		SETUP,
		UPLOAD,
		RENDER
	};

	State mSystemState;
	size_t mFrameCounter;
	std::atomic<uint32_t> mPendingImageIndex;
	std::atomic<uint32_t> mSubmittedImageIndex;

	std::chrono::system_clock::time_point mStartTime;

	vulkanutils::CommandPool mRenderCommandPool;
	vulkanutils::CommandBuffers mRenderCommandBuffers;
	std::map<size_t, vulkanutils::CommandBuffers> mExtraRenderCommandBuffers;

	vulkanutils::CommandPool mUploadCommandPool;
	vulkanutils::CommandBuffers mUploadCommandBuffers;

	vulkanutils::CommandPool mComputeCommandPool;
	vulkanutils::CommandBuffers mComputeCommandBuffers;

	void renderThreadFunc();
	void submitFrame(uint32_t imageIdx);

	static constexpr bool mAsyncSubmit = false;

	std::atomic<bool> mQuitRenderThread;
	std::mutex mRenderMutex;
	std::thread mRenderThread;
	std::unique_ptr<SceneType> mRenderScene;

	FpsTimer mFpsTimer;
};
