#pragma once

#include "skeletonpose.h"

#include <vector>
#include <stdint.h>
#include <assimp/scene.h>

#include <Eigen/Dense>

namespace skeleton
{

template<int Alignment = Eigen::Unaligned>
void interpolateRot(const float quat1[4], const float quat2[4], float factor, float ret[4])
{
	Eigen::Map<Eigen::Quaternionf, Alignment> q(ret);
	q = Eigen::Map<const Eigen::Quaternionf, Alignment>(quat1).slerp(factor, Eigen::Map<const Eigen::Quaternionf, Alignment>(quat2));
}

template<int Alignment = Eigen::Unaligned>
void interpolatePos(const float pos1[3], const float pos2[3], float factor, float ret[3])
{
	Eigen::Map<Eigen::Vector4f, Alignment> retMap(ret);
	retMap = (1.0f - factor) * Eigen::Map<const Eigen::Vector4f, Alignment>(pos1) + factor * Eigen::Map<const Eigen::Vector4f, Alignment>(pos2);
}

template<int Alignment = Eigen::Unaligned>
void interpolateScale(const float scale1[3], const float scale2[3], float factor, float ret[3])
{
	Eigen::Map<Eigen::Vector4f, Alignment> retMap(ret);
	retMap = (1.0f - factor) * Eigen::Map<const Eigen::Vector4f, Alignment>(scale1) + factor * Eigen::Map<const Eigen::Vector4f, Alignment>(scale2);
}

// Keyframes
struct AnimationKeyFrameRange // TMP
{
	uint32_t firstKeyFrame;
	uint32_t numKeyFrames;
};

struct SkeletonPose
{
	class BoneTransform
	{
	public:
		BoneTransform();
		BoneTransform(const Eigen::Matrix4f& matrix);
		BoneTransform(float position[4], float quaternion[4], float scale[3]);
		BoneTransform(const BoneTransform& prev, const BoneTransform& next, float progress);
		
		void setInterpolate(const BoneTransform& prev, const BoneTransform& next, float progress);

		const float* getPosition() const { return position; }
		const float* getQuaternion() const { return quaternion; }
		const float* getScale() const { return scale; }
		Eigen::Matrix4f getMatrix() const;
	private:
		float quaternion[4];
		float position[4];
		float scale[4];
	};
	std::vector<BoneTransform> boneTransforms;
};

};
