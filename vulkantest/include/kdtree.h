#pragma once

#include <stdint.h>
#include <stdlib.h>

#include <vector>
#include <array>
#include <span>


namespace kdtree
{

void testTree();

typedef uint32_t BranchId;
typedef uint32_t LeafId;

#pragma pack(push, 1)
struct Leaf
{
	uint32_t firstObject;
	uint32_t numObjects;
};


struct ClosestObject
{
	uint32_t entityId;
	uint32_t objectId;
	float distance;
};

template<uint32_t N>
struct KdTreeData
{
	static constexpr uint32_t NumBranches = (1 << N) - 1;
	static constexpr uint32_t NumLeaves = (1 << N);
	
	float dividers[NumBranches];
	Leaf leaves[NumLeaves];
};
#pragma pack(pop)

template<typename LeafDataType, uint32_t N>
class KdTree
{
public:
	KdTree()
		: mTreeData()
		, mLeafData()
	{
	}
	static constexpr uint32_t NumBranches = (1 << N) - 1;
	static constexpr uint32_t NumLeaves = (1 << N);

	template<uint32_t NO>
	std::array<LeafDataType, NO> getClosestObjects(const float point[3]) const;


	void setObjects(std::span<LeafDataType> objects);
	void rebalance();

	const KdTreeData<N>& getKdTreeData() const;
	std::span<const LeafDataType> getLeafData() const;

private:
	void rebalance(BranchId branch, uint32_t level, std::span<LeafDataType> objects);
	
	KdTreeData<N> mTreeData;
	std::vector<LeafDataType> mLeafData;


	static uint32_t getDim(uint32_t Level);
	static BranchId getLeftChild(BranchId branch);
	static BranchId getRightChild(BranchId branch);
	static BranchId getParent(BranchId branch);
	static bool isRoot(BranchId branch);
	static bool isLeaf(BranchId branch);
	static LeafId getLeafId(BranchId branch);
	
	std::span<const LeafDataType> getBranchObjects(BranchId branch, uint32_t level);
	LeafId getLeaf(const float point[3]) const;
	std::span<const LeafDataType> getLeafObjects(LeafId leafId) const;
};

};

#include "kdtree.tpp"
