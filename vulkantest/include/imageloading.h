#pragma once

#include <memory>


class ImageBitmap
{
public:
	ImageBitmap();
	ImageBitmap(const char* filename);
	ImageBitmap(const ImageBitmap&) = delete;
	ImageBitmap& operator=(const ImageBitmap&) = delete;
	
	ImageBitmap(ImageBitmap&&) noexcept;
	ImageBitmap& operator=(ImageBitmap&&) noexcept;

	~ImageBitmap();

	size_t width() const;
	size_t height() const;
	size_t bytesPerRow() const;
	const uint32_t* data() const;


private:
	std::unique_ptr<struct ImageBitmapImpl> mImpl;
};
