#pragma once

#include "system.h"
#include "scenesubset.h"
#include "watersurface.h"
#include "skeleton.h"

class WaterSystem : public ecs::System<WaterSystem>
{
public:
	typedef ecs::SceneSubSet<WaterSurface,
		component::RunMode,
		ecs::EntityRef,
		component::MeshVertex,
		component::MeshNormal,
		component::MeshTexCoord,
		component::MeshIndex,
		component::Color,
		component::Depth,
		component::Pipeline,
		component::MaterialProperties,
		component::SystemProperies,
		component::CameraProjection,
		component::Transform,
		component::Raw,
		skeleton::Bone,
		component::DirectionalLightGpu,
		component::PositionalLightGpu,
		WaterDepth> SceneType;

	WaterSystem(vulkanutils::VulkanContext& context, SceneType scene);
	static constexpr const char* getName()
	{
		return "WaterSystem";
	}

	void runImpl(SceneType scene);
private:
	void updateWater(ecs::Entity& entity, SceneType& scene) const;
	void setupWater(ecs::Entity& entity, SceneType& scene) const;

	ecs::Entity mWaterSurfacePipeline;
	ecs::Entity mWaterMaterialEntity;
};
