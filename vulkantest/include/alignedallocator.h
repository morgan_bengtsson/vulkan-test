#pragma once

#include <memory>

template <typename T, size_t Alignment>
class AlignedAllocator : public std::allocator<T>
{
public:
	AlignedAllocator()
	{}
	AlignedAllocator& operator=(const AlignedAllocator &rhs)
	{
		std::allocator<T>::operator=(rhs);
		return *this;
	}

	T* allocate(size_t n)
	{
		return reinterpret_cast<T*>(aligned_malloc(Alignment, n));
	}

	void deallocate(T* p, size_t n)
	{
		aligned_free(p);
	}

	void construct(T* p, T&& val)
	{
		new (p) T(val);
	}

	void destroy(T* p)
	{
		p->~T();
	}
};
