#pragma once

#include "system.h"

#include "watersurface.h"
#include "rain.h"
#include "scenesubset.h"
#include "skeleton.h"

#include <vulkanCommandPool.h>
#include <vulkanCommandBuffer.h>

#include <random>

class RainSystem : public ecs::System<RainSystem>
{
public:
	typedef ecs::SceneSubSet<
		WaterSurface,
		component::Surface<float>,
		component::Rain,
		component::RainDrop,
		component::SystemProperies,
		component::Position,
		component::Orientation,
		component::CameraProjection,
		component::Transform,
		component::MaterialProperties,
		component::Raw,
		component::MaterialTexture,
		ecs::EntityRef,
		component::MeshVertex,
		component::MeshNormal,
		component::MeshTexCoord,
		component::MeshIndex,
		skeleton::Skeleton,
		skeleton::VertexBoneData,
		component::Color,
		component::Depth,
		component::Pipeline,
		component::Visibility,
		skeleton::ActiveAnimation
		> SceneType;

	RainSystem(vulkanutils::VulkanContext& context, SceneType scene);
	static constexpr const char* getName()
	{
		return "RainSystem";
	}
	void runImpl(SceneType scene);

private:
	void setup(ecs::Entity& entity, SceneType& scene);

	std::vector<ecs::Entity> mRainMeshes;
	std::vector<ecs::Entity> mRainMaterials;
	ecs::Entity mRainDrawPipeline;

	vulkanutils::CommandPool mCommandPool;
	vulkanutils::CommandBuffers mCommandBuffers;

	std::default_random_engine mRandomRainGenerator;
};
