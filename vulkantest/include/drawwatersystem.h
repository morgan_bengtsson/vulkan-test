#pragma once

#include "drawsystem.h"
#include "idrawsystemextension.h"

class DrawWaterSystem : public IDrawSystemExtension
{
public:
	DrawWaterSystem();

	virtual size_t requiredComponents(const DrawSystem::SceneType& scene) const override;

	virtual void update(DrawSystem& ds, ecs::Entity& entity, DrawSystem::SceneType& scene, vulkanutils::BufferCopier& bufferCopier) override;
	virtual void draw(
		DrawSystem& ds,
		VkCommandBuffer commandBuffer,
		const std::vector<ecs::Entity>& rainEntities,
		const DrawSystem::SceneType& components) override;

};
