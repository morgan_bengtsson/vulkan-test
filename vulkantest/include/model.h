#pragma once

#include "mesh.h"
#include "camera.h"
#include "light.h"
#include "resourcesetbinding.h"
#include "scenesubset.h"

#include <vulkanResourceSet.h>
#include <vulkanDescriptorSet.h>

// Set of models sharing the same descriptor layout
class ModelSet : public EntityResourceSet
{
public:
	typedef ecs::SceneSubSet<
		skeleton::Bone,
		component::Transform
		> SceneType;

	ModelSet(vulkanutils::Device* device, SceneType scene, vulkanutils::DescriptorSetSlot descSetbp, bool withSkeleton = false);
};

