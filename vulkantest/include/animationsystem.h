#pragma once

#include "componenttypes.h"
#include "system.h"
#include "skeleton.h"
#include "scenesubset.h"
#include "computesystem.h"

#include <vulkanContext.h>

class AnimationSystem : public ecs::System<AnimationSystem>, public ComputeSystem
{
public:
	typedef ecs::SceneSubSet<
		skeleton::Skeleton,
		skeleton::ActiveAnimation,
		skeleton::Bone,
		ecs::EntityRef,
		component::SystemProperies,
		skeleton::Joint,
		skeleton::Animation::BoneAnimationKeyFrame,
		skeleton::AnimationKeyFrameRange,
		component::MeshVertex,
		component::MeshNormal,
		component::SkinnedMeshVertex,
		component::SkinnedMeshNormal,
		skeleton::VertexBoneData,
		component::Pipeline
		> SceneType;

	AnimationSystem(vulkanutils::VulkanContext& context, SceneType scene);
	static constexpr const char* getName()
	{
		return "AnimationSystem";
	}
	void runImpl(SceneType scene);

private:
	ComputeSystem::Pipeline mAnimationPipeline;
	ComputeSystem::Pipeline mSkinningPipeline;
	std::map<std::tuple<size_t, size_t>, std::vector<vulkanutils::DescriptorSet>> mSkinningDescSets;

	// WIP, remove this
	bool mSetup = false;
	void setup(SceneType scene);

	vulkanutils::CommandPool mCommandPool;
	vulkanutils::CommandBuffers mCommandBuffers;
	std::vector<uint64_t> mLastSignalledValue;
	size_t mCmdBufferIdx;

	vulkanutils::DescriptorPool mDescriptorPool;

};
