#pragma once

#include <vulkanResourceSet.h>
#include <vulkanDescriptorSet.h>

#include "resourcesetbinding.h"
#include "scenesubset.h"
	
typedef ecs::SceneSubSet<
	component::CameraProjection,
	component::Transform,
	component::Color,
	component::Depth
	> CameraSetSceneType;

EntityResourceSet cameraSet(vulkanutils::Device* device, CameraSetSceneType scene, vulkanutils::DescriptorSetSlot descSetbp);

void perspectiveTransform(float matrix[16], float near, float far, float left, float right, float up, float down, int width, int height);
void orthographicTransform(float matrix[16], float near, float far, float left, float right, float up, float down);