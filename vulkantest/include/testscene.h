#pragma once

#include "scene.h"
#include "entity.h"
#include "surface.h"
#include "skeleton.h"
#include "watersurface.h"
#include "inputsystem.h"
#include "rain.h"
#include "particle.h"
#include "kdtree.h"
#include "antsystem.h"
#include "grasssystem.h"
#include "renderpass.h"

#include "qtvulkanwindow.h"

#include <vulkanDevice.h>
#include <chrono>

template<typename... ComponentTypes>
class TestSceneT
	: public ecs::Scene<ComponentTypes...>
{
public:
	TestSceneT(vulkanutils::Device* device)
		: ecs::Scene<ComponentTypes...>(device)
	{}
protected:
	typedef TestSceneT<ComponentTypes...> BaseClass;
private:
};


class TestScene : public TestSceneT<component::Surface<float>,
	component::Raw,
	component::MeshVertex,
	component::MeshNormal,
	component::MeshTexCoord,
	component::MeshIndex,
	component::MaterialProperties,
	component::MaterialTexture,
	component::MaterialTextureView,
	component::Position,
	component::Scale,
	component::Speed,
	component::DirectionalLightGpu,
	component::PositionalLightGpu,
	component::CameraProjection,
	component::Pipeline,
	component::Orientation,
	component::DirectionalLight,
	component::PositionalLight,
	component::LightScene,
	ecs::EntityRef,
	component::RunMode,
	component::Visibility,
	component::Rain,
	component::RainDrop,
	component::SystemProperies,
	skeleton::Skeleton,
	skeleton::VertexBoneData,
	skeleton::ActiveAnimation,
	skeleton::Bone,
	component::Transform,
	WaterSurface,
	WaterDepth,
	InputSystem,
	Particle,
	ParticleProperties,
	component::CollidableObject,
	kdtree::KdTree<component::CollidableObject, 6>,
	kdtree::ClosestObject,
	AntHill,
	Ant,
	AntPheromoneMap,
	PheromoneData,
	component::SurfaceProperties,
	AntWorld,
	component::Elevation,
	GrassField,
	VkDrawIndexedIndirectCommand,
	RenderSubPass,
	component::Color,
	component::Depth,
	component::ShadowDistance,
	component::LightIndex,
	skeleton::Joint,
	skeleton::Animation::BoneAnimationKeyFrame,
	skeleton::AnimationKeyFrameRange,
	component::SkinnedMeshVertex,
	component::SkinnedMeshNormal,
	component::Sky,
	component::Character,
	component::MeshAABoundingBox,
	component::HumanPlayerInput,
	component::HumanPlayer
	>
{
public:
	TestScene(vulkanutils::VulkanContext& context);

	void setup(QtVulkanWindow& qtWindow);
	virtual void teardown() override;
	virtual void update() override;

private:
	std::chrono::system_clock::time_point mStartTime;
	vulkanutils::VulkanContext& mContext;
	vulkanutils::Device* mDevice;


	std::unique_ptr<ecs::SceneSystem<TestScene>> mSystems;
};
