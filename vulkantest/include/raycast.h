#pragma once

#include <componenttypes.h>

#include <scenesubset.h>

namespace raycast
{

typedef ecs::SceneSubSet<
	component::Position
	, component::Orientation
	, component::Scale
	, component::MeshAABoundingBox
	, component::MeshVertex
	, component::MeshIndex
	, ecs::EntityRef> RaycastSceneType;


bool intersects(const component::MeshAABoundingBox& bbox, const Eigen::ParametrizedLine<float, 3> ray);
float intersection(const std::span<const component::MeshIndex> faces, const component::MeshVertex* vertices, const Eigen::ParametrizedLine<float, 3> ray);
bool intersection(const RaycastSceneType& scene, const Eigen::ParametrizedLine<float, 3>& ray, component::RayEntityIntersection* intersectionRes);

Eigen::ParametrizedLine<float, 3> getCameraRay(const Eigen::Vector2f& normScreenPos, const Eigen::Vector3f& camPos, const Eigen::Matrix4f& invCamTransform);

};