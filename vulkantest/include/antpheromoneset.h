#pragma once

#include "resourcesetbinding.h"
#include "antsystem.h"

class AntPheromoneSet : public EntityResourceSet
{
public:
	template<typename SceneType>
	AntPheromoneSet(vulkanutils::Device* device, const SceneType& scene, vulkanutils::DescriptorSetSlot descSetbp, vulkanutils::VertexInputSlot vertInputBp);
};

template<typename SceneType>
AntPheromoneSet::AntPheromoneSet(vulkanutils::Device* device, const SceneType& scene, vulkanutils::DescriptorSetSlot descSetbp, vulkanutils::VertexInputSlot vertInputBp)
	:EntityResourceSet(device, "antpheromone", descSetbp, vertInputBp)
{
	vulkanutils::DescriptorBinding<vulkanutils::ShaderStorageBuffer> storageBindings[] =
	{
		{
			VK_SHADER_STAGE_VERTEX_BIT | VK_SHADER_STAGE_FRAGMENT_BIT, 0
		}
	};
	vulkanutils::DescriptorLayout descriptorLayout(*getDevice());
	descriptorLayout.addDescriptorBindings<vulkanutils::ShaderStorageBuffer>({storageBindings});
	setDescriptorLayout(std::move(descriptorLayout));

	getComponentTypeIds<vulkanutils::ShaderStorageBuffer>().addComponetTypes<PheromoneData>(scene);
}


