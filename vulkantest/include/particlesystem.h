#pragma once

#include "system.h"
#include "particle.h"
#include "entity.h"
#include "surface.h"
#include "vulkanBufferAllocator.h"
#include "resourcesetbinding.h"
#include "kdtree.h"
#include "scenesubset.h"

#include <vulkanComputePipeline.h>
#include <vulkanResourceSet.h>
#include <vulkanDescriptorSet.h>
#include <random>
#include <vulkanCommandPool.h>
#include <vulkanCommandBuffer.h>
#include <vulkanBufferPool.h>

class ParticleComputeSet : public EntityResourceSet
{
public:
	typedef ecs::SceneSubSet<
		component::Transform,
		ParticleProperties,
		component::Position,
		component::Speed
	> SceneType;

	ParticleComputeSet(vulkanutils::Device* device, SceneType scene, vulkanutils::DescriptorSetSlot descSetbp);
};

class KNNComputeSet : public EntityResourceSet
{
public:
	typedef ecs::SceneSubSet<
		component::CollidableObject,
		kdtree::ClosestObject,
		kdtree::KdTree<component::CollidableObject, 6>
	> SceneType;

	KNNComputeSet(vulkanutils::Device* device, SceneType scene, vulkanutils::DescriptorSetSlot descSetbp);
};


class ParticleSystem : public ecs::System<ParticleSystem>
{
public:
	typedef ecs::SceneSubSet<
		Particle,
		ParticleProperties,
		component::Position,
		component::Transform,
		component::Orientation,
		component::Speed,
		component::SystemProperies,
		component::Surface<float>,
		component::CollidableObject,
		kdtree::KdTree<component::CollidableObject, 6>,
		kdtree::ClosestObject
		> SceneType;

	ParticleSystem(vulkanutils::VulkanContext& context, SceneType scene);
	static constexpr const char* getName()
	{
		return "ParticleSystem";
	}
	void runImpl(SceneType scene);

private:
	void setupParticles(ecs::Entity& particeEntity, SceneType& scene);
	void updateParticles(ecs::Entity& particeEntity, SceneType& scene);

	void setParticleData(ecs::Entity& particeEntity, SceneType& scene);
	void getParticleData(ecs::Entity& particeEntity, SceneType& scene);

	vulkanutils::DescriptorPool mDescriptorPool;
	vulkanutils::CommandPool mCommandPool;

	struct ComputePipelineData
	{
		vulkanutils::ComputeShader shader;
		vulkanutils::ComputePipeline computePipeline;
		std::unique_ptr<EntityResourceSet> computeSet;
	};
	ComputePipelineData mCollisionPipeline;
	ComputePipelineData mParticlePipeline;

	std::default_random_engine mRandomGenerator;

	struct ParticleSystemState
	{
		size_t iteration;
		vulkanutils::DescriptorSet computeDescriptorSets[2];
		vulkanutils::CommandBuffers computeCommandBuffers;
	};
	std::map<size_t, ParticleSystemState> mParticleSystemStates;
};


#include "particlesystem.tpp"