#pragma once

#include "system.h"
#include "surface.h"
#include "scenesubset.h"
#include "skeleton.h"

#include <random>

class ForestSystem : public ecs::System<ForestSystem>
{
public:
	ForestSystem(vulkanutils::VulkanContext& vc);

	typedef ecs::SceneSubSet<component::SystemProperies
		, component::Pipeline
		, component::MaterialProperties
		, component::MaterialTexture
		, ecs::EntityRef
		, component::MeshVertex
		, component::MeshNormal
		, component::MeshTexCoord
		, component::MeshIndex
		, skeleton::Skeleton
		, skeleton::VertexBoneData
		, component::CameraProjection
		, component::Transform
		, component::Raw
		, skeleton::Bone
		, component::DirectionalLightGpu
		, component::PositionalLightGpu
		, component::Visibility
		, skeleton::ActiveAnimation
		, component::Surface<float>
		, component::Position
		, component::Orientation
		, component::Scale
		, component::Color
		, component::Depth
		, component::ShadowDistance
		, component::LightIndex>
		SceneType;

	static constexpr const char* getName()
	{
		return "ForestSystem";
	}
	void runImpl(SceneType scene);

private:
	void setup(SceneType scene);

	enum class State
	{
		SETUP,
		RUN,
	};
	State mState = State::SETUP;

	static constexpr const char* mTreeModelFile = "./models/EU43_Castanea_sativa_Chestnut_3ds/Models/EU43_1.3ds";


	ecs::Entity mTreeDrawPipeline;
	ecs::Entity mTreeShadowPipeline;
	ecs::Entity mTreeDebugDrawPipeline;
	std::vector<ecs::Entity> mTreeMeshes;
	std::vector<ecs::Entity> mTreeMaterials;
};
