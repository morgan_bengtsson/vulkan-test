#pragma once

#include "system.h"
#include "entity.h"
#include "listener.h"
#include "scenesubset.h"

#include "watersurface.h"

#include <chrono>
#include <queue>
#include <set>

#include <QWidget>
#include <QPlainTextEdit>
#include <QLineEdit>
#include <QBoxLayout>



class InputSystem : public ecs::System<InputSystem>, public comm::Listener<KeyboardInput>, public comm::Listener<MouseInput>
{
public:
	typedef ecs::SceneSubSet<WaterSurface
		, component::RunMode
		, component::CameraProjection
		, component::Position
		, component::Orientation
		, component::Scale
		, component::MeshAABoundingBox
		, component::MeshVertex
		, component::MeshIndex
		, ecs::EntityRef
		, component::HumanPlayerInput> SceneType;

	InputSystem(vulkanutils::VulkanContext& context);
	static constexpr const char* getName()
	{
		return "InputSystem";
	}
	void runImpl(SceneType scene);
private:
	struct KeyboardInputComp
	{
		bool operator()(const KeyboardInput& ki, const KeyboardInput& ki2) const
		{
			return ki.key < ki2.key;
		}
	};

	void setupDebugWindow();
	void processCommand(SceneType& scene);
	void updateRunMode(ecs::Entity& entity, SceneType& scene, const KeyboardInput& keyboardInput);
	void updateInput(ecs::Entity& entity, SceneType& scene, const KeyboardInput& keyboardInput);
	virtual void execute(const KeyboardInput& message) override;
	virtual void execute(const MouseInput& message) override;

	std::vector<KeyboardInput> mPressedKeys;
	std::vector<MouseInput> mMouseInputEvents;
	QWidget* mDebugWindow;
	QPlainTextEdit* mLogWidget;
	QLineEdit* mCmdWidget;
	std::string mCommandToProcess;
};
