#pragma once 

#include "kdtree.h"

#include <algorithm>
#include <cmath>
#include <iostream>


namespace kdtree
{

template<typename LeafDataType, uint32_t N>
void KdTree<LeafDataType, N>::setObjects(std::span<LeafDataType> objects)
{
	for (auto& leaf : mTreeData.leaves)
	{
		leaf.firstObject = 0;
		leaf.numObjects = 0;
	}
	for (auto& object : objects)
	{
		LeafId leafId = getLeaf(object.point);
		mTreeData.leaves[leafId].numObjects++;
	}
	
	for (uint32_t i = 0, idx = 0; i < NumLeaves; i++)
	{
		mTreeData.leaves[i].firstObject = idx;
		idx += mTreeData.leaves[i].numObjects;
		mTreeData.leaves[i].numObjects = 0;
	}

	mLeafData.resize(objects.size());
	for (auto& object : objects)
	{
		LeafId leafId = getLeaf(object.point);
		mLeafData[mTreeData.leaves[leafId].firstObject + mTreeData.leaves[leafId].numObjects] = object;
		mTreeData.leaves[leafId].numObjects++;
	}
}

template<typename LeafDataType, uint32_t N>
void KdTree<LeafDataType, N>::rebalance()
{
	rebalance(0, 0, { mLeafData });	
}

template<typename LeafDataType, uint32_t N>
void KdTree<LeafDataType, N>::rebalance(BranchId branch, uint32_t level, std::span<LeafDataType> objects)
{
	if (level < N)
	{
		uint32_t dim = getDim(level);
		uint32_t nLeft = uint32_t(objects.size() / 2);
		std::nth_element(std::begin(objects), std::begin(objects) + nLeft, std::end(objects),
			[dim](const LeafDataType& obj1, const LeafDataType& obj2)
		{
			return obj1.point[dim] < obj2.point[dim];
		});
		float divider = objects[nLeft].point[dim];
		mTreeData.dividers[branch] = divider;
		rebalance(getLeftChild(branch), level + 1, { objects.data(), nLeft });
		rebalance(getRightChild(branch), level + 1, { objects.data() + nLeft, objects.size() - nLeft });
	}
	else
	{
		LeafId leafId = getLeafId(branch);
		Leaf& leaf = mTreeData.leaves[leafId];
		leaf.numObjects = uint32_t(objects.size());
		if (leafId == 0)
			leaf.firstObject = 0;
		else
			leaf.firstObject = mTreeData.leaves[leafId - 1].firstObject + mTreeData.leaves[leafId - 1].numObjects;
	}
}

template<typename LeafDataType, uint32_t N>
const KdTreeData<N>& KdTree<LeafDataType, N>::getKdTreeData() const
{
	return mTreeData;
}

template<typename LeafDataType, uint32_t N>
std::span<const LeafDataType> KdTree<LeafDataType, N>::getLeafData() const
{
	return { mLeafData };
}


template<typename LeafDataType, uint32_t N>
template<uint32_t NO>
std::array<LeafDataType, NO> KdTree<LeafDataType, N>::getClosestObjects(const float point[3]) const
{
	std::array<LeafDataType, NO> closestObjects;
	//for (LeafDataType& obj : closestObjects) obj.objectId = std::numeric_limits<uint32_t>::max();
	float closestObjectDistace[NO];
	std::fill(std::begin(closestObjectDistace), std::end(closestObjectDistace), std::numeric_limits<float>::max() );

	BranchId queue[N + 1];
	for (auto& q : queue)
		q = (BranchId) -1;
	
	BranchId branch = 0;
	uint32_t level = 0;

	//std::cout << "point: " << point[0] << ", " << point[1] << ", " << point[2] << std::endl;

	while (level != (uint32_t) -1)
	{
		uint32_t dim = getDim(level);
		queue[level] = branch;
		//std::cout << "Branch " << branch << std::endl;
		if (!isLeaf(branch))
		{
			//std::cout << "DIM: " << dim << std::endl;
			//std::cout << "pos: " << point[dim] << ", Div: " << mTreeData.dividers[branch] << std::endl;
			uint32_t nextLevel = level - 1;
			BranchId nextBranch = getParent(branch);
			BranchId branchCandidate = (point[dim] < mTreeData.dividers[branch]) ? getLeftChild(branch) : getRightChild(branch);
			BranchId sibling = ((branchCandidate + 1) ^ 0x01) - 1;
			
			if (queue[level + 1] != branchCandidate && queue[level + 1] != sibling)
			{
				nextBranch = branchCandidate;
				nextLevel = level + 1;
			}
			else if (queue[level + 1] == branchCandidate)
			{
				for (uint32_t i = 0; i < NO; i++)
				{
					if (closestObjectDistace[i] > std::abs(point[dim] - mTreeData.dividers[branch]))
					{
						nextBranch = sibling;
						nextLevel = level + 1;
					}
				}
			}
			branch = nextBranch;
			level = nextLevel;
		}
		else
		{
			LeafId leafId = getLeafId(branch);
			std::span<const LeafDataType> objects = getLeafObjects(leafId);
			for (const LeafDataType& object : objects)
			{
				float diff[3] = { point[0] - object.point[0], point[1] - object.point[1], point[2] - object.point[2] };
				float dist = std::sqrt(diff[0] * diff[0] + diff[1] * diff[1] + diff[2] * diff[2]);
				for (uint32_t i = 0; i < NO; i++)
				{
					if (dist - object.radius < closestObjectDistace[i] && dist > 0)
					{
						closestObjectDistace[i] = dist - object.radius;
						closestObjects[i] = object;
						break;
					}
				}
			}
			branch = getParent(branch);
			level--;
		}
	}
	return closestObjects;
}

template<typename LeafDataType, uint32_t N>
uint32_t KdTree<LeafDataType, N>::getDim(uint32_t Level)
{
	return Level % 3;
}

template<typename LeafDataType, uint32_t N>
BranchId KdTree<LeafDataType, N>::getLeftChild(BranchId branch)
{
	return 2 * (branch + 1) - 1;
}

template<typename LeafDataType, uint32_t N>
BranchId KdTree<LeafDataType, N>::getRightChild(BranchId branch)
{
	return 2 * (branch + 1);
}

template<typename LeafDataType, uint32_t N>
BranchId KdTree<LeafDataType, N>::getParent(BranchId branch)
{
	return (branch + 1) / 2 - 1;
}

template<typename LeafDataType, uint32_t N>
bool KdTree<LeafDataType, N>::isRoot(BranchId branch)
{
	return branch == 0;
}

template<typename LeafDataType, uint32_t N>
bool KdTree<LeafDataType, N>::isLeaf(BranchId branch)
{
	return branch >= NumBranches;
}

template<typename LeafDataType, uint32_t N>
LeafId KdTree<LeafDataType, N>::getLeafId(BranchId branch)
{
	return branch - NumBranches;
}

template<typename LeafDataType, uint32_t N>
std::span<const LeafDataType> KdTree<LeafDataType, N>::getBranchObjects(BranchId branch, uint32_t level)
{
	BranchId leftChild = branch;
	BranchId rightChild = branch;
	for (uint32_t i = level; i < N; i++)
	{
		leftChild = getLeftChild(leftChild);
		rightChild = getRightChild(rightChild);
	}
	LeafId leftLeaf = getLeaf(leftChild);
	LeafId rightLeaf = getLeaf(rightChild);
	uint32_t first = mTreeData.leaves[leftLeaf].firstObject;
	uint32_t num = mTreeData.leaves[rightLeaf].firstObject + mTreeData.leaves[rightLeaf].numObjects - first;
	return { &mLeafData[first], num };
}

template<typename LeafDataType, uint32_t N>
LeafId KdTree<LeafDataType, N>::getLeaf(const float point[3]) const
{
	BranchId branch = 0;
	for (uint32_t i = 0; i < N; i++)
	{
		uint32_t dim = getDim(i);
		branch = (point[dim] < mTreeData.dividers[branch]) ? getLeftChild(branch) : getRightChild(branch);
	}
	return getLeafId(branch);
}

template<typename LeafDataType, uint32_t N>
std::span<const LeafDataType> KdTree<LeafDataType, N>::getLeafObjects(LeafId leafId) const
{
	return { &mLeafData[mTreeData.leaves[leafId].firstObject], mTreeData.leaves[leafId].numObjects };
}


}

