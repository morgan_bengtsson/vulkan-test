#pragma once

#include "skeleton.h"
#include "resourcesetbinding.h"
#include "scenesubset.h"

#include <vulkanResourceSet.h>
#include <vulkanDescriptorPool.h>
#include <vulkanUtils.h>


typedef ecs::SceneSubSet<
		component::MeshVertex,
		component::MeshNormal,
		component::MeshTexCoord,
		component::MeshIndex,
		skeleton::VertexBoneData,
		component::Raw
	> MeshSetSceneType;

EntityResourceSet meshSet(vulkanutils::Device* device
	, MeshSetSceneType scene, vulkanutils::VertexInputSlot vertInputBp, size_t numTexCoords = 1, bool withSkeleton = false);


typedef ecs::SceneSubSet<
		component::SkinnedMeshVertex,
		component::SkinnedMeshNormal,
		component::MeshTexCoord,
		component::MeshIndex,
		component::Raw
	> SkinnedMeshSetSceneType;

EntityResourceSet skinnedMeshSet(vulkanutils::Device* device
		, SkinnedMeshSetSceneType scene, vulkanutils::VertexInputSlot vertInputBp, size_t numTexCoords = 1);

