#pragma once


#include "surface.h"

namespace component
{
template<typename CellData>
DiscreteSurface<CellData>::DiscreteSurface(size_t width, size_t height)
	:mWidth(width), mHeight(height)
	, mCells(width * height)
{
}

template<typename CellData>
DiscreteSurface<CellData>::~DiscreteSurface()
{
}

template<typename CellData>
CellData& DiscreteSurface<CellData>::operator()(size_t x, size_t y)
{
	size_t idx = y * mWidth + x;
	return mCells[idx];
}

template<typename CellData>
const CellData& DiscreteSurface<CellData>::operator()(size_t x, size_t y) const
{
	return mCells[y * mWidth + x];
}

template<typename CellData>
std::span<CellData> DiscreteSurface<CellData>::data()
{
	return { mCells };
}

template<typename CellData>
std::span<const CellData> DiscreteSurface<CellData>::data() const
{
	return { mCells };
}

template<typename CellData>
size_t DiscreteSurface<CellData>::width() const
{
	return mWidth;
}

template<typename CellData>
size_t DiscreteSurface<CellData>::height() const
{
	return mHeight;
}




template<typename CellData>
Surface<CellData>::Surface(float xSize, float ySize, float cellSize)
	: mDiscreteSurface(size_t(xSize / cellSize), size_t(ySize / cellSize))
	, mCellSize(cellSize)
	, mInvCellSize(1.0f / cellSize)
{
}

template<typename CellData>
Surface<CellData>::~Surface()
{}

template<typename CellData>
template<typename Functor>
auto Surface<CellData>::get(float x, float y, Functor functor) const -> decltype(functor(CellData()))
{
	float nx = getDiscXCoord(x);
	float ny = getDiscYCoord(y);

	int xCell = std::clamp<int>(int(nx), 0, int(getDiscrete().width()) - 2);
	int yCell = std::clamp<int>(int(ny), 0, int(getDiscrete().height()) - 2);

	float dx = (nx - xCell);
	float dy = (ny - yCell);

	decltype(functor(CellData())) cells[] = { 
		functor(mDiscreteSurface(xCell, yCell))
		, functor(mDiscreteSurface(xCell + 1, yCell))
		, functor(mDiscreteSurface(xCell, yCell + 1))
		, functor(mDiscreteSurface(xCell + 1, yCell + 1))
	};

	return (1 - dy) * (dx * cells[1] + (1 - dx) * cells[0])
		+ dy * (dx * cells[3] + (1 - dx) * cells[2]);
}

template<typename CellData>
CellData& Surface<CellData>::getClosest(float x, float y)
{
	return mDiscreteSurface(size_t(x * mInvCellSize + mDiscreteSurface.width() / 2.0f),
		size_t(y * mInvCellSize + mDiscreteSurface.height() / 2.0f));
}

template<typename CellData>
const CellData& Surface<CellData>::getClosest(float x, float y) const
{
	return mDiscreteSurface(size_t(x * mInvCellSize + mDiscreteSurface.width() / 2.0f),
		size_t(y * mInvCellSize + mDiscreteSurface.height() / 2.0f));
}

template<typename CellData>
float Surface<CellData>::cellSize() const
{
	return mCellSize;
}

template<typename CellData>
float Surface<CellData>::getXSize() const
{
	return mDiscreteSurface.width() * mCellSize;
}

template<typename CellData>
float Surface<CellData>::getYSize() const
{
	return mDiscreteSurface.height() * mCellSize;
}

template<typename CellData>
float Surface<CellData>::getXCoord(size_t idx) const
{
	return (float(idx) - mDiscreteSurface.width() / 2.0f) * cellSize();
}

template<typename CellData>
float Surface<CellData>::getYCoord(size_t idx) const
{
	return (float(idx) - mDiscreteSurface.height() / 2.0f) * mCellSize;
}


template<typename CellData>
float Surface<CellData>::getDiscXCoord(float x) const
{
	return x * mInvCellSize + mDiscreteSurface.width() / 2.0f;

}

template<typename CellData>
float Surface<CellData>::getDiscYCoord(float y) const
{
	return y * mInvCellSize + mDiscreteSurface.height() / 2.0f;
}


template<typename CellData>
DiscreteSurface<CellData>& Surface<CellData>::getDiscrete()
{
	return mDiscreteSurface;
}

template<typename CellData>
const DiscreteSurface<CellData>& Surface<CellData>::getDiscrete() const
{
	return mDiscreteSurface;
}

template<typename CellData>
SurfaceProperties Surface<CellData>::getProperties() const
{
	return {uint32_t(getDiscrete().width()), uint32_t(getDiscrete().height()), cellSize()};
}


}
