#pragma once

#include <vulkanImageView.h>
#include <vulkanImageSampler.h>

#include <stdint.h>
#include <stack>
#include <vector>
#include <set>

#include <Eigen/Dense>
#include <Eigen/Geometry> 
#include <unsupported/Eigen/Splines>


struct Input
{

};

struct KeyboardInput : Input
{
	enum class State
	{
		UP,
		DOWN,
		PRESSED_DOWN,
		RELEASED,
	};
	struct KeyModifiers
	{
		bool operator==(const KeyModifiers& km) const
		{
			return ctrl == km.ctrl && alt == km.alt && shift == km.shift;
		}
		uint8_t ctrl : 1;
		uint8_t alt : 1;
		uint8_t shift : 1;
	};

	State state;
	KeyModifiers modifiers;
	uint32_t key;
	std::chrono::system_clock::time_point time;
};


struct MouseInput : Input
{
	enum class Button
	{
		LEFT = 1,
		RIGHT = 2,
		MIDDLE = 4,
	};
	typedef uint8_t Buttons;
	
	Buttons buttons;
	Eigen::Vector2i pos;
	Eigen::Vector2f normPos;
	std::chrono::system_clock::time_point time;
};

namespace component
{

struct Sky
{
	float sunPos[2];
};

struct Character
{
	char meshName[256];
	Eigen::Vector2f position;
	Eigen::Vector2f direction;
	Eigen::Vector2f goalPosition;
	Eigen::Spline<float, 2> pathSpline;
	float splinePos;
};

struct RayEntityIntersection
{
	Eigen::Vector3f origin;
	Eigen::Vector3f position;
	float distance;
	size_t entity;
};

struct HumanPlayerInput
{
	std::vector<KeyboardInput> keyboardEvents;
	std::vector<MouseInput> mouseEvents;
	std::vector<RayEntityIntersection> mouseRayIntersections;
};

struct HumanPlayer
{
	size_t controlledEntity;
};


struct MaterialProperties
{
	float emissive[4];
	float ambient[4];
	float diffuse[4];
	float specular[4]; // Use fourth value as "shininess"
};

struct MaterialTexture
{
	char name[256];
};

struct MaterialTextureView
{
	vulkanutils::ImageView imageView;
	vulkanutils::ImageSampler imageSampler;
	size_t componentType; // Not used yet
};

struct CollidableObject
{
	float point[3];
	float radius;
	uint32_t entityId;
	uint32_t objectId;
};


struct ShadowDistance
{
	float shadowDistance;
};

struct Depth
{
	float depth;
};

struct Color
{
	uint8_t color[4];
};

struct Raw
{
	uint8_t data;
};

struct SystemProperies
{
	uint32_t timeMs;
};

struct LightIndex
{
	uint32_t lightIndex;
};

struct CameraProjection : public Eigen::Matrix4f
{
	using Eigen::Matrix4f::Matrix4f;
};

struct Position : public Eigen::Vector3f
{
	using Eigen::Vector3f::Vector3f;
};

struct Direction : public Eigen::Vector3f
{
	using Eigen::Vector3f::Vector3f;
};

struct Scale : public Eigen::Vector3f
{
	using Eigen::Vector3f::Vector3f;
};

struct DirectionalLightGpu
{
	Eigen::Matrix4f view;
	Eigen::Matrix4f projection;
	Eigen::Matrix4f projectionView;
	float color[4];
	float direction[4];
};

struct PositionalLightGpu
{
	Eigen::Matrix4f view;
	Eigen::Matrix4f projection;
	Eigen::Matrix4f projectionView;
	float color[4];
	float position[4];
};

struct DirectionalLight
{
	float color[4];
	float shadowBoundingBox[6];
};

struct PositionalLight
{
	float color[4];
};

struct Orientation : public Eigen::Matrix3f
{
	using Eigen::Matrix3f::Matrix3f;
	Orientation()
		: Eigen::Matrix3f(Eigen::Matrix3f::Identity())
	{};
};

struct Speed : public Eigen::Vector3f
{
	using Eigen::Vector3f::Vector3f;
};
struct Visibility
{
	bool visible;
};

enum class RunMode
{
	RUN,
	PAUSE,
	STEP,
};

struct Transform
{
	float matrix[16];
};



struct LightScene
{
};


/*
struct ParentEntityRef : public EntityRef
{
};

struct ChildEntityRef : public EntityRef
{
};

struct DrawPipelineEntityRef : public EntityRef
{
};
*/

struct MeshAABoundingBox 
{
public:
	Eigen::AlignedBox<float, 3> aabb;


};

struct MeshVertex
{
	float point[3];
};

struct MeshNormal
{
	float normal[3];
};

struct SkinnedMeshVertex
{
	float point[3];
};

struct SkinnedMeshNormal
{
	float normal[3];
};

struct MeshTexCoord
{
	float coord[2];
};

struct MeshIndex
{
	uint32_t idx;
};

struct Elevation
{
	float elevation;
};


};

#include "resourcesetbinding.h"

namespace component
{

// Sub-pass
struct Pipeline
{
	Pipeline(const char* shaderName_ = "")
		:shaderName()
		, sets()
	{
		strncpy(shaderName, shaderName_, sizeof(shaderName));
	}
	Pipeline(Pipeline&&) = default;
	Pipeline(const Pipeline&) = delete;
	Pipeline& operator=(const Pipeline&) = delete;
	Pipeline& operator=(Pipeline&&) = default;
	char shaderName[64];

	VkCullModeFlagBits cullMode = VK_CULL_MODE_BACK_BIT;
	std::vector<EntityResourceSet> sets;


	struct Attachment
	{
		size_t componentType;
		uint32_t bindingNr;
	};
	std::vector<Attachment> renderPassAttachments;

	template<typename ComponentType, typename SceneType>
	void addRenderPassAttachment(const SceneType& scene, uint32_t bindingNr)
	{
		renderPassAttachments.emplace_back((size_t(1) << scene.getComponentCollection().template getComponentTypeIndex<ComponentType>()), bindingNr);
	}
};

};
