#pragma once

#include "surface.h"

#include <vector>

struct WaterSurfaceCell
{

	float depth;
	//float dh; // center
	float uRight, vDown;
	float bd;
};

struct WaterDepth
{
	float depth;
};


class WaterSurface : public component::Surface<WaterSurfaceCell>
{
public:
	WaterSurface() = default;
	WaterSurface(float depthScale, float cellSize
		, float xSize, float ySize, float startDepth, const component::Surface<float>* surface);
	~WaterSurface();

	void changeVolume(float x, float y, float volume, float width);
	void setVolume(float x, float y, float volume, float width);

private:
	friend class WaterSystem;
	float gaussian(float volume, float width, float x, float y);

	std::vector<WaterSurfaceCell> mCells;
	float mDepthScale;
};
