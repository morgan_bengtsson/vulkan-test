#pragma once

#include "renderpass.h"

template<typename ComponentType, typename SceneType>
void RenderSubPass::addAttachmentCompType(const SceneType& scene, uint32_t bindingNr, VkFormat format, VkClearValue clearColor, bool presentation)
{
	mAttachments.push_back(Attachment{ size_t(1)
		<< scene.getGpuComponentCollection().template getComponentTypeIndex<ComponentType>(), bindingNr, format, clearColor, presentation });
}


template<typename SceneType>
std::vector<RenderSubPass::ImageBinding> RenderSubPass::getEntityBindings(const SceneType& scene, const ecs::Entity& entity) const
{
	std::vector<ImageBinding> imageBindings;
	auto materialTextures = entity.getComponentArray<component::MaterialTextureView>(scene.getComponentCollection());

	for (const Attachment& attachment : mAttachments)
	{
		for (size_t i = 0; i < materialTextures.size(); i++)
		{
			const component::MaterialTextureView& matTexView = materialTextures[i];
			if ((size_t(1) << matTexView.componentType) == attachment.componentType)
			{
				ImageBinding ib;
				ib.bindingNr = attachment.bindingNr;
				ib.imageView = &matTexView.imageView;
				ib.imageSampler = &matTexView.imageSampler;
				imageBindings.push_back(ib);
				break;
			}
		}
	}
	return imageBindings;
}
