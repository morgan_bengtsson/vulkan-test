#pragma once

#include "drawsystem.h"

#include <vulkanBufferPool.h>

class IDrawSystemExtension
{
public:
	virtual ~IDrawSystemExtension() {}
	virtual size_t requiredComponents(const DrawSystem::SceneType& scene) const = 0;

	virtual void update(
		DrawSystem& ds,
		ecs::Entity& entity,
		DrawSystem::SceneType& scene,
		vulkanutils::BufferCopier& bufferCopier) {};
	virtual void draw(
		DrawSystem& ds,
		VkCommandBuffer commandBuffer,
		const std::vector<ecs::Entity>& entities,
		const DrawSystem::SceneType& scene) {};
protected:
	class ResourceAccessor
	{
	public:
		ResourceAccessor(DrawSystem& ds);


		//const std::vector<vulkanutils::GraphicsPipeline>& getPipelines(size_t entityId) const;
		vulkanutils::DescriptorSet createDescriptorSet(const EntityResourceSet& resourceSet) const;

	private:
		DrawSystem& mDs;
	};
};
