#pragma once

#include "system.h"
#include "scenesubset.h"
#include "skeleton.h"

#include "componenttypes.h"

#include <vulkanContext.h>
#include <vulkanCommandPool.h>

class ModelSystem : public ecs::System<ModelSystem>
{
public:
	typedef ecs::SceneSubSet<
		component::Position,
		component::Orientation,
		component::Scale,
		component::Transform,
		component::CameraProjection,
		component::MeshAABoundingBox,
		ecs::EntityRef
		> SceneType;

	ModelSystem(vulkanutils::VulkanContext& context, SceneType scene);

	static constexpr const char* getName()
	{
		return "ModelSystem";
	}

	void runImpl(SceneType scene);
private:
	// WIP, remove this
	bool mSetup = false;
	void setup(SceneType scene);

	vulkanutils::CommandPool mCommandPool;
	vulkanutils::CommandBuffers mCommandBuffers;
};
