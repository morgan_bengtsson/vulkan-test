#pragma once

#include "resourcesetbinding.h"

#include "watersurface.h"
#include "componenttypes.h"

#include <vulkanResourceSet.h>

class WaterMeshSet : public EntityResourceSet
{
public:

	typedef ecs::SceneSubSet<
			WaterDepth,
			component::MeshVertex,
			component::MeshNormal
		> SceneType;

	WaterMeshSet(vulkanutils::Device* device
		, SceneType scene, vulkanutils::DescriptorSetSlot descSetbp, vulkanutils::VertexInputSlot vertInputBp);
};



