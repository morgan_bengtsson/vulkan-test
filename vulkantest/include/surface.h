#pragma once

#include "perlinnoise.h"
#include "entity.h"
#include "componentcollectionsubset.h"
#include "componenttypes.h"

#include <type_traits>
#include <array>

namespace component
{

template<typename CellData>
class DiscreteSurface
{
public:
	DiscreteSurface() = default;
	DiscreteSurface(size_t width, size_t height);
	~DiscreteSurface();

	CellData& operator()(size_t x, size_t y);
	const CellData& operator()(size_t x, size_t y) const;
	std::span<CellData> data();
	std::span<const CellData> data() const;

	size_t width() const;
	size_t height() const;
private:
	size_t mWidth;
	size_t mHeight;
	std::vector<CellData> mCells;
};


struct SurfaceProperties
{
	uint32_t width;
	uint32_t height;
	float cellSize;
};

template<typename CellData>
class Surface
{
public:
	Surface() = default;
	Surface(float xSize, float ySize, float cellSize = 1.0f);
	~Surface();

	template<typename Functor>
	auto get(float x, float y, Functor functor) const -> decltype(functor(CellData()));

	CellData& getClosest(float x, float y);
	const CellData& getClosest(float x, float y) const;

	float getXCoord(size_t idx) const;
	float getYCoord(size_t idx) const;
	
	float getDiscXCoord(float idx) const;
	float getDiscYCoord(float idx) const;

	float getXSize() const;
	float getYSize() const;

	float cellSize() const;

	DiscreteSurface<CellData>& getDiscrete();
	const DiscreteSurface<CellData>& getDiscrete() const;

	SurfaceProperties getProperties() const;

private:
	DiscreteSurface<CellData> mDiscreteSurface;
	float mCellSize;
	float mInvCellSize;
};



class PerlinSurface
{
public:
	PerlinSurface(float heightScale
		, float xSize, float ySize);

	Surface<float> toSurface(float cellSize = 1.0f) const;

	float getHeight(float x, float y) const;
	std::array<float, 2> dxdy(float x, float y) const;
	

	float getXSize() const;
	float getYSize() const;

private:
	static constexpr float NOISE_RESOLUTION = 1.0f / 64.0f;
	static constexpr size_t NOISE_LEVELS = 5;

	std::array<float, 2> getDxDyNormalized(float xNorm, float yNorm) const;
	float getHeightNormalized(float xNorm, float yNorm) const;

	float mXSize;
	float mYSize;
	PerlinNoise mNoise;
	float mHeightScale;
};




}

typedef ecs::ComponentCollectionSubSet<
	ecs::EntityRef,
	component::MeshVertex,
	component::MeshNormal,
	component::MeshTexCoord,
	component::MeshIndex> CreateSurfaceMeshCCType;

ecs::Entity createSurfaceMesh(
	CreateSurfaceMeshCCType components,
	const component::Surface<float>& surface,
	float gridScale, bool hasTexCoords = true, bool normalizedTexCoords = false);


typedef ecs::ComponentCollectionSubSet<
	component::MeshVertex,
	component::MeshNormal,
	component::MeshIndex> CalcSurfaceNormalsCCType;

void calcSurfaceNormals(CalcSurfaceNormalsCCType components, ecs::Entity& meshEntity);



typedef ecs::ComponentCollectionSubSet<
	ecs::EntityRef,
	component::MeshVertex,
	component::MeshNormal,
	component::MeshTexCoord,
	component::MeshIndex> CreateFlatSurfaceMeshCCType;

ecs::Entity createFlatSurfaceMesh(
	CreateFlatSurfaceMeshCCType components,
	float xSize, float ySize,
	float gridScale);


#include "surface.tpp"