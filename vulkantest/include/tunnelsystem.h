#pragma once

#include "componenttypes.h"
#include "system.h"
#include "scenesubset.h"
#include "surface.h"

#include <vulkanContext.h>

class TunnelSystem : public ecs::System<TunnelSystem>
{
public:
		typedef ecs::SceneSubSet<
		ecs::EntityRef,
		component::SystemProperies,
		component::MeshVertex,
		component::MeshNormal,
		component::Surface<float>
		> SceneType;

	TunnelSystem(vulkanutils::VulkanContext& context, SceneType scene);
	static constexpr const char* getName()
	{
		return "AnimationSystem";
	}
	void runImpl(SceneType scene);

private:

};


