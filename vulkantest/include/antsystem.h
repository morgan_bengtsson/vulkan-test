#pragma once

#include "system.h"
#include "skeleton.h"
#include "scenesubset.h"
#include "watersurface.h"
#include "rain.h"
#include "computesystem.h"
#include "componenttypes.h"

#include <vulkanCommandPool.h>
#include <vulkanCommandBuffer.h>
#include <vulkanComputePipeline.h>

#include <random>
#include <vector>

#pragma pack(push, 1)
struct AntHill
{
	float position[2];
	float size;
	float score;
	uint32_t numAnts;
};

struct Ant
{
	enum class State : uint32_t
	{
		REST,						// 0
		SEARCH_FOOD,				// 1
		SEARCH_FOOD_FOLLOW_TRAIL,	// 2
		RETURN_FOOD,				// 3
		RETURN_FOOD_FOLLOW_TRAIL,	// 4
		RETURN,						// 5
		RETURN_FOLLOW_TRAIL,		// 6
		DEAD,						// 7
	};

	float position[2];
	State state;
	float directionAngle;
	float speed;
	float size;
	float stamina;
	float maxStamina;
	float hp;
	float maxHp;
};

struct AntWorld
{
	size_t pheromoneEntityId;
	size_t terrainEntityId;
	std::vector<size_t> antHillEntityIds;
};


union PheromoneData
{
	PheromoneData(Eigen::Vector<float, 5> d = Eigen::Vector<float, 5>::Zero())
		: data(d)
	{};
	PheromoneData(const PheromoneData& d)
		:data(d.data)
	{}
	PheromoneData& operator=(const PheromoneData& d)
	{
		data = d.data;
		return *this;
	}
	Eigen::Vector<float, 5> data;
	struct
	{
		float homeTrail;
		float foodTrail;
		float dangerTrail;
		float emptyTrail;
		float foodStore;
	};
};
#pragma pack(pop)

class AntPheromoneMap : public component::Surface<PheromoneData>
{
public:
	AntPheromoneMap(float xSize, float ySize, float cellSize);
	AntPheromoneMap()
		: component::Surface<PheromoneData>(0, 0, 0)
	{}
	~AntPheromoneMap(){}

private:

};

class AntSystem : public ecs::System<AntSystem>, public ComputeSystem
{
public:
	typedef ecs::SceneSubSet<
		AntWorld,
		AntHill,
		Ant,
		AntPheromoneMap,
		component::Pipeline,
		component::MaterialProperties,
		component::MaterialTexture,
		component::MeshVertex,
		component::MeshNormal,
		component::MeshTexCoord,
		component::MeshIndex,
		skeleton::Bone,
		skeleton::Skeleton,
		skeleton::VertexBoneData,
		skeleton::ActiveAnimation,
		component::SystemProperies,
		component::Surface<float>,
		component::Visibility,
		ecs::EntityRef,
		component::Transform,
		component::CameraProjection,
		component::Raw,
		component::DirectionalLightGpu,
		component::PositionalLightGpu,
		component::Position,
		component::Orientation,
		component::Scale,
		PheromoneData,
		component::SurfaceProperties,
		component::Elevation,
		component::MaterialTextureView,
		component::Color,
		component::Depth
		> SceneType;

	AntSystem(vulkanutils::VulkanContext& context, SceneType scene);

	static constexpr const char* getName()
	{
		return "AntSystem";
	}

	void runImpl(SceneType scene);
private:
	void update(SceneType scene);



	void setup(SceneType scene);
	ecs::Entity createAntWorld(SceneType scene);
	ecs::Entity createPheromoneMap(SceneType scene, ecs::Entity& terrainEntity);
	ecs::Entity createAntHill(SceneType scene, ecs::Entity& terrainEntity);
	ecs::Entity createAnt(SceneType scene, ecs::Entity& antHill, ecs::Entity& terrainEntity);

	void recordAntWorld(SceneType scene, ecs::Entity& antWorldEntity);

	vulkanutils::DescriptorPool mDescriptorPool;
	vulkanutils::CommandPool mCommandPool;

	std::vector<ecs::Entity> mAntHillMeshes;
	std::vector<ecs::Entity> mAntHillMaterials;

	std::vector<ecs::Entity> mAntMeshes;
	std::vector<ecs::Entity> mAntMaterials;
	
	ecs::Entity mAntHillDrawPipelineEntity;
	ecs::Entity mAntPheromoneDrawPipelineEntity;
	ecs::Entity mAntDrawPipelineEntity;

	const uint32_t mNumPlayers;
	const uint32_t mNumAntsPerPlayer;
	const float mAntSpeed;
	const float mTurnRate;
	const float mMaxTrailStrength;

	std::default_random_engine mRandomGenerator;

	ComputeSystem::Pipeline mAntComputePipeline;
	ComputeSystem::Pipeline mAntPheromoneComputePipeline;

	std::map<size_t, vulkanutils::CommandBuffers> mAntWorldCommandBuffers;
};



#include "antsystem.tpp"
