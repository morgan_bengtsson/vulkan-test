#pragma once

#include "system.h"
#include "surface.h"
#include "rain.h"
#include "scenesubset.h"

#include <random>

class InitPositionsSystem
{
public:
	InitPositionsSystem();

	typedef ecs::SceneSubSet<component::Rain, component::Position, component::Orientation, ecs::EntityRef,
		component::Surface<float>>
		SceneType;
	void run(SceneType scene);

private:
};
