#pragma once

#include <chrono>
#include <thread>

class FpsTimer
{
public:
	FpsTimer();
	void setTargetFps(float fps);
	float getTargetFps() const;
	float getFps() const;

	void tick();
	void tickWait();
	std::chrono::milliseconds duration() const;

private:
	float mTargetFps;
	float mFps;
	size_t mTick;
	std::chrono::system_clock::time_point mStartTime;
	std::chrono::system_clock::time_point mLastTime;
};

