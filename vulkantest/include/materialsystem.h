#pragma once

#include "system.h"
#include "scenesubset.h"
#include "skeleton.h"
#include "material.h"

#include "componenttypes.h"

#include <vulkanContext.h>
#include <vulkanCommandPool.h>

class MaterialSystem : public ecs::System<MaterialSystem>
{
public:
	typedef ecs::SceneSubSet<
			component::MaterialProperties,
			component::MaterialTexture,
			component::MaterialTextureView,
			component::Raw
		> SceneType;

	MaterialSystem(vulkanutils::VulkanContext& context, SceneType scene);

	static constexpr const char* getName()
	{
		return "MaterialSystem";
	}

	void runImpl(SceneType scene);
private:
	vulkanutils::CommandPool mCommandPool;
	vulkanutils::CommandBuffers mUploadCommandBuffers;
	std::vector<uint64_t> mLastSignalledValue;
	size_t mCmdBufferIdx;
};
