#pragma once

#include "system.h"
#include "scenesubset.h"
#include "componenttypes.h"
#include "surface.h"
#include "skeleton.h"

#include "computesystem.h"

#include <vulkanCommandPool.h>
#include <vulkanCommandBuffer.h>

struct GrassField
{
	uint32_t gridSizeX;
	uint32_t gridSizeY;
};


class GrassSystem : public ecs::System<GrassSystem>, public ComputeSystem
{
public:
	typedef ecs::SceneSubSet<
		component::Pipeline,
		component::Position,
		component::SystemProperies,
		component::CameraProjection,
		component::Transform,
		component::MaterialProperties,
		component::Raw,
		component::MeshVertex,
		component::MeshNormal,
		component::MeshIndex,
		skeleton::Bone,
		component::DirectionalLightGpu,
		component::PositionalLightGpu,
		component::Surface<float>,
		GrassField,
		ecs::EntityRef,
		component::MaterialTexture,
		component::MeshTexCoord,
		skeleton::Skeleton,
		skeleton::VertexBoneData,
		component::SurfaceProperties,
		component::Elevation,
		component::Orientation,
		component::Visibility,
		component::MaterialTextureView,
		VkDrawIndexedIndirectCommand,
		component::Color,
		component::Depth,
		component::ShadowDistance,
		component::LightIndex
		> SceneType;

	GrassSystem(vulkanutils::VulkanContext& context, SceneType scene);

	static constexpr const char* getName()
	{
		return "GrassSystem";
	}
	
	void runImpl(SceneType scene);
private:
	void setup(SceneType scene);

	ecs::Entity mDrawPipelineEntity;
	ecs::Entity mShadowPipelineEntity;
	std::vector<ecs::Entity> mGrassMeshEntities;
	std::vector<ecs::Entity> mGrassMaterialEntities;

	vulkanutils::CommandPool mCommandPool;
	vulkanutils::CommandBuffers mUploadCommandBuffers;
	vulkanutils::DescriptorPool mDescriptorPool;

	ComputeSystem::Pipeline mComputePipeline;
	std::map<size_t, vulkanutils::CommandBuffers> mGrassComputeCommandBuffers;
};

class GrassHeightMapSet : public EntityResourceSet
{
public:
	GrassHeightMapSet(vulkanutils::Device* device, const GrassSystem::SceneType& scene, vulkanutils::DescriptorSetSlot descSetbp, vulkanutils::VertexInputSlot vertInputBp);
};

class GrassModelSet : public EntityResourceSet
{
public:
	GrassModelSet(vulkanutils::Device* device, const GrassSystem::SceneType& scene, vulkanutils::DescriptorSetSlot descSetbp);
};

class GrassComputeSet : public EntityResourceSet
{
public:
	GrassComputeSet(vulkanutils::Device* device, const GrassSystem::SceneType& scene, vulkanutils::DescriptorSetSlot descSetbp);
};


class GrassCameraComputeSet : public EntityResourceSet
{
public:
	GrassCameraComputeSet(vulkanutils::Device* device, const GrassSystem::SceneType& scene, vulkanutils::DescriptorSetSlot descSetbp);
};

#include "grasssystem.tpp"
