#pragma once

#include "surface.h"
namespace component
{

template<typename CellData, typename Functor>
void blur(DiscreteSurface<CellData>& surface, Functor functor)
{
	float filter[3][3] = {
		//{ 1.0f / 9.0f, 1.0f / 9.0f, 1.0f / 9.0f },
		//{ 1.0f / 9.0f, 1.0f / 9.0f, 1.0f / 9.0f },
		//{ 1.0f / 9.0f, 1.0f / 9.0f, 1.0f / 9.0f },
		//{ 1.0f, 2.0f, 1.0f },
		//{ 2.0f, 4.0f, 2.0f },
		//{ 1.0f, 2.0f, 1.0f },
		{ 0.0f, 1.0f, 0.0f },
		{ 1.0f, 9, 1.0f },
		{ 0.0f, 1.0f, 0.0f },
	};
	float sum = 0;
	for (size_t i = 0; i < 9; i++)
	{
		sum += filter[0][i];
	}
	for (size_t i = 0; i < 9; i++)
	{
		filter[0][i] /= sum;
	}


	std::vector<CellData> copy(surface.width() * 2);
	for (size_t r = 1; r < surface.height() + 1; r++)
	{
		size_t copyRow = (r - 1) % 2;
		if (r > 2)
		{
			for (size_t cc = 1; cc < surface.width() - 1; cc++)
			{
				functor(surface.data()[(r - 2) * surface.width() + cc]) = functor(copy[copyRow * surface.width() + cc]);
			}
		}
		if (r < surface.height() - 1)
		{
			for (size_t c = 1; c < surface.width() - 1; c++)
			{
				size_t upperLeftIdx = (r - 1) * surface.width() + c - 1;
				size_t idx = r * surface.width() + c;
				CellData sum;
				for (size_t rr = 0; rr < 3; rr++)
				{
					for (size_t cc = 0; cc < 3; cc++)
					{
						size_t i = upperLeftIdx + rr * surface.width() + cc;
						functor(sum) += filter[rr][cc] * functor(surface.data()[i]);
					}
				}
				functor(copy[copyRow * surface.width() + c]) = functor(sum);
			}
		}
	}
}

};