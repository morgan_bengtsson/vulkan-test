#pragma once

#include "entity.h"
#include "skeletonpose.h"

#include <Eigen/Dense>

template<typename CCType>
class EntityBuilder
{
public:
	EntityBuilder(CCType& components)
		:mComponents(components)
	{}

	void addMesh(ecs::Entity& entity, const std::vector<ecs::Entity>& meshEntities);
	void addCamera(ecs::Entity& entity, float near, float far,
		float width, float height, int xres, int yres);
	void addPosition(ecs::Entity& entity, const component::Position& pos = Eigen::Vector3f::Zero());
	void addOrientation(ecs::Entity& entity, const component::Orientation& orientation = Eigen::Matrix3f::Identity());
	void addDrawPipeline(ecs::Entity& entity, const std::vector<ecs::Entity>& pipelineEntity);
private:
	CCType& mComponents;
};


template<typename CCType>
void EntityBuilder<CCType>::addMesh(ecs::Entity& entity, const std::vector<ecs::Entity>& meshEntities)
{
	entity.addComponent<component::Visibility>(mComponents, { true });
	ecs::EntityRef* childs = entity.addComponentArray<ecs::EntityRef>(mComponents, meshEntities.size());
	size_t numSkeletons = 0;
	size_t numBones = 0;
	for (size_t i = 0; i < meshEntities.size(); i++)
	{
		const ecs::Entity& meshEntity = meshEntities[i];
		childs[i].id = meshEntity.getId();
		childs[i].type = ecs::EntityRef::Type::Child;
		auto skeletons = meshEntity.getComponentArray<skeleton::Skeleton>(mComponents);
		numSkeletons += skeletons.size();
		for (auto& skeleton : skeletons)
			numBones += skeleton.numBones();
	}
	if (numSkeletons > 0)
	{
		skeleton::ActiveAnimation* activeAnim = entity.addComponentArray<skeleton::ActiveAnimation>(mComponents, numBones);
		for (size_t i = 0; i < numBones; i++)
			activeAnim[i] = skeleton::ActiveAnimation{0, 0};
	}
}

template<typename CCType>
void EntityBuilder<CCType>::addCamera(ecs::Entity& entity, float near, float far,
	float width,
	float height, int xres, int yres)
{
	component::CameraProjection& camProj = entity.addComponent<component::CameraProjection>(mComponents);
	perspectiveTransform(camProj.data(), near, far, -width / 2.0f, width / 2.0f, -height / 2.0f, height / 2.0f, xres, yres);
}

template<typename CCType>
void EntityBuilder<CCType>::addPosition(ecs::Entity& entity, const component::Position& pos)
{
	entity.addComponent<component::Position>(mComponents, pos);
}

template<typename CCType>
void EntityBuilder<CCType>::addOrientation(ecs::Entity& entity, const component::Orientation& orientation)
{
	entity.addComponent<component::Orientation>(mComponents, orientation);
}


template<typename CCType>
void EntityBuilder<CCType>::addDrawPipeline(ecs::Entity& entity, const std::vector<ecs::Entity>& pipelineEntities)
{
	ecs::EntityRef* pipelines = entity.addComponentArray<ecs::EntityRef>(mComponents, pipelineEntities.size());
	for (size_t i = 0; i < pipelineEntities.size(); i++)
	{
		pipelines[i].id = pipelineEntities[i].getId();
		pipelines[i].type = ecs::EntityRef::Type::Link;
	}
}
