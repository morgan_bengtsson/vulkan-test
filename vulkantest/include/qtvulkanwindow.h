#pragma once

#include <QWindow>
#include <QEvent>

#include <vulkan/vulkan.h>
#include <QVulkanInstance>

#include "entity.h"
#include "drawsystem.h"
#include "dispatcher.h"
#include "inputsystem.h"
#include <set>




class QtVulkanWindow : public QWindow, public comm::Dispatcher<KeyboardInput, MouseInput>
{
public:
	QtVulkanWindow(QWindow* parent = nullptr);
	void setVulkanInstance(VkInstance instance);

	virtual void exposeEvent(QExposeEvent *) override;
	virtual bool event(QEvent *e) override;
	virtual void resizeEvent(QResizeEvent* resizeEvent) override;
	virtual void keyPressEvent(QKeyEvent* event) override;
	virtual void keyReleaseEvent(QKeyEvent* event) override;

	virtual void mouseDoubleClickEvent(QMouseEvent* ev) override;
	virtual void mouseMoveEvent(QMouseEvent* ev) override;
	virtual void mousePressEvent(QMouseEvent* ev) override;
	virtual void mouseReleaseEvent(QMouseEvent* ev) override;

private:
	bool mInitialized;
	void render();

	void setVulkanInstance(QVulkanInstance *instance) = delete;

	QVulkanInstance mVulkanInstance;
};
