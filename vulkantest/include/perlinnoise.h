#pragma once

#include <vector>
#include <array>
#include <memory>


class PerlinNoise
{
public:
	PerlinNoise(size_t seed = 0);
	float get(float x, float y) const;
	std::array<float, 2> getDxDy(float x, float y) const;
private:
	float smoothFunc(float x) const;
	float smoothFuncDerrivate(float x) const;
	size_t mPermutations[256];
	static float Gradients[4][2];
};
