#pragma once

#include "system.h"
#include "scenesubset.h"
#include "componenttypes.h"

#include <vulkanCommandPool.h>
#include <vulkanCommandBuffer.h>

class SkySystem : public ecs::System<SkySystem>
{
public:
	typedef ecs::SceneSubSet<
		component::SystemProperies,
		component::Position,
		component::Orientation,
		component::CameraProjection,
		component::Transform,
		component::MaterialProperties,
		component::Raw,
		component::MaterialTexture,
		ecs::EntityRef,
		component::MeshIndex,
		component::Color,
		component::Depth,
		component::Pipeline,
		component::Sky,
		component::DirectionalLight,
		component::PositionalLight,
		component::LightScene,
		component::DirectionalLightGpu,
		component::PositionalLightGpu
	> SceneType;


	SkySystem(vulkanutils::VulkanContext& context, SceneType scene);

	static constexpr const char* getName()
	{
		return "SkySystem";
	}
	void runImpl(SceneType scene);

private:
	void setupSkyDomeMesh(SceneType scene);

	ecs::Entity mSkyDomeScreenMeshEntity;
	ecs::Entity mSkyDomePipelineEntity;


	vulkanutils::CommandPool mCommandPool;
	vulkanutils::CommandBuffers mCommandBuffers;
	std::vector<uint64_t> mLastSignalledValue;
	size_t mCmdBufferIdx;
};



static Eigen::Vector3f calculate_scattering(
	const Eigen::Vector3f& start, 				// the start of the ray (the camera position)
    const Eigen::Vector3f& dir, 					// the direction of the ray (the camera vector)
    float max_dist, 			// the maximum distance the ray can travel (because something is in the way, like an object)
    const Eigen::Vector3f& scene_color,			// the color of the scene
    const Eigen::Vector3f& light_dir, 			// the direction of the light
    const Eigen::Vector3f& light_intensity,		// how bright the light is, affects the brightness of the atmosphere
    const Eigen::Vector3f& planet_position, 		// the position of the planet
    float planet_radius, 		// the radius of the planet
    float atmo_radius, 			// the radius of the atmosphere
    const Eigen::Vector3f& beta_ray, 				// the amount rayleigh scattering scatters the colors (for earth: causes the blue atmosphere)
    const Eigen::Vector3f& beta_mie, 				// the amount mie scattering scatters colors
    const Eigen::Vector3f& beta_absorption,   	// how much air is absorbed
    const Eigen::Vector3f& beta_ambient,			// the amount of scattering that always occurs, cna help make the back side of the atmosphere a bit brighter
    float g, 					// the direction mie scatters the light in (like a cone). closer to -1 means more towards a single direction
    float height_ray, 			// how high do you have to go before there is no rayleigh scattering?
    float height_mie, 			// the same, but for mie
    float height_absorption,	// the height at which the most absorption happens
    float absorption_falloff,	// how fast the absorption falls off from the absorption height
    int steps_i, 				// the amount of steps along the 'primary' ray, more looks better but slower
    int steps_l 				// the amount of steps along the light ray, more looks better but slower
);