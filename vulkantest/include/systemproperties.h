#pragma once

#include <vulkanResourceSet.h>
#include <vulkanDescriptorSet.h>

#include "resourcesetbinding.h"

typedef ecs::SceneSubSet<
	component::SystemProperies
	> SystemPropertiesSetSceneType;

EntityResourceSet systemPropertiesSet(vulkanutils::Device* device, SystemPropertiesSetSceneType scene, vulkanutils::DescriptorSetSlot descSetbp);


