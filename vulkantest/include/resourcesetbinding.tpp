#pragma once

#include "componenttypes.h"
#include "resourcesetbinding.h"
#include "material.h"

namespace component
{};

template<typename BufferType>
template<typename... ComponentTypes, typename SceneType>
void EntityResourceSet::BindingCompTypeIds<BufferType>::addComponetTypes(const SceneType& scene)
{
	(push_back(size_t(1) << scene.getGpuComponentCollection().template getComponentTypeIndex<ComponentTypes>()), ...);
}

template<typename... ComponentTypes, typename SceneType>
void EntityResourceSet::addComponetTypes(const SceneType& scene)
{
	mRequiredComponentTypeIds |= ((size_t(1) << scene.getComponentCollection().template getComponentTypeIndex<ComponentTypes>()) | ...);
}

template<typename... ComponentTypes, typename SceneType>
void EntityResourceSet::addImageComponetTypes(const SceneType& scene)
{
	(mRequiredImageComponentTypeIds.push_back(size_t(1) << scene.getGpuComponentCollection().template getComponentTypeIndex<ComponentTypes>()), ...);
}

template<typename BufferType>
EntityResourceSet::BindingCompTypeIds<BufferType>& EntityResourceSet::getComponentTypeIds()
{
	return std::get<BindingCompTypeIds<BufferType>>(mBindingCompTypeId);
}

template<typename BufferType>
const EntityResourceSet::BindingCompTypeIds<BufferType>& EntityResourceSet::getComponentTypeIds() const
{
	return std::get<BindingCompTypeIds<BufferType>>(mBindingCompTypeId);
}

template<typename BufferType>
size_t EntityResourceSet::getRequiredCompTypeIds() const
{
	size_t requiredComponentTypeIds = 0;
	for (size_t compType : getComponentTypeIds<BufferType>())
		requiredComponentTypeIds = (requiredComponentTypeIds | compType);
	return requiredComponentTypeIds;
}


template<typename BufferType, typename SceneType>
std::vector<EntityResourceSet::BufferBinding<BufferType>>
	EntityResourceSet::getEntityBufferDescriptorBindings(const SceneType& scene, const ecs::Entity& entity) const
{
	std::vector<BufferBinding<BufferType>> bufferBindings;
	std::map<size_t, size_t> arrayIdx;
	auto bindings = getDescriptorLayout().getDescriptorBindings<BufferType>();
	for (size_t i = 0; i < bindings.size(); i++)
	{
		uint32_t binding = bindings[i].binding;
		size_t compType = getComponentTypeIds<BufferType>()[i];
		if (entity.hasGpuComponentIds<BufferType>(compType))
		{
			const vulkanutils::BufferAllocation<BufferType>& bufferAllocation
				= entity.getGpuComponentArray<BufferType>(scene.getGpuComponentCollection(), compType, arrayIdx[compType]++);
			bufferBindings.push_back(BufferBinding<BufferType>{binding, &bufferAllocation });
		}
	}
	return bufferBindings;
}

template<typename SceneType>
std::vector<EntityResourceSet::ImageBinding> EntityResourceSet::getEntityImageBindings(const SceneType& scene, const ecs::Entity& entity) const
{
	// Needs some rework for multiple textures from multiple entities to work 
	const std::vector<vulkanutils::DescriptorImageBinding>& bindings = getDescriptorLayout().getDescriptorImageBindings();
	auto materialTextures = entity.getComponentArray<component::MaterialTextureView>(scene.getComponentCollection());

	std::vector<EntityResourceSet::ImageBinding> imageBindings;
	for (size_t i = 0; i < bindings.size(); i++)
	{
		const vulkanutils::DescriptorImageBinding& binding = bindings[i];
		size_t compType = getImageComponentTypeIds()[i];
		if (entity.hasImageComponentIds(compType))
		{
			size_t elementNum = 0;
			for (const component::MaterialTextureView& materialTexture : materialTextures)
			{
				if ((size_t(1) << materialTexture.componentType) == compType)
				{
					ImageBinding ib;
					ib.bindingNr = binding.binding;
					ib.elementNr = elementNum++;
					ib.imageView = &materialTexture.imageView;
					ib.imageSampler = &materialTexture.imageSampler;
					imageBindings.push_back(ib);
				}
			}
		}
	}
	return imageBindings;
}


template<typename SceneType>
std::vector<EntityResourceSet::VertexAttributeBinding> EntityResourceSet::getEntityBufferVertexAttributeBindings(const SceneType& scene, const ecs::Entity& entity) const
{
	std::vector<VertexAttributeBinding> bufferBindings;
	const std::vector<VkVertexInputBindingDescription>& bindingDescriptions = getAttributeLayout().getVertexBindingDescriptions();
	const auto& bindingComponentTypeIds = getComponentTypeIds<vulkanutils::VertexBuffer>();

	for (size_t i = 0; i < std::min<size_t>(bindingComponentTypeIds.size(), bindingDescriptions.size()); i++) // FIX THIS (?)
	{
		const VkVertexInputBindingDescription& vbd = bindingDescriptions[i];
		size_t compType = bindingComponentTypeIds[i];
		if (entity.hasGpuComponentIds<vulkanutils::VertexBuffer>(compType))
		{
			const std::vector<const vulkanutils::BufferAllocation<vulkanutils::VertexBuffer>*>& bufferAllocations
				= entity.getGpuComponentArrays<vulkanutils::VertexBuffer>(scene.getGpuComponentCollection(), compType);
			for (const vulkanutils::BufferAllocation<vulkanutils::VertexBuffer>* bufferAllocation : bufferAllocations)
			{
				size_t nInstances = 1;
				if (vbd.inputRate == VK_VERTEX_INPUT_RATE_INSTANCE)
					nInstances = bufferAllocation->getMemRange().size / vbd.stride;
				bufferBindings.push_back(VertexAttributeBinding{vbd.inputRate, nInstances, vbd.binding, bufferAllocation});
			}
		}
	}
	return bufferBindings;
}

template<typename BufferType, typename SceneType>
void EntityResourceSet::bindEntityToDescriptors(const SceneType& scene, vulkanutils::DescriptorSet& descriptorSet, const class ecs::Entity& entity) const
{
	std::map<size_t, size_t> arrayIdx;
	auto bindings = getDescriptorLayout().getDescriptorBindings<BufferType>();
	for (size_t i = 0; i < bindings.size(); i++)
	{
		uint32_t binding = bindings[i].binding;
		size_t compType = getComponentTypeIds<BufferType>()[i];
		if (entity.hasGpuComponentIds<BufferType>(compType))
		{
			const vulkanutils::BufferAllocation<BufferType>& bufferAllocation
				= entity.getGpuComponentArray<BufferType>(scene.getGpuComponentCollection(), compType, arrayIdx[compType]++);
			descriptorSet.bindBuffer(bufferAllocation, binding);
		}
	}
}

template<typename SceneType>
void EntityResourceSet::bindEntitiesToDescriptors(const SceneType& scene, vulkanutils::DescriptorSet& descriptorSet, const std::vector<class ecs::Entity>& entities) const
{
	for (auto& entity : entities)
		bindEntityToDescriptors(scene, descriptorSet, entity);
}

template<typename SceneType>
void EntityResourceSet::bindEntityToDescriptors(const SceneType& scene, vulkanutils::DescriptorSet& descriptorSet, const class ecs::Entity& entity) const
{
	bindEntityToDescriptors<vulkanutils::UniformBuffer>(scene, descriptorSet, entity);
	bindEntityToDescriptors<vulkanutils::ShaderStorageBuffer>(scene, descriptorSet, entity);
}

template<typename... BufferTypes>
bool EntityResourceSet::compatible(const ecs::Entity& entity) const
{
	return (compatibleHelp<BufferTypes>(entity) && ...);
}

template<typename BufferType>
bool EntityResourceSet::compatibleHelp(const ecs::Entity& entity) const
{
	size_t req = getRequiredCompTypeIds<BufferType>();
	if (req != 0)
		return entity.hasGpuComponentIds<BufferType>(req);
	return true;
}

template<typename ComponentType, typename SceneType>
void EntityResourceSet::setPerVertexAttributeBinding(
	SceneType& scene, uint32_t location, VkFormat format)
{
	uint32_t binding = getNextBindPoint();
	mVertexAttributeLayout.addPerVertexAttributeBindings(binding, { { location, format, sizeof(ComponentType) } });
	getComponentTypeIds<vulkanutils::VertexBuffer>().addComponetTypes<ComponentType>(scene);
}
template<typename ComponentType, typename SceneType>
void EntityResourceSet::setPerVertexAttributeBinding(
	SceneType& scene, const std::vector<vulkanutils::VertexAttrDescr>& attributes)
{
	uint32_t binding = getNextBindPoint();
	mVertexAttributeLayout.addPerVertexAttributeBindings(binding, attributes);
	getComponentTypeIds<vulkanutils::VertexBuffer>().addComponetTypes<ComponentType>(scene);
}

template<typename ComponentType, typename SceneType>
void EntityResourceSet::setPerInstanceAttributeBinding(
	SceneType& scene, uint32_t location, VkFormat format)
{
	uint32_t binding = getNextBindPoint();
	mVertexAttributeLayout.addPerInstanceAttributeBindings(binding, { { location, format, sizeof(ComponentType) } });
	getComponentTypeIds<vulkanutils::VertexBuffer>().addComponetTypes<ComponentType>(scene);
}
template<typename ComponentType, typename SceneType>
void EntityResourceSet::setPerInstanceAttributeBinding(
	SceneType& scene, const std::vector<vulkanutils::VertexAttrDescr>& attributes)
{
	uint32_t binding = getNextBindPoint();
	mVertexAttributeLayout.addPerInstanceAttributeBindings(binding, attributes);
	getComponentTypeIds<vulkanutils::VertexBuffer>().addComponetTypes<ComponentType>(scene);
}


template<typename BufferType, typename ComponentType, typename SceneType>
void EntityResourceSet::setDescriptorBinding(SceneType& scene
	, const vulkanutils::TypedDescriptorBinding<BufferType, ComponentType>& descBinding)
{
	const vulkanutils::DescriptorBinding<BufferType>& descBindingBase = descBinding;

	mDescriptorLayout.addDescriptorBindings<BufferType>( { &descBindingBase, 1 } );
	getComponentTypeIds<BufferType>().addComponetTypes<ComponentType>(scene);
}
template<typename SceneType, typename... TypedDescriptorBindings>
void EntityResourceSet::setDescriptorBindings(SceneType& scene
	, const TypedDescriptorBindings&... descBindings)
{
	(setDescriptorBinding(scene, descBindings), ...);
}


template<typename ComponentType, typename SceneType>
void EntityResourceSet::setImageDescriptorBinding(SceneType& scene, const vulkanutils::TypedImageDescriptorBinding<ComponentType>& descBinding)
{
	const vulkanutils::DescriptorImageBinding& descBindingBase = descBinding;
	mDescriptorLayout.addImageDescriptorBindings( &descBindingBase, 1 );
	addImageComponetTypes<ComponentType>(scene);
}


template<typename BufferType>
uint32_t EntityResourceSet::getDescrLayoutBinding(const vulkanutils::DescriptorLayout& layout, size_t idx)
{
	return layout.getDescriptorBindings<BufferType>()[idx].binding;
}

