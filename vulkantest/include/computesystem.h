#pragma once

#include <vulkanContext.h>
#include <vulkanDescriptorSet.h>
#include <vulkanComputePipeline.h>

#include "resourcesetbinding.h"

#include <map>
#include <array>

class ComputeSystem
{
public:
	ComputeSystem() = default;
	ComputeSystem(const ComputeSystem&) = delete;
	ComputeSystem(ComputeSystem&&) = default;
	ComputeSystem& operator=(ComputeSystem&& p) = default;

	class Pipeline
	{
	public:
		Pipeline() = default;
		Pipeline(const Pipeline&) = delete;
		Pipeline(Pipeline&& p) = default;
		Pipeline& operator=(Pipeline&& p) = default;

		std::vector<vulkanutils::DescriptorSet>& createDescriptorSets(size_t entityId, const vulkanutils::DescriptorPool& pool, const std::vector<size_t> variableSetSize = {});
		const std::vector<vulkanutils::DescriptorSet>& getDescriptorSets(size_t entityId) const;
		std::vector<vulkanutils::DescriptorSet>& getDescriptorSets(size_t entityId);
		void removeDescriptorSet(size_t entityId);

		const std::vector<EntityResourceSet>& getComputeSets() const;

		void execute(VkCommandBuffer commandBuffer, size_t entityId, std::array<uint32_t, 3> groupSize);

	public:
		friend class ComputeSystem;

		vulkanutils::ComputeShader mShader;
		vulkanutils::ComputePipeline mComputePipeline;
		std::vector<EntityResourceSet> mComputeSets;
		std::map<size_t, std::vector<vulkanutils::DescriptorSet>> mComputeDescSets;
	};

	ComputeSystem(vulkanutils::VulkanContext& context, const char* shaderPath);
	Pipeline createComputePipeline(const char* shaderName, std::vector<EntityResourceSet> computeSets);

private:
	vulkanutils::VulkanContext& mContext;
	const std::string mShaderPath;
};
