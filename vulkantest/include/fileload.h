#pragma once

#include "entity.h"
#include "skeleton.h"
#include "componentcollectionsubset.h"
#include "componenttypes.h"

#include <vulkanUtils.h>
#include <tuple>




typedef ecs::ComponentCollectionSubSet<component::MaterialProperties,
	component::MaterialTexture,
	ecs::EntityRef,
	component::MeshVertex,
	component::MeshNormal,
	component::MeshTexCoord,
	component::MeshIndex,
	skeleton::Skeleton,
	skeleton::VertexBoneData>
		FileLoadComponentCollection;

std::tuple<std::vector<ecs::Entity>, std::vector<ecs::Entity> > loadFile(const char* file, FileLoadComponentCollection collection, vulkanutils::Device* dev);
