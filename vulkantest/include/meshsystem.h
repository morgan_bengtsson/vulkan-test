#pragma once

#include "system.h"
#include "scenesubset.h"
#include "skeleton.h"

#include "componenttypes.h"

#include <vulkanContext.h>
#include <vulkanCommandPool.h>

class MeshSystem : public ecs::System<MeshSystem>
{
public:
	typedef ecs::SceneSubSet<
		component::MeshIndex,
		component::MeshVertex,
		component::MeshNormal,
		component::MeshTexCoord,
		skeleton::VertexBoneData,
		component::Raw,
		component::MeshAABoundingBox
		> SceneType;

	MeshSystem(vulkanutils::VulkanContext& context, SceneType scene);

	static constexpr const char* getName()
	{
		return "MeshSystem";
	}

	void runImpl(SceneType scene);
private:
	vulkanutils::CommandPool mCommandPool;
	vulkanutils::CommandBuffers mUploadCommandBuffers;
	std::vector<uint64_t> mLastSignalledValue;
	size_t mCmdBufferIdx;
};
