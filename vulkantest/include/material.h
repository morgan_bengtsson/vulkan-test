#pragma once

#include <vulkanResourceSet.h>
#include <vulkanDevice.h>
#include <vulkanUtils.h>

#include "resourcesetbinding.h"
#include "scenesubset.h"


typedef ecs::SceneSubSet<
	component::MaterialProperties,
	component::Raw
	> MaterialSetSceneType;

EntityResourceSet materialSet(vulkanutils::Device* device, MaterialSetSceneType scene, vulkanutils::DescriptorSetSlot descSetbp, size_t numImages = 1);
