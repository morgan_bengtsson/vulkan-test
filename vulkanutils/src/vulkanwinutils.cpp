#include "vulkanwinutils.h"

#include <Windows.h>
#include <vulkan/vulkan_win32.h>

namespace vulkanutils
{

VkSurfaceKHR createSurface(VkInstance instance, uint64_t winId)
{
	VkSurfaceKHR surface = VK_NULL_HANDLE;
	VkWin32SurfaceCreateInfoKHR createInfo = {};
	createInfo.sType = VK_STRUCTURE_TYPE_WIN32_SURFACE_CREATE_INFO_KHR;
	createInfo.hwnd = HWND(winId);
	createInfo.hinstance = GetModuleHandle(nullptr);

	auto func = (PFN_vkCreateWin32SurfaceKHR) vkGetInstanceProcAddr(instance, "vkCreateWin32SurfaceKHR");
	if (func)
	{
		func(instance, &createInfo, nullptr, &surface);
	}
	return surface;
}

}
