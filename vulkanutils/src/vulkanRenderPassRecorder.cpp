#include "vulkanRenderPassRecorder.h"


namespace vulkanutils
{


RenderPassRecorder::RenderPassRecorder(VkCommandBuffer cb, const RenderPass& renderPass, VkFramebuffer frameBuffer, const std::vector<VkClearValue>& clearColors)
	: mCommandBuffer(cb)
{
	VkRenderPassBeginInfo renderPassBeginInfo = {};
	renderPassBeginInfo.sType = VK_STRUCTURE_TYPE_RENDER_PASS_BEGIN_INFO;
	renderPassBeginInfo.renderPass = renderPass;
	renderPassBeginInfo.framebuffer = frameBuffer;
	renderPassBeginInfo.renderArea.offset = { 0, 0 };
	renderPassBeginInfo.renderArea.extent = { uint32_t(renderPass.width()),
										uint32_t(renderPass.height()) };
	renderPassBeginInfo.clearValueCount = (uint32_t) std::size(clearColors);
	renderPassBeginInfo.pClearValues = clearColors.data();
	vkCmdBeginRenderPass(mCommandBuffer,
		&renderPassBeginInfo, VK_SUBPASS_CONTENTS_INLINE);
}

RenderPassRecorder::~RenderPassRecorder()
{
	vkCmdEndRenderPass(mCommandBuffer);
}


};