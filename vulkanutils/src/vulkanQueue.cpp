#include "vulkanQueue.h"

#include <algorithm>
#include <bit>
#include <iostream>

namespace vulkanutils
{

std::vector<uint32_t> Queue::GetFamily(VkPhysicalDevice device, VkQueueFlagBits flags)
{
	uint32_t numQueueFamilies = 0;
	vkGetPhysicalDeviceQueueFamilyProperties(device, &numQueueFamilies, nullptr);
	std::vector<VkQueueFamilyProperties> queueFamilies(numQueueFamilies);
	vkGetPhysicalDeviceQueueFamilyProperties(device, &numQueueFamilies, queueFamilies.data());

	std::vector<uint32_t> indices;
	for (uint32_t i = 0; i < queueFamilies.size(); i++)
		indices.push_back(i);

	indices.erase(std::remove_if(std::begin(indices), std::end(indices),
		[&](uint32_t idx)
	{
		auto& queueFamily = queueFamilies[idx];
		return queueFamily.queueCount == 0 || (queueFamily.queueFlags & flags) != flags;
	}), std::end(indices));

	auto countSetBits = [](uint32_t value)
	{
		uint32_t nSetBits = 0;
		while(value > 0)
		{
			if (value & 0x1) nSetBits++;
			value = (value >> 1);
		}
		return nSetBits;
	};

	std::sort(std::begin(indices), std::end(indices),
		[&](uint32_t idx1, uint32_t idx2)
	{
		auto& prop1 = queueFamilies[idx1];
		auto& prop2 = queueFamilies[idx2];
		uint32_t extraBits1 = countSetBits(prop1.queueFlags & ~flags);
		uint32_t extraBits2 = countSetBits(prop2.queueFlags & ~flags);
		return extraBits1 < extraBits2;
	});
	return indices;
}

void Queue::wait() const
{
	vkQueueWaitIdle(mQueue);
}

bool Queue::wait(VkDevice device, uint64_t signalValue, uint64_t timeoutNs) const
{
	VkSemaphore semaphore = mSemaphore;

	VkSemaphoreWaitInfo info = {};
	info.sType = VK_STRUCTURE_TYPE_SEMAPHORE_WAIT_INFO;
	info.pNext = nullptr;
	info.flags = 0;
	info.semaphoreCount = 1;
	info.pValues = &signalValue;
	info.pSemaphores = &semaphore;
	return VK_SUCCESS == vkWaitSemaphores(device, &info, timeoutNs);
}

uint64_t Queue::getSignalValue() const
{
	return mLastSignalValue;
}


std::vector<uint32_t> Queue::GetPresentationFamily(VkPhysicalDevice device, VkSurfaceKHR surface)
{
	uint32_t numQueueFamilies = 0;
	vkGetPhysicalDeviceQueueFamilyProperties(device, &numQueueFamilies, nullptr);
	std::vector<VkQueueFamilyProperties> queueFamilies(numQueueFamilies);
	vkGetPhysicalDeviceQueueFamilyProperties(device, &numQueueFamilies, queueFamilies.data());

	// Queue families with least amount of capabilities first
	std::vector<uint32_t> indices = GetFamily(device, (VkQueueFlagBits)0);

	// Remove any families without presentation support
	indices.erase(std::remove_if(std::begin(indices), std::end(indices),
		[&](uint32_t idx)
	{
		VkBool32 presentationSupport = false;
		vkGetPhysicalDeviceSurfaceSupportKHR(device, idx, surface, &presentationSupport);
		return !presentationSupport;
	}), std::end(indices));
	return indices;
}


Queue::Queue()
	: mQueue(VK_NULL_HANDLE)
	, mQueueFamilyIdx((uint32_t)-1)
	, mSemaphore()
	, mLastSignalValue()
	, mMutex()
{}

Queue::Queue(VkDevice device, uint32_t queueFamilyIdx, uint32_t queueIdx)
	: mQueue(VK_NULL_HANDLE)
	, mQueueFamilyIdx(queueFamilyIdx)
	, mSemaphore(device)
	, mLastSignalValue()
	, mMutex()
{
	vkGetDeviceQueue(device, queueFamilyIdx, queueIdx, &mQueue);
}

Queue::~Queue()
{
}

Queue::Queue(Queue&& q)
	: mQueue(q.mQueue)
	, mQueueFamilyIdx(q.mQueueFamilyIdx)
	, mSemaphore(std::move(q.mSemaphore))
	, mLastSignalValue(q.mLastSignalValue)
	, mMutex()
{
	q.mQueue = VK_NULL_HANDLE;
	q.mQueueFamilyIdx = (uint32_t)-1;
	q.mSemaphore = TimelineSemaphore();
}
Queue& Queue::operator=(Queue&& q)
{
	this->~Queue();
	mQueue = q.mQueue;
	mQueueFamilyIdx = q.mQueueFamilyIdx;
	mSemaphore = std::move(q.mSemaphore);
	mLastSignalValue = q.mLastSignalValue;
	q.mQueue = VK_NULL_HANDLE;
	q.mQueueFamilyIdx = (uint32_t)-1;
	return *this;
}

uint32_t Queue::getQueueFamilyIdx() const
{
	return mQueueFamilyIdx;
}


void Queue::submit(std::vector<VkCommandBuffer> commandBuffers
		, const Queue* waitOnQueue
		, std::vector<WaitCondition> waitOnBinSemaphores
		, std::vector<BinarySemaphore*> signalBinSemaphores
		, VkFence fence) const
{
	std::vector<VkPipelineStageFlags> waitFlags;
	std::vector<VkSemaphore> signalSemaphores;
	std::vector<uint64_t> waitValues;
	std::vector<uint64_t> singalValues;

	uint64_t nextSignal = mLastSignalValue + 1;

	VkTimelineSemaphoreSubmitInfo timelineInfo = {};
	timelineInfo.sType = VK_STRUCTURE_TYPE_TIMELINE_SEMAPHORE_SUBMIT_INFO;
	timelineInfo.pNext = NULL;

	if (waitOnQueue) waitValues.push_back(waitOnQueue->mLastSignalValue);
	for (WaitCondition& wc : waitOnBinSemaphores)
		waitValues.push_back(0);
	timelineInfo.waitSemaphoreValueCount = uint32_t(waitValues.size());
	timelineInfo.pWaitSemaphoreValues = waitValues.data();


	singalValues.push_back(nextSignal);
	for (BinarySemaphore* s : signalBinSemaphores)
		singalValues.push_back(0);
	timelineInfo.signalSemaphoreValueCount = uint32_t(singalValues.size());
	timelineInfo.pSignalSemaphoreValues = singalValues.data();


	VkSubmitInfo submitInfo = {};
	submitInfo.sType = VK_STRUCTURE_TYPE_SUBMIT_INFO;
	submitInfo.pNext = &timelineInfo;
	submitInfo.commandBufferCount = uint32_t(commandBuffers.size());
	submitInfo.pCommandBuffers = commandBuffers.data();
	std::vector<VkSemaphore> waitSemaphores;
	if (waitOnQueue)
	{
		waitSemaphores.push_back(waitOnQueue->mSemaphore);
		waitFlags.push_back(VK_PIPELINE_STAGE_COLOR_ATTACHMENT_OUTPUT_BIT );
	}
	for (WaitCondition& wc : waitOnBinSemaphores)
	{
		waitSemaphores.push_back(*wc.semaphore);
		waitFlags.push_back(wc.waitFlag);
	}

	signalSemaphores.push_back(mSemaphore);
	for (BinarySemaphore* s : signalBinSemaphores)
		signalSemaphores.push_back(*s);

	submitInfo.waitSemaphoreCount = uint32_t(waitSemaphores.size());
	submitInfo.pWaitSemaphores = waitSemaphores.data();
	submitInfo.pWaitDstStageMask = waitFlags.data();

	submitInfo.signalSemaphoreCount = uint32_t(signalSemaphores.size());
	submitInfo.pSignalSemaphores = signalSemaphores.data();

	std::lock_guard<std::mutex> lock(mMutex);
	mLastSignalValue = nextSignal;
	vkQueueSubmit(mQueue, 1, &submitInfo, fence);
}

Queue::operator VkQueue() const
{
	return mQueue;
}


};

