#include "vulkanBarrier.h"



namespace vulkanutils
{

Barrier::Barrier(VkCommandBuffer commandBuffer)
	: mCommandBuffer(commandBuffer)
	, mBufferRanges()
	, mSrcStages(), mDstStages()
	, mSrcAccess(), mDstAccess()
	, mGlobalBarrier(false)
{}

Barrier::~Barrier()
{
	execute();
}

void Barrier::execute()
{
	if (!mCommandBuffer)
		return;
	std::vector<VkBufferMemoryBarrier> bufferBarriers;
	bufferBarriers.reserve(mBufferRanges.size());
	for (auto& [buffer, range] : mBufferRanges)
	{
		VkBufferMemoryBarrier& barrier = bufferBarriers.emplace_back();
		barrier.sType = VK_STRUCTURE_TYPE_BUFFER_MEMORY_BARRIER;
		barrier.srcAccessMask = mSrcAccess;
		barrier.dstAccessMask = mDstAccess;
		barrier.srcQueueFamilyIndex = VK_QUEUE_FAMILY_IGNORED;
		barrier.dstQueueFamilyIndex = VK_QUEUE_FAMILY_IGNORED;
		barrier.buffer = buffer;
		barrier.offset = range.offset;
		barrier.size = range.size;
	}
	VkMemoryBarrier globalBarrier = {};
	globalBarrier.sType = VK_STRUCTURE_TYPE_MEMORY_BARRIER;
	globalBarrier.srcAccessMask = mSrcAccess;
	globalBarrier.dstAccessMask = mDstAccess;

	vkCmdPipelineBarrier(
		mCommandBuffer
		, mSrcStages
		, mDstStages
		, 0
		, mGlobalBarrier ? 1 : 0
		, mGlobalBarrier ? &globalBarrier : nullptr
		, uint32_t(bufferBarriers.size()), bufferBarriers.data()
		, 0, nullptr);
	*this = Barrier();
}

Barrier& Barrier::globalBarrier()
{
	mGlobalBarrier = true;
	return *this;
}

Barrier& Barrier::addSrcStage(VkPipelineStageFlags stage)
{
	mSrcStages |= stage;
	return *this;
}

Barrier& Barrier::addDstStage(VkPipelineStageFlags stage)
{
	mDstStages |= stage;
	return *this;
}

Barrier& Barrier::addSrcAccess(VkAccessFlags access)
{
	mSrcAccess |= access;
	return *this;
}
Barrier& Barrier::addDstAccess(VkAccessFlags access)
{
	mDstAccess |= access;
	return *this;
}

Barrier& Barrier::addBuffer(VkBuffer buffer, MemRange range)
{
	if (range.size > 0)
		mBufferRanges.emplace_back(buffer, range);
	return *this;
}



ImageBarrier::ImageBarrier(VkCommandBuffer commandBuffer)
	: mCommandBuffer(commandBuffer)
	, mImages()
	, mSrcStages(), mDstStages()
	, mSrcAccess(), mDstAccess()
{}

ImageBarrier::~ImageBarrier()
{
	execute();
}

void ImageBarrier::execute()
{
	if (!mCommandBuffer)
		return;
	std::vector<VkImageMemoryBarrier> bufferBarriers;
	bufferBarriers.reserve(mImages.size());
	for (auto& [ image, fromLayout, toLayout, aspectMask, mipMapLevels ] : mImages)
	{
		VkImageMemoryBarrier& barrier = bufferBarriers.emplace_back();
		barrier.sType = VK_STRUCTURE_TYPE_IMAGE_MEMORY_BARRIER;
		barrier.oldLayout = fromLayout;
		barrier.newLayout = toLayout;
		barrier.srcQueueFamilyIndex = VK_QUEUE_FAMILY_IGNORED;
		barrier.dstQueueFamilyIndex = VK_QUEUE_FAMILY_IGNORED;
		barrier.image = image;

		barrier.subresourceRange.aspectMask = aspectMask;
		barrier.subresourceRange.baseMipLevel = 0;
		barrier.subresourceRange.levelCount = uint32_t(mipMapLevels);
		barrier.subresourceRange.baseArrayLayer = 0;
		barrier.subresourceRange.layerCount = 1;

		barrier.srcAccessMask = mSrcAccess;
		barrier.dstAccessMask = mDstAccess;
	}
	vkCmdPipelineBarrier(
		mCommandBuffer
		, mSrcStages != 0 ? mSrcStages : VK_PIPELINE_STAGE_TOP_OF_PIPE_BIT
		, mDstStages != 0 ? mDstStages : VK_PIPELINE_STAGE_BOTTOM_OF_PIPE_BIT
		, 0
		, 0, nullptr
		, 0, nullptr
		, uint32_t(bufferBarriers.size()), bufferBarriers.data());
	*this = ImageBarrier();
}

ImageBarrier& ImageBarrier::addSrcStage(VkPipelineStageFlags stage)
{
	mSrcStages |= stage;
	return *this;
}

ImageBarrier& ImageBarrier::addDstStage(VkPipelineStageFlags stage)
{
	mDstStages |= stage;
	return *this;
}

ImageBarrier& ImageBarrier::addSrcAccess(VkAccessFlags access)
{
	mSrcAccess |= access;
	return *this;
}
ImageBarrier& ImageBarrier::addDstAccess(VkAccessFlags access)
{
	mDstAccess |= access;
	return *this;
}

ImageBarrier& ImageBarrier::addImage(vulkanutils::Image& image, VkImageLayout fromLayout, VkImageLayout toLayout)
{
	VkImageAspectFlags aspectMask = (image.format() == VK_FORMAT_D32_SFLOAT)
		?  VK_IMAGE_ASPECT_DEPTH_BIT : VK_IMAGE_ASPECT_COLOR_BIT;
	mImages.emplace_back(ImageTransition{ image, fromLayout, toLayout, aspectMask, image.mipMapLevels() });
	return *this;
}


};
