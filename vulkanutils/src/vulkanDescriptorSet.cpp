#include "vulkanDescriptorSet.h"
#include "vulkanUtils.h"
#include "vulkanBuffer.h"
#include "vulkanDescriptorLayout.h"


#include <iostream>

namespace vulkanutils
{


DescriptorSet::DescriptorSet()
	:mDevice(nullptr)
	, mDescriptorPool(nullptr)
	, mDescriptorSet(nullptr)
{
}

DescriptorSet::DescriptorSet(VkDevice device, VkDescriptorPool descriptorPool, const DescriptorLayout& layout, size_t variableSetSize)
	: mDevice(device)
	, mDescriptorPool(descriptorPool)
	, mDescriptorSet(nullptr)
{


	// Allocate one descriptor set per swap image
	// based on layout
	VkDescriptorSetAllocateInfo allocInfo = {};
	allocInfo.sType = VK_STRUCTURE_TYPE_DESCRIPTOR_SET_ALLOCATE_INFO;
	allocInfo.descriptorPool = mDescriptorPool;
	allocInfo.descriptorSetCount = 1;
	VkDescriptorSetLayout vkLayout = layout;
	allocInfo.pSetLayouts = &vkLayout;

	uint32_t variableCount = variableSetSize;
	VkDescriptorSetVariableDescriptorCountAllocateInfo variableDescriptorCountAllocateInfo = {};
	if (variableSetSize > 0)
	{
		variableDescriptorCountAllocateInfo.sType = VK_STRUCTURE_TYPE_DESCRIPTOR_SET_VARIABLE_DESCRIPTOR_COUNT_ALLOCATE_INFO;
		variableDescriptorCountAllocateInfo.descriptorSetCount = 1;
		variableDescriptorCountAllocateInfo.pDescriptorCounts = &variableCount;
		allocInfo.pNext = &variableDescriptorCountAllocateInfo;
	}

	if (VkResult res = vkAllocateDescriptorSets(mDevice, &allocInfo, &mDescriptorSet); res != VK_SUCCESS)
	{
		vulkanutils::logger(Log::Severity::CRITICAL) << "failed to allocate descriptor set! " << res << std::endl;
	}
}

DescriptorSet::~DescriptorSet()
{
	cleanup();
}

void DescriptorSet::cleanup()
{
	if (mDevice)
	{
		if (mDescriptorPool && mDescriptorSet)
		{
			vulkanutils::logger(Log::Severity::INFO) << "Destroy descriptor set" << std::endl;
			// not required, nor allowed
			//vkFreeDescriptorSets(mDevice, mDescriptorPool, 1, &mDescriptorSet);
		}
	}
	mDevice = nullptr;
	mDescriptorPool = nullptr;
	mDescriptorSet = nullptr;
}


DescriptorSet& DescriptorSet::operator=(DescriptorSet&& d) noexcept
{
	cleanup();
	mDevice = d.mDevice;
	mDescriptorPool = d.mDescriptorPool;
	mDescriptorSet = d.mDescriptorSet;
	
	d.mDevice = nullptr;
	d.mDescriptorPool = nullptr;
	d.mDescriptorSet = nullptr;
	return *this;
}

DescriptorSet::DescriptorSet(DescriptorSet&& d) noexcept
	:mDevice(d.mDevice)
	, mDescriptorPool(d.mDescriptorPool)
	, mDescriptorSet(d.mDescriptorSet)
{
	d.mDevice = nullptr;
	d.mDescriptorPool = nullptr;
	d.mDescriptorSet = nullptr;
}


void DescriptorSet::bindImage(VkImageView imageView, VkSampler sampler, size_t binding, size_t element)
{
	VkDescriptorImageInfo imageInfo;
	imageInfo.imageLayout = VK_IMAGE_LAYOUT_SHADER_READ_ONLY_OPTIMAL;
	imageInfo.imageView = imageView;
	imageInfo.sampler = sampler;

	VkWriteDescriptorSet wds = {};
	wds.sType = VK_STRUCTURE_TYPE_WRITE_DESCRIPTOR_SET;
	wds.dstSet = mDescriptorSet;
	wds.dstBinding = uint32_t(binding);
	wds.dstArrayElement = element;
	wds.descriptorType = VK_DESCRIPTOR_TYPE_COMBINED_IMAGE_SAMPLER;
	wds.descriptorCount = 1;
	wds.pBufferInfo = nullptr;
	wds.pImageInfo = &imageInfo;
	wds.pTexelBufferView = nullptr;
	vkUpdateDescriptorSets(mDevice, 1, &wds, 0, nullptr);
}


DescriptorSet::operator VkDescriptorSet() const
{
	return mDescriptorSet;
}

void DescriptorSet::bindToGraphics(VkCommandBuffer commandBuffer, VkPipelineLayout pipelineLayout, uint32_t offset) const
{
	VkDescriptorSet sets[] = { mDescriptorSet };
	vkCmdBindDescriptorSets(commandBuffer,
		VK_PIPELINE_BIND_POINT_GRAPHICS,
		pipelineLayout, offset,
		uint32_t(std::size(sets)), sets,
		0, nullptr);
}

void DescriptorSet::bindToCompute(VkCommandBuffer commandBuffer, VkPipelineLayout pipelineLayout, uint32_t offset) const
{
	VkDescriptorSet sets[] = { mDescriptorSet };
	vkCmdBindDescriptorSets(commandBuffer,
		VK_PIPELINE_BIND_POINT_COMPUTE,
		pipelineLayout, offset,
		uint32_t(std::size(sets)), sets,
		0, nullptr);
}

};
