#include "vulkanDeviceMemory.h"
#include "vulkanDevice.h"
#include "vulkanBuffer.h"
#include "vulkanImage.h"


namespace vulkanutils
{

VulkanMemory::VulkanMemory()
	: mDevice(nullptr)
	, mDeviceMemory(VK_NULL_HANDLE)
	, mTypeFlags(0)
	, mPropertyFlags(0)
	, mAlignment(0)
	, mMappedPtr(nullptr)
{}

void VulkanMemory::setup()
{
	mAlignment = mDevice->alignment();
}

VulkanMemory::VulkanMemory(Device* dev, size_t size, uint32_t typeFlags, uint32_t propertyFlags)
	: mDevice(dev)
	, mDeviceMemory(VK_NULL_HANDLE)
	, mTypeFlags(typeFlags)
	, mPropertyFlags(propertyFlags)
	, mAlignment(4)
	, mMappedPtr(nullptr)
{
	setup();
	size = (size + mAlignment - 1) / mAlignment * mAlignment;
	mDeviceMemory = dev->allocate(size, typeFlags, propertyFlags);
}

VulkanMemory::VulkanMemory(Device* dev, const std::vector<Buffer*>& buffers, uint32_t propertyFlags)
	: mDevice(dev)
	, mDeviceMemory(VK_NULL_HANDLE)
	, mTypeFlags(0)
	, mPropertyFlags(propertyFlags)
	, mAlignment(4)
	, mMappedPtr(nullptr)
{
	setup();

	size_t size = 0;
	uint32_t memoryType = 0;
	for (auto buffer : buffers)
	{
		if (*buffer)
		{
			VkMemoryRequirements memReq = buffer->getMemoryRequirements();
			size += memReq.size;
			memoryType = memReq.memoryTypeBits;
		}
	}
	if (size == 0)
	{
		*this = VulkanMemory();
	}
	else
	{
		mTypeFlags = memoryType;
		size = (size + mAlignment - 1) / mAlignment * mAlignment;
		mDeviceMemory = dev->allocate(size, memoryType, propertyFlags);
	}
}

VulkanMemory::VulkanMemory(Device* dev, const std::vector<Image*>& images, uint32_t propertyFlags)
	: mDevice(dev)
	, mDeviceMemory(VK_NULL_HANDLE)
	, mTypeFlags(0)
	, mPropertyFlags(propertyFlags)
	, mAlignment(4)
	, mMappedPtr(nullptr)
{
	setup();

	size_t size = 0;
	uint32_t memoryType = 0;
	for (auto image : images)
	{
		VkMemoryRequirements memReq = image->getMemoryRequirements();
		size += memReq.size;
		memoryType = memReq.memoryTypeBits;
	}
	if (size == 0)
	{
		*this = VulkanMemory();
	}
	else
	{
		mTypeFlags = memoryType;
		size = (size + mAlignment - 1) / mAlignment * mAlignment;
		mDeviceMemory = dev->allocate(size, memoryType, propertyFlags);
	}
}


VulkanMemory::~VulkanMemory()
{
	cleanup();
}

VulkanMemory::VulkanMemory(VulkanMemory&& d) noexcept
	: mDevice(d.mDevice)
	, mDeviceMemory(d.mDeviceMemory)
	, mTypeFlags(d.mTypeFlags)
	, mPropertyFlags(d.mPropertyFlags)
	, mAlignment(d.mAlignment)
	, mMappedPtr(d.mMappedPtr)
{
	d.mDevice = nullptr;
	d.mDeviceMemory = VK_NULL_HANDLE;
	d.mTypeFlags = 0;
	d.mPropertyFlags = 0;
	d.mAlignment = 0;
	d.mMappedPtr = nullptr;
}

VulkanMemory& VulkanMemory::operator=(VulkanMemory&& d) noexcept
{
	cleanup();
	mDevice = d.mDevice;
	mDeviceMemory = d.mDeviceMemory;
	mTypeFlags = d.mTypeFlags;
	mPropertyFlags = d.mPropertyFlags;
	mAlignment = d.mAlignment;
	mMappedPtr = d.mMappedPtr;

	d.mDevice = nullptr;
	d.mDeviceMemory = VK_NULL_HANDLE;
	d.mTypeFlags = 0;
	d.mPropertyFlags = 0;
	d.mAlignment = 0;
	d.mMappedPtr = nullptr;
	return *this;
}

void VulkanMemory::cleanup()
{
	if (mDevice && mDeviceMemory)
	{
		if (mMappedPtr)
		{
			unmap();
		}
		mDevice->free(mDeviceMemory);
	}
	mDevice = nullptr;
	mDeviceMemory = VK_NULL_HANDLE;
}

uint32_t VulkanMemory::typeFlags() const
{
	return mTypeFlags;
}

uint32_t VulkanMemory::propertyFlags() const
{
	return mPropertyFlags;
}

size_t VulkanMemory::alignment() const
{
	return mAlignment;
}

VulkanMemory::operator VkDeviceMemory()
{
	return mDeviceMemory;
}

VulkanMemory::operator const VkDeviceMemory() const
{
	return mDeviceMemory;
}

void VulkanMemory::map()
{
	if (mDeviceMemory && mMappedPtr == nullptr)
	{
		vkMapMemory(*mDevice, mDeviceMemory, 0, VK_WHOLE_SIZE, 0, (void**)&mMappedPtr);
	}
}

void VulkanMemory::unmap()
{
	if (mMappedPtr)
	{
		vkUnmapMemory(*mDevice, mDeviceMemory);
		mMappedPtr = nullptr;
	}
}

uint8_t* VulkanMemory::getMappedPtr()
{
	return mMappedPtr;
}

const uint8_t* VulkanMemory::getMappedPtr() const
{
	return mMappedPtr;
}

};
