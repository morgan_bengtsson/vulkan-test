#include "vulkanContext.h"
#include "vulkanUtils.h"
#include "vulkanShader.h"

#include <iostream>


namespace vulkanutils
{

VulkanContext::VulkanContext()
	:mInstance()
	, mSurface()
	, mDevice()
	, mSwapChain()
	, mWinId(0)
	, mWidth(0)
	, mHeight(0)
	, mFrameNumber(0)
{}

VulkanContext::VulkanContext(const char* name, uint64_t winId)
	:VulkanContext()
{
	mWinId = winId;
	vulkanutils::InstanceBuilder vulkanInstanceBuilder;
	vulkanInstanceBuilder.input.applicationName = name;
	mInstance = vulkanInstanceBuilder();
}

void VulkanContext::teardown()
{
	if (mDevice)
	{
		vkDeviceWaitIdle(mDevice);
		for (size_t i = 0; i < MAX_FRAMES_IN_FLIGHT; i++)
		{
			mRenderFinishedSemaphores[i] = BinarySemaphore();
			mImageAvailableSemaphores[i] = BinarySemaphore();
			vkDestroyFence(mDevice, mInFlightFences[i], nullptr);
		}
		mSwapChain = SwapChain();
		mDevice = Device();
		mSurface = Surface();
		mInstance = Instance();
	}
}

VulkanContext::~VulkanContext()
{
	teardown();
}

VulkanContext::VulkanContext(VulkanContext&& vc)
	:mInstance(std::move(vc.mInstance))
	, mSurface(std::move(vc.mSurface))
	, mDevice(std::move(vc.mDevice))
	, mSwapChain(std::move(vc.mSwapChain))
	, mImageAvailableSemaphores{}
	, mRenderFinishedSemaphores{}
	, mInFlightFences{}
	, mWinId(vc.mWinId)
	, mWidth(vc.mWidth)
	, mHeight(vc.mHeight)
	, mFrameNumber(vc.mFrameNumber)
{

	std::swap(mImageAvailableSemaphores, vc.mImageAvailableSemaphores);
	std::swap(mRenderFinishedSemaphores, vc.mRenderFinishedSemaphores);
	std::swap(mInFlightFences, vc.mInFlightFences);
	vc.mWinId = 0;
	vc.mWidth = 0;
	vc.mHeight = 0;
	vc.mFrameNumber = 0;
}

VulkanContext& VulkanContext::operator=(VulkanContext&& vc)
{
	teardown();
	mInstance = std::move(vc.mInstance);
	mSurface = std::move(vc.mSurface);
	mDevice = std::move(vc.mDevice);
	mSwapChain = std::move(vc.mSwapChain);
	
	for (size_t i = 0; i < MAX_FRAMES_IN_FLIGHT; i++)
	{
		mImageAvailableSemaphores[i] = std::move(vc.mImageAvailableSemaphores[i]);
		mRenderFinishedSemaphores[i] = std::move(vc.mRenderFinishedSemaphores[i]);
		mInFlightFences[i] = vc.mInFlightFences[i];
		vc.mInFlightFences[i] = VK_NULL_HANDLE;
	}
	
	mWinId = vc.mWinId;
	vc.mWinId = 0;
	mWidth = vc.mWidth;
	vc.mWidth = 0;
	mHeight = vc.mHeight;
	vc.mHeight = 0;
	mFrameNumber = vc.mFrameNumber;
	vc.mFrameNumber = 0;
	return *this;
}

void VulkanContext::setup()
{
	VkSurfaceFormatKHR surfaceFormat;
	surfaceFormat.colorSpace = VK_COLOR_SPACE_SRGB_NONLINEAR_KHR;
	surfaceFormat.format = VK_FORMAT_B8G8R8A8_UNORM;

	vulkanutils::SwapChainBuilder vulkanSwapchainBuilder;
	vulkanSwapchainBuilder.input.device = &mDevice;
	vulkanSwapchainBuilder.input.surface = mSurface;
	vulkanSwapchainBuilder.input.phyDevice = mDevice.getPhysicalDevice();
	vulkanSwapchainBuilder.input.surfaceFormat = surfaceFormat;
	vulkanSwapchainBuilder.input.presentMode = VK_PRESENT_MODE_FIFO_KHR;
	vulkanSwapchainBuilder.input.numImages = NUM_SWAP_IMAGES;
	vulkanSwapchainBuilder.input.width = uint32_t(mWidth);
	vulkanSwapchainBuilder.input.height = uint32_t(mHeight);
	mSwapChain = vulkanSwapchainBuilder();
}

void VulkanContext::init(size_t width, size_t height)
{
	mWidth = width;
	mHeight = height;
	mSurface = vulkanutils::Surface(mInstance, mWinId);

	vulkanutils::DeviceBuilder vulkanDeviceBuilder;
	vulkanDeviceBuilder.input.instance = mInstance;
	vulkanDeviceBuilder.input.surface = mSurface;
	mDevice = vulkanDeviceBuilder();

	setup();

	VkSemaphoreCreateInfo semaphoreInfo = {};
	semaphoreInfo.sType = VK_STRUCTURE_TYPE_SEMAPHORE_CREATE_INFO;
	VkFenceCreateInfo fenceInfo = {};
	fenceInfo.sType = VK_STRUCTURE_TYPE_FENCE_CREATE_INFO;
	fenceInfo.flags = VK_FENCE_CREATE_SIGNALED_BIT;

	mImageAvailableSemaphores[0] = BinarySemaphore(mDevice, "ImageAvailableSemaphores 0");
	mImageAvailableSemaphores[1] = BinarySemaphore(mDevice, "ImageAvailableSemaphores 1");

	mRenderFinishedSemaphores[0] = BinarySemaphore(mDevice, "RenderFinishedSemaphores 0");
	mRenderFinishedSemaphores[1] = BinarySemaphore(mDevice, "RenderFinishedSemaphores 1");

	for (size_t i = 0; i < MAX_FRAMES_IN_FLIGHT; i++)
	{
		if (VK_SUCCESS != vkCreateFence(mDevice, &fenceInfo, nullptr, &mInFlightFences[i]))
		{
			logger(Log::Severity::CRITICAL) << "failed to create fences!" << std::endl;
		}
	}
}

Device& VulkanContext::device()
{
	return mDevice;
}

const Device& VulkanContext::device() const
{
	return mDevice;
}

SwapChain& VulkanContext::getSwapChain()
{
	return mSwapChain;
}

const SwapChain& VulkanContext::getSwapChain() const
{
	return mSwapChain;
}

bool VulkanContext::waitForNextImage(uint32_t* imageIndex)
{
	uint64_t frameNr = mFrameNumber + 1;
	size_t semaphoreIdx = (frameNr % MAX_FRAMES_IN_FLIGHT);
	VkResult waitResult = vkWaitForFences(mDevice, 1, &mInFlightFences[semaphoreIdx], VK_TRUE, 1000000);// std::numeric_limits<uint64_t>::max());
	if (waitResult != VK_SUCCESS)
		return false;

	mFrameNumber = frameNr;

	vkResetFences(mDevice, 1, &mInFlightFences[semaphoreIdx]);

	VkResult result =
		vkAcquireNextImageKHR(mDevice,
			mSwapChain,
			std::numeric_limits<uint64_t>::max(),
			mImageAvailableSemaphores[semaphoreIdx],
			VK_NULL_HANDLE,
			imageIndex);
	if (result != VK_SUCCESS)
	{
		logger(Log::Severity::WARNING) << "failed to aquire next image!" << std::endl;
		return false;
	}
	return true;
}

void VulkanContext::render(uint32_t imageIndex, const std::vector<VkCommandBuffer>& commandBuffers)
{
	size_t semaphoreIdx = (mFrameNumber % MAX_FRAMES_IN_FLIGHT);

	VkPresentInfoKHR presentInfo = {};
	presentInfo.sType = VK_STRUCTURE_TYPE_PRESENT_INFO_KHR;

	presentInfo.waitSemaphoreCount = 1;
	VkSemaphore sigSem = mRenderFinishedSemaphores[semaphoreIdx];
	presentInfo.pWaitSemaphores = &sigSem;

	VkSwapchainKHR swapChains[] = { mSwapChain };
	presentInfo.swapchainCount = uint32_t(std::size(swapChains));
	presentInfo.pSwapchains = swapChains;
	presentInfo.pImageIndices = &imageIndex;
	presentInfo.pResults = nullptr;

	mDevice.getGraphicsQueue().submit({ commandBuffers }, &mDevice.getComputeQueue(), 
		{ { &mImageAvailableSemaphores[semaphoreIdx], VK_PIPELINE_STAGE_COLOR_ATTACHMENT_OUTPUT_BIT } },
		{ &mRenderFinishedSemaphores[semaphoreIdx] }, mInFlightFences[semaphoreIdx]);

	vkQueuePresentKHR(mDevice.getPresentationQueue(), &presentInfo);
}

void VulkanContext::compute(const std::vector<VkCommandBuffer>& commandBuffers)
{
	mDevice.getComputeQueue().submit({ commandBuffers }, &mDevice.getGraphicsQueue(), {}, {});
}

void VulkanContext::resize(size_t width, size_t height)
{
	if (width != mWidth || height != mHeight)
	{
		mWidth = width;
		mHeight = height;

		teardown();
		setup();
	}
}

size_t VulkanContext::width() const
{
	return mWidth;
}

size_t VulkanContext::height() const
{
	return mHeight;
}

};
