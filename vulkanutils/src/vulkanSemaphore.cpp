#include "vulkanSemaphore.h"

namespace vulkanutils
{

Semaphore::Semaphore()
	: mDevice()
	, mSemaphore()
	, mName()
{}

Semaphore::Semaphore(Semaphore&& sp)
	: mDevice(sp.mDevice)
	, mSemaphore(sp.mSemaphore)
	, mName(sp.mName)
{
	sp.mDevice = VK_NULL_HANDLE;
	sp.mSemaphore = VK_NULL_HANDLE;
	sp.mName = "";
}

Semaphore& Semaphore::operator=(Semaphore&& sp)
{
	mDevice = sp.mDevice;
	sp.mDevice = VK_NULL_HANDLE;

	mSemaphore = sp.mSemaphore;
	sp.mSemaphore = VK_NULL_HANDLE;

	mName = sp.mName;
	sp.mName = "";
	return *this;
}

Semaphore::~Semaphore()
{
	if (mDevice && mSemaphore)
		vkDestroySemaphore(mDevice, mSemaphore, nullptr);
}

Semaphore::Semaphore(VkDevice device, const char* name)
	: mDevice(device)
	, mSemaphore()
	, mName(name)
{
}

Semaphore::operator VkSemaphore()
{
	return mSemaphore;
}

Semaphore::operator VkSemaphore() const
{
	return mSemaphore;
}

BinarySemaphore::BinarySemaphore()
	:Semaphore()
{}

BinarySemaphore::BinarySemaphore(VkDevice device, const char* name)
	:Semaphore(device, name)
{
	VkSemaphoreCreateInfo semaphoreInfo = {};
	semaphoreInfo.sType = VK_STRUCTURE_TYPE_SEMAPHORE_CREATE_INFO;
	semaphoreInfo.pNext = nullptr;

	vkCreateSemaphore(device, &semaphoreInfo, nullptr, &mSemaphore);
}

TimelineSemaphore::TimelineSemaphore()
	:Semaphore()
{}

TimelineSemaphore::TimelineSemaphore(VkDevice device, const char* name)
	:Semaphore(device, name)
{
	VkSemaphoreTypeCreateInfo timelineCreateInfo;
	timelineCreateInfo.sType = VK_STRUCTURE_TYPE_SEMAPHORE_TYPE_CREATE_INFO;
	timelineCreateInfo.pNext = NULL;
	timelineCreateInfo.semaphoreType = VK_SEMAPHORE_TYPE_TIMELINE;
	timelineCreateInfo.initialValue = 0;

	VkSemaphoreCreateInfo semaphoreInfo = {};
	semaphoreInfo.sType = VK_STRUCTURE_TYPE_SEMAPHORE_CREATE_INFO;
	semaphoreInfo.pNext = &timelineCreateInfo;

	vkCreateSemaphore(device, &semaphoreInfo, nullptr, &mSemaphore);
}


};
