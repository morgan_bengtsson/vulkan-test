#include "vulkanVertexAttributeLayout.h"

namespace vulkanutils
{

VertexAttributeLayout::VertexAttributeLayout()
	:mBindingDescriptions{}
	, mAttributeDescriptions{}
{
}

void VertexAttributeLayout::addPerVertexAttributeBindings(uint32_t binding, const std::vector<VertexAttrDescr>& attributes)
{
	addVertexAttributeBindings(binding, attributes, VK_VERTEX_INPUT_RATE_VERTEX);
}

void VertexAttributeLayout::addPerInstanceAttributeBindings(uint32_t binding, const std::vector<VertexAttrDescr>& attributes)
{
	addVertexAttributeBindings(binding, attributes, VK_VERTEX_INPUT_RATE_INSTANCE);
}

void VertexAttributeLayout::addVertexAttributeBindings(
	uint32_t binding, const std::vector<VertexAttrDescr>& attributes, VkVertexInputRate rate)
{
	VkVertexInputBindingDescription bd;
	bd.binding = binding;
	bd.inputRate = rate;
	bd.stride = 0;

	for (const auto& attribute : attributes)
	{
		VkVertexInputAttributeDescription ad = {};
		ad.binding = bd.binding;
		ad.format = attribute.format;
		ad.location = attribute.location;
		ad.offset = bd.stride;
		mAttributeDescriptions.push_back(ad);
		bd.stride += attribute.bytes;
	}
	mBindingDescriptions.push_back(bd);
}

std::vector<VkVertexInputBindingDescription> VertexAttributeLayout::getVertexBindingDescriptions() const
{
	return mBindingDescriptions;
}

std::vector<VkVertexInputAttributeDescription> VertexAttributeLayout::getVertexAttributeDescriptions() const
{
	return mAttributeDescriptions;
}

size_t VertexAttributeLayout::vertexAttributeSize() const
{
	size_t bytes = 0;
	for (auto& bd : mBindingDescriptions)
	{
		bytes += bd.stride;
	}
	return bytes;
}

};
