#include "vulkanSwapChain.h"

#include "vulkanutils.h"

#include <algorithm>

namespace vulkanutils
{

SwapChain::SwapChain()
	: mSwapChain(VK_NULL_HANDLE)
	, mDevice()
	, mImages()
{}

SwapChain::SwapChain(VkDevice device, VkSurfaceKHR surface
		  , VkPhysicalDevice phyDevice, VkSurfaceFormatKHR surfaceFormat
		  , VkQueue graphicsQueue, uint32_t graphicsQueueFamilyIdx
		  , VkQueue presentationQueue, uint32_t presentationQueueFamilyIdx
		  , VkPresentModeKHR presentMode, uint32_t numImages
		  , uint32_t width, uint32_t height)
	:SwapChain()
{
	mDevice = device;
	// Query device info
	VkPhysicalDeviceProperties properties;
	vkGetPhysicalDeviceProperties(phyDevice, &properties);

	VkSurfaceCapabilitiesKHR surfaceCapabilities;
	vkGetPhysicalDeviceSurfaceCapabilitiesKHR(phyDevice, surface, &surfaceCapabilities);

	uint32_t numSurfaceFormats = 0;
	vkGetPhysicalDeviceSurfaceFormatsKHR(phyDevice, surface, &numSurfaceFormats, nullptr);
	std::vector<VkSurfaceFormatKHR> surfaceFormats(numSurfaceFormats);
	vkGetPhysicalDeviceSurfaceFormatsKHR(phyDevice, surface, &numSurfaceFormats, surfaceFormats.data());

	uint32_t numPresentModes = 0;
	vkGetPhysicalDeviceSurfacePresentModesKHR(phyDevice, surface, &numPresentModes, nullptr);
	std::vector<VkPresentModeKHR> presentModes(numPresentModes);
	vkGetPhysicalDeviceSurfacePresentModesKHR(phyDevice, surface, &numPresentModes, presentModes.data());

	VkSwapchainCreateInfoKHR createInfo = {};
	createInfo.sType = VK_STRUCTURE_TYPE_SWAPCHAIN_CREATE_INFO_KHR;
	createInfo.surface = surface;
	createInfo.minImageCount = std::clamp(numImages,
										  surfaceCapabilities.minImageCount,
										  surfaceCapabilities.maxImageCount);
	createInfo.imageFormat = surfaceFormat.format;
	createInfo.imageColorSpace = surfaceFormat.colorSpace;
	if (surfaceCapabilities.currentExtent.width != (uint32_t) -1)
	{
		createInfo.imageExtent = surfaceCapabilities.currentExtent;
	}
	else
	{
		createInfo.imageExtent.height =
				std::clamp(height,
						   surfaceCapabilities.minImageExtent.height,
						   surfaceCapabilities.maxImageExtent.height);
		createInfo.imageExtent.width =
				std::clamp(width,
						   surfaceCapabilities.minImageExtent.width,
						   surfaceCapabilities.maxImageExtent.width);
	}
	createInfo.imageArrayLayers = 1;
	createInfo.imageUsage = VK_IMAGE_USAGE_COLOR_ATTACHMENT_BIT;

	uint32_t queueFamilyIdx[] = { graphicsQueueFamilyIdx, presentationQueueFamilyIdx };
	if (graphicsQueueFamilyIdx != presentationQueueFamilyIdx)
	{
		createInfo.imageSharingMode = VK_SHARING_MODE_CONCURRENT;
		createInfo.queueFamilyIndexCount = uint32_t(std::size(queueFamilyIdx));
		createInfo.pQueueFamilyIndices = queueFamilyIdx;
	}
	else
	{
		createInfo.imageSharingMode = VK_SHARING_MODE_EXCLUSIVE;
		createInfo.queueFamilyIndexCount = 0;
		createInfo.pQueueFamilyIndices = nullptr;
	}
	createInfo.preTransform = surfaceCapabilities.currentTransform;
	createInfo.compositeAlpha = VK_COMPOSITE_ALPHA_OPAQUE_BIT_KHR;
	createInfo.presentMode = presentMode;
	createInfo.clipped = VK_TRUE;
	createInfo.oldSwapchain = VK_NULL_HANDLE;

	if (VK_SUCCESS == vkCreateSwapchainKHR(mDevice, &createInfo, nullptr, &mSwapChain))
	{
		uint32_t numImages = 0;
		vkGetSwapchainImagesKHR(mDevice, mSwapChain, &numImages, nullptr);
		mImages.resize(numImages);
		vkGetSwapchainImagesKHR(mDevice, mSwapChain, &numImages, mImages.data());
	}
}

SwapChain::~SwapChain()
{
	teardown();
}

SwapChain::SwapChain(SwapChain&& sc)
	:SwapChain()
{
	*this = std::move(sc);
}

SwapChain& SwapChain::operator=(SwapChain&& sc)
{
	teardown();
	std::swap(mSwapChain, sc.mSwapChain);
	std::swap(mDevice, sc.mDevice);
	std::swap(mImages, sc.mImages);
	return *this;
}

void SwapChain::teardown()
{
	if (mDevice && mSwapChain)
	{
		vkDestroySwapchainKHR(mDevice, mSwapChain, nullptr);
	}
	mSwapChain = VK_NULL_HANDLE;
	mDevice = VK_NULL_HANDLE;
	mImages.clear();
}

SwapChain::operator VkSwapchainKHR()
{
	return mSwapChain;
}
const std::vector<VkImage>& SwapChain::getImages() const
{
	return mImages;
}

SwapChain SwapChainBuilder::operator()()
{
	return SwapChain(*input.device, input.surface, input.device->getPhysicalDevice()
					 , input.surfaceFormat
					 , input.device->getGraphicsQueue(), input.device->getGraphicsQueue().getQueueFamilyIdx()
					 , input.device->getPresentationQueue(), input.device->getPresentationQueue().getQueueFamilyIdx()
					 , input.presentMode, input.numImages
					 , input.width, input.height);
}

};
