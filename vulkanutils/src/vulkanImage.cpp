#include "vulkanImage.h"
#include "vulkanUtils.h"

#include <iostream>
#include <algorithm>


namespace vulkanutils
{

DepthImage::DepthImage(VkDevice device, size_t width, size_t height, VkFormat format)
	:Image(device, width, height, format, 1
		, VK_IMAGE_USAGE_DEPTH_STENCIL_ATTACHMENT_BIT)
{
}

TextureImage::TextureImage(VkDevice device, size_t width, size_t height, VkFormat format)
	: Image(device, width, height, format, size_t(1 + std::log2(std::max(width, height)))
		, VK_IMAGE_USAGE_TRANSFER_SRC_BIT | VK_IMAGE_USAGE_TRANSFER_DST_BIT | VK_IMAGE_USAGE_SAMPLED_BIT)
{
}

void Image::createMipMap(VkCommandBuffer commandBuffer)
{
	VkImageMemoryBarrier barrier = {};
	barrier.sType = VK_STRUCTURE_TYPE_IMAGE_MEMORY_BARRIER;
	barrier.image = mImage;
	barrier.srcQueueFamilyIndex = VK_QUEUE_FAMILY_IGNORED;
	barrier.dstQueueFamilyIndex = VK_QUEUE_FAMILY_IGNORED;
	barrier.subresourceRange.aspectMask = VK_IMAGE_ASPECT_COLOR_BIT;
	barrier.subresourceRange.baseArrayLayer = 0;
	barrier.subresourceRange.layerCount = 1;
	barrier.subresourceRange.levelCount = 1;


	size_t mipWidth = mWidth;
	size_t mipHeight = mHeight;

	for (size_t i = 1; i < mMipMapLevels; i++)
	{
		barrier.subresourceRange.baseMipLevel = uint32_t(i - 1);
		barrier.oldLayout = VK_IMAGE_LAYOUT_TRANSFER_DST_OPTIMAL;
		barrier.newLayout = VK_IMAGE_LAYOUT_TRANSFER_SRC_OPTIMAL;
		barrier.srcAccessMask = VK_ACCESS_TRANSFER_WRITE_BIT;
		barrier.dstAccessMask = VK_ACCESS_TRANSFER_READ_BIT;

		vkCmdPipelineBarrier(commandBuffer,
			VK_PIPELINE_STAGE_TRANSFER_BIT, VK_PIPELINE_STAGE_TRANSFER_BIT, 0,
			0, nullptr,
			0, nullptr,
			1, &barrier);


		VkImageBlit blit = {};
		blit.srcOffsets[0] = { 0, 0, 0 };
		blit.srcOffsets[1] = { int32_t(std::max<size_t>(mipWidth, 1)),
								int32_t(std::max<size_t>(mipHeight, 1)),
								1 };
		blit.srcSubresource.aspectMask = VK_IMAGE_ASPECT_COLOR_BIT;
		blit.srcSubresource.mipLevel = uint32_t(i - 1);
		blit.srcSubresource.baseArrayLayer = 0;
		blit.srcSubresource.layerCount = 1;
		blit.dstOffsets[0] = { 0, 0, 0 };
		blit.dstOffsets[1] = { int32_t(std::max<size_t>(mipWidth / 2, 1)),
								int32_t(std::max<size_t>(mipHeight / 2, 1)),
								1 };
		blit.dstSubresource.aspectMask = VK_IMAGE_ASPECT_COLOR_BIT;
		blit.dstSubresource.mipLevel = uint32_t(i);
		blit.dstSubresource.baseArrayLayer = 0;
		blit.dstSubresource.layerCount = 1;


		vkCmdBlitImage(commandBuffer,
			mImage, VK_IMAGE_LAYOUT_TRANSFER_SRC_OPTIMAL,
			mImage, VK_IMAGE_LAYOUT_TRANSFER_DST_OPTIMAL,
			1, &blit,
			VK_FILTER_LINEAR);

		barrier.oldLayout = VK_IMAGE_LAYOUT_TRANSFER_SRC_OPTIMAL;
		barrier.newLayout = VK_IMAGE_LAYOUT_SHADER_READ_ONLY_OPTIMAL;
		barrier.srcAccessMask = VK_ACCESS_TRANSFER_READ_BIT;
		barrier.dstAccessMask = VK_ACCESS_SHADER_READ_BIT;

		vkCmdPipelineBarrier(commandBuffer,
			VK_PIPELINE_STAGE_TRANSFER_BIT, VK_PIPELINE_STAGE_FRAGMENT_SHADER_BIT, 0,
			0, nullptr,
			0, nullptr,
			1, &barrier);

		mipWidth /= 2;
		mipHeight /= 2;
	}

	barrier.subresourceRange.baseMipLevel = uint32_t(mMipMapLevels - 1);
	barrier.oldLayout = VK_IMAGE_LAYOUT_TRANSFER_DST_OPTIMAL;
	barrier.newLayout = VK_IMAGE_LAYOUT_SHADER_READ_ONLY_OPTIMAL;
	barrier.srcAccessMask = VK_ACCESS_TRANSFER_WRITE_BIT;
	barrier.dstAccessMask = VK_ACCESS_SHADER_READ_BIT;


	vkCmdPipelineBarrier(commandBuffer,
		VK_PIPELINE_STAGE_TRANSFER_BIT, VK_PIPELINE_STAGE_FRAGMENT_SHADER_BIT, 0,
		0, nullptr,
		0, nullptr,
		1, &barrier);

	mImageLayout = VK_IMAGE_LAYOUT_SHADER_READ_ONLY_OPTIMAL;
}

Image::Image()
	:mDevice(nullptr)
	, mImage(nullptr)
	, mUsage(0)
	, mImageLayout(VK_IMAGE_LAYOUT_UNDEFINED)
	, mFormat(VK_FORMAT_UNDEFINED)
	, mWidth(0)
	, mHeight(0)
	, mMipMapLevels(0)
	, mImageMemory(nullptr)
	, mBoundMemAlignment(1)
	, mBoundMemPropertyFlags(0)
	, mBoundMemOffset(0)
{}


size_t Image::mipMapLevels() const
{
	return mMipMapLevels;
}

Image::Image(VkDevice device,
	size_t width, size_t height,
	VkFormat format,
	size_t mipMapLevels,
	VkImageUsageFlags usageFlags)
	:Image()
{
	mDevice = device;
	mWidth = width;
	mHeight = height;
	mFormat = format;
	mUsage = usageFlags;
	mMipMapLevels = mipMapLevels;

	VkImageCreateInfo imageInfo = {};
	imageInfo.sType = VK_STRUCTURE_TYPE_IMAGE_CREATE_INFO;
	imageInfo.imageType = VK_IMAGE_TYPE_2D;
	imageInfo.format = mFormat;
	imageInfo.extent.width = uint32_t(mWidth);
	imageInfo.extent.height = uint32_t(mHeight);
	imageInfo.extent.depth = 1;
	
	imageInfo.mipLevels = uint32_t(mMipMapLevels);
	
	imageInfo.arrayLayers = 1;
	imageInfo.tiling = VK_IMAGE_TILING_OPTIMAL;
	imageInfo.initialLayout = VK_IMAGE_LAYOUT_UNDEFINED;
	imageInfo.usage = mUsage;
	imageInfo.sharingMode = VK_SHARING_MODE_EXCLUSIVE;
	imageInfo.samples = VK_SAMPLE_COUNT_1_BIT;
	imageInfo.flags = 0;

	if (vkCreateImage(mDevice, &imageInfo, nullptr, &mImage) != VK_SUCCESS)
	{
		vulkanutils::logger(Log::Severity::CRITICAL) << "failed to create image!" << std::endl;
	}
}

VkMemoryRequirements Image::getMemoryRequirements() const
{
	VkMemoryRequirements memRequirements;
	vkGetImageMemoryRequirements(mDevice, mImage, &memRequirements);	
	return memRequirements;
}

void Image::bindImagesToMemory(const std::vector<Image*>& images, VulkanMemory& deviceMemory)
{
	size_t offset = 0;
	for (auto image : images)
	{
		image->bindToMemory(deviceMemory, offset);
		offset += image->getMemoryRequirements().size;
	}
}

bool Image::bindToMemory(VulkanMemory& devMem, size_t offset)
{
	VkMemoryRequirements memReq = getMemoryRequirements();

	if (VK_SUCCESS != vkBindImageMemory(mDevice, mImage, devMem, offset))
	{
		vulkanutils::logger(Log::Severity::CRITICAL) << "failed to bind image to device memory" << std::endl;
		if ((offset % devMem.alignment()) != 0)
		{
			vulkanutils::logger(Log::Severity::CRITICAL) << "Invalid offset " << offset << ", must be \
			a multiple of alignment " << devMem.alignment() << std::endl;
		}
		return false;
	}
	mImageMemory = devMem;
	mBoundMemOffset = offset;
	mBoundMemAlignment = devMem.alignment();
	mBoundMemPropertyFlags = devMem.propertyFlags();
	return true;
}



Image::~Image()
{
	teardown();
}

Image::Image(Image&& b) noexcept
	:mDevice(b.mDevice)
	, mImage(b.mImage)
	, mUsage(b.mUsage)
	, mImageLayout(b.mImageLayout)
	, mFormat(b.mFormat)
	, mWidth(b.mWidth)
	, mHeight(b.mHeight)
	, mMipMapLevels(b.mMipMapLevels)
	, mImageMemory(b.mImageMemory)
	, mBoundMemAlignment(b.mBoundMemAlignment)
	, mBoundMemPropertyFlags(b.mBoundMemPropertyFlags)
	, mBoundMemOffset(b.mBoundMemOffset)

{
	b.mDevice = nullptr;
	b.mImage = VK_NULL_HANDLE;
	b.mUsage = 0;
	b.mImageLayout = VK_IMAGE_LAYOUT_UNDEFINED;
	b.mFormat = VK_FORMAT_UNDEFINED;
	b.mWidth = 0;
	b.mHeight = 0;
	b.mMipMapLevels = 0;
	b.mImageMemory = VK_NULL_HANDLE;
	b.mBoundMemAlignment = 0;
	b.mBoundMemPropertyFlags = 0;
	b.mBoundMemOffset = 0;
}

Image& Image::operator=(Image&& b) noexcept
{
	if (this != &b)
	{
		teardown();
		std::swap(mDevice, b.mDevice);
		std::swap(mImage, b.mImage);
		std::swap(mImageLayout, b.mImageLayout);
		std::swap(mFormat, b.mFormat);
		std::swap(mWidth, b.mWidth);
		std::swap(mHeight, b.mHeight);
		std::swap(mMipMapLevels, b.mMipMapLevels);
		std::swap(mImageMemory, b.mImageMemory);
		std::swap(mBoundMemAlignment, b.mBoundMemAlignment);
		std::swap(mBoundMemPropertyFlags, b.mBoundMemPropertyFlags);
		std::swap(mBoundMemOffset, b.mBoundMemOffset);
	}
	return *this;
}

Image::operator const VkImage() const
{
	return mImage;
}

Image::operator VkImage()
{
	return mImage;
}

void Image::teardown()
{
	if (mDevice && mImage)
	{
		vkDestroyImage(mDevice, mImage, nullptr);
	}
	mDevice = nullptr;
	mImage = VK_NULL_HANDLE;
	mUsage = 0;
	mImageLayout = VK_IMAGE_LAYOUT_UNDEFINED;
	mFormat = VK_FORMAT_UNDEFINED;
	mWidth = 0;
	mHeight = 0;
	mMipMapLevels = 0;
	mImageMemory = VK_NULL_HANDLE;
	mBoundMemAlignment = 0;
	mBoundMemPropertyFlags = 0;
	mBoundMemOffset = 0;
}

size_t Image::size() const
{
	return mWidth * mHeight;
}

size_t Image::width() const
{
	return mWidth;
}

size_t Image::height() const
{
	return mHeight;
}

VkFormat Image::format() const
{
	return mFormat;
}

VkImageLayout Image::layout() const
{
	return mImageLayout;
}

};
