#include "vulkanBufferAllocator.h"

#include <vulkanDevice.h>
#include <vulkanBuffer.h>
#include <vulkanImage.h>

#include <algorithm>
#include <assert.h>

namespace vulkanutils
{

bool overlaps(const vulkanutils::MemRange& r1, const vulkanutils::MemRange& r2)
{
	return (r1.offset >= r2.offset && r1.offset <= r2.offset + r2.size) ||
		(r2.offset >= r1.offset && r2.offset <= r1.offset + r1.size);
}


std::vector<vulkanutils::MemRange> combine(const std::vector<vulkanutils::MemRange>& ranges)
{
	std::vector<vulkanutils::MemRange> sortedRanges = ranges;
	std::sort(std::begin(sortedRanges), std::end(sortedRanges),
		[](const vulkanutils::MemRange& r1, const vulkanutils::MemRange& r2)
	{
		return r1.offset < r2.offset;
	});

	std::vector<vulkanutils::MemRange> ret;
	for (const auto& range : sortedRanges)
	{
		if (ret.empty())
		{
			ret.push_back(range);
		}
		else if (overlaps(ret.back(), range))
		{
			size_t offset = std::min<size_t>(ret.back().offset, range.offset);
			size_t offset2 = std::max<size_t>(ret.back().offset + ret.back().size, range.offset + range.size);
			ret.back().offset = offset;
			ret.back().size = offset2 - offset;
		}
		else
		{
			ret.push_back(range);
		}
	}
	return ret;
}

template<typename BufferType>
BufferAllocator<BufferType>::BufferAllocator(vulkanutils::Device* device, size_t chunkSize, VkSharingMode sharingMode)
	: mChunks()
	, mDefaultChunkSize(chunkSize)
	, mDevice(device)
	, mSharingMode(sharingMode)
{
}

template<typename BufferType>
BufferAllocation<BufferType> BufferAllocator<BufferType>::allocate(size_t chunkIdx, size_t size)
{
	Chunk& chunk = mChunks[chunkIdx];
	const size_t minBlockSize = mDevice->getPhysicalDeviceProperties().limits.minUniformBufferOffsetAlignment;
	Block* freeblock = nullptr;
	for (auto it = std::begin(chunk.blocks); it != std::end(chunk.blocks); it++)
	{
		Block* block = &(*it);
		size_t freespace = 0;
		if (!block->used)
		{
			if (freeblock == nullptr)
			{
				freeblock = block;
			}
			else
			{
				// Expand first empty block in sequence over this block
				freeblock->size = block->offset + block->size - freeblock->offset;
				chunk.blocks.erase(it);
			}
			if (freeblock->size >= size)
			{
				size_t diff = freeblock->size - size;
				if (diff > minBlockSize)
				{
					freeblock->size = size;
					Block newblock{ diff, freeblock->offset + freeblock->size, false };
					chunk.blocks.insert(++it, newblock);
				}
				freeblock->used = true;
				if constexpr (std::is_same_v<BufferType, vulkanutils::UploadBuffer>)
				{
					return BufferAllocation<BufferType>(this, chunkIdx, vulkanutils::MemRange { freeblock->offset, freeblock->size }, chunk.memory.getMappedPtr());
				}
				else
				{
					return BufferAllocation<BufferType>(this, chunkIdx, vulkanutils::MemRange { freeblock->offset, freeblock->size });
				}
			}
		}
		else
		{
			freeblock = nullptr;
		}
	}
	return BufferAllocation<BufferType>();
}

template<typename BufferType>
void BufferAllocator<BufferType>::appendChunk(size_t size)
{
	size = size ? size : mDefaultChunkSize;
	Chunk& chunk = mChunks.emplace_back();
	chunk.buffer.reset(new BufferType(*mDevice, size, mSharingMode));
	VkMemoryRequirements memReq = chunk.buffer->getMemoryRequirements();
	chunk.size = size;
	chunk.memory = vulkanutils::VulkanMemory(mDevice, memReq.size, memReq.memoryTypeBits, VK_MEMORY_PROPERTY_DEVICE_LOCAL_BIT);
	chunk.buffer->bindToMemory(chunk.memory, 0);
	chunk.blocks.push_back(Block{ chunk.buffer->size(), 0, false });
}



template<>
void BufferAllocator<vulkanutils::UploadBuffer>::appendChunk(size_t size)
{
	size = size ? size : mDefaultChunkSize;
	Chunk& chunk = mChunks.emplace_back();
	chunk.buffer.reset(new vulkanutils::UploadBuffer(*mDevice, size, mSharingMode));
	VkMemoryRequirements memReq = chunk.buffer->getMemoryRequirements();
	chunk.size = size;
	chunk.memory = vulkanutils::VulkanMemory(mDevice, memReq.size, memReq.memoryTypeBits, VK_MEMORY_PROPERTY_HOST_VISIBLE_BIT | VK_MEMORY_PROPERTY_HOST_COHERENT_BIT);
	chunk.buffer->bindToMemory(chunk.memory, 0);
	chunk.memory.map();
	chunk.blocks.push_back(Block{ chunk.buffer->size(), 0, false });
}

template<>
void BufferAllocator<vulkanutils::IndirectDrawBuffer>::appendChunk(size_t size)
{
	size = size ? size : mDefaultChunkSize;
	Chunk& chunk = mChunks.emplace_back();
	chunk.buffer.reset(new vulkanutils::IndirectDrawBuffer(*mDevice, size, mSharingMode));
	VkMemoryRequirements memReq = chunk.buffer->getMemoryRequirements();
	chunk.size = size;
	chunk.memory = vulkanutils::VulkanMemory(mDevice, memReq.size, memReq.memoryTypeBits, VK_MEMORY_PROPERTY_HOST_VISIBLE_BIT);
	chunk.buffer->bindToMemory(chunk.memory, 0);
	chunk.memory.map();
	chunk.blocks.push_back(Block{ chunk.buffer->size(), 0, false });
}



template<typename BufferType>
BufferAllocation<BufferType> BufferAllocator<BufferType>::allocate(size_t requestedSize, bool pack)
{
	size_t minBlockSize = 1;
	if (!pack)
	{
		if constexpr (std::is_same_v<BufferType, vulkanutils::UniformBuffer>)
			minBlockSize = mDevice->getPhysicalDeviceProperties().limits.minUniformBufferOffsetAlignment;
		else if constexpr (std::is_same_v<BufferType, vulkanutils::ShaderStorageBuffer>)
			minBlockSize = mDevice->getPhysicalDeviceProperties().limits.minStorageBufferOffsetAlignment;
	}
	
	size_t size = ((requestedSize + minBlockSize - 1) / minBlockSize) * minBlockSize;

	for (size_t chunkIdx = 0; chunkIdx < mChunks.size(); chunkIdx++)
	{
		BufferAllocation alloc = allocate(chunkIdx, size);
		if (alloc.getBuffer())
		{
			return alloc;
		}
	}
	appendChunk(std::max(mDefaultChunkSize, size));
	return allocate(mChunks.size() - 1, size);
}

template<typename BufferType>
void BufferAllocator<BufferType>::deallocate(BufferAllocation<BufferType>& allocation)
{
	auto& chunk = mChunks[allocation.mChunkIdx];
	for (auto& block : chunk.blocks)
	{
		if (block.offset == allocation.getMemRange().offset)
		{
			block.used = false;
			return;
		}
	}
	assert(0 && "Tried to deallocate unknown memory block");
}



ImageAllocator::ImageAllocator(vulkanutils::Device* device)
	: mChunks()
	, mDevice(device)
{
}


void ImageAllocator::deallocate(Image* image)
{
	for (auto chunkIt = std::begin(mChunks); chunkIt != std::end(mChunks); chunkIt++)
	{
		if (chunkIt->image.get() == image)
		{
			mChunks.erase(chunkIt);
			return;
		}
	}
	assert(0 && "Tried to deallocate unknown memory block");
}








BufferAllocation<vulkanutils::UploadBuffer>::BufferAllocation() 
	: mAllocator(nullptr)
	, mChunkIdx(0)
	, mRange()
	, mData(nullptr)
{}

BufferAllocation<vulkanutils::UploadBuffer>::BufferAllocation(BufferAllocation&& d) noexcept
	:mAllocator(d.mAllocator), mChunkIdx(d.mChunkIdx)
	, mRange(d.mRange), mData(d.mData)
{
	d.mAllocator = nullptr;
	d.mChunkIdx = 0;
	d.mRange = {0, 0};
	d.mData = nullptr;
}

BufferAllocation<vulkanutils::UploadBuffer>& BufferAllocation<vulkanutils::UploadBuffer>::operator=(BufferAllocation&& d) noexcept
{
	this->~BufferAllocation();
	mAllocator = d.mAllocator;
	mChunkIdx = d.mChunkIdx;
	mRange = d.mRange;
	mData = d.mData;
	d.mAllocator = nullptr;
	d.mChunkIdx = 0;
	d.mRange = {0, 0};
	d.mData = nullptr;
	return *this;
}

BufferAllocation<vulkanutils::UploadBuffer>::~BufferAllocation()
{
	if (mAllocator)
		mAllocator->deallocate(*this);
}

std::span<const uint8_t> BufferAllocation<vulkanutils::UploadBuffer>::getData() const
{
	return { mData + mRange.offset, mRange.size };
}

std::span<uint8_t> BufferAllocation<vulkanutils::UploadBuffer>::getData()
{
	return { mData + mRange.offset, mRange.size };
}

std::shared_ptr<vulkanutils::UploadBuffer> BufferAllocation<vulkanutils::UploadBuffer>::getBuffer() const
{
	if (mAllocator)
		return mAllocator->getBuffer(mChunkIdx);
	return nullptr;
}

const MemRange& BufferAllocation<vulkanutils::UploadBuffer>::getMemRange() const
{
	return mRange;
}

BufferAllocation<vulkanutils::UploadBuffer>::BufferAllocation(BufferAllocator<vulkanutils::UploadBuffer>* allocator, size_t chunkIdx, MemRange range, uint8_t* data)
	: mAllocator(allocator), mChunkIdx(chunkIdx), mRange(range), mData(data)
{}


ImageAllocation::ImageAllocation()
	: mAllocator(nullptr)
	, mImage()
{}

ImageAllocation::ImageAllocation(ImageAllocator* allocator, std::shared_ptr<Image> image)
	: mAllocator(allocator), mImage(image)
{
}

ImageAllocation::ImageAllocation(ImageAllocation&& d)
{
	mAllocator = d.mAllocator;
	d.mAllocator = nullptr;
	mImage = d.mImage;
	d.mImage.reset();
}

ImageAllocation& ImageAllocation::operator=(ImageAllocation&& d)
{
	this->~ImageAllocation();
	mAllocator = d.mAllocator;
	d.mAllocator = nullptr;
	mImage = d.mImage;
	d.mImage.reset();
	return *this;
}


ImageAllocation::~ImageAllocation()
{
	if (mAllocator)
		mAllocator->deallocate(mImage.get());
}

std::shared_ptr<Image> ImageAllocation::getImage() const
{
	return mImage;
}

#pragma optimize ("", off)

template class BufferAllocator<vulkanutils::UploadBuffer>;
template class BufferAllocator<vulkanutils::VertexBuffer>;
template class BufferAllocator<vulkanutils::IndexBuffer>;
template class BufferAllocator<vulkanutils::UniformBuffer>;
template class BufferAllocator<vulkanutils::ShaderStorageBuffer>;
template class BufferAllocator<vulkanutils::IndirectDrawBuffer>;


};
