#include "vulkanImageSampler.h"
#include "vulkanUtils.h"


namespace vulkanutils
{

ImageSampler::ImageSampler()
	:mDevice(VK_NULL_HANDLE)
	, mSampler(VK_NULL_HANDLE)
{

}

ImageSampler::ImageSampler(VkDevice device, VkFilter magFilter, VkFilter minFilter, VkSamplerAddressMode addressMode[3]
	, float anisotropy, VkBorderColor borderColor)
	:ImageSampler()
{
	mDevice = device;

	VkSamplerCreateInfo createInfo = {};
	createInfo.sType = VK_STRUCTURE_TYPE_SAMPLER_CREATE_INFO;
	createInfo.magFilter = magFilter;
	createInfo.minFilter = minFilter;
	createInfo.mipmapMode = VK_SAMPLER_MIPMAP_MODE_LINEAR;

	createInfo.addressModeU = addressMode[0];
	createInfo.addressModeV = addressMode[1];
	createInfo.addressModeW = addressMode[2];

	createInfo.mipLodBias = 0.0f;
	createInfo.anisotropyEnable = (anisotropy >= 1.0f);
	createInfo.compareEnable = VK_TRUE;
	createInfo.compareOp = VK_COMPARE_OP_LESS;
	createInfo.maxAnisotropy = anisotropy;
	createInfo.borderColor = borderColor;
	createInfo.unnormalizedCoordinates = VK_FALSE;

	createInfo.minLod = 0.0f;
	createInfo.maxLod = 10.0f;

	if (VK_SUCCESS != vkCreateSampler(device, &createInfo, nullptr, &mSampler))
	{
		logger(Log::Severity::ERROR) << "Failed to create image sampler" << std::endl;
	}
}

ImageSampler::~ImageSampler()
{
	teardown();
}

void ImageSampler::teardown()
{
	if (mDevice && mSampler)
	{
		vkDestroySampler(mDevice, mSampler, nullptr);
	}
}

ImageSampler::ImageSampler(ImageSampler&& d) noexcept
	: mDevice(d.mDevice)
	, mSampler(d.mSampler)
{
	d.mDevice = VK_NULL_HANDLE;
	d.mSampler = VK_NULL_HANDLE;
}

ImageSampler& ImageSampler::operator=(ImageSampler&& d) noexcept
{
	teardown();
	mDevice = d.mDevice;
	mSampler = d.mSampler;
	d.mDevice = VK_NULL_HANDLE;
	d.mSampler = VK_NULL_HANDLE;
	return *this;
}

ImageSampler::operator VkSampler()
{
	return mSampler;
}

ImageSampler::operator const VkSampler() const
{
	return mSampler;
}

};
