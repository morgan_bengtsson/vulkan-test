#include "vulkanSurface.h"
#include "vulkanwinutils.h"
#include "vulkanutils.h"

namespace vulkanutils
{


Surface::Surface()
	:mSurface(VK_NULL_HANDLE)
	, mInstance(VK_NULL_HANDLE)
{
}


Surface::Surface(VkInstance instance, uint64_t winId)
	:Surface()
{
	int err = 0;
	if (!err)
	{
		mSurface = vulkanutils::createSurface(instance, winId);
		if(!mSurface)
		{
			err = VK_ERROR_FEATURE_NOT_PRESENT;
			logger(Log::Severity::CRITICAL) << "Failed to create surface" << std::endl;
		}
	}
	if (!err)
	{
		mInstance = instance;
		logger(Log::Severity::INFO) << "Created vulkan surface" << std::endl;
	}
}

Surface::~Surface()
{
	if (mInstance != VK_NULL_HANDLE && mSurface != VK_NULL_HANDLE)
	{
		logger(Log::Severity::INFO) << "Tear down vulkan surface" << std::endl;
		vkDestroySurfaceKHR(mInstance, mSurface, nullptr);
	}
}

Surface::Surface(Surface&& s)
{
	*this = std::move(s);
}

Surface& Surface::operator=(Surface&& s)
{
	mSurface = s.mSurface;
	mInstance = s.mInstance;
	s.mSurface = VK_NULL_HANDLE;
	s.mInstance = VK_NULL_HANDLE;
	return *this;
}

Surface::operator VkSurfaceKHR()
{
	return mSurface;
}

};
