#include "vulkanBuffer.h"
#include "vulkanUtils.h"

#include <algorithm>

namespace vulkanutils
{


Buffer::Buffer(VkDevice device, size_t size, VkBufferUsageFlags bufferUsage, VkSharingMode sharingMode)
	:mDevice(device)
	, mSize(size)
	, mBuffer(VK_NULL_HANDLE)
	, mBufferMemory(VK_NULL_HANDLE)
	, mBoundMemAlignment(1)
	, mBoundMemPropertyFlags(0)
	, mBoundMemOffset(0)
{
	VkBufferCreateInfo bufferInfo = {};
	bufferInfo.sType = VK_STRUCTURE_TYPE_BUFFER_CREATE_INFO;
	bufferInfo.size = size;
	bufferInfo.usage = bufferUsage;
	bufferInfo.sharingMode = sharingMode;
	bufferInfo.flags = 0;
	bufferInfo.queueFamilyIndexCount = 0;
	bufferInfo.pQueueFamilyIndices = nullptr;
	if (vkCreateBuffer(mDevice, &bufferInfo, nullptr, &mBuffer) != VK_SUCCESS)
	{
		vulkanutils::logger(Log::Severity::CRITICAL) << "failed to create buffer!" << std::endl;
	}
}

VkMemoryRequirements Buffer::getMemoryRequirements() const
{
	VkMemoryRequirements memRequirements;
	vkGetBufferMemoryRequirements(mDevice, mBuffer, &memRequirements);
	return memRequirements;
}

bool Buffer::bindToMemory(VulkanMemory& devMem, size_t offset)
{
	VkMemoryRequirements memReq = getMemoryRequirements();

	if (VK_SUCCESS != vkBindBufferMemory(mDevice, mBuffer, devMem, offset))
	{
		vulkanutils::logger(Log::Severity::CRITICAL) << "failed to bind buffer to device memory" << std::endl;
		if ((offset % devMem.alignment()) != 0)
		{
			vulkanutils::logger(Log::Severity::CRITICAL) << "Invalid offset " << offset << ", must be \
			a multiple of alignment " << devMem.alignment() << std::endl;
		}
		return false;
	}
	mBufferMemory = devMem;
	mBoundMemOffset = offset;
	mBoundMemAlignment = devMem.alignment();
	mBoundMemPropertyFlags = devMem.propertyFlags();
	return true;
}

void Buffer::bindBuffersToMemory(const std::vector<Buffer*>& buffers, VulkanMemory& deviceMemory)
{
	size_t offset = 0;
	for (auto buffer : buffers)
	{
		if (*buffer)
		{
			buffer->bindToMemory(deviceMemory, offset);
			offset += buffer->getMemoryRequirements().size;
		}
	}
}

Buffer::Buffer()
	:mDevice(nullptr)
	, mSize(0)
	, mBuffer(nullptr)
	, mBufferMemory(nullptr)
	, mBoundMemAlignment(0)
	, mBoundMemPropertyFlags(0)
	, mBoundMemOffset(0)
{}

Buffer::~Buffer()
{
	teardown();
}

Buffer::Buffer(Buffer&& b) noexcept
	:mDevice(b.mDevice)
	, mSize(b.mSize)
	, mBuffer(b.mBuffer)
	, mBufferMemory(b.mBufferMemory)
	, mBoundMemAlignment(b.mBoundMemAlignment)
	, mBoundMemPropertyFlags(b.mBoundMemPropertyFlags)
	, mBoundMemOffset(b.mBoundMemOffset)

{
	b.mDevice = nullptr;
	b.mSize = 0;
	b.mBuffer = VK_NULL_HANDLE;
	b.mBufferMemory = VK_NULL_HANDLE;
	b.mBoundMemAlignment = 0;
	b.mBoundMemPropertyFlags = 0;
	b.mBoundMemOffset = 0;
}

Buffer& Buffer::operator=(Buffer&& b) noexcept
{
	if (this != &b)
	{
		teardown();
		std::swap(mDevice, b.mDevice);
		std::swap(mSize, b.mSize);
		std::swap(mBuffer, b.mBuffer);
		std::swap(mBufferMemory, b.mBufferMemory);
		std::swap(mBoundMemAlignment, b.mBoundMemAlignment);
		std::swap(mBoundMemPropertyFlags, b.mBoundMemPropertyFlags);
		std::swap(mBoundMemOffset, b.mBoundMemOffset);
	}
	return *this;
}

Buffer::operator const VkBuffer() const
{
	return mBuffer;
}

Buffer::operator VkBuffer()
{
	return mBuffer;
}

void Buffer::teardown()
{
	if (mDevice && mBuffer)
	{
		vkDestroyBuffer(mDevice, mBuffer, nullptr);
	}
	mDevice = nullptr;
	mSize = 0;
	mBuffer = VK_NULL_HANDLE;
	mBufferMemory = VK_NULL_HANDLE;
	mBoundMemAlignment = 0;
	mBoundMemPropertyFlags = 0;
	mBoundMemOffset = 0;
}

size_t Buffer::size() const
{
	return mSize;
}

size_t Buffer::offset() const
{
	return mBoundMemOffset;
}

bool Buffer::mappable() const
{
	return (mBoundMemPropertyFlags & VK_MEMORY_PROPERTY_HOST_VISIBLE_BIT) != 0;
}

void Buffer::flush(size_t offset_, size_t size) const
{
	if (mBufferMemory && mappable())
	{
		size_t offset = (mBoundMemOffset + offset_) / mBoundMemAlignment * mBoundMemAlignment;

		VkMappedMemoryRange memRange = {};
		memRange.sType = VK_STRUCTURE_TYPE_MAPPED_MEMORY_RANGE;
		memRange.memory = mBufferMemory;
		memRange.offset = offset;
		memRange.size = std::min<std::size_t>(size ? size : mSize, (mSize - offset));
		memRange.size = (memRange.size + mBoundMemAlignment - 1) / mBoundMemAlignment * mBoundMemAlignment;
		vkFlushMappedMemoryRanges(mDevice, 1, &memRange);
	}
}

void Buffer::invalidate(size_t offset_, size_t size)
{
	if (mBufferMemory && mappable())
	{
		size_t offset = (mBoundMemOffset + offset_) / mBoundMemAlignment * mBoundMemAlignment;

		VkMappedMemoryRange memRange = {};
		memRange.sType = VK_STRUCTURE_TYPE_MAPPED_MEMORY_RANGE;
		memRange.memory = mBufferMemory;
		memRange.offset = offset;
		memRange.size = std::min<std::size_t>(size ? size : mSize, (mSize - offset));
		memRange.size = (memRange.size + mBoundMemAlignment - 1) / mBoundMemAlignment * mBoundMemAlignment;
		vkInvalidateMappedMemoryRanges(mDevice, 1, &memRange);
	}
}

UploadBuffer::UploadBuffer(VkDevice device, size_t size, VkSharingMode sharingMode)
	:Buffer(device, size, Type | VK_BUFFER_USAGE_TRANSFER_SRC_BIT | VK_BUFFER_USAGE_TRANSFER_DST_BIT, sharingMode)
{}

VertexBuffer::VertexBuffer(VkDevice device, size_t size, VkSharingMode sharingMode)
	: Buffer(device, size, Type | VK_BUFFER_USAGE_TRANSFER_SRC_BIT | VK_BUFFER_USAGE_TRANSFER_DST_BIT, sharingMode)
{}


IndexBuffer::IndexBuffer(VkDevice device, size_t size, VkSharingMode sharingMode)
	: Buffer(device, size, Type | VK_BUFFER_USAGE_TRANSFER_SRC_BIT | VK_BUFFER_USAGE_TRANSFER_DST_BIT, sharingMode)
{}

UniformBuffer::UniformBuffer(VkDevice device, size_t size, VkSharingMode sharingMode)
	: Buffer(device, size, Type | VK_BUFFER_USAGE_TRANSFER_SRC_BIT | VK_BUFFER_USAGE_TRANSFER_DST_BIT, sharingMode)
{}

ShaderStorageBuffer::ShaderStorageBuffer(VkDevice device, size_t size, VkSharingMode sharingMode)
	: Buffer(device, size, Type | VK_BUFFER_USAGE_TRANSFER_SRC_BIT | VK_BUFFER_USAGE_TRANSFER_DST_BIT, sharingMode)
{}

IndirectDrawBuffer::IndirectDrawBuffer(VkDevice device, size_t size, VkSharingMode sharingMode)
	: Buffer(device, size, Type | VK_BUFFER_USAGE_TRANSFER_SRC_BIT |  VK_BUFFER_USAGE_TRANSFER_DST_BIT, sharingMode)
{}


};
