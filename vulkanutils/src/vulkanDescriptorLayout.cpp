#include "vulkanDescriptorLayout.h"

#include "vulkanUtils.h"

namespace vulkanutils
{

DescriptorLayout::DescriptorLayout()
	:mDevice(nullptr)
	, mDescriptorBindings{}
	, mDescriptorImageBindings{}
	, mDescriptorSetLayout(nullptr)
{
}

DescriptorLayout::DescriptorLayout(VkDevice device)
	:DescriptorLayout()
{
	mDevice = device;
}

DescriptorLayout::~DescriptorLayout()
{
	teardown();
}

DescriptorLayout::DescriptorLayout(DescriptorLayout&& d) noexcept
	:mDevice(d.mDevice)
	, mDescriptorBindings(std::move(d.mDescriptorBindings))
	, mDescriptorImageBindings(std::move(d.mDescriptorImageBindings))
	, mDescriptorSetLayout(d.mDescriptorSetLayout)
{
	d.mDevice = nullptr;
	d.mDescriptorImageBindings.clear();
	d.mDescriptorSetLayout = nullptr;
}


void DescriptorLayout::addImageDescriptorBindings(const DescriptorImageBinding* descriptorImageBindings, size_t numImageBindings)
{
	mDescriptorImageBindings = std::vector<DescriptorImageBinding>(descriptorImageBindings, descriptorImageBindings + numImageBindings);
}

void DescriptorLayout::teardown()
{
	if (mDevice && mDescriptorSetLayout)
	{
		vkDestroyDescriptorSetLayout(mDevice, mDescriptorSetLayout, nullptr);
	}
}


DescriptorLayout& DescriptorLayout::operator=(DescriptorLayout&& d) noexcept
{
	teardown();
	mDevice = d.mDevice;
	mDescriptorBindings = std::move(d.mDescriptorBindings);
	mDescriptorImageBindings = std::move(d.mDescriptorImageBindings);
	mDescriptorSetLayout = d.mDescriptorSetLayout;
	
	d.mDevice = nullptr;
	d.mDescriptorImageBindings.clear();
	d.mDescriptorSetLayout = nullptr;
	return *this;
}

DescriptorLayout::operator VkDescriptorSetLayout() const
{
	return mDescriptorSetLayout;
}

DescriptorSet DescriptorLayout::createDescriptorSet(VkDescriptorPool pool) const
{
	return DescriptorSet(mDevice, pool, *this);
}

const std::vector<DescriptorImageBinding>& DescriptorLayout::getDescriptorImageBindings() const
{
	return mDescriptorImageBindings;
}

template<typename BufferType>
std::vector<VkDescriptorSetLayoutBinding> DescriptorLayout::build(const std::vector<DescriptorBinding<BufferType>>& bindings)
{
	std::vector<VkDescriptorSetLayoutBinding> layoutBindings;
	for (auto& bufferBinding : bindings)
	{
		VkDescriptorSetLayoutBinding lb = {};
		lb.binding = bufferBinding.binding;
		lb.descriptorType = DescriptorType<BufferType>;
		lb.descriptorCount = bufferBinding.numElements;
		lb.stageFlags = bufferBinding.shaderStages;
		lb.pImmutableSamplers = nullptr;
		layoutBindings.emplace_back(lb);
	}
	return layoutBindings;
}

void DescriptorLayout::build()
{
	// One per uniform object in the shader
	std::vector<VkDescriptorSetLayoutBinding> layoutBindings;

	bool dynamic = false;
	auto add = [&](auto& arg)
	{
		const auto& bl = build(arg);
		for (auto& b : arg)
		{
			if (b.dynamic)
			{
				dynamic = true;
				break;
			}
		}
		for (auto& binding : bl)
		{
			layoutBindings.emplace_back(binding);
		}
	};

	std::apply([&](auto&... args)
	{
		(add(args), ...);
	}, mDescriptorBindings);


	for (auto& imageBinding : mDescriptorImageBindings)
	{
		if (imageBinding.dynamic)
			dynamic = true;
		VkDescriptorSetLayoutBinding lb = {};
		lb.binding = imageBinding.binding;
		lb.descriptorType = VK_DESCRIPTOR_TYPE_COMBINED_IMAGE_SAMPLER;
		lb.descriptorCount = imageBinding.numElements;
		lb.stageFlags = imageBinding.shaderStages;
		lb.pImmutableSamplers = nullptr;
		layoutBindings.emplace_back(lb);
	}

	std::vector<VkDescriptorBindingFlags> bindingFlags(std::size(layoutBindings), VK_DESCRIPTOR_BINDING_PARTIALLY_BOUND_BIT);
	if (dynamic)
		bindingFlags.back() |=  VK_DESCRIPTOR_BINDING_VARIABLE_DESCRIPTOR_COUNT_BIT;

	VkDescriptorSetLayoutBindingFlagsCreateInfo layoutBindingFlagsInfo = {};
	layoutBindingFlagsInfo.sType = VK_STRUCTURE_TYPE_DESCRIPTOR_SET_LAYOUT_BINDING_FLAGS_CREATE_INFO;
	layoutBindingFlagsInfo.bindingCount = uint32_t(bindingFlags.size());
	layoutBindingFlagsInfo.pBindingFlags = bindingFlags.data();

	std::sort(layoutBindings.begin(), layoutBindings.end(), [](VkDescriptorSetLayoutBinding lhs, VkDescriptorSetLayoutBinding rhs)
	{
		return lhs.binding < rhs.binding;
	});

	// layout for set of bindings (all used by the shader(?))
	VkDescriptorSetLayoutCreateInfo layoutInfo = {};
	layoutInfo.pNext = &layoutBindingFlagsInfo;
	layoutInfo.sType = VK_STRUCTURE_TYPE_DESCRIPTOR_SET_LAYOUT_CREATE_INFO;
	layoutInfo.bindingCount = uint32_t(std::size(layoutBindings));
	layoutInfo.pBindings = layoutBindings.data();
	if (VK_SUCCESS != vkCreateDescriptorSetLayout(mDevice, &layoutInfo, nullptr, &mDescriptorSetLayout))
	{
		vulkanutils::logger(Log::Severity::CRITICAL) << "failed to create descriptor set layout!" << std::endl;
	}
}


};
