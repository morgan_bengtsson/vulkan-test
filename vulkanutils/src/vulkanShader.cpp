#include "vulkanShader.h"
#include "vulkanutils.h"

#include <memory>
#include <algorithm>
#include <fstream>
#include <iostream>
#include <filesystem>
#include <string>
#include <regex>


namespace vulkanutils
{

ComputeShader loadComputeShader(VkDevice device, const char* path, const char* name)
{
	ComputeShader shader;
	std::string nameStr(name);
	
	std::vector<std::string> parts;
	while (!nameStr.empty())
	{
		size_t n = nameStr.find(',');
		parts.push_back(nameStr.substr(0, n));
		if (n == std::string::npos)
			nameStr.clear();
		else
			nameStr = nameStr.substr(n + 1);
	}

	std::string computeName;
	std::string commonName;

	for (std::string& part : parts)
	{
		std::filesystem::path name(part);
		if (name.extension() == ".comp")
			computeName = name.stem().string();
		else if (!name.has_extension())
			commonName = name.stem().string();
	}
	if (computeName.empty()) computeName = commonName;

	for (auto& item : std::filesystem::directory_iterator(path))
	{
		if (item.is_regular_file())
		{
			auto& itemPath = item.path();
			std::string fileName = itemPath.filename().string();

			std::string fullPath = itemPath.string();
			if (fileName == computeName + ".comp.spv")
			{
				shader = ComputeShader(device, fullPath.c_str());
				break;
			}
		}
	}
	return shader;
}

ShaderSet loadShaderSet(VkDevice device, const char* path, const char* name)
{
	ShaderSet set;
	std::string nameStr(name);
	
	std::vector<std::string> parts;
	while (!nameStr.empty())
	{
		size_t n = nameStr.find(',');
		parts.push_back(nameStr.substr(0, n));
		if (n == std::string::npos)
			nameStr.clear();
		else
			nameStr = nameStr.substr(n + 1);
	}
	std::string vertName;
	std::string tescName;
	std::string teseName;
	std::string geomName;
	std::string fragName;
	std::string commonName;

	for (std::string& part : parts)
	{
		std::filesystem::path name(part);
		if (name.extension() == ".vert")
			vertName = name.stem().string();
		else if (name.extension() == ".tesc")
			tescName = name.stem().string();
		else if (name.extension() == ".tese")
			teseName = name.stem().string();
		else if (name.extension() == ".geom")
			geomName = name.stem().string();
		else if (name.extension() == ".frag")
			fragName = name.stem().string();
		else if (!name.has_extension())
			commonName = name.stem().string();
	}
	if (vertName.empty()) vertName = commonName;
	if (tescName.empty()) tescName = commonName;
	if (teseName.empty()) teseName = commonName;
	if (geomName.empty()) geomName = commonName;
	if (fragName.empty()) fragName = commonName;

	for (auto& item : std::filesystem::directory_iterator(path))
	{
		if (item.is_regular_file())
		{
			auto& itemPath = item.path();
			std::string fileName = itemPath.filename().string();

			std::string fullPath = itemPath.string();
			if (fileName == vertName + ".vert.spv")
			{
				set.vert = VertexShader(device, fullPath.c_str());
			}
			else if (fileName == tescName + ".tesc.spv")
			{
				set.tesc = TessellationControlShader(device, fullPath.c_str());
			}
			else if (fileName == teseName + ".tese.spv")
			{
				set.tese = TessellationEvaluationShader(device, fullPath.c_str());
			}
			else if (fileName == geomName + ".geom.spv")
			{
				set.geom = GeometryShader(device, fullPath.c_str());
			}
			else if (fileName == fragName + ".frag.spv")
			{
				set.frag = FragmentShader(device, fullPath.c_str());
			}
		}
	}
	return set;
}


Shader::Shader()
	:mDevice(nullptr)
	, mShader(nullptr)
{
}

Shader::Shader(VkDevice device, const char* filename)
	:mDevice(device)
	, mShader(nullptr)
{
	mShader = loadShader(filename);
}

Shader::~Shader()
{
	cleanup();
}

Shader::Shader(Shader&& d) noexcept
	:mDevice(d.mDevice)
	, mShader(d.mShader)
{
	d.mDevice = nullptr;
	d.mShader = nullptr;
}

Shader& Shader::operator=(Shader&& d) noexcept
{
	cleanup();
	mDevice = d.mDevice;
	mShader = d.mShader;

	d.mDevice = nullptr;
	d.mShader = nullptr;
	return *this;
}

void Shader::cleanup()
{
	if (mDevice && mShader)
	{
		vkDestroyShaderModule(mDevice, mShader, nullptr);
	}
}

Shader::operator const VkShaderModule() const
{
	return mShader;
}

VkPipelineShaderStageCreateInfo Shader::getShaderStageInfo(VkShaderStageFlagBits shaderType) const
{
	VkPipelineShaderStageCreateInfo shaderStageInfo = {};
	shaderStageInfo.sType = VK_STRUCTURE_TYPE_PIPELINE_SHADER_STAGE_CREATE_INFO;
	shaderStageInfo.stage = shaderType;
	shaderStageInfo.module = mShader;
	shaderStageInfo.pName = "main";
	return shaderStageInfo;
}


VkShaderModule Shader::loadShader(const uint8_t* buffer, size_t bufferSize)
{
	VkShaderModuleCreateInfo createInfo = {};
	createInfo.sType = VK_STRUCTURE_TYPE_SHADER_MODULE_CREATE_INFO;
	createInfo.codeSize = bufferSize;
	createInfo.pCode = reinterpret_cast<const uint32_t*>(buffer);

	VkShaderModule shaderModule = VK_NULL_HANDLE;
	if (VK_SUCCESS != vkCreateShaderModule(mDevice, &createInfo, nullptr, &shaderModule))
	{
		logger(Log::Severity::ERROR) << "Failed to create shader module" << std::endl;
	}
	return shaderModule;
}

VkShaderModule Shader::loadShader(const char* fileName)
{
	auto fileData = readFile(fileName);
	return loadShader(fileData.data(), fileData.size());
}

std::vector<uint8_t> Shader::readFile(const char* fileName)
{
	std::vector<uint8_t> buffer;
	std::ifstream file(fileName, std::ios::ate | std::ios::binary);
	if (file.is_open())
	{
		size_t fileSize = (size_t) file.tellg();
		file.seekg(0);
		buffer.reserve(fileSize);
		buffer.assign(std::istreambuf_iterator<char>(file),
					  std::istreambuf_iterator<char>());
	}
	return buffer;
}

TessellationControlShader::TessellationControlShader()
	: Shader()
	, mPatchSize(0)
{
}

TessellationControlShader::TessellationControlShader(VkDevice device, const char* filename, size_t patchSize)
	: Shader(device, filename)
	, mPatchSize(patchSize)
{
}

size_t TessellationControlShader::patchSize() const
{
	return mPatchSize;
}


};
