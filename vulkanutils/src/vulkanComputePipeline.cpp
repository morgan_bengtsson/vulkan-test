#include "vulkanComputePipeline.h"
#include "vulkanutils.h"
#include "vulkanContext.h"
#include "vulkanVertexAttributeLayout.h"

#include <algorithm>


namespace vulkanutils
{

vulkanutils::ComputePipeline::ComputePipeline(
	const VulkanContext& context,
	const ComputeShader& shader,
	const std::vector<const ResourceSet*>& sets,
	size_t pushConstantsSize)
	:ComputePipeline()
{
	std::vector<VkDescriptorSetLayout> descLayouts;
	for (const ResourceSet* set : sets)
	{
		if (VkDescriptorSetLayout descLayout = set->getDescriptorLayout())
		{
			descLayouts.push_back(descLayout);
		}
	};
	// Create pipeline
	*this = ComputePipeline(context.device()
		, shader.getShaderStageInfo()
		, descLayouts.data(), descLayouts.size(), pushConstantsSize);
}

ComputePipeline::ComputePipeline()
	: mComputePipeline(VK_NULL_HANDLE)
	, mComputePipelineLayout(VK_NULL_HANDLE)
	, mDevice(VK_NULL_HANDLE)
{
}

ComputePipeline::ComputePipeline(VkDevice device
				   , VkPipelineShaderStageCreateInfo shaderStage
				   , VkDescriptorSetLayout* descriptorLayouts, size_t nDescriptorLayouts
				   , size_t pushConstantsSize)
	: mComputePipeline(VK_NULL_HANDLE)
	, mComputePipelineLayout(VK_NULL_HANDLE)
	, mDevice(device)
{
	VkPipelineLayoutCreateInfo pipelineLayoutInfo = {};
	pipelineLayoutInfo.sType = VK_STRUCTURE_TYPE_PIPELINE_LAYOUT_CREATE_INFO;
	pipelineLayoutInfo.setLayoutCount = uint32_t(nDescriptorLayouts);
	pipelineLayoutInfo.pSetLayouts = descriptorLayouts;

	VkPushConstantRange pushConstants;
	pushConstants.stageFlags = VK_SHADER_STAGE_COMPUTE_BIT;
	pushConstants.offset = 0;
	pushConstants.size = pushConstantsSize;
	if (pushConstantsSize > 0)
	{
		pipelineLayoutInfo.pushConstantRangeCount = 1;
		pipelineLayoutInfo.pPushConstantRanges = &pushConstants;
	}
	else
	{
		pipelineLayoutInfo.pushConstantRangeCount = 0;
		pipelineLayoutInfo.pPushConstantRanges = nullptr;
	}

	if (vkCreatePipelineLayout(mDevice, &pipelineLayoutInfo, nullptr, &mComputePipelineLayout) != VK_SUCCESS)
	{
		logger(Log::Severity::CRITICAL) << "Failed to create pipeline layout" << std::endl;
	}

	VkComputePipelineCreateInfo pipelineInfo = {};
	pipelineInfo.sType = VK_STRUCTURE_TYPE_COMPUTE_PIPELINE_CREATE_INFO;
	pipelineInfo.stage = shaderStage;

	pipelineInfo.layout = mComputePipelineLayout;
	pipelineInfo.basePipelineHandle = VK_NULL_HANDLE;
	pipelineInfo.basePipelineIndex = -1;

	if (vkCreateComputePipelines(mDevice,
								  VK_NULL_HANDLE, 1,
								  &pipelineInfo,
								  nullptr,
								  &mComputePipeline) != VK_SUCCESS)
	{
		logger(Log::Severity::CRITICAL) << "Failed to setup graphics pipeline" << std::endl;
	}
}

ComputePipeline::~ComputePipeline()
{
	cleanup();
}

ComputePipeline::ComputePipeline(ComputePipeline&& s)
	:mComputePipeline(s.mComputePipeline)
	, mComputePipelineLayout(s.mComputePipelineLayout)
	, mDevice(s.mDevice)
{
	s.mComputePipeline = VK_NULL_HANDLE;
	s.mComputePipelineLayout = VK_NULL_HANDLE;
}

ComputePipeline& ComputePipeline::operator=(ComputePipeline&& s)
{
	cleanup();
	mComputePipeline = s.mComputePipeline;
	mComputePipelineLayout = s.mComputePipelineLayout;
	mDevice = s.mDevice;

	s.mComputePipeline = VK_NULL_HANDLE;
	s.mComputePipelineLayout = VK_NULL_HANDLE;
	s.mDevice = VK_NULL_HANDLE;
	return *this;
}

void ComputePipeline::cleanup()
{
	if (mDevice)
	{
		if (mComputePipelineLayout)
		{
			logger(Log::Severity::INFO) << "Tear down vulkan pipeline layout" << std::endl;
			vkDestroyPipelineLayout(mDevice, mComputePipelineLayout, nullptr);
		}
		if (mComputePipeline)
		{
			logger(Log::Severity::INFO) << "Tear down vulkan pipeline" << std::endl;
			vkDestroyPipeline(mDevice, mComputePipeline, nullptr);
		}
	}
}

ComputePipeline::operator VkPipeline() const
{
	return mComputePipeline;
}


VkPipelineLayout ComputePipeline::getLayout() const
{
	return mComputePipelineLayout;
}

void ComputePipeline::bind(VkCommandBuffer commandBuffer) const
{
	vkCmdBindPipeline(commandBuffer, VK_PIPELINE_BIND_POINT_COMPUTE, mComputePipeline);
}

};
