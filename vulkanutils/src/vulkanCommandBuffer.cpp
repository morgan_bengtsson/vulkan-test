#include "vulkanCommandBuffer.h"
#include "vulkanUtils.h"


namespace vulkanutils
{

CommandBufferRecorder::CommandBufferRecorder(VkCommandBuffer cb, VkCommandBufferUsageFlagBits usage)
	: mBuffer(cb)
{
	VkCommandBufferBeginInfo beginInfo = {};
	beginInfo.sType = VK_STRUCTURE_TYPE_COMMAND_BUFFER_BEGIN_INFO;
	beginInfo.flags = usage;
	beginInfo.pInheritanceInfo = nullptr;

	if (VK_SUCCESS != vkBeginCommandBuffer(mBuffer, &beginInfo))
	{
		vulkanutils::logger(Log::Severity::CRITICAL) << "failed to begin recording command buffer!" << std::endl;
	}
}

CommandBufferRecorder::~CommandBufferRecorder()
{
	if (VK_SUCCESS != vkEndCommandBuffer(mBuffer))
	{
		vulkanutils::logger(Log::Severity::CRITICAL) << "failed to record command buffer!" << std::endl;
	}
}

VkCommandBuffer CommandBufferRecorder::getCommandBuffer() const
{
	return mBuffer;
}


CommandBuffers::CommandBuffers()
	: mBuffers()
	, mDevice(VK_NULL_HANDLE)
	, mCommandPool(VK_NULL_HANDLE)
{
}

CommandBuffers::CommandBuffers(VkDevice device, VkCommandPool commandPool, size_t nBuffers)
	: mBuffers(nBuffers)
	, mDevice(device)
	, mCommandPool(commandPool)
{
	VkCommandBufferAllocateInfo allocInfo = {};
	allocInfo.sType = VK_STRUCTURE_TYPE_COMMAND_BUFFER_ALLOCATE_INFO;
	allocInfo.commandPool = mCommandPool;
	allocInfo.level = VK_COMMAND_BUFFER_LEVEL_PRIMARY;
	allocInfo.commandBufferCount = uint32_t(mBuffers.size());

	if (VK_SUCCESS != vkAllocateCommandBuffers(mDevice, &allocInfo, mBuffers.data()))
	{
		logger(Log::Severity::CRITICAL) << "failed to allocate command buffers!" << std::endl;
		mBuffers.clear();
	}
}

CommandBuffers::CommandBuffers(CommandBuffers&& cb)
	: mBuffers(std::move(cb.mBuffers))
	, mDevice(cb.mDevice)
	, mCommandPool(cb.mCommandPool)
{
	cb.mBuffers.clear();
	cb.mDevice = VK_NULL_HANDLE;
	cb.mCommandPool = VK_NULL_HANDLE;
}

CommandBuffers& CommandBuffers::operator=(CommandBuffers&& cb)
{
	mBuffers = std::move(cb.mBuffers);
	mDevice = cb.mDevice;
	cb.mDevice = VK_NULL_HANDLE;
	mCommandPool = cb.mCommandPool;
	cb.mCommandPool = VK_NULL_HANDLE;
	return *this;
}

CommandBuffers::~CommandBuffers()
{
	if (mDevice && mCommandPool && !mBuffers.empty())
	{
		vkFreeCommandBuffers(mDevice, mCommandPool, uint32_t(mBuffers.size()), mBuffers.data());
	}
}

size_t CommandBuffers::size() const
{
	return mBuffers.size();
}

const VkCommandBuffer& CommandBuffers::operator[](size_t i) const
{
	return mBuffers[i];
}

};