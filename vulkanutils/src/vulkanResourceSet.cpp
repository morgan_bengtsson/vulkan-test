#include "vulkanResourceSet.h"

namespace vulkanutils
{

ResourceSet::ResourceSet(Device* device, DescriptorSetSlot descSetbp, VertexInputSlot vertInputBp)
	:mDevice(device)
	, mDescriptorLayout(*device)
	, mVertexAttributeLayout()
	, mDescriptorSetBindpoint(descSetbp)
	, mVertexInputBindpoint(vertInputBp)
{
}

ResourceSet::~ResourceSet()
{}

Device* ResourceSet::getDevice() const
{
	return mDevice;
}

void ResourceSet::setDescriptorLayout(vulkanutils::DescriptorLayout&& descLayout)
{
	mDescriptorLayout = std::move(descLayout);
	mDescriptorLayout.build();
}

void ResourceSet::setVertexAttributeLayout(vulkanutils::VertexAttributeLayout&& vertLayout)
{
	mVertexAttributeLayout = std::move(vertLayout);
}


DescriptorSetSlot ResourceSet::getDescriptorSetBindpoint() const
{
	return mDescriptorSetBindpoint;
}

VertexInputSlot ResourceSet::getVertexInputBindpoint() const
{
	return mVertexInputBindpoint;
}

DescriptorSet ResourceSet::createDescriptorSet(const DescriptorPool& descPool, size_t variableSetSize) const
{
	if (mDescriptorLayout)
	{
		return DescriptorSet(*getDevice(), descPool, mDescriptorLayout, variableSetSize);
	}
	else
	{
		return DescriptorSet();
	}
}

const DescriptorLayout& ResourceSet::getDescriptorLayout() const
{
	return mDescriptorLayout;
}

DescriptorLayout& ResourceSet::getDescriptorLayout()
{
	return mDescriptorLayout;
}


const VertexAttributeLayout& ResourceSet::getAttributeLayout() const
{
	return mVertexAttributeLayout;
}

};