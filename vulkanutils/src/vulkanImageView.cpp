#include "vulkanImageView.h"
#include "vulkanUtils.h"

namespace vulkanutils
{

ImageView::ImageView()
	: mDevice(VK_NULL_HANDLE)
	, mImageView(VK_NULL_HANDLE)
{}

ImageView::ImageView(VkDevice device, const Image& image, VkImageAspectFlags flags)
	:ImageView(device, image, image.format(), flags)
{}

ImageView::ImageView(VkDevice device, const DepthImage& image)
	: ImageView(device, image, image.format(), VK_IMAGE_ASPECT_DEPTH_BIT)
{}

ImageView::ImageView(VkDevice device, const TextureImage& image)
	: ImageView(device, image, image.format(), VK_IMAGE_ASPECT_COLOR_BIT)
{}

ImageView::ImageView(VkDevice device, VkImage image, VkFormat format, VkImageAspectFlags flags)
	: mDevice(device)
	, mImageView(VK_NULL_HANDLE)
{
	VkImageViewCreateInfo createInfo = {};
	createInfo.sType = VK_STRUCTURE_TYPE_IMAGE_VIEW_CREATE_INFO;
	createInfo.image = image;
	createInfo.viewType = VK_IMAGE_VIEW_TYPE_2D;
	createInfo.format = format;
	createInfo.components.r = VK_COMPONENT_SWIZZLE_IDENTITY;
	createInfo.components.g = VK_COMPONENT_SWIZZLE_IDENTITY;
	createInfo.components.b = VK_COMPONENT_SWIZZLE_IDENTITY;
	createInfo.components.a = VK_COMPONENT_SWIZZLE_IDENTITY;
	createInfo.subresourceRange.aspectMask = flags;
	createInfo.subresourceRange.baseMipLevel = 0;
	createInfo.subresourceRange.levelCount = VK_REMAINING_MIP_LEVELS;
	createInfo.subresourceRange.baseArrayLayer = 0;
	createInfo.subresourceRange.layerCount = 1;

	if (VK_SUCCESS != vkCreateImageView(device, &createInfo, nullptr, &mImageView))
	{
		logger(Log::Severity::ERROR) << "Failed to create image view" << std::endl;
	}
}

ImageView::~ImageView()
{
	teardown();
}

void ImageView::teardown()
{
	if (mDevice && mImageView)
	{
		logger(Log::Severity::INFO) << "Tear down vulkan image view" << std::endl;
		vkDestroyImageView(mDevice, mImageView, nullptr);
	}
}

ImageView::ImageView(ImageView&& i) noexcept
	: mDevice(i.mDevice)
	, mImageView(i.mImageView)
{
	i.mDevice = VK_NULL_HANDLE;
	i.mImageView = VK_NULL_HANDLE;
}

ImageView& ImageView::operator=(ImageView&& i) noexcept
{
	teardown();
	mDevice = i.mDevice;
	mImageView = i.mImageView;
	i.mDevice = VK_NULL_HANDLE;
	i.mImageView = VK_NULL_HANDLE;
	return *this;
}

ImageView::operator VkImageView()
{
	return mImageView;
}

ImageView::operator VkImageView() const
{
	return mImageView;
}

};
