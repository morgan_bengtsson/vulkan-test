#include "vulkanInstance.h"

#include "vulkanutils.h"
#include "vulkanwinutils.h"

namespace vulkanutils
{

Instance::Instance()
	:mInstance(VK_NULL_HANDLE)
	, mDebugHandle(VK_NULL_HANDLE)
{
}

Instance::Instance(const char* appName
				   , const char** extentions, size_t nExtensions
				   , const char** validationLayers, size_t nValidationLayers)
	: Instance()
{
	VkApplicationInfo appInfo = {};
	appInfo.sType = VK_STRUCTURE_TYPE_APPLICATION_INFO;
	appInfo.pApplicationName = appName;
	appInfo.applicationVersion = VK_MAKE_VERSION(1, 0, 0);
	appInfo.pEngineName = "No Engine";
	appInfo.engineVersion = VK_MAKE_VERSION(1, 0, 0);
	appInfo.apiVersion = VK_API_VERSION_1_2;

	VkInstanceCreateInfo createInfo = {};
	createInfo.sType = VK_STRUCTURE_TYPE_INSTANCE_CREATE_INFO;
	createInfo.pApplicationInfo = &appInfo;
	createInfo.enabledExtensionCount = (uint32_t) nExtensions;
	createInfo.ppEnabledExtensionNames = extentions;
	createInfo.enabledLayerCount = (uint32_t) nValidationLayers;
	createInfo.ppEnabledLayerNames = validationLayers;
	
	VkResult err = vkCreateInstance(&createInfo, nullptr, &mInstance);
	if (err)
	{
		logger(Log::Severity::CRITICAL) << "vkCreateInstance failed with error "
										<< int(err) << std::endl;
	}
	if (!err)
	{
		VkDebugUtilsMessengerCreateInfoEXT debugUtilsMessengerCreateInfo = {};
		debugUtilsMessengerCreateInfo.sType = VK_STRUCTURE_TYPE_DEBUG_UTILS_MESSENGER_CREATE_INFO_EXT;
		debugUtilsMessengerCreateInfo.messageSeverity =
				VK_DEBUG_UTILS_MESSAGE_SEVERITY_VERBOSE_BIT_EXT
				| VK_DEBUG_UTILS_MESSAGE_SEVERITY_WARNING_BIT_EXT
				| VK_DEBUG_UTILS_MESSAGE_SEVERITY_ERROR_BIT_EXT;
		debugUtilsMessengerCreateInfo.messageType =
				VK_DEBUG_UTILS_MESSAGE_TYPE_GENERAL_BIT_EXT
				| VK_DEBUG_UTILS_MESSAGE_TYPE_VALIDATION_BIT_EXT
				| VK_DEBUG_UTILS_MESSAGE_TYPE_PERFORMANCE_BIT_EXT;
		debugUtilsMessengerCreateInfo.pfnUserCallback = debugCallback;

		auto func = (PFN_vkCreateDebugUtilsMessengerEXT) vkGetInstanceProcAddr(mInstance, "vkCreateDebugUtilsMessengerEXT");
		if (func == nullptr)
		{
			err = VK_ERROR_EXTENSION_NOT_PRESENT;
		}
		if (!err)
		{
			err = func(mInstance, &debugUtilsMessengerCreateInfo, nullptr, &mDebugHandle);
		}
		if (err)
		{
			logger(Log::Severity::WARNING) << "Failed to setup vulkan debugging. err: " << err << std::endl;
		}
	}
	if (!err)
	{
		logger(Log::Severity::INFO) << "Created vulkan instance" << std::endl;
	}
}

Instance::Instance(Instance&& i)
{
	*this = std::move(i);
}

Instance& Instance::operator=(Instance&& i)
{
	mInstance = i.mInstance;
	mDebugHandle = i.mDebugHandle;
	i.mInstance = VK_NULL_HANDLE;
	i.mDebugHandle = VK_NULL_HANDLE;
	return *this;
}


Instance::~Instance()
{
	if (mInstance != VK_NULL_HANDLE)
	{
		logger(Log::Severity::INFO) << "Tear down vulkan instance";
		if (mDebugHandle != VK_NULL_HANDLE)
		{
			vulkanutils::tearDownDebugging(mInstance, mDebugHandle);
		}
		vkDestroyInstance(mInstance, nullptr);
	}
}

Instance::operator VkInstance()
{
	return mInstance;
}

VkDebugUtilsMessengerEXT Instance::getDebugHandle() const
{
	return mDebugHandle;
}


Instance InstanceBuilder::operator()()
{
	return Instance(input.applicationName
					, input.extentions
					, std::size(input.extentions)
					, input.validationLayers
					, std::size(input.validationLayers));
}


};
