#include "outputdebugstringbuf.h"

#include <string>
#include <sstream>

#include <Windows.h>

#include <iostream>
#include <vector>

// ----------------------------------------------------------------------------
//
// ----------------------------------------------------------------------------
template<typename TChar, typename TTraits = std::char_traits<TChar>>
class OutputDebugStringBuf : public std::basic_stringbuf<TChar, TTraits>
{
public:
	typedef std::basic_stringbuf<TChar, TTraits> Base;
	explicit OutputDebugStringBuf() : mBuffer(256)
	{
		std::basic_stringbuf<TChar, TTraits>::setg(nullptr, nullptr, nullptr);
		std::basic_stringbuf<TChar, TTraits>::setp(mBuffer.data(), mBuffer.data(), mBuffer.data() + mBuffer.size());
	}

	static_assert(std::is_same<TChar, char>::value || std::is_same<TChar, wchar_t>::value, "OutputDebugStringBuf only supports char and wchar_t types");

	int sync() override
	{
		try
		{
			MessageOutputer<TChar, TTraits>()(std::basic_stringbuf<TChar, TTraits>::pbase(), std::basic_stringbuf<TChar, TTraits>::pptr());
			std::basic_stringbuf<TChar, TTraits>::setp(mBuffer.data(), mBuffer.data(), mBuffer.data() + mBuffer.size());
			return 0;
		}
		catch (...)
		{
			return -1;
		}
	}
	typename Base::int_type overflow(typename Base::int_type c = TTraits::eof()) override
	{
		auto syncRet = sync();
		if (c != TTraits::eof()) {
			mBuffer[0] = c;
			std::basic_stringbuf<TChar, TTraits>::setp(mBuffer.data(), mBuffer.data() + 1, mBuffer.data() + mBuffer.size());
		}
		return syncRet == -1 ? TTraits::eof() : 0;
	}
private:
	std::vector<TChar> mBuffer;

	template<typename TChar, typename TTraits>
	struct MessageOutputer;

	template<>
	struct MessageOutputer<char, std::char_traits<char>>
	{
		template<typename TIterator>
		void operator()(TIterator begin, TIterator end) const
		{
			std::string s(begin, end);
			OutputDebugStringA(s.c_str());
		}
	};

	template<>
	struct MessageOutputer<wchar_t, std::char_traits<wchar_t>>
	{
		template<typename TIterator>
		void operator()(TIterator begin, TIterator end) const
		{
			std::wstring s(begin, end);
			OutputDebugStringW(s.c_str());
		}
	};
};


void setupWindowsDebugOutput()
{
	static OutputDebugStringBuf<char> outputDebugBufChar;
	static OutputDebugStringBuf<wchar_t> outputDebugBufWChar;

	std::cout.rdbuf(&outputDebugBufChar);
	std::cerr.rdbuf(&outputDebugBufChar);
	std::clog.rdbuf(&outputDebugBufChar);

	std::wcout.rdbuf(&outputDebugBufWChar);
	std::wcerr.rdbuf(&outputDebugBufWChar);
	std::wclog.rdbuf(&outputDebugBufWChar);
}
