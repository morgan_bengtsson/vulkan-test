#include "vulkanRenderPass.h"

#include "vulkanutils.h"

namespace vulkanutils
{

RenderPass::RenderPass()
	:mRenderPass(VK_NULL_HANDLE)
	, mDevice(VK_NULL_HANDLE)
{

}

RenderPass::RenderPass(VkDevice device, size_t width, size_t height, std::vector<Attachement> attachments, std::vector<RenderSubPass> subPasses)
	:mRenderPass(VK_NULL_HANDLE)
	, mDevice(device)
	, mWidth(width)
	, mHeight(height)
{
	std::vector<VkAttachmentDescription> vkAttachements;
	for (size_t i = 0; i < attachments.size(); i++)
	{
		Attachement& attachmentDescr = attachments[i];
		bool isDepth = attachmentDescr.format == VK_FORMAT_D32_SFLOAT;

		VkAttachmentDescription attachment = {};
		attachment.format = attachmentDescr.format;
		attachment.samples = VK_SAMPLE_COUNT_1_BIT;
		attachment.loadOp = VK_ATTACHMENT_LOAD_OP_CLEAR;
		/*if (isDepth)
			attachment.storeOp = VK_ATTACHMENT_STORE_OP_DONT_CARE;
		else*/
		attachment.storeOp = VK_ATTACHMENT_STORE_OP_STORE;
		attachment.stencilLoadOp = VK_ATTACHMENT_LOAD_OP_DONT_CARE;
		attachment.stencilStoreOp = VK_ATTACHMENT_STORE_OP_DONT_CARE;
		attachment.initialLayout = VK_IMAGE_LAYOUT_UNDEFINED;
		if (isDepth)
			attachment.finalLayout = VK_IMAGE_LAYOUT_DEPTH_STENCIL_READ_ONLY_OPTIMAL;
		else if (attachmentDescr.presentation)
			attachment.finalLayout = VK_IMAGE_LAYOUT_PRESENT_SRC_KHR;
		else
			attachment.finalLayout = VK_IMAGE_LAYOUT_COLOR_ATTACHMENT_OPTIMAL;
		vkAttachements.push_back(attachment);
	}

	std::vector<VkSubpassDescription> vkSubPasses;
	std::vector<VkSubpassDependency> vkDependencies;
	struct AttachmentReferences
	{
		std::optional<VkAttachmentReference> depthAttachmentRef;
		std::vector<VkAttachmentReference> colorAttachmentRefs;
	};
	std::vector<AttachmentReferences> attachmentReferences;

	vkSubPasses.resize(subPasses.size());
	attachmentReferences.resize(subPasses.size());
	vkDependencies.resize(subPasses.size());
	for (size_t i = 0; i < subPasses.size(); i++)
	{
		RenderSubPass& subPass = subPasses[i];
		AttachmentReferences& attachmentRefs = attachmentReferences[i];

		attachmentRefs.colorAttachmentRefs.reserve(subPass.attachments.size());
		for (size_t j = 0; j < subPass.attachments.size(); j++)
		{
			uint32_t attachmentIdx = subPass.attachments[j];
			const VkAttachmentDescription& vkAttDescr = vkAttachements[attachmentIdx];
			bool isDepth = vkAttDescr.format == VK_FORMAT_D32_SFLOAT;
			if (isDepth)
			{
				attachmentRefs.depthAttachmentRef = VkAttachmentReference{ attachmentIdx, VK_IMAGE_LAYOUT_DEPTH_STENCIL_ATTACHMENT_OPTIMAL };
			}
			else
			{
				attachmentRefs.colorAttachmentRefs.emplace_back(VkAttachmentReference{ attachmentIdx, VK_IMAGE_LAYOUT_COLOR_ATTACHMENT_OPTIMAL });
			}
		}
		
		VkSubpassDescription& subpass = vkSubPasses[i];
		subPass = {};
		subpass.pipelineBindPoint = VK_PIPELINE_BIND_POINT_GRAPHICS;
		subpass.colorAttachmentCount = attachmentRefs.colorAttachmentRefs.size();
		subpass.pColorAttachments = attachmentRefs.colorAttachmentRefs.data();
		subpass.pDepthStencilAttachment = attachmentRefs.depthAttachmentRef ? &attachmentRefs.depthAttachmentRef.value() : nullptr;
		
		VkSubpassDependency& dependency = vkDependencies[i];
		dependency = {};
		if (i == 0)
			dependency.srcSubpass = VK_SUBPASS_EXTERNAL;
		else
			dependency.srcSubpass = i - 1;
		dependency.dstSubpass = i;
		dependency.srcStageMask = VK_PIPELINE_STAGE_COLOR_ATTACHMENT_OUTPUT_BIT;
		dependency.srcAccessMask = 0;
		dependency.dstStageMask = VK_PIPELINE_STAGE_COLOR_ATTACHMENT_OUTPUT_BIT;
		dependency.dstAccessMask = VK_ACCESS_COLOR_ATTACHMENT_READ_BIT | VK_ACCESS_COLOR_ATTACHMENT_WRITE_BIT;
	}

	VkRenderPassCreateInfo renderPassInfo = {};
	renderPassInfo.sType = VK_STRUCTURE_TYPE_RENDER_PASS_CREATE_INFO;
	renderPassInfo.attachmentCount = (uint32_t) vkAttachements.size();
	renderPassInfo.pAttachments = vkAttachements.data();
	renderPassInfo.subpassCount = vkSubPasses.size();
	renderPassInfo.pSubpasses = vkSubPasses.data();
	renderPassInfo.dependencyCount = vkDependencies.size();
	renderPassInfo.pDependencies = vkDependencies.data();

	if (VK_SUCCESS != vkCreateRenderPass(mDevice, &renderPassInfo, nullptr, &mRenderPass))
	{
		mRenderPass = VK_NULL_HANDLE;
		logger(Log::Severity::CRITICAL) << "Unable to create render pass" << std::endl;
	}
	else
	{
		logger(Log::Severity::INFO) << "Created vulkan render pass" << std::endl;
	}
}

RenderPass::~RenderPass()
{
	teardown();
}

RenderPass::RenderPass(RenderPass&& rp)
	:mRenderPass(rp.mRenderPass)
	, mDevice(rp.mDevice)
	, mWidth(rp.mWidth)
	, mHeight(rp.mHeight)
{
	rp.mDevice = VK_NULL_HANDLE;
	rp.mRenderPass = VK_NULL_HANDLE;
}

RenderPass& RenderPass::operator=(RenderPass&& rp)
{
	teardown();
	std::swap(mRenderPass, rp.mRenderPass);
	std::swap(mDevice, rp.mDevice);
	std::swap(mWidth, rp.mWidth);
	std::swap(mHeight, rp.mHeight);
	return *this;
}

void RenderPass::teardown()
{
	if (mDevice && mRenderPass)
	{
		logger(Log::Severity::INFO) << "Tear down vulkan render pass" << std::endl;
		vkDestroyRenderPass(mDevice, mRenderPass, nullptr);
	}
	mDevice = VK_NULL_HANDLE;
	mRenderPass = VK_NULL_HANDLE;
}

RenderPass::operator VkRenderPass() const
{
	return mRenderPass;
}

VkViewport RenderPass::viewport() const
{
	VkViewport viewport = {};
	viewport.x = 0.0f;
	viewport.y = 0.0f;
	viewport.width = float(mWidth);
	viewport.height = float(mHeight);
	viewport.minDepth = 0.0f;
	viewport.maxDepth = 1.0f;
	return viewport;
}

VkRect2D RenderPass::scissor() const
{
	VkRect2D scissor = {};
	scissor.offset = { 0, 0 };
	scissor.extent.width = uint32_t(mWidth);
	scissor.extent.height = uint32_t(mHeight);
	return scissor;
}

size_t RenderPass::width() const
{
	return mWidth;
}

size_t RenderPass::height() const
{
	return mHeight;
}


};
