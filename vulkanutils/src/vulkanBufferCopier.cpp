#include "vulkanBufferCopier.h"

#include "vulkanBarrier.h"

#include <tuple>
#include <map>
#include <algorithm>

namespace vulkanutils
{

BufferCopier::BufferCopier(VkCommandBuffer commandBuffer, size_t sizeHint)
	:mCommandBuffer(commandBuffer)
	, mPendingBufferCopy()
	, mPendingBufferToImageCopy()
{
	mPendingBufferCopy.reserve(sizeHint);
}


void BufferCopier::queue(
		const vulkanutils::Buffer& src,
		const vulkanutils::MemRange& srcRange,
		vulkanutils::Buffer& dst,
		const vulkanutils::MemRange& dstRange)
{
	if (srcRange.size > 0 && dstRange.size > 0)
		mPendingBufferCopy.emplace_back(PendingBufferCopy{ &src, &dst, srcRange, dstRange });
}


void BufferCopier::queue(
		const vulkanutils::Buffer& src,
		const vulkanutils::MemRange& srcRange,
		vulkanutils::Image& dst)
{
	if (srcRange.size > 0)
		mPendingBufferToImageCopy.emplace_back(PendingBufferToImageCopy{ src, dst, srcRange });
}

void BufferCopier::queue(
		vulkanutils::Image& src,
		vulkanutils::Buffer& dst,
		const vulkanutils::MemRange& dstRange)
{
	if (dstRange.size > 0)
		mPendingImageToBufferCopy.emplace_back(PendingImageToBufferCopy{ src, dst, dstRange });
}

void BufferCopier::execute()
{
	doBufferCopies();
	mPendingBufferCopy.clear();
	mPendingBufferToImageCopy.clear();
}

void BufferCopier::doBufferCopies()
{
	auto sameBufferCopies = [](const PendingBufferCopy& lhs, const PendingBufferCopy& rhs)
	{
		return lhs.src == rhs.src && lhs.dst == rhs.dst;
	};
	auto neighbouringCopies = [sameBufferCopies](const PendingBufferCopy& lhs, const PendingBufferCopy& rhs)
	{
		size_t lhsSize = std::min<size_t>(lhs.srcRange.size, lhs.dstRange.size);
		//size_t rhsSize = std::min<size_t>(rhs.srcRange.size, rhs.dstRange.size);
		return sameBufferCopies(lhs, rhs) &&  
			lhs.srcRange.offset + lhsSize == rhs.srcRange.offset &&
			lhs.dstRange.offset + lhsSize == rhs.dstRange.offset;
	};
	auto doBufferCopies = [&](const std::vector<PendingBufferCopy>& bufferCopies)
	{
		auto& srcBuffer = bufferCopies[0].src;
		auto& dstBuffer = bufferCopies[0].dst;
		std::vector<VkBufferCopy> copyRegions(bufferCopies.size());
		for (size_t j = 0; j < copyRegions.size(); j++)
		{
			VkBufferCopy& c = copyRegions[j];
			c.srcOffset = bufferCopies[j].srcRange.offset;
			c.dstOffset = bufferCopies[j].dstRange.offset;
			c.size = std::min(bufferCopies[j].srcRange.size, bufferCopies[j].dstRange.size);
			//if (srcBuffer->mappable()) srcBuffer->flush(c.srcOffset, c.size);
		}
		vkCmdCopyBuffer(
			mCommandBuffer,
			*srcBuffer, *dstBuffer,
			(uint32_t)copyRegions.size(), copyRegions.data());
		for (size_t j = 0; j < copyRegions.size(); j++)
		{
			VkBufferCopy& c = copyRegions[j];
			c.srcOffset = bufferCopies[j].srcRange.offset;
			c.dstOffset = bufferCopies[j].dstRange.offset;
			c.size = std::min(bufferCopies[j].srcRange.size, bufferCopies[j].dstRange.size);
			//if (dstBuffer->mappable()) dstBuffer->invalidate(c.srcOffset, c.size);
		}
	};

	std::sort(mPendingBufferCopy.begin(), mPendingBufferCopy.end(), [](const PendingBufferCopy& lhs, const PendingBufferCopy& rhs)
	{
		return std::make_tuple(lhs.src, lhs.dst, lhs.srcRange.offset, lhs.dstRange.offset) < std::make_tuple(rhs.src, rhs.dst, rhs.srcRange.offset, rhs.dstRange.offset);
	});

	std::vector<PendingBufferCopy> compactedCopies;
	compactedCopies.reserve(mPendingBufferCopy.size());
	if (!mPendingBufferCopy.empty()) compactedCopies.push_back(mPendingBufferCopy[0]);
	for (size_t i = 1; i < mPendingBufferCopy.size(); i++)
	{
		if (neighbouringCopies(compactedCopies.back(), mPendingBufferCopy[i]))
		{
			compactedCopies.back().srcRange.size += mPendingBufferCopy[i].srcRange.size;
			compactedCopies.back().dstRange.size += mPendingBufferCopy[i].dstRange.size;
		}
		else if (sameBufferCopies(compactedCopies.back(), mPendingBufferCopy[i])) compactedCopies.push_back(mPendingBufferCopy[i]);
		else
		{
			doBufferCopies(compactedCopies);
			compactedCopies.clear();
			compactedCopies.push_back(mPendingBufferCopy[i]);
		}
	}
	if (!compactedCopies.empty()) doBufferCopies(compactedCopies);


	// Buffer to image
	{
		std::map<std::tuple<const Buffer*, Image*>, std::vector<vulkanutils::MemRange> > rangeSets;
		for (auto& bufferToImageCopy : mPendingBufferToImageCopy)
		{
			rangeSets[{&bufferToImageCopy.src, &bufferToImageCopy.dst}].push_back(bufferToImageCopy.srcRange);
		}
		for (auto& it : rangeSets)
		{
			auto& ranges = it.second;
			auto& buffer = std::get<0>(it.first);
			auto& image = std::get<1>(it.first);
			{
				ImageBarrier imageBarrier(mCommandBuffer);
				imageBarrier.doBeforeCopyTo().addImage(*image, VK_IMAGE_LAYOUT_UNDEFINED, VK_IMAGE_LAYOUT_TRANSFER_DST_OPTIMAL);
			}

			VkImageAspectFlags aspectMask = (image->format() == VK_FORMAT_D32_SFLOAT)
					?  VK_IMAGE_ASPECT_DEPTH_BIT : VK_IMAGE_ASPECT_COLOR_BIT;

			std::vector<VkBufferImageCopy> copyRegions(ranges.size());
			for (size_t i = 0; i < copyRegions.size(); i++)
			{
				buffer->flush(ranges[i].offset, ranges[i].size);

				VkBufferImageCopy& c = copyRegions[i];
				c.bufferOffset = ranges[i].offset;
				c.bufferRowLength = 0;
				c.bufferImageHeight = 0;
				c.imageSubresource.aspectMask = aspectMask;
				c.imageSubresource.mipLevel = 0;
				c.imageSubresource.baseArrayLayer = 0;
				c.imageSubresource.layerCount = 1;
				c.imageOffset = { 0, 0, 0 };
				c.imageExtent =
				{
					uint32_t(image->width()),
					uint32_t(image->height()),
					1
				};
				vkCmdCopyBufferToImage(mCommandBuffer,
					*buffer,
					*image,
					VK_IMAGE_LAYOUT_TRANSFER_DST_OPTIMAL,
					1, &c);
			}
			if (image->mipMapLevels() > 1)
				image->createMipMap(mCommandBuffer);
		}
	}


	// Image to buffer
	{
		std::map<std::tuple<Image*, const Buffer*>, std::vector<vulkanutils::MemRange> > rangeSets;
		for (auto& imageToBufferCopy : mPendingImageToBufferCopy)
		{
			rangeSets[{&imageToBufferCopy.src, &imageToBufferCopy.dst}].push_back(imageToBufferCopy.dstRange);
		}
		for (auto& it : rangeSets)
		{
			auto& ranges = it.second;
			auto& image = std::get<0>(it.first);
			auto& buffer = std::get<1>(it.first);

			VkImageAspectFlags aspectMask = (image->format() == VK_FORMAT_D32_SFLOAT)
					?  VK_IMAGE_ASPECT_DEPTH_BIT : VK_IMAGE_ASPECT_COLOR_BIT;

			std::vector<VkBufferImageCopy> copyRegions(ranges.size());
			for (size_t i = 0; i < copyRegions.size(); i++)
			{
				buffer->flush(ranges[i].offset, ranges[i].size);

				VkBufferImageCopy& c = copyRegions[i];
				c.bufferOffset = ranges[i].offset;
				c.bufferRowLength = 0;
				c.bufferImageHeight = 0;
				c.imageSubresource.aspectMask = aspectMask;
				c.imageSubresource.mipLevel = 0;
				c.imageSubresource.baseArrayLayer = 0;
				c.imageSubresource.layerCount = 1;
				c.imageOffset = { 0, 0, 0 };
				c.imageExtent =
				{
					uint32_t(image->width()),
					uint32_t(image->height()),
					1
				};
				vkCmdCopyImageToBuffer(mCommandBuffer,
					*image,
					VK_IMAGE_LAYOUT_TRANSFER_SRC_OPTIMAL,
					*buffer,
					1, &c);
			}
		}
	}

}

BufferCopier::~BufferCopier()
{
	doBufferCopies();
}

};
