#include "vulkanutils.h"
#include "log.h"


#include <vulkan/vulkan.h>

#include <map>

namespace vulkanutils
{

static const char* vulkanValidationLayerNames[] =
{
	"VK_LAYER_LUNARG_standard_validation"
};

static const char* deviceExtensions[] = {
	VK_KHR_SWAPCHAIN_EXTENSION_NAME,
	VK_KHR_RELAXED_BLOCK_LAYOUT_EXTENSION_NAME
};

Log logger("vulkanutils");


VKAPI_ATTR VkBool32 VKAPI_CALL debugCallback(VkDebugUtilsMessageSeverityFlagBitsEXT messageSeverity
													, VkDebugUtilsMessageTypeFlagsEXT messageType
													, const VkDebugUtilsMessengerCallbackDataEXT* callbackData
													, void* userData)
{
	Log::Severity sev = Log::Severity::INFO;
	VkBool32 ret = VK_FALSE;
	switch (messageSeverity)
	{
	case VK_DEBUG_UTILS_MESSAGE_SEVERITY_VERBOSE_BIT_EXT:
		ret = VK_TRUE;
		sev = Log::Severity::DEBUG;
		break;
	case VK_DEBUG_UTILS_MESSAGE_SEVERITY_INFO_BIT_EXT:
		ret = VK_TRUE;
		sev = Log::Severity::INFO;
		break;
	case VK_DEBUG_UTILS_MESSAGE_SEVERITY_WARNING_BIT_EXT:
		ret = VK_FALSE;
		sev = Log::Severity::WARNING;
		break;
	case VK_DEBUG_UTILS_MESSAGE_SEVERITY_ERROR_BIT_EXT:
 		ret = VK_FALSE;
		sev = Log::Severity::CRITICAL;
		break;
	default:
		return VK_FALSE;
	}
	logger(sev) << callbackData->pMessage << std::endl;
	return ret;
}

void tearDownDebugging(VkInstance instance, VkDebugUtilsMessengerEXT handle)
{
	auto func = (PFN_vkDestroyDebugUtilsMessengerEXT) vkGetInstanceProcAddr(instance, "vkDestroyDebugUtilsMessengerEXT");
	if (func != nullptr)
	{
		func(instance, handle, nullptr);
	}
}


}


