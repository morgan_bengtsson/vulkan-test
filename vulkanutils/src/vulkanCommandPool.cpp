#include "vulkanCommandPool.h"

#include "vulkanUtils.h"

namespace vulkanutils
{

CommandPool::CommandPool()
	: mCommandPool(VK_NULL_HANDLE)
{
}

CommandPool::CommandPool(VkDevice device, const Queue& queue, VkCommandPoolCreateFlags flags)
	: mDevice(device)
	, mCommandPool(VK_NULL_HANDLE)
{
	VkCommandPoolCreateInfo poolInfo = {};
	poolInfo.sType = VK_STRUCTURE_TYPE_COMMAND_POOL_CREATE_INFO;
	poolInfo.queueFamilyIndex = queue.getQueueFamilyIdx();
	poolInfo.flags = flags;

	if (VK_SUCCESS != vkCreateCommandPool(device, &poolInfo, nullptr, &mCommandPool))
	{
		logger(Log::Severity::CRITICAL) << "failed to create command pool!" << std::endl;
	}
}

CommandPool::CommandPool(CommandPool&& cb)
	: mDevice(cb.mDevice)
	, mCommandPool(cb.mCommandPool)
{
	cb.mDevice = VK_NULL_HANDLE;
	cb.mCommandPool = VK_NULL_HANDLE;
}

CommandPool& CommandPool::operator=(CommandPool&& cb)
{
	mDevice = cb.mDevice;
	cb.mDevice = VK_NULL_HANDLE;
	mCommandPool = cb.mCommandPool;
	cb.mCommandPool = VK_NULL_HANDLE;
	return *this;
}

CommandPool::operator VkCommandPool() const
{
	return mCommandPool;
}

CommandPool::~CommandPool()
{
	if (mDevice && mCommandPool)
	{
		vkDestroyCommandPool(mDevice, mCommandPool, nullptr);
	}
}

};
