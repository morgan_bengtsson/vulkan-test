#include "vulkanFrameBuffer.h"
#include "vulkanImage.h"
#include "vulkanImageView.h"

#include "vulkanutils.h"



namespace vulkanutils
{

FrameBuffer::FrameBuffer()
	:mDevice(VK_NULL_HANDLE)
	, mFrameBuffer(VK_NULL_HANDLE)
{}

FrameBuffer::FrameBuffer(Device* device, VkRenderPass renderPass
						 , VkImageView* attachments, size_t nAttachments
						 , size_t width, size_t height)
	: FrameBuffer()
{
	mDevice = *device;

	std::vector<VkImageView> att(attachments, attachments + nAttachments);
	VkFramebufferCreateInfo framebufferInfo = {};
	framebufferInfo.sType = VK_STRUCTURE_TYPE_FRAMEBUFFER_CREATE_INFO;
	framebufferInfo.renderPass = renderPass;
	framebufferInfo.attachmentCount = uint32_t(std::size(att));
	framebufferInfo.pAttachments = att.data();
	framebufferInfo.width = uint32_t(width);
	framebufferInfo.height = uint32_t(height);
	framebufferInfo.layers = 1;

	if (VK_SUCCESS != vkCreateFramebuffer(*device,
							&framebufferInfo,
							nullptr,
							&mFrameBuffer))
	{
		logger(Log::Severity::ERROR) << "Failed to create vulkan framebuffer" << std::endl;
		teardown();
	}
}

void FrameBuffer::teardown()
{
	if (mDevice && mFrameBuffer)
	{
		logger(Log::Severity::INFO) << "Tear down vulkan frame buffer" << std::endl;
		vkDestroyFramebuffer(mDevice, mFrameBuffer, nullptr);
	}
	mFrameBuffer = VK_NULL_HANDLE;
}

FrameBuffer::~FrameBuffer()
{
	teardown();
}

FrameBuffer::FrameBuffer(FrameBuffer&& f) noexcept
	:mDevice(f.mDevice)
	, mFrameBuffer(f.mFrameBuffer)
{
	f.mDevice = nullptr;
	f.mFrameBuffer = VK_NULL_HANDLE;
}

FrameBuffer& FrameBuffer::operator=(FrameBuffer&& f) noexcept
{
	teardown();
	mDevice = f.mDevice;
	mFrameBuffer = f.mFrameBuffer;

	f.mDevice = nullptr;
	f.mFrameBuffer = VK_NULL_HANDLE;
	return *this;
}

FrameBuffer::operator VkFramebuffer() const 
{
	return mFrameBuffer;
}

};
