#include "vulkanDevice.h"

#include "vulkanutils.h"
#include "vulkanBuffer.h"

#include <map>
#include <algorithm>
#include <iostream>

namespace vulkanutils
{

Device::Device()
	: mDevice(VK_NULL_HANDLE)
	, mPhyDevice(VK_NULL_HANDLE)
	, mGraphicsQueue()
	, mPresentationQueue()
	, mComputeQueue()
	, mDeviceProperties()
	, mMemProperties()
{}

Device::Device(VkInstance instance, VkSurfaceKHR surface
			   , VkPhysicalDeviceType preferredType
			   , const char** extensions, size_t nExtensions
			   , const char** validationLayers, size_t nValidationLayers)
	: Device()
{
	uint32_t numDevices = 0;
	vkEnumeratePhysicalDevices(instance, &numDevices, nullptr);
	std::vector<VkPhysicalDevice> devices(numDevices);
	vkEnumeratePhysicalDevices(instance, &numDevices, devices.data());

	std::map<VkPhysicalDeviceType, int> typeScore =
	{
		{preferredType, 1000}
		, {VK_PHYSICAL_DEVICE_TYPE_DISCRETE_GPU, 900}
		, {VK_PHYSICAL_DEVICE_TYPE_INTEGRATED_GPU, 800}
		, {VK_PHYSICAL_DEVICE_TYPE_VIRTUAL_GPU, 700 }
	};
	int selectedDeviceScore = 0;
	for(auto& device : devices)
	{
		// Query device info
		VkPhysicalDeviceProperties deviceProperties;
		vkGetPhysicalDeviceProperties(device, &deviceProperties);

		VkSurfaceCapabilitiesKHR surfaceCapabilities;
		vkGetPhysicalDeviceSurfaceCapabilitiesKHR(device, surface, &surfaceCapabilities);


		uint32_t numDeviceExtensions = 0;
		vkEnumerateDeviceExtensionProperties(device, nullptr, &numDeviceExtensions, nullptr);
		std::vector<VkExtensionProperties> availableExtensions(numDeviceExtensions);
		vkEnumerateDeviceExtensionProperties(device, nullptr, &numDeviceExtensions, availableExtensions.data());

		uint32_t numSurfaceFormats = 0;
		vkGetPhysicalDeviceSurfaceFormatsKHR(device, surface, &numSurfaceFormats, nullptr);
		std::vector<VkSurfaceFormatKHR> surfaceFormats(numSurfaceFormats);
		vkGetPhysicalDeviceSurfaceFormatsKHR(device, surface, &numSurfaceFormats, surfaceFormats.data());

		uint32_t numPresentModes = 0;
		vkGetPhysicalDeviceSurfacePresentModesKHR(device, surface, &numPresentModes, nullptr);
		std::vector<VkPresentModeKHR> presentModes(numPresentModes);
		vkGetPhysicalDeviceSurfacePresentModesKHR(device, surface, &numPresentModes, presentModes.data());

		std::vector<const char*> missingExtensions(extensions, extensions + nExtensions);
		for (auto& ext : availableExtensions)
		{
			missingExtensions.erase(std::remove_if(std::begin(missingExtensions), std::end(missingExtensions),
						[ext](const char* str)
			{
				return std::strcmp(ext.extensionName, str) == 0;
			}), missingExtensions.end());
		}

		if (missingExtensions.empty() && !surfaceFormats.empty() && !presentModes.empty())
		{
			logger(Log::Severity::DEBUG) << "Detected device " << deviceProperties.deviceName
										 << "\nApi version: " << deviceProperties.apiVersion
										 << "\nDriver version: " << deviceProperties.driverVersion
										 << std::endl;

			int score = typeScore[deviceProperties.deviceType];
			if (score > selectedDeviceScore)
			{
				selectedDeviceScore = score;
				mPhyDevice = device;
			}
		}
	}
	if (mPhyDevice)
	{
		vkGetPhysicalDeviceProperties(mPhyDevice, &mDeviceProperties);
		logger(Log::Severity::INFO) << "Selected device " << mDeviceProperties.deviceName
									<< "\nApi version: " << mDeviceProperties.apiVersion
									<< "\nDriver version: " << mDeviceProperties.driverVersion << std::endl;
	}
	else
	{
		logger(Log::Severity::WARNING) << "Unable to detect suitable device" << std::endl;
		return;
	}

	auto graphicsQueueFamilies = vulkanutils::Queue::GetFamily(mPhyDevice, VkQueueFlagBits(VK_QUEUE_GRAPHICS_BIT | VK_QUEUE_COMPUTE_BIT | VK_QUEUE_TRANSFER_BIT));
	auto computeQueueFamilies = vulkanutils::Queue::GetFamily(mPhyDevice, VkQueueFlagBits(VK_QUEUE_COMPUTE_BIT | VK_QUEUE_TRANSFER_BIT));
	auto transferQueueFamilies = vulkanutils::Queue::GetFamily(mPhyDevice, VkQueueFlagBits(VK_QUEUE_TRANSFER_BIT));
	auto presentationQueueFamilies = vulkanutils::Queue::GetPresentationFamily(mPhyDevice, surface);

	if (std::empty(graphicsQueueFamilies))
	{
		logger(Log::Severity::ERROR) << "Failed to find compatible vulkan graphics queue" << std::endl;
		return;
	}
	if (std::empty(presentationQueueFamilies))
	{
		logger(Log::Severity::ERROR) << "Failed to find compatible vulkan presentation queue" << std::endl;
		return;
	}
	if (std::empty(computeQueueFamilies))
	{
		logger(Log::Severity::ERROR) << "Failed to find compatible vulkan compute queue" << std::endl;
		return;
	}
	if (std::empty(transferQueueFamilies))
	{
		logger(Log::Severity::ERROR) << "Failed to find compatible vulkan transfer queue" << std::endl;
		return;
	}
	
	
	VkPhysicalDeviceFeatures deviceFeatures = {};
	deviceFeatures.samplerAnisotropy = VK_TRUE;
	deviceFeatures.geometryShader = VK_TRUE;
	deviceFeatures.shaderUniformBufferArrayDynamicIndexing = VK_TRUE;
	deviceFeatures.vertexPipelineStoresAndAtomics = VK_TRUE;
	deviceFeatures.fragmentStoresAndAtomics = VK_TRUE;
	deviceFeatures.tessellationShader = VK_TRUE;
	deviceFeatures.multiDrawIndirect = VK_TRUE;
	deviceFeatures.drawIndirectFirstInstance = VK_TRUE;
	deviceFeatures.shaderInt16 = VK_FALSE;


	VkPhysicalDeviceScalarBlockLayoutFeatures scalarBlockLayoutFeatures = {};
	scalarBlockLayoutFeatures.scalarBlockLayout = VK_TRUE;
	scalarBlockLayoutFeatures.sType = VK_STRUCTURE_TYPE_PHYSICAL_DEVICE_SCALAR_BLOCK_LAYOUT_FEATURES;


	VkPhysicalDeviceDescriptorIndexingFeaturesEXT extraDeviceDescriptorIndexingFeatures = {};
	extraDeviceDescriptorIndexingFeatures.sType = VK_STRUCTURE_TYPE_PHYSICAL_DEVICE_DESCRIPTOR_INDEXING_FEATURES_EXT;
	extraDeviceDescriptorIndexingFeatures.shaderUniformBufferArrayNonUniformIndexing = VK_TRUE;
	extraDeviceDescriptorIndexingFeatures.descriptorBindingPartiallyBound = VK_TRUE;
	extraDeviceDescriptorIndexingFeatures.descriptorBindingVariableDescriptorCount = VK_TRUE;
	extraDeviceDescriptorIndexingFeatures.runtimeDescriptorArray = VK_TRUE;
	extraDeviceDescriptorIndexingFeatures.pNext = &scalarBlockLayoutFeatures;

	std::map<uint32_t, uint32_t> familyQueueIdx;
	familyQueueIdx[graphicsQueueFamilies.front()]++;
	familyQueueIdx[presentationQueueFamilies.front()]++;
	familyQueueIdx[computeQueueFamilies.front()]++;
	familyQueueIdx[transferQueueFamilies.front()]++;

	float queuePriority = 1.0f;
	std::vector<VkDeviceQueueCreateInfo> queueCreateInfos;
	for (auto& [familyIdx, nQueues] : familyQueueIdx)
	{
		VkDeviceQueueCreateInfo& queueCreateInfo = queueCreateInfos.emplace_back();
		queueCreateInfo.sType = VK_STRUCTURE_TYPE_DEVICE_QUEUE_CREATE_INFO;
		queueCreateInfo.queueFamilyIndex = familyIdx;
		queueCreateInfo.queueCount = nQueues;
		queueCreateInfo.pQueuePriorities = &queuePriority;
	}


	VkDeviceCreateInfo createInfo = {};
	createInfo.sType = VK_STRUCTURE_TYPE_DEVICE_CREATE_INFO;
	createInfo.pNext = &extraDeviceDescriptorIndexingFeatures;
	createInfo.pQueueCreateInfos = queueCreateInfos.data();
	createInfo.queueCreateInfoCount = uint32_t(std::size(queueCreateInfos));
	createInfo.pEnabledFeatures = &deviceFeatures;
	createInfo.enabledLayerCount = static_cast<uint32_t>(nValidationLayers);
	createInfo.ppEnabledLayerNames = validationLayers;
	createInfo.enabledExtensionCount = static_cast<uint32_t>(nExtensions);
	createInfo.ppEnabledExtensionNames = extensions;

	if (VK_SUCCESS != vkCreateDevice(mPhyDevice, &createInfo, nullptr, &mDevice))
	{
		logger(Log::Severity::ERROR) << "Failed to create vulkan device" << std::endl;
		return;
	}

	mGraphicsQueue = vulkanutils::Queue(mDevice, graphicsQueueFamilies.front(), --(familyQueueIdx[graphicsQueueFamilies.front()]));
	mPresentationQueue = vulkanutils::Queue(mDevice, presentationQueueFamilies.front(), --(familyQueueIdx[presentationQueueFamilies.front()]));
	mComputeQueue = vulkanutils::Queue(mDevice, computeQueueFamilies.front(), --(familyQueueIdx[computeQueueFamilies.front()]));
	mTransferQueue = vulkanutils::Queue(mDevice, transferQueueFamilies.front(), --(familyQueueIdx[transferQueueFamilies.front()]));

	logger(Log::Severity::INFO) << "Created compatible vulkan device" << std::endl;
	vkGetPhysicalDeviceMemoryProperties(mPhyDevice, &mMemProperties);
}


Device::~Device()
{
	if (mDevice != VK_NULL_HANDLE)
	{
		logger(Log::Severity::INFO) << "Tear down vulkan device" << std::endl;
		vkDestroyDevice(mDevice, nullptr);
	}
}

Device::Device(Device&& d)
	:Device()
{
	*this = std::move(d);
}

Device& Device::operator=(Device&& d)
{
	if (mDevice && this != &d)
	{
		vkDestroyDevice(mDevice, nullptr);
	}
	mDevice = d.mDevice;
	mPhyDevice = d.mPhyDevice;
	mGraphicsQueue = std::move(d.mGraphicsQueue);
	mPresentationQueue = std::move(d.mPresentationQueue);
	mComputeQueue = std::move(d.mComputeQueue);
	mTransferQueue = std::move(d.mTransferQueue);
	mDeviceProperties = d.mDeviceProperties;
	mMemProperties = d.mMemProperties;

	d.mDevice = VK_NULL_HANDLE;
	d.mPhyDevice = VK_NULL_HANDLE;
	d.mGraphicsQueue = vulkanutils::Queue();
	d.mPresentationQueue = vulkanutils::Queue();
	d.mComputeQueue = vulkanutils::Queue();
	d.mTransferQueue = vulkanutils::Queue();
	return *this;
}


Device::operator VkDevice() const
{
	return mDevice;
}

VkDeviceMemory Device::allocate(size_t size, uint32_t typeFlags, uint32_t propertyFlags)
{
	uint32_t typeIdx = 0;
	for (; typeIdx < mMemProperties.memoryTypeCount; typeIdx++)
	{
		if ((typeFlags & (size_t(1) << typeIdx))
				&& (mMemProperties.memoryTypes[typeIdx].propertyFlags & propertyFlags) == propertyFlags)
		{
			break;
		}
	}
	if (typeIdx == mMemProperties.memoryTypeCount)
	{
		logger(Log::Severity::CRITICAL) << "Failed to allocate device memory!" << std::endl;
		return VK_NULL_HANDLE;
	}

	VkMemoryAllocateInfo allocInfo = {};
	allocInfo.sType = VK_STRUCTURE_TYPE_MEMORY_ALLOCATE_INFO;

	allocInfo.allocationSize = size;
	allocInfo.memoryTypeIndex = typeIdx;

	VkDeviceMemory memory;
	if (vkAllocateMemory(mDevice, &allocInfo, nullptr, &memory) != VK_SUCCESS)
	{
		return VK_NULL_HANDLE;
	}
	return memory;
}

void Device::free(VkDeviceMemory mem)
{
	if (mDevice)
	{
		vkFreeMemory(mDevice, mem, nullptr);
	}
}

size_t Device::alignment() const
{
	VkPhysicalDeviceProperties deviceProperties;
	vkGetPhysicalDeviceProperties(mPhyDevice, &deviceProperties);
	size_t memChunkSize = deviceProperties.limits.nonCoherentAtomSize;
	return memChunkSize;
}

VkPhysicalDevice Device::getPhysicalDevice() const
{
	return mPhyDevice;
}

const VkPhysicalDeviceProperties& Device::getPhysicalDeviceProperties() const
{
	return mDeviceProperties;
}

const vulkanutils::Queue& Device::getGraphicsQueue() const
{
	return mGraphicsQueue;
}

const vulkanutils::Queue& Device::getPresentationQueue() const
{
	return mPresentationQueue;
}

const vulkanutils::Queue& Device::getComputeQueue() const
{
	return mComputeQueue;
}

const vulkanutils::Queue& Device::getTransferQueue() const
{
	return mTransferQueue;
}

Device DeviceBuilder::operator()()
{
	return Device(input.instance, input.surface, input.preferredType
				  , input.extensions, std::size(input.extensions)
				  , input.validationLayers.data(), std::size(input.validationLayers));
}


};
