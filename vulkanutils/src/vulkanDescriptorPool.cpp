#include "vulkanDescriptorPool.h"

#include "vulkanutils.h"

namespace vulkanutils
{

DescriptorPool::DescriptorPool()
	:mDevice(VK_NULL_HANDLE)
	, mDescriptorPool(VK_NULL_HANDLE)
{
}

DescriptorPool::DescriptorPool(VkDevice device, size_t size)
	: mDevice(device)
	, mDescriptorPool(VK_NULL_HANDLE)
{
	VkDescriptorPoolSize poolSizes[] =
	{
		{
			VK_DESCRIPTOR_TYPE_UNIFORM_BUFFER,
			uint32_t(size),
		},
		{
			VK_DESCRIPTOR_TYPE_STORAGE_BUFFER,
			uint32_t(size),
		},
		{
			VK_DESCRIPTOR_TYPE_COMBINED_IMAGE_SAMPLER,
			uint32_t(size),
		},
	};

	VkDescriptorPoolCreateInfo poolInfo = {};
	poolInfo.sType = VK_STRUCTURE_TYPE_DESCRIPTOR_POOL_CREATE_INFO;
	poolInfo.poolSizeCount = uint32_t(std::size(poolSizes));
	poolInfo.pPoolSizes = poolSizes;
	poolInfo.maxSets = uint32_t(size);
	poolInfo.flags = 0;

	if (vkCreateDescriptorPool(device, &poolInfo, nullptr, &mDescriptorPool) != VK_SUCCESS)
	{
		vulkanutils::logger(Log::Severity::CRITICAL) << "failed to create descriptor pool!" << std::endl;
	}
}

DescriptorPool::~DescriptorPool()
{
	if (mDevice && mDescriptorPool)
	{
		vkDestroyDescriptorPool(mDevice, mDescriptorPool, nullptr);
	}
}

DescriptorPool::DescriptorPool(DescriptorPool&& d) noexcept
	:mDevice(d.mDevice)
	, mDescriptorPool(d.mDescriptorPool)
{
	d.mDevice = VK_NULL_HANDLE;
	d.mDescriptorPool = VK_NULL_HANDLE;
}

DescriptorPool::operator VkDescriptorPool()
{
	return mDescriptorPool;
}

DescriptorPool::operator const VkDescriptorPool() const
{
	return mDescriptorPool;
}

};
