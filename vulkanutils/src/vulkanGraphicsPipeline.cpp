#include "vulkanGraphicsPipeline.h"
#include "vulkanutils.h"
#include "vulkanContext.h"
#include "vulkanVertexAttributeLayout.h"

#include <algorithm>

namespace vulkanutils
{

uint32_t vulkanutils::GraphicsPipeline::getNumSets() const
{
	return mOffsetBufferSize.numSets;
}

uint32_t vulkanutils::GraphicsPipeline::getMaxNumComponents() const
{
	return mOffsetBufferSize.maxNumShaderStorageBindings;
}

vulkanutils::GraphicsPipeline::GraphicsPipeline(
	VkDevice device,
	const RenderPass& renderPass,
	uint32_t subPassIdx,
	const ShaderSet& shaderSet,
	const std::vector<const ResourceSet*>& sets,
	VkCullModeFlagBits cullMode)
	: mPipeline(VK_NULL_HANDLE)
	, mPipelineLayout(VK_NULL_HANDLE)
	, mDevice(device)
	, mEmptyLayouts()
{
	std::vector<VkDescriptorSetLayout> descLayouts;
	std::vector<VkVertexInputBindingDescription> vertexBindingDescriptions;
	std::vector<VkVertexInputAttributeDescription> vertexAttributeDescriptions;

	mOffsetBufferSize = {};
	for (const ResourceSet* set : sets)
	{
		while (int(set->getDescriptorSetBindpoint()) > int(descLayouts.size()))
		{
			DescriptorLayout layout(device);
			layout.build();
			mEmptyLayouts.emplace_back(std::move(layout));
			descLayouts.push_back(mEmptyLayouts.back());
		}
		if (VkDescriptorSetLayout descLayout = set->getDescriptorLayout())
			descLayouts.push_back(descLayout);
		
		auto& shaderStorageBindings = set->getDescriptorLayout().getDescriptorBindings<vulkanutils::ShaderStorageBuffer>();
		if (!shaderStorageBindings.empty())
		{
			uint32_t maxNr = std::max_element(shaderStorageBindings.begin(), shaderStorageBindings.end(), [](const auto& lhs, const auto& rhs)
				{ return lhs.binding < rhs.binding; })->binding + 1;
			mOffsetBufferSize.maxNumShaderStorageBindings = std::max(mOffsetBufferSize.maxNumShaderStorageBindings, maxNr);
		}

		for (auto bindingDesc : set->getAttributeLayout().getVertexBindingDescriptions())
			vertexBindingDescriptions.push_back(bindingDesc);
		for (auto attributeDesc : set->getAttributeLayout().getVertexAttributeDescriptions())
			vertexAttributeDescriptions.push_back(attributeDesc);
	};
	if (descLayouts.empty())
		__debugbreak();
	mOffsetBufferSize.numSets = descLayouts.size();


	size_t tessellationPatchSize = 0;
	std::vector<VkPipelineShaderStageCreateInfo> shaderStages;

	VkSpecializationMapEntry specializationMapEntries[] =
	{
		{ 0, offsetof(OffsetBufferSize, numSets), sizeof(OffsetBufferSize::numSets) },
		{ 1, offsetof(OffsetBufferSize, maxNumShaderStorageBindings), sizeof(OffsetBufferSize::maxNumShaderStorageBindings) },
	};
	VkSpecializationInfo specializationInfo = {};
	specializationInfo.mapEntryCount = std::size(specializationMapEntries);
	specializationInfo.dataSize = sizeof(OffsetBufferSize);
	specializationInfo.pData = &mOffsetBufferSize;
	specializationInfo.pMapEntries = specializationMapEntries;

	if (auto& shader = shaderSet.vert)
	{
		shaderStages.push_back(shader.getShaderStageInfo());
	}
	if (auto& shader = shaderSet.tesc)
	{
		shaderStages.push_back(shader.getShaderStageInfo());
		tessellationPatchSize = shader.patchSize();
	}
	if (auto & shader = shaderSet.tese)
	{
		shaderStages.push_back(shader.getShaderStageInfo());
	}
	if (auto& shader = shaderSet.geom)
	{
		shaderStages.push_back(shader.getShaderStageInfo());
	}
	if (auto& shader = shaderSet.frag)
	{
		shaderStages.push_back(shader.getShaderStageInfo());
	}
	for (auto& shaderStage : shaderStages)
	{
		shaderStage.pSpecializationInfo = &specializationInfo;
	}

	VkPrimitiveTopology topology = VK_PRIMITIVE_TOPOLOGY_TRIANGLE_LIST;
	if (shaderSet.tesc && shaderSet.tese)
	{
		topology = VK_PRIMITIVE_TOPOLOGY_PATCH_LIST;
	}
	VkViewport vp = renderPass.viewport();
	VkRect2D scissor = renderPass.scissor();
	// Create pipeline
	init(device
		, shaderStages.data(), shaderStages.size()
		, descLayouts.data(), descLayouts.size()
		, vertexBindingDescriptions.data(), vertexBindingDescriptions.size()
		, vertexAttributeDescriptions.data(), vertexAttributeDescriptions.size()
		, topology
		, &vp, 1
		, &scissor, 1
		, renderPass
		, subPassIdx
		, cullMode
		, tessellationPatchSize);
}

GraphicsPipeline::GraphicsPipeline()
	: mPipeline(VK_NULL_HANDLE)
	, mPipelineLayout(VK_NULL_HANDLE)
	, mDevice(VK_NULL_HANDLE)
	, mEmptyLayouts()
	, mOffsetBufferSize()
{
}

void GraphicsPipeline::init(VkDevice device
				   , VkPipelineShaderStageCreateInfo* shaderStages, size_t nShaderStages
				   , VkDescriptorSetLayout* descriptorLayouts, size_t nDescriptorLayouts
				   , VkVertexInputBindingDescription* vertexBindingDescriptions, size_t numVertexBindingDescriptions
				   , VkVertexInputAttributeDescription* vertexAttributeDescriptions, size_t numVertexAttributeDescriptions
				   , VkPrimitiveTopology topology
				   , VkViewport* viewports, size_t nViewports
				   , VkRect2D* scissors, size_t nScissors
				   , VkRenderPass renderPass
				   , uint32_t subPassIdx
				   , VkCullModeFlagBits cullMode
				   , size_t tessellationPatchSize)
{
	std::unique_ptr<VkPipelineTessellationStateCreateInfo> tessellationState;
	if (topology == VK_PRIMITIVE_TOPOLOGY_PATCH_LIST)
	{
		tessellationState = std::make_unique<VkPipelineTessellationStateCreateInfo>();
		tessellationState->sType = VK_STRUCTURE_TYPE_PIPELINE_TESSELLATION_STATE_CREATE_INFO;
		tessellationState->pNext = nullptr;
		tessellationState->flags = 0;
		tessellationState->patchControlPoints = uint32_t(tessellationPatchSize);
	}
	
	VkPipelineInputAssemblyStateCreateInfo inputAssembly = {};
	inputAssembly.sType = VK_STRUCTURE_TYPE_PIPELINE_INPUT_ASSEMBLY_STATE_CREATE_INFO;
	inputAssembly.topology = topology;
	
	inputAssembly.primitiveRestartEnable = VK_FALSE;

	VkPipelineLayoutCreateInfo pipelineLayoutInfo = {};
	pipelineLayoutInfo.sType = VK_STRUCTURE_TYPE_PIPELINE_LAYOUT_CREATE_INFO;
	pipelineLayoutInfo.setLayoutCount = uint32_t(nDescriptorLayouts);
	pipelineLayoutInfo.pSetLayouts = descriptorLayouts;
	pipelineLayoutInfo.pushConstantRangeCount = 0; // Optional
	pipelineLayoutInfo.pPushConstantRanges = nullptr; // Optional

	if (vkCreatePipelineLayout(mDevice, &pipelineLayoutInfo, nullptr, &mPipelineLayout) != VK_SUCCESS)
	{
		logger(Log::Severity::CRITICAL) << "Failed to create pipeline layout" << std::endl;
	}


	VkPipelineColorBlendAttachmentState colorBlendAttachment = {};
	colorBlendAttachment.colorWriteMask = VK_COLOR_COMPONENT_R_BIT | VK_COLOR_COMPONENT_G_BIT | VK_COLOR_COMPONENT_B_BIT | VK_COLOR_COMPONENT_A_BIT;
	colorBlendAttachment.blendEnable = VK_TRUE;
	colorBlendAttachment.srcColorBlendFactor = VK_BLEND_FACTOR_SRC_ALPHA;
	colorBlendAttachment.dstColorBlendFactor = VK_BLEND_FACTOR_ONE_MINUS_SRC_ALPHA;
	colorBlendAttachment.colorBlendOp = VK_BLEND_OP_ADD;
	colorBlendAttachment.srcAlphaBlendFactor = VK_BLEND_FACTOR_ONE;
	colorBlendAttachment.dstAlphaBlendFactor = VK_BLEND_FACTOR_ZERO;
	colorBlendAttachment.alphaBlendOp = VK_BLEND_OP_ADD;


	VkPipelineColorBlendStateCreateInfo colorBlending = {};
	colorBlending.sType = VK_STRUCTURE_TYPE_PIPELINE_COLOR_BLEND_STATE_CREATE_INFO;
	colorBlending.logicOpEnable = VK_FALSE;
	colorBlending.logicOp = VK_LOGIC_OP_COPY;
	colorBlending.attachmentCount = 1;
	colorBlending.pAttachments = &colorBlendAttachment;
	colorBlending.blendConstants[0] = 0.0f;
	colorBlending.blendConstants[1] = 0.0f;
	colorBlending.blendConstants[2] = 0.0f;
	colorBlending.blendConstants[3] = 0.0f;


	VkPipelineRasterizationStateCreateInfo rasterizer = {};
	rasterizer.sType = VK_STRUCTURE_TYPE_PIPELINE_RASTERIZATION_STATE_CREATE_INFO;
	rasterizer.depthClampEnable = VK_FALSE;
	rasterizer.rasterizerDiscardEnable = VK_FALSE;
	rasterizer.polygonMode = VK_POLYGON_MODE_FILL;
	rasterizer.lineWidth = 1.0f;
	rasterizer.cullMode = cullMode;
	rasterizer.frontFace = VK_FRONT_FACE_COUNTER_CLOCKWISE;
	rasterizer.depthBiasEnable = VK_FALSE;
	rasterizer.depthBiasConstantFactor = 0.0f;
	rasterizer.depthBiasClamp = 0.0f;
	rasterizer.depthBiasSlopeFactor = 0.0f;

	VkPipelineMultisampleStateCreateInfo multisampling = {};
	multisampling.sType = VK_STRUCTURE_TYPE_PIPELINE_MULTISAMPLE_STATE_CREATE_INFO;
	multisampling.sampleShadingEnable = VK_FALSE;
	multisampling.rasterizationSamples = VK_SAMPLE_COUNT_1_BIT;
	multisampling.minSampleShading = 1.0f;
	multisampling.pSampleMask = nullptr;
	multisampling.alphaToCoverageEnable = VK_FALSE;
	multisampling.alphaToOneEnable = VK_FALSE;

	VkPipelineViewportStateCreateInfo viewportState = {};
	viewportState.sType = VK_STRUCTURE_TYPE_PIPELINE_VIEWPORT_STATE_CREATE_INFO;
	viewportState.viewportCount = uint32_t(nViewports);
	viewportState.pViewports = viewports;
	viewportState.scissorCount = uint32_t(nScissors);
	viewportState.pScissors = scissors;

	VkPipelineDepthStencilStateCreateInfo depthStencil = {};
	depthStencil.sType = VK_STRUCTURE_TYPE_PIPELINE_DEPTH_STENCIL_STATE_CREATE_INFO;
	depthStencil.depthTestEnable = VK_TRUE;
	depthStencil.depthWriteEnable = VK_TRUE;
	depthStencil.depthCompareOp = VK_COMPARE_OP_LESS_OR_EQUAL;
	depthStencil.depthBoundsTestEnable = VK_FALSE;
	depthStencil.minDepthBounds = 0.0f;
	depthStencil.maxDepthBounds = 1.0f;
	depthStencil.stencilTestEnable = VK_FALSE;
	depthStencil.front = {};
	depthStencil.back = {};

	VkGraphicsPipelineCreateInfo pipelineInfo = {};
	pipelineInfo.sType = VK_STRUCTURE_TYPE_GRAPHICS_PIPELINE_CREATE_INFO;
	pipelineInfo.stageCount = uint32_t(nShaderStages);
	pipelineInfo.pStages = shaderStages;


	VkPipelineVertexInputStateCreateInfo pipelineVertexInputStateCreateInfo = {};
	pipelineVertexInputStateCreateInfo.sType = VK_STRUCTURE_TYPE_PIPELINE_VERTEX_INPUT_STATE_CREATE_INFO;
	pipelineVertexInputStateCreateInfo.vertexBindingDescriptionCount = uint32_t(numVertexBindingDescriptions);
	pipelineVertexInputStateCreateInfo.pVertexBindingDescriptions = vertexBindingDescriptions;
	pipelineVertexInputStateCreateInfo.vertexAttributeDescriptionCount = uint32_t(numVertexAttributeDescriptions);
	pipelineVertexInputStateCreateInfo.pVertexAttributeDescriptions = vertexAttributeDescriptions;

	pipelineInfo.pVertexInputState = &pipelineVertexInputStateCreateInfo;
	pipelineInfo.pInputAssemblyState = &inputAssembly;
	pipelineInfo.pViewportState = &viewportState;
	pipelineInfo.pRasterizationState = &rasterizer;
	pipelineInfo.pMultisampleState = &multisampling;
	pipelineInfo.pDepthStencilState = &depthStencil;
	pipelineInfo.pColorBlendState = &colorBlending;
	pipelineInfo.pDynamicState = nullptr;//&dynamicState;
	pipelineInfo.pTessellationState = tessellationState.get();

	pipelineInfo.layout = mPipelineLayout;
	pipelineInfo.renderPass = renderPass;
	pipelineInfo.subpass = subPassIdx;
	pipelineInfo.basePipelineHandle = VK_NULL_HANDLE;
	pipelineInfo.basePipelineIndex = -1;

	if (vkCreateGraphicsPipelines(mDevice,
								  VK_NULL_HANDLE, 1,
								  &pipelineInfo,
								  nullptr,
								  &mPipeline) != VK_SUCCESS)
	{
		logger(Log::Severity::CRITICAL) << "Failed to setup graphics pipeline" << std::endl;
	}
}

GraphicsPipeline::~GraphicsPipeline()
{
	cleanup();
}

GraphicsPipeline::GraphicsPipeline(GraphicsPipeline&& s)
	:mPipeline(s.mPipeline)
	, mPipelineLayout(s.mPipelineLayout)
	, mDevice(s.mDevice)
	, mOffsetBufferSize(s.mOffsetBufferSize)
{
	s.mPipeline = VK_NULL_HANDLE;
	s.mPipelineLayout = VK_NULL_HANDLE;
	s.mDevice = VK_NULL_HANDLE;
}

GraphicsPipeline& GraphicsPipeline::operator=(GraphicsPipeline&& s)
{
	cleanup();
	mPipeline = s.mPipeline;
	mPipelineLayout = s.mPipelineLayout;
	mDevice = s.mDevice;
	mOffsetBufferSize = s.mOffsetBufferSize;

	s.mPipeline = VK_NULL_HANDLE;
	s.mPipelineLayout = VK_NULL_HANDLE;
	s.mDevice = VK_NULL_HANDLE;
	return *this;
}

void GraphicsPipeline::cleanup()
{
	if (mDevice)
	{
		if (mPipelineLayout)
		{
			logger(Log::Severity::INFO) << "Tear down vulkan pipeline layout" << std::endl;
			vkDestroyPipelineLayout(mDevice, mPipelineLayout, nullptr);
		}
		if (mPipeline)
		{
			logger(Log::Severity::INFO) << "Tear down vulkan pipeline" << std::endl;
			vkDestroyPipeline(mDevice, mPipeline, nullptr);
		}
	}
}

GraphicsPipeline::operator VkPipeline() const
{
	return mPipeline;
}


VkPipelineLayout GraphicsPipeline::getLayout() const
{
	return mPipelineLayout;
}

void GraphicsPipeline::bind(VkCommandBuffer commandBuffer) const
{
	vkCmdBindPipeline(commandBuffer, VK_PIPELINE_BIND_POINT_GRAPHICS, mPipeline);
}

};
