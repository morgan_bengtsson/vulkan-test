#pragma once

#include <vulkanDeviceMemory.h>
#include <vulkanBufferAllocator.h>
#include <vulkan/vulkan.h>

#include <vector>

namespace vulkanutils
{

class Barrier
{
public:
	Barrier(VkCommandBuffer commandBuffer = VK_NULL_HANDLE);
	~Barrier();

	Barrier& dependsOnCompute()
	{
		return addSrcStage(VK_PIPELINE_STAGE_COMPUTE_SHADER_BIT)
			.addSrcAccess(VK_ACCESS_SHADER_WRITE_BIT);
	}
	Barrier& dependsOnCopy() 
	{
		return addSrcStage(VK_PIPELINE_STAGE_TRANSFER_BIT)
			.addSrcAccess(VK_ACCESS_TRANSFER_WRITE_BIT);
	}
	Barrier& dependsOnVertexShader() 
	{
		return addSrcStage(VK_PIPELINE_STAGE_VERTEX_SHADER_BIT)
			.addSrcAccess(VK_ACCESS_SHADER_READ_BIT);
	}

	Barrier& doBeforeCompute()
	{
		return addDstStage(VK_PIPELINE_STAGE_COMPUTE_SHADER_BIT)
			.addDstAccess(VK_ACCESS_SHADER_READ_BIT);
	}
	Barrier& doBeforeCopy()
	{
		return addDstStage(VK_PIPELINE_STAGE_TRANSFER_BIT)
			.addDstAccess(VK_ACCESS_TRANSFER_READ_BIT);
	}

	Barrier& doBeforeVertexShader()
	{
		return addDstStage(VK_PIPELINE_STAGE_VERTEX_INPUT_BIT)
			.addDstAccess(VK_ACCESS_VERTEX_ATTRIBUTE_READ_BIT);
	}


	Barrier& addSrcStage(VkPipelineStageFlags stage);
	Barrier& addDstStage(VkPipelineStageFlags stage);
	Barrier& addBuffer(VkBuffer buffer, MemRange range);
	Barrier& addSrcAccess(VkAccessFlags stage);
	Barrier& addDstAccess(VkAccessFlags stage);
	Barrier& globalBarrier();

	template<typename BufferType>
	Barrier& addBuffer(const BufferAllocation<BufferType>& bufferAlloc)
	{
		return addBuffer(*bufferAlloc.getBuffer(), bufferAlloc.getMemRange());
	}

	Barrier& addBuffer(vulkanutils::Image& image, MemRange range);

	void execute();
private:


	VkCommandBuffer mCommandBuffer;
	std::vector<std::tuple<VkBuffer, MemRange> > mBufferRanges;
	VkPipelineStageFlags mSrcStages;
	VkPipelineStageFlags mDstStages;
	VkAccessFlags mSrcAccess;
	VkAccessFlags mDstAccess;
	bool mGlobalBarrier;

};


class ImageBarrier
{
public:
	ImageBarrier(VkCommandBuffer commandBuffer = VK_NULL_HANDLE);
	~ImageBarrier();

	ImageBarrier& dependsOnCompute()
	{
		return addSrcStage(VK_PIPELINE_STAGE_COMPUTE_SHADER_BIT)
			.addSrcAccess(VK_ACCESS_SHADER_WRITE_BIT);
	}
	ImageBarrier& dependsOnCopy() 
	{
		return addSrcStage(VK_PIPELINE_STAGE_TRANSFER_BIT)
			.addSrcAccess(VK_ACCESS_TRANSFER_WRITE_BIT);
	}
	ImageBarrier& dependsOnVertexShader() 
	{
		return addSrcStage(VK_PIPELINE_STAGE_VERTEX_SHADER_BIT)
			.addSrcAccess(VK_ACCESS_SHADER_READ_BIT);
	}

	ImageBarrier& doBeforeCompute()
	{
		return addDstStage(VK_PIPELINE_STAGE_COMPUTE_SHADER_BIT)
			.addDstAccess(VK_ACCESS_SHADER_READ_BIT);
	}
	ImageBarrier& doBeforeCopyFrom()
	{
		return addDstStage(VK_PIPELINE_STAGE_TRANSFER_BIT)
			.addDstAccess(VK_ACCESS_TRANSFER_READ_BIT);
	}

	ImageBarrier& doBeforeCopyTo()
	{
		return addDstStage(VK_PIPELINE_STAGE_TRANSFER_BIT)
			.addDstAccess(VK_ACCESS_TRANSFER_WRITE_BIT);
	}

	ImageBarrier& doBeforeVertexShader()
	{
		return addDstStage(VK_PIPELINE_STAGE_VERTEX_INPUT_BIT)
			.addDstAccess(VK_ACCESS_VERTEX_ATTRIBUTE_READ_BIT);
	}


	ImageBarrier& addSrcStage(VkPipelineStageFlags stage);
	ImageBarrier& addDstStage(VkPipelineStageFlags stage);
	ImageBarrier& addSrcAccess(VkAccessFlags stage);
	ImageBarrier& addDstAccess(VkAccessFlags stage);
	ImageBarrier& addImage(vulkanutils::Image& image, VkImageLayout fromLayout, VkImageLayout toLayout);

	void execute();
private:
	VkCommandBuffer mCommandBuffer;
	struct ImageTransition
	{
		VkImage image;
		VkImageLayout fromLayout;
		VkImageLayout toLayout;
		VkImageAspectFlags aspectMask;
		size_t mipMapLevels;
	};

	std::vector<ImageTransition> mImages;
	VkPipelineStageFlags mSrcStages;
	VkPipelineStageFlags mDstStages;
	VkAccessFlags mSrcAccess;
	VkAccessFlags mDstAccess;
};








};