#pragma once

#include "vulkanBufferAllocator.h"
#include "vulkanDevice.h"
/*
namespace vulkanutils
{

class BufferPool
{
public:
	BufferPool(Device* device);

	template<typename BufferType>
	BufferAllocation<BufferType> allocate(size_t size)
	{
		return std::get<vulkanutils::BufferAllocator<BufferType>>(mBufferAllocators).allocate(size);
	}

	ImageAllocation allocate(size_t width, size_t height)
	{
		return mImageAllocator.allocate(width, height);
	}

private:
	std::tuple<
		vulkanutils::BufferAllocator<vulkanutils::UploadBuffer>,
		vulkanutils::BufferAllocator<vulkanutils::VertexBuffer>,
		vulkanutils::BufferAllocator<vulkanutils::IndexBuffer>,
		vulkanutils::BufferAllocator<vulkanutils::UniformBuffer>,
		vulkanutils::BufferAllocator<vulkanutils::ShaderStorageBuffer> > mBufferAllocators;
	ImageAllocator<vulkanutils::TextureImage> mImageAllocator;

};

struct BufferAllocations
{
	std::vector<BufferAllocation<UploadBuffer>> hostData;
	std::vector<BufferAllocation<VertexBuffer>> vertexData;
	std::vector<BufferAllocation<IndexBuffer>> vertexIndexData;
	std::vector<BufferAllocation<UniformBuffer>> uniformData;
	std::vector<BufferAllocation<ShaderStorageBuffer>> shaderStorageData;
	std::vector<ImageAllocation> imageBufferData;
};

};

*/