#pragma once

#include <vulkan/vulkan.h>

#include "vulkanRenderPass.h"

#include <vector>

namespace vulkanutils
{

class RenderPassRecorder
{
public:
	RenderPassRecorder(VkCommandBuffer cb, const RenderPass& renderPass, VkFramebuffer frameBuffer, const std::vector<VkClearValue>& clearColors);
	~RenderPassRecorder();

private:
	VkCommandBuffer mCommandBuffer;
};


};