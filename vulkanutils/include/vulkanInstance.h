#pragma once

#include <vulkan/vulkan.h>
#include "log.h"


namespace vulkanutils
{


class Instance
{
public:
	Instance();
	Instance(const char* appName
			 , const char** extentions, size_t nExtensions
			 , const char** validationLayers, size_t nValidationLayers);
	~Instance();

	Instance(Instance&& i);
	Instance& operator=(Instance&& i);
	Instance(const Instance&) = delete;
	Instance& operator=(const Instance&) = delete;

	operator VkInstance();
	VkDebugUtilsMessengerEXT getDebugHandle() const;

private:
	VkInstance mInstance;
	VkDebugUtilsMessengerEXT mDebugHandle;

};


class InstanceBuilder
{
public:
	struct
	{
		const char* applicationName;
		const char* extentions[3] =
		{
			"VK_KHR_surface",
			"VK_KHR_win32_surface",
			VK_EXT_DEBUG_UTILS_EXTENSION_NAME
		};
		const char* validationLayers[1] =
		{
			"VK_LAYER_KHRONOS_validation"
		};
	} input;
	Instance operator()();
};


};
