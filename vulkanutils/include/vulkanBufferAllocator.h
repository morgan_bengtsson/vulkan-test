#pragma once

#include <vulkanBuffer.h>
#include <vulkanImage.h>

#include <vulkan/vulkan.h>

#include <vector>
#include <list>
#include <memory>
#include <span>


namespace vulkanutils
{

template<typename BufferType>
class BufferAllocator;

class ImageAllocation
{
public:
	ImageAllocation();
	ImageAllocation(ImageAllocation&& d);
	ImageAllocation& operator=(ImageAllocation&& d);
	ImageAllocation(const ImageAllocation&) = delete;
	ImageAllocation& operator=(const ImageAllocation&) = delete;

	~ImageAllocation();

	std::shared_ptr<Image> getImage() const;

private:
	friend class ImageAllocator;

	ImageAllocation(ImageAllocator* allocator, std::shared_ptr<Image> image);

	ImageAllocator* mAllocator;
	std::shared_ptr<Image> mImage;
};

template<typename BufferType>
class BufferAllocation
{
public:
	BufferAllocation();
	BufferAllocation(BufferAllocation&& d);
	BufferAllocation& operator=(BufferAllocation&& d);
	BufferAllocation(const BufferAllocation&) = delete;
	BufferAllocation& operator=(const BufferAllocation&) = delete;

	~BufferAllocation();

	std::shared_ptr<BufferType> getBuffer() const;
	const MemRange& getMemRange() const;

private:
	friend class BufferAllocator<BufferType>;

	BufferAllocation(BufferAllocator<BufferType>* allocator, size_t chunkIdx, MemRange range);

	BufferAllocator<BufferType>* mAllocator;
	size_t mChunkIdx;
	MemRange mRange;
};


template<>
class BufferAllocation<vulkanutils::UploadBuffer>
{
public:
	BufferAllocation();
	BufferAllocation(BufferAllocation&& d) noexcept;
	BufferAllocation& operator=(BufferAllocation&& d) noexcept;
	BufferAllocation(const BufferAllocation&) = delete;
	BufferAllocation& operator=(const BufferAllocation&) = delete;

	~BufferAllocation();

	std::span<const uint8_t> getData() const;
	std::span<uint8_t> getData();

	std::shared_ptr<vulkanutils::UploadBuffer> getBuffer() const;
	const MemRange& getMemRange() const;

	template<typename T2>
	std::span<T2> getData();
	template<typename T2>
	std::span<const T2> getData() const;

private:
	friend class BufferAllocator<vulkanutils::UploadBuffer>;
	template <typename Data>
	friend class TypedBufferAllocation;

	BufferAllocation(BufferAllocator<vulkanutils::UploadBuffer>* allocator, size_t chunkIdx, MemRange range, uint8_t* data);

	BufferAllocator<vulkanutils::UploadBuffer>* mAllocator;
	size_t mChunkIdx;
	MemRange mRange;
	uint8_t* mData;
};


template<typename Data>
class TypedBufferAllocation
{
public:
	TypedBufferAllocation(BufferAllocation<UploadBuffer>& ba)
		: mBa(ba)
	{}

	TypedBufferAllocation& operator=(const TypedBufferAllocation<Data>& src)
	{
		auto srcData = src.getData();
		auto dstData = getData();
		size_t size = std::min<size_t>(srcData.size(), dstData.size());
		memcpy(dstData.data(), srcData.data(), size * sizeof(Data));
		return *this;
	}

	std::span<Data> getData()
	{
		return mBa.getData<Data>();
	}
	std::span<const Data> getData() const
	{
		return mBa.getData<Data>();
	}

	std::shared_ptr<vulkanutils::UploadBuffer> getBuffer() const
	{
		return mBa.getBuffer();
	}

	const MemRange getMemRange() const
	{
		return mBa.getMemRange();
	}

	operator BufferAllocation<vulkanutils::UploadBuffer>&() const
	{
		return mBa;
	}
private:
	BufferAllocation<UploadBuffer>& mBa;
};


template<typename ComponentType, typename BufferType>
struct BufferAllocationType;

template<typename ComponentType, typename BufferType>
struct BufferAllocationType : public vulkanutils::BufferAllocation<BufferType>
{};

template<typename ComponentType>
struct BufferAllocationType<ComponentType, vulkanutils::UploadBuffer> : public vulkanutils::TypedBufferAllocation<ComponentType>
{};


bool overlaps(const MemRange& r1, const MemRange& r2);
std::vector<MemRange> combine(const std::vector<MemRange>& ranges);

template<typename DstBufferType, typename SrcBufferType>
void upload(VkCommandBuffer cmdBuffer, DstBufferType& dst, size_t dstOffset, const SrcBufferType& src, size_t srcOffset, size_t size)
{
	VkBufferCopy copyRegion = {};
	copyRegion.srcOffset = srcOffset;
	copyRegion.dstOffset = dstOffset;
	copyRegion.size = size;
	vkCmdCopyBuffer(cmdBuffer, src, dst, 1, &copyRegion);
}

template<typename BufferType>
class BufferAllocator
{
public:
	BufferAllocator(vulkanutils::Device* device
		, size_t chunkSize
		, VkSharingMode sharingMode = VK_SHARING_MODE_EXCLUSIVE);
	BufferAllocator(const BufferAllocator&) = delete;
	BufferAllocator& operator=(const BufferAllocator&) = delete;

	BufferAllocator(BufferAllocator&&) = default;
	BufferAllocator& operator=(BufferAllocator&&) = default;

	BufferAllocation<BufferType> allocate(size_t size, bool pack = true);

private:
	friend class BufferAllocation<BufferType>;

	void deallocate(BufferAllocation<BufferType>& allocation);
	const std::shared_ptr<BufferType>& getBuffer(size_t chunkIdx)
	{
		return mChunks[chunkIdx].buffer;
	}

	BufferAllocator() = default;

	struct Block
	{
		size_t size;
		size_t offset;
		bool used;
	};
	struct Chunk
	{
		Chunk(){};
		Chunk(Chunk&& c) = default;
		Chunk& operator=(Chunk&&) = default;
		Chunk(const Chunk&) = delete;
		Chunk& operator=(const Chunk&) = delete;

		size_t size;
		vulkanutils::VulkanMemory memory;
		std::shared_ptr<BufferType> buffer;
		std::list<Block> blocks;
	};

	void appendChunk(size_t size = 0);
	BufferAllocation<BufferType> allocate(size_t chunkIdx, size_t size);

	std::vector<Chunk> mChunks;
	size_t mDefaultChunkSize;
	vulkanutils::Device* mDevice;
	VkSharingMode mSharingMode;
};

class ImageAllocator
{
public:
	ImageAllocator(vulkanutils::Device* device);

	template<typename ImageType, typename... Args>
	ImageAllocation allocate(Args... args);
	void deallocate(Image* image);

private:
	ImageAllocator() = default;

	struct Chunk
	{
		vulkanutils::VulkanMemory memory;
		std::shared_ptr<Image> image;
	};
	
	template<typename ImageType, typename... Args>
	void appendChunk(Args... args);

	std::vector<Chunk> mChunks;
	vulkanutils::Device* mDevice;
};

};


#include "vulkanBufferAllocator.tpp"