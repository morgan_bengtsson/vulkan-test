#pragma once

#include <ostream>
#include <vector>
#include <string>


class Log
{
public:
	enum class Severity
	{
		CRITICAL,
		ERROR,
		WARNING,
		INFO,
		DEBUG
	};


	class Stream
	{
	public:
		Stream(Log& log, Severity sev)
			:mLog(log), mSeverity(sev)
		{}
		template<typename T>
		Stream& operator<<(const T& obj)
		{
			mLog.log(mSeverity, obj);
			return *this;
		}
		Stream& operator<<(std::ostream& (*fn)(std::ostream&))
		{
			mLog.log(mSeverity, fn);
			return *this;
		}

	private:
		Log& mLog;
		Severity mSeverity;
	};


	Log(const char* tag)
		:mTag(tag), mMinLevel(Severity::INFO), mStreams()
	{}

	void setLevel(Severity sev)
	{
		mMinLevel = sev;
	}

	void addStream(std::ostream* stream)
	{
		mStreams.push_back(stream);
	}

	template<typename T>
	void log(Severity sev, const T& obj)
	{
		if (sev <= mMinLevel)
		{
			for (auto& stream : mStreams)
			{
				*stream << obj;
			}
		}
	}

	void log(Severity sev, std::ostream& (*fn)(std::ostream&))
	{
		if (sev <= mMinLevel)
		{
			for (auto& stream : mStreams)
			{
				*stream << fn;
			}
		}
	}


	Stream operator() (Severity sev)
	{
		return Stream(*this, sev) << mTag << ": ";
	}

private:
	std::string mTag;
	Severity mMinLevel;
	std::vector<std::ostream*> mStreams;
};


