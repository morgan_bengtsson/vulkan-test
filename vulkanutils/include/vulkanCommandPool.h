#pragma once

#include "vulkanDevice.h"

#include <vulkan/vulkan.h>

namespace vulkanutils
{

class CommandPool
{
public:
	CommandPool();
	CommandPool(VkDevice device, const Queue& queue, VkCommandPoolCreateFlags flags = 0);

	CommandPool(CommandPool&& d);
	CommandPool& operator=(CommandPool&&);
	CommandPool(const CommandPool&) = delete;
	CommandPool& operator=(const CommandPool&) = delete;
	~CommandPool();

	operator VkCommandPool() const;

public:
	VkDevice mDevice;
	VkCommandPool mCommandPool;
};



};