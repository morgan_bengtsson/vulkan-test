#pragma once

#include <vulkan/vulkan.h>


namespace vulkanutils
{

VkSurfaceKHR createSurface(VkInstance instance,	uint64_t winid);

}

