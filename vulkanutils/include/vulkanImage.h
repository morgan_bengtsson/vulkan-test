#pragma once

#include "vulkanDeviceMemory.h"
#include "vulkanDevice.h"

#include <vulkan/vulkan.h>

namespace vulkanutils
{

class Image
{
public:
	Image();
	Image(VkDevice device, size_t width, size_t height, VkFormat format, size_t mipMapLevels, VkImageUsageFlags usageFlags);
	~Image();

	Image(Image&& d) noexcept;
	Image& operator=(Image&&) noexcept;

	Image(const Image&) = delete;
	Image& operator=(const Image&) = delete;

	VkMemoryRequirements getMemoryRequirements() const;
	bool bindToMemory(VulkanMemory& devMem, size_t offset);
	static void bindImagesToMemory(const std::vector<Image*>& images, VulkanMemory& deviceMemory);


	operator const VkImage() const;
	operator VkImage();

	size_t size() const;
	size_t width() const;
	size_t height() const;
	VkFormat format() const;
	VkImageLayout layout() const;
	size_t mipMapLevels() const;

	void createMipMap(VkCommandBuffer commandBuffer);

private:
	void teardown();

	VkDevice mDevice;
	VkImage mImage;
	VkImageUsageFlags mUsage;

	VkImageLayout mImageLayout;
	VkFormat mFormat;
	size_t mWidth;
	size_t mHeight;
	size_t mMipMapLevels;

	VkDeviceMemory mImageMemory;
	size_t mBoundMemAlignment;
	uint32_t mBoundMemPropertyFlags;
	size_t mBoundMemOffset;
};

class TextureImage : public Image
{
public:
	using Image::Image;
	TextureImage(VkDevice device, size_t width, size_t height, VkFormat format = VK_FORMAT_R8G8B8A8_UNORM);
};

class DepthImage : public Image
{
public:
	using Image::Image;
	DepthImage(VkDevice device, size_t width, size_t height, VkFormat format = VK_FORMAT_D32_SFLOAT);
};

};
