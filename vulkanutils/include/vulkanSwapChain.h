#pragma once

#include <vulkanDevice.h>

#include <vulkan/vulkan.h>
#include "log.h"


namespace vulkanutils
{

class SwapChain
{
public:
	SwapChain();
	SwapChain(VkDevice device, VkSurfaceKHR surface
			  , VkPhysicalDevice phyDevice, VkSurfaceFormatKHR surfaceFormat
			  , VkQueue graphicsQueue, uint32_t graphicsQueueFamilyIdx
			  , VkQueue presentationQueue, uint32_t presentationQueueFamilyIdx
			  , VkPresentModeKHR presentMode, uint32_t numImages
			  , uint32_t width, uint32_t height);
	~SwapChain();

	SwapChain(SwapChain&& sc);
	SwapChain& operator=(SwapChain&&);

	SwapChain(const SwapChain&) = delete;
	SwapChain& operator=(const SwapChain&) = delete;


	operator VkSwapchainKHR();
	const std::vector<VkImage>& getImages() const;

private:
	void teardown();

	VkSwapchainKHR mSwapChain;
	VkDevice mDevice;
	std::vector<VkImage> mImages;
};


class SwapChainBuilder
{
public:
	struct
	{
		Device* device;
		VkSurfaceKHR surface = VK_NULL_HANDLE;
		VkPhysicalDevice phyDevice = VK_NULL_HANDLE;
		VkSurfaceFormatKHR surfaceFormat;
		VkPresentModeKHR presentMode;
		uint32_t numImages = 3; // Tripple buffer
		uint32_t width = 1024;
		uint32_t height = 768;
	} input;

	SwapChain operator()();
};

};
