#pragma once

#include "vulkanDevice.h"
#include "vulkanBuffer.h"
#include "vulkanUtils.h"
#include "vulkanImage.h"
#include "vulkanImageView.h"
#include "vulkanImageSampler.h"
#include "vulkanBufferAllocator.h"

#include <vector>
#include <span>

#include <vulkan/vulkan.h>

namespace vulkanutils
{
template<typename BufferType>
constexpr VkDescriptorType DescriptorType = (VkDescriptorType)0;

template<>
constexpr VkDescriptorType DescriptorType<UniformBuffer> = VK_DESCRIPTOR_TYPE_UNIFORM_BUFFER;

template<>
constexpr VkDescriptorType DescriptorType<ShaderStorageBuffer> = VK_DESCRIPTOR_TYPE_STORAGE_BUFFER;


struct DescriptorBindingBase
{
	int shaderStages;
	uint32_t binding;
	uint32_t numElements = 1;
	bool dynamic = false;
};

template<typename BufferType>
struct DescriptorBinding : DescriptorBindingBase
{
};

template<typename _BufferType, typename _ComponentType>
struct TypedDescriptorBinding : DescriptorBinding<_BufferType>
{
	typedef _BufferType BufferType;
	typedef _ComponentType ComponentType;

};

struct DescriptorImageBinding : DescriptorBindingBase
{
};

template<typename _ComponentType>
struct TypedImageDescriptorBinding : DescriptorImageBinding
{
	typedef _ComponentType ComponentType;
};


// Set of uniform descriptor sets, one per swap image
// , all from the same layout
class DescriptorSet
{
public:
	DescriptorSet();
	DescriptorSet(VkDevice device,
		VkDescriptorPool descriptorPool,
		const class DescriptorLayout& layout, size_t variableSetSize = 0);
	~DescriptorSet();

	DescriptorSet(DescriptorSet&& d) noexcept;
	DescriptorSet(const DescriptorSet&) = delete;
	DescriptorSet& operator=(DescriptorSet&&) noexcept;
	DescriptorSet& operator=(const DescriptorSet&) = delete;

	template<typename BufferType>
	void bindBuffer(const vulkanutils::BufferAllocation<BufferType>& buffer,
		size_t binding, size_t arrayIndex = 0, size_t numElements = 1);

	template<typename BufferType>
	void bindBuffer(VkBuffer buffer, size_t bufferOffset, size_t size
		, size_t binding, size_t arrayIndex = 0, size_t numElements = 1);
	
	template<typename BufferType>
	void bindBuffers(std::vector<const vulkanutils::BufferAllocation<BufferType>*> buffers,
		size_t binding, size_t arrayIndex = 0);


	void bindImage(VkImageView imageView, VkSampler sampler, size_t binding, size_t element = 0);
	operator VkDescriptorSet() const;

	void bindToGraphics(VkCommandBuffer commandBuffer, VkPipelineLayout pipelineLayout, uint32_t offset) const;
	void bindToCompute(VkCommandBuffer commandBuffer, VkPipelineLayout pipelineLayout, uint32_t offset) const;
private:
	void cleanup();

	VkDevice mDevice;
	VkDescriptorPool mDescriptorPool;
	VkDescriptorSet mDescriptorSet;
};

};


#include "vulkanDescriptorSet.tpp"
