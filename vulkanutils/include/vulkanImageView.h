#pragma once

#include <vulkanImage.h>
#include <vulkan/vulkan.h>


namespace vulkanutils
{

class ImageView
{
public:
	ImageView();

	//TODO: Determine flags based on image format
	ImageView(VkDevice device, VkImage image, VkFormat format, VkImageAspectFlags flags);
	ImageView(VkDevice device, const Image& image, VkImageAspectFlags flags);
	ImageView(VkDevice device, const DepthImage& image);
	ImageView(VkDevice device, const TextureImage& image);

	~ImageView();

	ImageView(ImageView&& d) noexcept;
	ImageView& operator=(ImageView&&) noexcept;

	ImageView(const ImageView&) = delete;
	ImageView& operator=(const ImageView&) = delete;

	operator VkImageView();
	operator VkImageView() const;
private:
	void teardown();
	VkDevice mDevice;
	VkImageView mImageView;
};


};
