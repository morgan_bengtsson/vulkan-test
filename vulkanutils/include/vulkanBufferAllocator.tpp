#pragma once

#include "vulkanBufferAllocator.h"

namespace vulkanutils
{

template<typename BufferType>
BufferAllocation<BufferType>::BufferAllocation() 
	: mAllocator(nullptr)
	, mChunkIdx(0)
	, mRange()
{}

template<typename BufferType>
BufferAllocation<BufferType>::BufferAllocation(BufferAllocation&& d)
	:mAllocator(d.mAllocator), mChunkIdx(d.mChunkIdx)
	, mRange(d.mRange)
{
	d.mAllocator = nullptr;
	d.mChunkIdx = 0;
	d.mRange = {0, 0};
}

template<typename BufferType>
BufferAllocation<BufferType>& BufferAllocation<BufferType>::operator=(BufferAllocation&& d)
{
	this->~BufferAllocation();
	mAllocator = d.mAllocator;
	mChunkIdx = d.mChunkIdx;
	mRange = d.mRange;
	d.mAllocator = nullptr;
	d.mChunkIdx = 0;
	d.mRange = {0, 0};
	return *this;
}

template<typename BufferType>
BufferAllocation<BufferType>::~BufferAllocation()
{
	if (mAllocator)
		mAllocator->deallocate(*this);
}


template<typename BufferType>
std::shared_ptr<BufferType> BufferAllocation<BufferType>::getBuffer() const
{
	if (mAllocator)
		return mAllocator->getBuffer(mChunkIdx);
	return nullptr;
}

template<typename BufferType>
const MemRange& BufferAllocation<BufferType>::getMemRange() const
{
	return mRange;
}

template<typename BufferType>
BufferAllocation<BufferType>::BufferAllocation(BufferAllocator<BufferType>* allocator, size_t chunkIdx, MemRange range)
	: mAllocator(allocator), mChunkIdx(chunkIdx), mRange(range)
{}

template<typename T2>
std::span<T2> BufferAllocation<vulkanutils::UploadBuffer>::getData()
{
	if (mData)
		return { (T2*) (mData + mRange.offset), mRange.size / sizeof(T2) };
	return {};
}

template<typename T2>
std::span<const T2> BufferAllocation<vulkanutils::UploadBuffer>::getData() const
{
	if (mData)
		return { (const T2*) (mData + mRange.offset), mRange.size / sizeof(T2) };
	return {};
}


template<typename ImageType, typename... Args>
ImageAllocation ImageAllocator::allocate(Args... args)
{
	appendChunk<ImageType>(args...);
	return ImageAllocation(this, mChunks.back().image);
}

template<typename ImageType, typename... Args>
void ImageAllocator::appendChunk(Args... args)
{
	Chunk& chunk = mChunks.emplace_back();
	chunk.image.reset(new ImageType(*mDevice, args...));
	VkMemoryRequirements memReq = chunk.image->getMemoryRequirements();
	chunk.memory = vulkanutils::VulkanMemory(mDevice, memReq.size, memReq.memoryTypeBits, VK_MEMORY_PROPERTY_DEVICE_LOCAL_BIT);
	chunk.image->bindToMemory(chunk.memory, 0);
}



};
