#pragma once

#include <vulkan/vulkan.h>
#include <string>

namespace vulkanutils
{

class Semaphore
{
public:
	const char* getName() const
	{
		return mName.c_str();
	}

	~Semaphore();
	Semaphore(Semaphore&&);
	Semaphore& operator=(Semaphore&&);

	operator VkSemaphore();
	operator VkSemaphore() const;

protected:
	Semaphore();
	Semaphore(VkDevice device, const char* name = "noname");
	Semaphore(const Semaphore&) = delete;
	Semaphore& operator=(const Semaphore&) = delete;

	VkSemaphore mSemaphore;

private:
	VkDevice mDevice;
	std::string mName;
};

class TimelineSemaphore : public Semaphore
{
public:
	TimelineSemaphore();
	TimelineSemaphore(VkDevice device, const char* name = "noname");
	TimelineSemaphore(const TimelineSemaphore&) = delete;
	TimelineSemaphore& operator=(const TimelineSemaphore&) = delete;
	TimelineSemaphore(TimelineSemaphore&&) = default;
	TimelineSemaphore& operator=(TimelineSemaphore&&) = default;
private:
};



class BinarySemaphore : public Semaphore
{
public:
	BinarySemaphore();
	BinarySemaphore(VkDevice device, const char* name = "noname");
	BinarySemaphore(const BinarySemaphore&) = delete;
	BinarySemaphore& operator=(const BinarySemaphore&) = delete;
	BinarySemaphore(BinarySemaphore&&) = default;
	BinarySemaphore& operator=(BinarySemaphore&&) = default;
private:
};


};
