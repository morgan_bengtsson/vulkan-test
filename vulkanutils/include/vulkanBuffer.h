#pragma once

#include "vulkanDeviceMemory.h"
#include <vulkan/vulkan.h>

#include <vector>

namespace vulkanutils
{

class Buffer
{
public:
	Buffer();
	Buffer(VkDevice device, size_t size, VkBufferUsageFlags bufferUsage, VkSharingMode sharingMode = VK_SHARING_MODE_EXCLUSIVE);
	virtual ~Buffer();

	Buffer(Buffer&& d) noexcept;
	Buffer& operator=(Buffer&&) noexcept;

	Buffer(const Buffer&) = delete;
	Buffer& operator=(const Buffer&) = delete;

	VkMemoryRequirements getMemoryRequirements() const;
	bool bindToMemory(VulkanMemory& devMem, size_t offset);
	static void bindBuffersToMemory(const std::vector<Buffer*>& buffers, VulkanMemory& deviceMemory);

	size_t size() const;
	size_t offset() const;
	bool mappable() const;
	void flush(size_t offset = 0, size_t size = 0) const;
	void invalidate(size_t offset = 0, size_t size = 0);

	operator const VkBuffer() const;
	operator VkBuffer();
private:
	void teardown();

	VkDevice mDevice;
	size_t mSize;
	VkBuffer mBuffer;
	VkDeviceMemory mBufferMemory;
	size_t mBoundMemAlignment;
	uint32_t mBoundMemPropertyFlags;
	size_t mBoundMemOffset;
};


class UploadBuffer : public Buffer
{
public:
	UploadBuffer() = default;
	UploadBuffer(VkDevice device, size_t size, VkSharingMode sharingMode = VK_SHARING_MODE_EXCLUSIVE);
	static constexpr VkBufferUsageFlagBits Type = (VkBufferUsageFlagBits)0;
private:
};

class VertexBuffer : public Buffer
{
public:
	VertexBuffer() = default;
	VertexBuffer(VkDevice device, size_t size, VkSharingMode sharingMode = VK_SHARING_MODE_EXCLUSIVE );
	static constexpr VkBufferUsageFlagBits Type = VK_BUFFER_USAGE_VERTEX_BUFFER_BIT;
};

class IndexBuffer : public Buffer
{
public:
	IndexBuffer() = default;
	IndexBuffer(VkDevice device, size_t size, VkSharingMode sharingMode = VK_SHARING_MODE_EXCLUSIVE);
	static constexpr VkBufferUsageFlagBits Type = VK_BUFFER_USAGE_INDEX_BUFFER_BIT;
};

class UniformBuffer : public Buffer
{
public:
	UniformBuffer() = default;
	UniformBuffer(VkDevice device, size_t size, VkSharingMode sharingMode = VK_SHARING_MODE_EXCLUSIVE);
	static constexpr VkBufferUsageFlagBits Type = VK_BUFFER_USAGE_UNIFORM_BUFFER_BIT;
};

class ShaderStorageBuffer : public Buffer
{
public:
	ShaderStorageBuffer() = default;
	ShaderStorageBuffer(VkDevice device, size_t size, VkSharingMode sharingMode = VK_SHARING_MODE_EXCLUSIVE);
	static constexpr VkBufferUsageFlagBits Type = VK_BUFFER_USAGE_STORAGE_BUFFER_BIT;
};

class IndirectDrawBuffer : public Buffer
{
public:
	IndirectDrawBuffer() = default;
	IndirectDrawBuffer(VkDevice device, size_t size, VkSharingMode sharingMode = VK_SHARING_MODE_EXCLUSIVE);
	static constexpr VkBufferUsageFlagBits Type = VK_BUFFER_USAGE_INDIRECT_BUFFER_BIT;
};



};
