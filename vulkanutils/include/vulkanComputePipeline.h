#pragma once

#include "vulkanShader.h"
#include "vulkanDescriptorSet.h"
#include "vulkanResourceSet.h"

#include <vulkan/vulkan.h>
#include <vector>


namespace vulkanutils
{
class VulkanContext;

class ComputePipeline
{
public:
	ComputePipeline();

	ComputePipeline(const VulkanContext& context,
		const ComputeShader& shader,
		const std::vector<const ResourceSet*>& sets,
		size_t pushConstantsSize = 0);

	ComputePipeline(VkDevice device
			 , VkPipelineShaderStageCreateInfo shaderStage
			 , VkDescriptorSetLayout* descriptorLayouts, size_t nDescriptorLayouts
			 , size_t pushConstantsSize = 0);

	~ComputePipeline();
	ComputePipeline(ComputePipeline&& d);
	ComputePipeline& operator=(ComputePipeline&&);

	ComputePipeline(const ComputePipeline&) = delete;
	ComputePipeline& operator=(const ComputePipeline&) = delete;

	operator VkPipeline() const;

	VkPipelineLayout getLayout() const;
	void bind(VkCommandBuffer commandBuffer) const;

private:
	void cleanup();

	VkPipeline mComputePipeline;
	VkPipelineLayout mComputePipelineLayout;
	VkDevice mDevice;
};

};
