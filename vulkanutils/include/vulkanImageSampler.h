#pragma once

#include <vulkan/vulkan.h>

namespace vulkanutils
{

class ImageSampler
{
public:
	ImageSampler();
	ImageSampler(VkDevice device, VkFilter magFilter, VkFilter minFilter,
		VkSamplerAddressMode addressMode[3], float anisotropy,
		VkBorderColor borderColor);

	ImageSampler(ImageSampler&& d) noexcept;
	ImageSampler& operator=(ImageSampler&&) noexcept;

	ImageSampler(const ImageSampler&) = delete;
	ImageSampler& operator=(const ImageSampler&) = delete;

	~ImageSampler();

	operator VkSampler();
	operator const VkSampler() const;

private:
	void teardown();
	VkDevice mDevice;
	VkSampler mSampler;
};

}