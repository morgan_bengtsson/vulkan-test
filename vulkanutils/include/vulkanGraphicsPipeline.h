#pragma once

#include "vulkanShader.h"
#include "vulkanDescriptorSet.h"
#include "vulkanResourceSet.h"
#include "vulkanRenderPass.h"

#include <vulkan/vulkan.h>
#include <vector>


namespace vulkanutils
{
class VulkanContext;

class GraphicsPipeline
{
public:
	GraphicsPipeline();

	GraphicsPipeline(VkDevice device,
		const RenderPass& renderPass,
		uint32_t subPassIdx,
		const ShaderSet& shaderSet,
		const std::vector<const ResourceSet*>& sets,
		VkCullModeFlagBits cullMode = VK_CULL_MODE_BACK_BIT);


	~GraphicsPipeline();
	GraphicsPipeline(GraphicsPipeline&& d);
	GraphicsPipeline& operator=(GraphicsPipeline&&);

	GraphicsPipeline(const GraphicsPipeline&) = delete;
	GraphicsPipeline& operator=(const GraphicsPipeline&) = delete;

	operator VkPipeline() const;

	VkPipelineLayout getLayout() const;
	void bind(VkCommandBuffer commandBuffer) const;

	uint32_t getNumSets() const;
	uint32_t getMaxNumComponents() const;

private:
	void init(VkDevice device
			, VkPipelineShaderStageCreateInfo* shaderStages, size_t nShaderStages
			, VkDescriptorSetLayout* descriptorLayouts, size_t nDescriptorLayouts
			, VkVertexInputBindingDescription* vertexBindingDescriptions, size_t numVertexBindingDescriptions
			, VkVertexInputAttributeDescription* vertexAttributeDescriptions, size_t numVertexAttributeDescriptions
			, VkPrimitiveTopology topology
			, VkViewport* viewports, size_t nViewPorts
			, VkRect2D* scissors, size_t nScissors
			, VkRenderPass renderPass
			, uint32_t subPassIdx
			, VkCullModeFlagBits cullMode
			, size_t tessellationPatchSize);


	void cleanup();

	VkPipeline mPipeline;
	VkPipelineLayout mPipelineLayout;
	VkDevice mDevice;
	std::vector<DescriptorLayout> mEmptyLayouts;
	
	struct OffsetBufferSize
	{
		uint32_t maxNumShaderStorageBindings;
		uint32_t numSets;
	};

	OffsetBufferSize mOffsetBufferSize;
};

};
