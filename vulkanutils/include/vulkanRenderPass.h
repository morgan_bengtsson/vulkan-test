#pragma once

#include <vulkan/vulkan.h>
#include "log.h"

namespace vulkanutils
{

class RenderPass
{
public:
	RenderPass();

	struct Attachement
	{
		VkFormat format;
		bool presentation = false;
	};
	struct RenderSubPass
	{
		std::vector<uint32_t> attachments;
	};

	RenderPass(VkDevice device, size_t width, size_t height, std::vector<Attachement> attachments, std::vector<RenderSubPass> subPasses);
	~RenderPass();

	RenderPass(RenderPass&& d);
	RenderPass(const RenderPass&) = delete;

	RenderPass& operator=(RenderPass&&);
	RenderPass& operator=(const RenderPass&) = delete;

	operator VkRenderPass() const;

	VkViewport viewport() const;
	VkRect2D scissor() const;
	size_t width() const;
	size_t height() const;

private:
	void teardown();
	VkRenderPass mRenderPass;
	VkDevice mDevice;

	size_t mWidth;
	size_t mHeight;
};


};
