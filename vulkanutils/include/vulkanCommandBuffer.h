#pragma once

#include <vulkan/vulkan.h>

#include <vector>

namespace vulkanutils
{

class CommandBufferRecorder
{
public:
	CommandBufferRecorder(VkCommandBuffer cb, VkCommandBufferUsageFlagBits usage = VK_COMMAND_BUFFER_USAGE_SIMULTANEOUS_USE_BIT);
	~CommandBufferRecorder();

	VkCommandBuffer getCommandBuffer() const;
private:
	VkCommandBuffer mBuffer;
};


class CommandBuffers
{
public:
	CommandBuffers();
	CommandBuffers(VkDevice device, VkCommandPool commandPool, size_t nBuffers);

	CommandBuffers(CommandBuffers&& d);
	CommandBuffers& operator=(CommandBuffers&&);
	CommandBuffers(const CommandBuffers&) = delete;
	CommandBuffers& operator=(const CommandBuffers&) = delete;
	~CommandBuffers();

	size_t size() const;
	const VkCommandBuffer& operator[](size_t i) const;

public:
	std::vector<VkCommandBuffer> mBuffers;
	VkDevice mDevice;
	VkCommandPool mCommandPool;
};

};
