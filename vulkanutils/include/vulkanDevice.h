#pragma once

#include <vulkan/vulkan.h>

#include "vulkanQueue.h"
#include "log.h"

namespace vulkanutils
{

class Device
{
public:
	Device();
	Device(VkInstance instance, VkSurfaceKHR surface
	   , VkPhysicalDeviceType preferredType
	   , const char** extentions, size_t nExtensions
	   , const char** validationLayers, size_t nValidationLayers);
	~Device();

	Device(Device&& d);
	Device& operator=(Device&&);

	Device(const Device&) = delete;
	Device& operator=(const Device&) = delete;

	operator VkDevice() const;
	VkPhysicalDevice getPhysicalDevice() const;
	const VkPhysicalDeviceProperties& getPhysicalDeviceProperties() const;
	const vulkanutils::Queue& getGraphicsQueue() const;
	const vulkanutils::Queue& getPresentationQueue() const;
	const vulkanutils::Queue& getComputeQueue() const;
	const vulkanutils::Queue& getTransferQueue() const;


	VkDeviceMemory allocate(size_t size, uint32_t typeFlags, uint32_t propertyFlags);
	void free(VkDeviceMemory mem);

	size_t alignment() const;

private:
	VkDevice mDevice;
	VkPhysicalDevice mPhyDevice;

	vulkanutils::Queue mGraphicsQueue;
	vulkanutils::Queue mPresentationQueue;
	vulkanutils::Queue mComputeQueue;
	vulkanutils::Queue mTransferQueue;


	VkPhysicalDeviceProperties mDeviceProperties;
	VkPhysicalDeviceMemoryProperties mMemProperties;
};



class DeviceBuilder
{
public:
	struct
	{
		VkInstance instance = VK_NULL_HANDLE;
		VkSurfaceKHR surface = VK_NULL_HANDLE;
		VkPhysicalDeviceType preferredType = VK_PHYSICAL_DEVICE_TYPE_DISCRETE_GPU;
		const char* extensions[1] =
		{
			VK_KHR_SWAPCHAIN_EXTENSION_NAME,
		};
		std::vector<const char*> validationLayers =
		{
			"VK_LAYER_LUNARG_standard_validation",
			"VK_LAYER_KHRONOS_validation",
			"VK_LAYER_LUNARG_parameter_validation",
		};
	} input;
	Device operator()();
};


};
