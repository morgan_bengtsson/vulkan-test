#pragma once

#include "vulkanDescriptorSet.h"

#include <vulkan/vulkan.h>

#include <tuple>

namespace vulkanutils
{

class DescriptorLayout
{
public:
	DescriptorLayout();
	DescriptorLayout(VkDevice device);
	~DescriptorLayout();

	DescriptorLayout(DescriptorLayout&& d) noexcept;
	DescriptorLayout& operator=(DescriptorLayout&&) noexcept;
	
	DescriptorLayout(const DescriptorLayout&) = delete;
	DescriptorLayout& operator=(const DescriptorLayout&) = delete;

	operator VkDescriptorSetLayout() const;

	template<typename BufferType>
	void addDescriptorBindings(std::span<const DescriptorBinding<BufferType>> descriptorBindings);
	void addImageDescriptorBindings(const DescriptorImageBinding* descriptorImageBindings, size_t numImageBindings);

	void build();

	DescriptorSet createDescriptorSet(VkDescriptorPool pool) const;

	template<typename BufferType>
	const std::vector<DescriptorBinding<BufferType>>& getDescriptorBindings() const;
	const std::vector<DescriptorImageBinding>& getDescriptorImageBindings() const;

private:
	template<typename BufferType>
	static std::vector<VkDescriptorSetLayoutBinding> build(const std::vector<DescriptorBinding<BufferType>>& bindings);
	void teardown();

	VkDevice mDevice;
	
	std::tuple<std::vector<DescriptorBinding<UniformBuffer>>,
			std::vector<DescriptorBinding<ShaderStorageBuffer>>,
			std::vector<DescriptorBinding<VertexBuffer>>,
			std::vector<DescriptorBinding<IndexBuffer>>>
		mDescriptorBindings;
	std::vector<DescriptorImageBinding> mDescriptorImageBindings;
	VkDescriptorSetLayout mDescriptorSetLayout;
};

template<typename BufferType>
void DescriptorLayout::addDescriptorBindings(std::span<const DescriptorBinding<BufferType>> descriptorBindings)
{
	auto& db = std::get<std::vector<DescriptorBinding<BufferType>>>(mDescriptorBindings);
	db.insert(std::end(db), std::begin(descriptorBindings), std::end(descriptorBindings));
}

template<typename BufferType>
const std::vector<DescriptorBinding<BufferType>>& DescriptorLayout::getDescriptorBindings() const
{
	return std::get<std::vector<DescriptorBinding<BufferType>>>(mDescriptorBindings);
}


};
