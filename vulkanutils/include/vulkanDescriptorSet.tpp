#pragma once

#include "vulkanDescriptorSet.h"


namespace vulkanutils
{

template<typename BufferType>
void DescriptorSet::bindBuffer(VkBuffer buffer, size_t bufferOffset, size_t size
	, size_t binding, size_t arrayIndex, size_t numElements)
{
	if (size == 0)
		return;
	VkDescriptorBufferInfo bufferInfo = {};
	bufferInfo.buffer = buffer;
	bufferInfo.offset = bufferOffset;
	bufferInfo.range = size;

	VkWriteDescriptorSet descWrite = {};
	descWrite.sType = VK_STRUCTURE_TYPE_WRITE_DESCRIPTOR_SET;
	descWrite.dstSet = mDescriptorSet;
	descWrite.dstBinding = uint32_t(binding);
	descWrite.dstArrayElement = uint32_t(arrayIndex);
	descWrite.descriptorCount = uint32_t(numElements);
	descWrite.descriptorType = DescriptorType<BufferType>;
	descWrite.pImageInfo = nullptr;
	descWrite.pBufferInfo = &bufferInfo;
	descWrite.pTexelBufferView = nullptr;

	vkUpdateDescriptorSets(mDevice, 1, &descWrite, 0, nullptr);
}

template<typename BufferType>
void DescriptorSet::bindBuffer(const vulkanutils::BufferAllocation<BufferType>& bufferAlloc,
	size_t binding, size_t arrayIndex, size_t numElements)
{
	bindBuffer<BufferType>(*bufferAlloc.getBuffer(), bufferAlloc.getMemRange().offset
		, bufferAlloc.getMemRange().size, binding, arrayIndex, numElements);
}




template<typename BufferType>
void DescriptorSet::bindBuffers(std::vector<const vulkanutils::BufferAllocation<BufferType>*> buffers
	, size_t binding, size_t arrayIndex)
{
	if (buffers.empty())
		return;
	std::vector<VkDescriptorBufferInfo> bufferInfos(buffers.size());
	for (size_t i = 0; i < buffers.size(); i++)
	{
		VkDescriptorBufferInfo& bufferInfo = bufferInfos[i];
		bufferInfo.buffer = *buffers[i]->getBuffer();
		bufferInfo.offset = buffers[i]->getMemRange().offset;
		bufferInfo.range = buffers[i]->getMemRange().size;
	}

	VkWriteDescriptorSet descWrite = {};
	descWrite.sType = VK_STRUCTURE_TYPE_WRITE_DESCRIPTOR_SET;
	descWrite.dstSet = mDescriptorSet;
	descWrite.dstBinding = uint32_t(binding);
	descWrite.dstArrayElement = uint32_t(arrayIndex);
	descWrite.descriptorCount = uint32_t(bufferInfos.size());
	descWrite.descriptorType = DescriptorType<BufferType>;
	descWrite.pImageInfo = nullptr;
	descWrite.pBufferInfo = bufferInfos.data();
	descWrite.pTexelBufferView = nullptr;

	vkUpdateDescriptorSets(mDevice, 1, &descWrite, 0, nullptr);
}

};
