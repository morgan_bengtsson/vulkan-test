#pragma once

#include <vulkan/vulkan.h>

#include <vector>

namespace vulkanutils
{
class Device;

struct MemRange
{
	bool operator==(const MemRange&) const = default;
	size_t offset;
	size_t size;
};


class VulkanMemory
{
public:
	VulkanMemory();
	VulkanMemory(Device* dev, size_t size, uint32_t typeFlags, uint32_t propertyFlags);
	VulkanMemory(Device* dev, const std::vector<class Buffer*>& buffers, uint32_t propertyFlags);
	VulkanMemory(Device* dev, const std::vector<class Image*>& images, uint32_t propertyFlags);
	~VulkanMemory();

	VulkanMemory(VulkanMemory&& d) noexcept;
	VulkanMemory& operator=(VulkanMemory&& d) noexcept;

	VulkanMemory(const VulkanMemory&) = delete;
	VulkanMemory& operator=(const VulkanMemory&) = delete;

	operator VkDeviceMemory();
	operator const VkDeviceMemory() const;

	uint32_t typeFlags() const;
	uint32_t propertyFlags() const;
	size_t alignment() const;
	
	void map();
	void unmap();
	uint8_t* getMappedPtr();
	const uint8_t* getMappedPtr() const;

private:
	void setup();
	void cleanup();

	Device* mDevice;
	VkDeviceMemory mDeviceMemory;
	uint32_t mTypeFlags;
	uint32_t mPropertyFlags;
	size_t mAlignment;
	uint8_t* mMappedPtr;
};

};
