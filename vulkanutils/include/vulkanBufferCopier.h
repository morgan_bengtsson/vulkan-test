#pragma once

#include "vulkanBufferAllocator.h"

#include <memory>
#include <vector>
#include <vulkanBuffer.h>
#include <vulkanImage.h>

namespace vulkanutils
{

class BufferCopier
{
public:
	BufferCopier(VkCommandBuffer commandBuffer, size_t sizeHint = 16);

	BufferCopier(BufferCopier&) = delete;
	BufferCopier(const BufferCopier&) = delete;
	
	~BufferCopier();

	template<typename B>
	void queue(vulkanutils::Image& src, B& dst)
	{
		queue(src, *dst.getBuffer(), dst.getMemRange());
	}

	template<typename B>
	void queue(const B& src, vulkanutils::Image& dst)
	{
		queue(*src.getBuffer(), src.getMemRange(), dst);
	}

	template<typename BA1, typename BA2>
	void queue(const BA1& src, BA2& dst)
	{
		queue(*src.getBuffer(), src.getMemRange()
			, *dst.getBuffer(), dst.getMemRange());
	}

	void queue(
		const vulkanutils::Buffer& src,
		const vulkanutils::MemRange& srcRange,
		vulkanutils::Buffer& dst,
		const vulkanutils::MemRange& dstRange);

	void execute();

private:

	void queue(
		const vulkanutils::Buffer& src,
		const vulkanutils::MemRange& srcRange,
		vulkanutils::Image& dst);

	void queue(
		vulkanutils::Image& src,
		vulkanutils::Buffer& dst,
		const vulkanutils::MemRange& dstRange
		);

	void doBufferCopies();

	// Uploading of data
	struct PendingBufferCopy
	{
		const vulkanutils::Buffer* src;
		vulkanutils::Buffer* dst;
		vulkanutils::MemRange srcRange;
		vulkanutils::MemRange dstRange;
	};
	struct PendingBufferToImageCopy
	{
		const vulkanutils::Buffer& src;
		vulkanutils::Image& dst;
		const vulkanutils::MemRange srcRange;
	};

	struct PendingImageToBufferCopy
	{
		vulkanutils::Image& src;
		vulkanutils::Buffer& dst;
		const vulkanutils::MemRange dstRange;
	};

	VkCommandBuffer mCommandBuffer;
	std::vector<PendingBufferCopy> mPendingBufferCopy;
	std::vector<PendingBufferToImageCopy> mPendingBufferToImageCopy;
	std::vector<PendingImageToBufferCopy> mPendingImageToBufferCopy;
};

};
