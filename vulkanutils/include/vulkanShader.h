#pragma once

#include <vulkan/vulkan.h>

#include <vector>

namespace vulkanutils
{


class Shader
{
public:

	Shader();
	Shader(VkDevice device, const char* filename);
	~Shader();

	Shader(Shader&&) noexcept;
	Shader& operator=(Shader&&) noexcept;
	Shader(const Shader&) = delete;
	Shader& operator=(const Shader&) = delete;

	operator const VkShaderModule() const;

	virtual VkPipelineShaderStageCreateInfo getShaderStageInfo() const = 0;

protected:
	VkPipelineShaderStageCreateInfo getShaderStageInfo(VkShaderStageFlagBits shaderType) const;
private:
	void cleanup();
	static std::vector<uint8_t> readFile(const char* fileName);

	VkShaderModule loadShader(const uint8_t* buffer, size_t bufferSize);
	VkShaderModule loadShader(const char* fileName);

	VkDevice mDevice;
	VkShaderModule mShader;
};

class ComputeShader : public Shader
{
public:
	using Shader::Shader;
	virtual VkPipelineShaderStageCreateInfo getShaderStageInfo() const
	{
		return Shader::getShaderStageInfo(VK_SHADER_STAGE_COMPUTE_BIT);
	}
};

class VertexShader : public Shader
{
public:
	using Shader::Shader;
	virtual VkPipelineShaderStageCreateInfo getShaderStageInfo() const
	{
		return Shader::getShaderStageInfo(VK_SHADER_STAGE_VERTEX_BIT);
	}
};

class TessellationControlShader : public Shader
{
public:
	TessellationControlShader();
	TessellationControlShader(VkDevice device, const char* filename, size_t patchSize = 3);
	virtual VkPipelineShaderStageCreateInfo getShaderStageInfo() const
	{
		return Shader::getShaderStageInfo(VK_SHADER_STAGE_TESSELLATION_CONTROL_BIT);
	}
	size_t patchSize() const;
private:
	size_t mPatchSize;
};

class TessellationEvaluationShader : public Shader
{
public:
	using Shader::Shader;
	virtual VkPipelineShaderStageCreateInfo getShaderStageInfo() const
	{
		return Shader::getShaderStageInfo(VK_SHADER_STAGE_TESSELLATION_EVALUATION_BIT);
	}
};

class GeometryShader : public Shader
{
public:
	using Shader::Shader;
	virtual VkPipelineShaderStageCreateInfo getShaderStageInfo() const
	{
		return Shader::getShaderStageInfo(VK_SHADER_STAGE_GEOMETRY_BIT);
	}
};

class FragmentShader : public Shader
{
public:
	using Shader::Shader;
	virtual VkPipelineShaderStageCreateInfo getShaderStageInfo() const
	{
		return Shader::getShaderStageInfo(VK_SHADER_STAGE_FRAGMENT_BIT);
	}
};




struct ShaderSet
{
	vulkanutils::VertexShader vert;
	vulkanutils::TessellationControlShader tesc;
	vulkanutils::TessellationEvaluationShader tese;
	vulkanutils::GeometryShader geom;
	vulkanutils::FragmentShader frag;
};

ComputeShader loadComputeShader(VkDevice device, const char* path, const char* name);
ShaderSet loadShaderSet(VkDevice device, const char* path, const char* name);

};
