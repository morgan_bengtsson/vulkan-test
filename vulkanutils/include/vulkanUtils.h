#pragma once

#include "log.h"

#include <vulkan/vulkan.h>
#include <optional>


namespace vulkanutils
{
extern Log logger;


void tearDownDebugging(VkInstance instance, VkDebugUtilsMessengerEXT handle);


VKAPI_ATTR VkBool32 VKAPI_CALL debugCallback(VkDebugUtilsMessageSeverityFlagBitsEXT messageSeverity
													, VkDebugUtilsMessageTypeFlagsEXT messageType
													, const VkDebugUtilsMessengerCallbackDataEXT* callbackData
													, void* userData);

}



