#pragma once

#include <vulkan/vulkan.h>


namespace vulkanutils
{

class DescriptorPool
{
public:
	DescriptorPool();
	DescriptorPool(VkDevice device, size_t size);
	~DescriptorPool();

	DescriptorPool(DescriptorPool&& d) noexcept;
	DescriptorPool(const DescriptorPool&) = delete;

	DescriptorPool& operator=(DescriptorPool&&) = delete;
	DescriptorPool& operator=(const DescriptorPool&) = delete;

	operator VkDescriptorPool();
	operator const VkDescriptorPool() const;
private:
	VkDevice mDevice;
	VkDescriptorPool mDescriptorPool;
};


};
