#pragma once

#include "vulkanDevice.h"
#include "vulkanDescriptorPool.h"
#include "vulkanDescriptorSet.h"
#include "vulkanDescriptorLayout.h"
#include "vulkanShader.h"
#include "vulkanVertexAttributeLayout.h"

#include <vulkan/vulkan.h>

namespace vulkanutils
{
enum class DescriptorSetSlot : int
{
	NONE = -1,
	SLOT_0 = 0,
	SLOT_1 = 1,
	SLOT_2 = 2,
	SLOT_3 = 3,
	SLOT_4 = 4,
	SLOT_5 = 5,
	SLOT_6 = 6,
	SLOT_7 = 7,
	SLOT_8 = 8,
	NUM_SLOTS = 9,
};

enum class VertexInputSlot : int
{
	NONE = -1,
	SLOT_0 = 0,
	SLOT_1 = 1,
	SLOT_2 = 2,
	SLOT_3 = 3,
	SLOT_4 = 4,
	SLOT_5 = 5,
	SLOT_6 = 6,
	SLOT_7 = 7,
	SLOT_8 = 8,
	NUM_SLOTS = 9,
};


// One per subpass and per destriptor set
class ResourceSet
{
public:
	ResourceSet(vulkanutils::Device* device, DescriptorSetSlot descSetbp, VertexInputSlot vertInputBp);
	ResourceSet(ResourceSet&&) = default;
	ResourceSet& operator=(ResourceSet&&) = default;
	
	ResourceSet(const ResourceSet&) = delete;
	ResourceSet& operator=(const ResourceSet&) = delete;

	virtual ~ResourceSet();

	vulkanutils::DescriptorSet createDescriptorSet(const vulkanutils::DescriptorPool& descPool, size_t variableSetSize = 0) const;
	const vulkanutils::DescriptorLayout& getDescriptorLayout() const;
	vulkanutils::DescriptorLayout& getDescriptorLayout();
	const vulkanutils::VertexAttributeLayout& getAttributeLayout() const;

	DescriptorSetSlot getDescriptorSetBindpoint() const;
	VertexInputSlot getVertexInputBindpoint() const;

	vulkanutils::Device* getDevice() const;

	void setDescriptorLayout(vulkanutils::DescriptorLayout&& descLayout);
	void setVertexAttributeLayout(vulkanutils::VertexAttributeLayout&& vertLayout);

protected:
	
	vulkanutils::Device* mDevice;
	vulkanutils::DescriptorLayout mDescriptorLayout;
	vulkanutils::VertexAttributeLayout mVertexAttributeLayout;

	std::vector<uint32_t> mAttachmentBindings;

	const DescriptorSetSlot mDescriptorSetBindpoint;
	const VertexInputSlot mVertexInputBindpoint;
};


};
