#pragma once

#include "vulkanSemaphore.h"

#include <vulkan/vulkan.h>

#include <vector>
#include <span>
#include <mutex>

namespace vulkanutils
{

class Queue
{
public:
	static std::vector<uint32_t> GetFamily(VkPhysicalDevice device, VkQueueFlagBits flags);
	static std::vector<uint32_t> GetPresentationFamily(VkPhysicalDevice device, VkSurfaceKHR surface);

	Queue();
	Queue(VkDevice device, uint32_t queueFamilyIdx, uint32_t queueIdx);
	~Queue();

	Queue(Queue&& d);
	Queue& operator=(Queue&&);

	Queue(const Queue&) = delete;
	Queue& operator=(const Queue&) = delete;

	struct WaitCondition
	{
		const BinarySemaphore* semaphore;
		VkPipelineStageFlags waitFlag;
	};

	void submit(std::vector<VkCommandBuffer> commandBuffers
		, const Queue* waitOnQueue
		, std::vector<WaitCondition> waitOnBinSemaphores = {}
		, std::vector<BinarySemaphore*> signalBinSemaphores = {}
		, VkFence fence = VK_NULL_HANDLE) const;

	void wait() const;
	bool wait(VkDevice device, uint64_t signalValue, uint64_t timeoutNs) const;
	uint64_t getSignalValue() const;

	uint32_t getQueueFamilyIdx() const;
	operator VkQueue() const;
private:
	VkQueue mQueue;
	uint32_t mQueueFamilyIdx;
	TimelineSemaphore mSemaphore;
	mutable uint64_t mLastSignalValue;
	mutable std::mutex mMutex;
};


};
