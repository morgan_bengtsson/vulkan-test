#pragma once

#include <vulkan/vulkan.h>


namespace vulkanutils
{

class Surface
{
public:
	Surface();
	Surface(VkInstance instance, uint64_t winId);
	~Surface();

	Surface(Surface&&);
	Surface& operator=(Surface&&);

	Surface(const Surface&) = delete;
	Surface& operator=(const Surface&) = delete;

	operator VkSurfaceKHR();

private:
	VkSurfaceKHR mSurface;
	VkInstance mInstance;
};


};
