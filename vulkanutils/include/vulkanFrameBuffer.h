#pragma once

#include <vulkanDevice.h>
#include <vulkanImage.h>
#include <vulkanImageView.h>

#include <vulkan/vulkan.h>
#include "log.h"


namespace vulkanutils
{


class FrameBuffer
{
public:
	FrameBuffer();
	FrameBuffer(Device* device, VkRenderPass renderPass
				, VkImageView* attachments, size_t nAttachments
				, size_t width, size_t height);
	~FrameBuffer();

	FrameBuffer(FrameBuffer&& d) noexcept;
	FrameBuffer& operator=(FrameBuffer&&) noexcept;

	FrameBuffer(const FrameBuffer&) = delete;	
	FrameBuffer& operator=(const FrameBuffer&) = delete;

	operator VkFramebuffer() const;


private:
	void teardown();
	VkDevice mDevice;
	VkFramebuffer mFrameBuffer;
};



};
