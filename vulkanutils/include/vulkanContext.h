#pragma once

#include "vulkanInstance.h"
#include "vulkanSurface.h"
#include "vulkanDevice.h"
#include "vulkanSwapChain.h"
#include "vulkanRenderPass.h"
#include "vulkanGraphicsPipeline.h"
#include "vulkanFrameBuffer.h"
#include "vulkanImageView.h"
#include "vulkanCommandBuffer.h"
#include "vulkanCommandPool.h"
#include "vulkanSemaphore.h"

#include <vulkan/vulkan.h>

#include <stdint.h>
#include <thread>

namespace vulkanutils
{

class VulkanContext
{
public:
	VulkanContext();
	VulkanContext(const char* name, uint64_t winId);

	VulkanContext(VulkanContext&& d);
	VulkanContext& operator=(VulkanContext&& d);

	VulkanContext(const VulkanContext&) = delete;
	VulkanContext& operator=(const VulkanContext&) = delete;

	~VulkanContext();

	void init(size_t width, size_t height);
	void render(uint32_t imageIndex, const std::vector<VkCommandBuffer>& commandBuffers);
	void compute(const std::vector<VkCommandBuffer>& commandBuffers);
	void resize(size_t width, size_t height);
	size_t width() const;
	size_t height() const;

	Device& device();
	const Device& device() const;


	SwapChain& getSwapChain();
	const SwapChain& getSwapChain() const;

	bool waitForNextImage(uint32_t* imageIndex);

	static constexpr size_t NUM_SWAP_IMAGES = 3;

private:
	static constexpr size_t MAX_FRAMES_IN_FLIGHT = 2;

	void setup();
	void teardown();

	Instance mInstance;
	Surface mSurface;
	Device mDevice;
	SwapChain mSwapChain;

	BinarySemaphore mImageAvailableSemaphores[MAX_FRAMES_IN_FLIGHT] = {};
	BinarySemaphore mRenderFinishedSemaphores[MAX_FRAMES_IN_FLIGHT] = {};
	bool mComputeInitialized[MAX_FRAMES_IN_FLIGHT] = {false, false};

	VkFence mInFlightFences[MAX_FRAMES_IN_FLIGHT] = {};

	uint64_t mWinId;
	size_t mWidth;
	size_t mHeight;
	uint64_t mFrameNumber;

};

};
