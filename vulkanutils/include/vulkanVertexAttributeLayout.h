#pragma once

#include <vulkan/vulkan.h>

#include <vector>

namespace vulkanutils
{

struct VertexAttrDescr
{
	uint32_t location;
	VkFormat format;
	uint32_t bytes;
};

class VertexAttributeLayout
{
public:
	VertexAttributeLayout();
	void addPerVertexAttributeBindings(uint32_t binding, const std::vector<VertexAttrDescr>& attributes);
	void addPerInstanceAttributeBindings(uint32_t binding, const std::vector<VertexAttrDescr>& attributes);

	VertexAttributeLayout(const VertexAttributeLayout&) = default;
	~VertexAttributeLayout() = default;

	std::vector<VkVertexInputBindingDescription> getVertexBindingDescriptions() const;
	std::vector<VkVertexInputAttributeDescription> getVertexAttributeDescriptions() const;

	size_t vertexAttributeSize() const;

private:
	void addVertexAttributeBindings(uint32_t binding, const std::vector<VertexAttrDescr>& attributes, VkVertexInputRate rate);

	std::vector<VkVertexInputBindingDescription> mBindingDescriptions;
	std::vector<VkVertexInputAttributeDescription> mAttributeDescriptions;
};

};
