cmake_minimum_required(VERSION 3.4)

#set_property(GLOBAL PROPERTY USE_FOLDERS ON)



project(vulkan-test)

add_definitions("/Zc:__cplusplus")
add_compile_options(/wd4014)
set(CMAKE_CXX_STANDARD 17)
set(CMAKE_RUNTIME_OUTPUT_DIRECTORY "${CMAKE_BINARY_DIR}/bin")

set(BUILD_TESTING OFF CACHE BOOL "" FORCE)
set(ASSIMP_BUILD_ASSIMP_TOOLS OFF CACHE BOOL "" FORCE)
set(ASSIMP_BUILD_SAMPLES OFF CACHE BOOL "" FORCE)
set(ASSIMP_BUILD_TESTS OFF CACHE BOOL "" FORCE)
set(ASSIMP_NO_EXPORT OFF CACHE BOOL "" FORCE)
set(ASSIMP_BUILD_STATIC_LIB ON CACHE BOOL "" FORCE)
#set(EIGEN_TEST_CXX11 OFF CACHE BOOL "" FORCE)
#add_subdirectory(eigen3 EXCLUDE_FROM_ALL)
add_subdirectory(assimp EXCLUDE_FROM_ALL)
add_subdirectory(tetgen EXCLUDE_FROM_ALL)
add_custom_target(dependencies ALL DEPENDS assimp eigen3 tetgen)

add_subdirectory(vulkanutils)
add_subdirectory(ecs)
add_subdirectory(vulkantest)
