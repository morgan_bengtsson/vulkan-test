cmake_minimum_required(VERSION 3.4)

project(ecs)

find_package(Vulkan REQUIRED)

set(CMAKE_CXX_STANDARD 20)
add_compile_options(/wd4014 /wd4996)
if(WIN32)
	add_compile_options("/std:c++latest")
endif()

file(
	GLOB
	SRC_FILES
	src/*.cpp
)
file(
	GLOB
	INCLUDE_FILES
	include/*.h
	include/*.tpp
)


add_library(${PROJECT_NAME} STATIC ${SRC_FILES} ${INCLUDE_FILES})

target_link_libraries(${PROJECT_NAME} PUBLIC Vulkan::Vulkan vulcanutils)

target_include_directories(${PROJECT_NAME} PUBLIC include ../eigen3)
target_compile_definitions(${PROJECT_NAME} PRIVATE EIGEN_HAS_STD_RESULT_OF=0)
target_compile_options(${PROJECT_NAME} PRIVATE /Zi)
target_link_options(${PROJECT_NAME} PRIVATE /DEBUG)

