#pragma once

#include <vulkanBufferAllocator.h>
#include <vulkanDevice.h>

namespace ecs
{

template<typename BufferType>
class GpuComponentAllocator : public vulkanutils::BufferAllocator<BufferType>
{
public:
	GpuComponentAllocator(const GpuComponentAllocator&) = delete;
	GpuComponentAllocator& operator=(const GpuComponentAllocator&) = delete;
	GpuComponentAllocator(GpuComponentAllocator&&) = default;
	GpuComponentAllocator& operator=(GpuComponentAllocator&&) = default;
	GpuComponentAllocator(vulkanutils::Device* device, size_t chunkSize);
};


class GpuImageComponentAllocator : public vulkanutils::ImageAllocator
{
public:
	GpuImageComponentAllocator(const GpuImageComponentAllocator&) = delete;
	GpuImageComponentAllocator& operator=(const GpuImageComponentAllocator&) = delete;
	GpuImageComponentAllocator(GpuImageComponentAllocator&&) = default;
	GpuImageComponentAllocator& operator=(GpuImageComponentAllocator&&) = default;
	GpuImageComponentAllocator(vulkanutils::Device* device);
};

};

#include "gpucomponentallocator.tpp"
