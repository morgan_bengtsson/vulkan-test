#pragma once

#include "component.h"
#include "gpucomponentstorage.h"

namespace ecs
{

template<Component... ComponentTypes>
class GpuComponentCollection
{
	template<typename ComponentType>
	static constexpr bool IsMember = (std::is_same_v<ComponentType, ComponentTypes> || ...);
public:
	GpuComponentCollection(vulkanutils::Device* device);
	~GpuComponentCollection();

	GpuComponentCollection(const GpuComponentCollection&) = delete;
	GpuComponentCollection(const GpuComponentCollection&&) = delete;

	GpuComponentCollection& operator=(const GpuComponentCollection&) = delete;
	GpuComponentCollection& operator=(GpuComponentCollection&&) = delete;

	static constexpr size_t numComponentTypes();

	template<typename ComponentType, typename BufferType>
	GpuComponentStorage<ComponentType, BufferType>& getComponentStorage() requires IsMember<ComponentType>;
	template<typename ComponentType, typename BufferType>
	const GpuComponentStorage<ComponentType, BufferType>& getComponentStorage() const requires IsMember<ComponentType>;

	template<typename BufferType>
	GpuComponentStorage<BufferType>& getComponentStorage(size_t compTypeId);
	template<typename BufferType>
	const GpuComponentStorage<BufferType>& getComponentStorage(size_t compTypeId) const;


	template<typename ComponentType>
	GpuImageComponentStorage& getImageComponentStorage() requires IsMember<ComponentType>;
	template<typename ComponentType>
	const GpuImageComponentStorage& getImageComponentStorage() const requires IsMember<ComponentType>;

	GpuImageComponentStorage& getImageComponentStorage(size_t compTypeId);
	template<typename BufferType>
	const GpuImageComponentStorage& getImageComponentStorage(size_t compTypeId) const;

	template<typename ComponentType>
	static constexpr size_t getComponentTypeIndex() requires IsMember<ComponentType>;

private:
	template<typename... BufferTypes>
	class GpuBufferAllocators
	{
	public:
		GpuBufferAllocators(vulkanutils::Device* device);
		template<typename BufferType>
		GpuComponentAllocator<BufferType>& getAllocator();
	private:
		std::tuple<GpuComponentAllocator<BufferTypes>...> mAllocators;
	};

	template<typename... BufferTypes>
	class ComponentArrays
	{
	public:
		ComponentArrays(vulkanutils::Device* device);
		template<typename BufferType>
		GpuComponentStorage<BufferType>& getArray(size_t compTypeId);
		template<typename ComponentType, typename BufferType>
		GpuComponentStorage<ComponentType, BufferType>& getArray();
	private:
		template<typename BufferType>
		class BufferComponentArrays
		{
		public:
			BufferComponentArrays(const BufferComponentArrays&) = delete;
			BufferComponentArrays& operator=(const BufferComponentArrays&) = delete;

			BufferComponentArrays(BufferComponentArrays&& a)
				:mBufferComponentArrays(std::move(a.mBufferComponentArrays))
				, mPtrArray { &std::get<GpuComponentStorage<ComponentTypes, BufferType> >(mBufferComponentArrays)... }
			{};
			BufferComponentArrays& operator=(BufferComponentArrays&&) = default;

			BufferComponentArrays(vulkanutils::Device* device);
			template<typename ComponentType>
			GpuComponentStorage<ComponentType, BufferType>& getArray();
			GpuComponentStorage<BufferType>& getArray(size_t idx);
		private:
			std::tuple<GpuComponentStorage<ComponentTypes, BufferType>...> mBufferComponentArrays;
			std::array<GpuComponentStorage<BufferType>*, sizeof...(ComponentTypes)> mPtrArray;
		};
		std::tuple<BufferComponentArrays<BufferTypes>...> mComponentArrays;
	};


	ComponentArrays<vulkanutils::UploadBuffer
	, vulkanutils::VertexBuffer
	, vulkanutils::IndexBuffer
	, vulkanutils::UniformBuffer
	, vulkanutils::ShaderStorageBuffer
	, vulkanutils::IndirectDrawBuffer>
		mComponentArrays;

	std::array<GpuImageComponentStorage, sizeof...(ComponentTypes)> mImageStorage;

};

};


#include "gpucomponentcollection.tpp"
