#pragma once

namespace ecs
{

template<Component... ComponentTypeSubset>
template<Component... ComponentTypes>
SceneSubSet<ComponentTypeSubset...>::SceneSubSet(Scene<ComponentTypes...>& scene)
	: mComponents(scene.getComponentCollection())
	, mGpuComponents(scene.getGpuComponentCollection())
	, mEntitySet(scene)
{}

template<Component... ComponentTypeSubset>
template<Component... ComponentTypes>
SceneSubSet<ComponentTypeSubset...>::SceneSubSet(SceneSubSet<ComponentTypes...>& scene)
	: mComponents(scene.getComponentCollection())
	, mGpuComponents(scene.getGpuComponentCollection())
	, mEntitySet(scene.mEntitySet)
{}

template<Component... ComponentTypeSubset>
std::vector<Entity>& SceneSubSet<ComponentTypeSubset...>::getAllEntities()
{
	return mEntitySet.getAllEntities();
}

template<Component... ComponentTypeSubset>
const std::vector<Entity>& SceneSubSet<ComponentTypeSubset...>::getAllEntities() const
{
	return mEntitySet.getAllEntities();
}

template<Component... ComponentTypeSubset>
Entity* SceneSubSet<ComponentTypeSubset...>::getEntity(size_t entityId)
{
	return mEntitySet.getEntity(entityId);
}

template<Component... ComponentTypeSubset>
const Entity* SceneSubSet<ComponentTypeSubset...>::getEntity(size_t entityId) const
{
	return mEntitySet.getEntity(entityId);
}

template<Component... ComponentTypeSubset>
template<typename... T>
std::vector<const Entity*> SceneSubSet<ComponentTypeSubset...>::getEntities() const
{
	const auto& c = mComponents;
	const auto& es = mEntitySet;
	return es.getEntities<T...>(c);
}

template<Component... ComponentTypeSubset>
template<typename... T>
std::vector<Entity*> SceneSubSet<ComponentTypeSubset...>::getEntities()
{
	return mEntitySet.getEntities<T...>(mComponents);
}

template<Component... ComponentTypeSubset>
template<typename BufferType, typename... T>
std::vector<Entity*> SceneSubSet<ComponentTypeSubset...>::getGpuEntities()
{
	return mEntitySet.getGpuEntities<BufferType, T...>(mComponents);
}


template<Component... ComponentTypeSubset>
void SceneSubSet<ComponentTypeSubset...>::addEntity(const Entity& entity)
{
	mEntitySet.addEntity(entity);
}

template<Component... ComponentTypeSubset>
void SceneSubSet<ComponentTypeSubset...>::removeEntity(size_t id)
{
	mEntitySet.queueRemoveEntity(id);
}


template<Component... ComponentTypeSubset>
ComponentCollectionSubSet<ComponentTypeSubset...>& SceneSubSet<ComponentTypeSubset...>::getComponentCollection()
{
	return mComponents;
}

template<Component... ComponentTypeSubset>
const ComponentCollectionSubSet<ComponentTypeSubset...>& SceneSubSet<ComponentTypeSubset...>::getComponentCollection() const
{
	return mComponents;
}


template<Component... ComponentTypeSubset>
GpuComponentCollectionSubSet<ComponentTypeSubset...>& SceneSubSet<ComponentTypeSubset...>::getGpuComponentCollection()
{
	return mGpuComponents;
}

template<Component... ComponentTypeSubset>
const GpuComponentCollectionSubSet<ComponentTypeSubset...>& SceneSubSet<ComponentTypeSubset...>::getGpuComponentCollection() const
{
	return mGpuComponents;
}

/*
template<Component... ComponentTypeSubset>
GpuImageComponentStorage& SceneSubSet<ComponentTypeSubset...>::getImageStorage()
{
	return mImageStorage;
}

template<Component... ComponentTypeSubset>
const GpuImageComponentStorage& SceneSubSet<ComponentTypeSubset...>::getImageStorage() const
{
	return mImageStorage;
}
*/

template<Component... ComponentTypeSubset>
template<typename... T>
std::vector<const Entity*> SceneSubSet<ComponentTypeSubset...>::getRefEntities(const Entity* entity, EntityRef::Type refType) const
{
	const auto& es = mEntitySet;
	return es.getRefEntities<T...>(mComponents, entity, refType);
}

template<Component... ComponentTypeSubset>
template<typename... T>
std::vector<Entity*> SceneSubSet<ComponentTypeSubset...>::getRefEntities(const Entity* entity, EntityRef::Type refType)
{
	return mEntitySet.getRefEntities<T...>(mComponents, entity, refType);
}





};