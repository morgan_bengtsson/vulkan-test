#pragma once

#include "stablevector.h"

namespace ecs
{

class ComponentStorageBase
{
public:
protected:
};

template<typename ComponentType>
class ComponentStorage : public StableVector<ComponentType>//, public ComponentStorageBase
{
public:
	ComponentStorage(size_t typeIdx)
		: mTypeIdx(typeIdx)
	{}
	void update();

	const size_t typeIdx() const
	{
		return mTypeIdx;
	}
private:
	const size_t mTypeIdx;
};


};

#include "componentstorage.tpp"
