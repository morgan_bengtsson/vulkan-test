#pragma once

#include "entity.h"
#include "entityset.h"
#include "scene.h"
#include "componentcollectionsubset.h"
#include "gpucomponentcollectionsubset.h"

namespace ecs
{

template<Component... ComponentTypeSubset>
class SceneSubSet
{
public:
	template<Component... ComponentTypes>
	SceneSubSet(Scene<ComponentTypes...>& scene);

	template<Component... ComponentTypes>
	SceneSubSet(SceneSubSet<ComponentTypes...>& scene);

	std::vector<Entity>& getAllEntities();
	const std::vector<Entity>& getAllEntities() const;

	Entity* getEntity(size_t entityId);
	const Entity* getEntity(size_t entityId) const;

	template<typename... T>
	std::vector<const Entity*> getEntities() const;
	template<typename... T>
	std::vector<Entity*> getEntities();


	template<typename BufferType, typename... T>
	std::vector<Entity*> getGpuEntities();


	template<typename... T>
	std::vector<const Entity*> getRefEntities(const Entity* entity, EntityRef::Type refType) const;
	template<typename... T>
	std::vector<Entity*> getRefEntities(const Entity* entity, EntityRef::Type refType);


	void addEntity(const Entity& entity);
	void removeEntity(size_t id);

	ComponentCollectionSubSet<ComponentTypeSubset...>& getComponentCollection();
	const ComponentCollectionSubSet<ComponentTypeSubset...>& getComponentCollection() const;

	GpuComponentCollectionSubSet<ComponentTypeSubset...>& getGpuComponentCollection();
	const GpuComponentCollectionSubSet<ComponentTypeSubset...>& getGpuComponentCollection() const;

private:
	template<Component... ComponentTypeSubset2>
	friend class SceneSubSet;

	ComponentCollectionSubSet<ComponentTypeSubset...> mComponents;
	GpuComponentCollectionSubSet<ComponentTypeSubset...> mGpuComponents;
	
	EntitySet& mEntitySet;
};

};


#include "scenesubset.tpp"