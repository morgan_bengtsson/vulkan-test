#pragma once

#include "component.h"
#include "componentcollection.h"

namespace ecs
{

template<Component... ComponentTypeSubset>
class ComponentCollectionSubSet
{
	template<typename ComponentType>
	static constexpr bool IsMember = std::disjunction<std::is_same<ComponentType, ComponentTypeSubset>...>::value;
public:
	template<typename... ComponentTypes>
	ComponentCollectionSubSet(ComponentCollection<ComponentTypes...>& cc);

	template<typename... ComponentTypes>
	ComponentCollectionSubSet(ComponentCollectionSubSet<ComponentTypes...>& cc);

	static constexpr size_t numComponentTypes();

	template<typename ComponentType>
	const ComponentStorage<ComponentType>& getComponentStorage() const requires IsMember<ComponentType>;

	template<typename ComponentType>
	ComponentStorage<ComponentType>& getComponentStorage() requires IsMember<ComponentType>;

	template<typename ComponentType>
	const size_t getComponentTypeIndex() const requires IsMember<ComponentType>;
private:
	const std::tuple<ComponentStorage<ComponentTypeSubset>*...> mArrayPtrs;
	//const std::array<const size_t, sizeof...(ComponentTypeSubset)> mTypeIdx;
};

};


#include "componentcollectionsubset.tpp"
