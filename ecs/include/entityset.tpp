#pragma once

#include "entityset.h"

namespace ecs
{

template<typename... T, typename CCType>
std::vector<Entity*> EntitySet::getEntities(CCType& components)
{
	size_t nRefs = 0;
	for (const Entity& e : mEntities)
		if (e.hasComponents<T...>(components))
			++nRefs;

	std::vector<Entity*> outEntities;
	outEntities.reserve(nRefs);
	for (Entity& e : mEntities)
		if (e.hasComponents<T...>(components))
			outEntities.push_back(&e);
	return outEntities;
}

template<typename BufferType, typename... T, typename CCType>
std::vector<Entity*> EntitySet::getGpuEntities(CCType& components)
{
	size_t nRefs = 0;
	for (const Entity& e : mEntities)
		if (e.hasGpuComponents<BufferType, T...>(components))
			++nRefs;

	std::vector<Entity*> outEntities;
	outEntities.reserve(nRefs);
	for (Entity& e : mEntities)
		if (e.hasGpuComponents<BufferType, T...>(components))
			outEntities.push_back(&e);
	return outEntities;
}


template<typename... T, typename CCType>
std::vector<const Entity*> EntitySet::getEntities(const CCType& components) const
{
	size_t nRefs = 0;
	for (const Entity& e : mEntities)
		if (e.hasComponents<T...>(components))
			++nRefs;

	std::vector<const Entity*> outEntities;
	outEntities.reserve(nRefs);
	for (const Entity& e : mEntities)
		if (e.hasComponents<T...>(components))
			outEntities.push_back(&e);
	return outEntities;
}


template<typename... T, typename CCType>
std::vector<const Entity*> EntitySet::getPendingDestructedEntities(const CCType& components) const
{
	std::vector<const Entity*> outEntities;
	for (const Entity& e : mEntities)
	{
		if ((e->pendingDelete<T, CCType>(components) || ...))
			outEntities.push_back(e);
	}
	return outEntities;
}


template<typename... T, typename CCType>
std::vector<Entity*> EntitySet::getPendingDestructedEntities(CCType& components)
{
	std::vector<const Entity*> outEntities;
	for (const Entity& e : mEntities)
	{
		if ((e->pendingDelete<T, CCType>(components) || ...))
			outEntities.push_back(e);
	}
	return outEntities;
}

template<typename BufferType, typename... T, typename CCType>
std::vector<const Entity*> EntitySet::getGpuPendingDestructedEntities(const CCType& components) const
{
	std::vector<const Entity*> outEntities;
	for (const Entity& e : mEntities)
	{
		if ((e->pendingGpuDelete<BufferType, T, CCType>(components) || ...))
			outEntities.push_back(e);
	}
	return outEntities;
}


template<typename BufferType, typename... T, typename CCType>
std::vector<Entity*> EntitySet::getGpuPendingDestructedEntities(CCType& components)
{
	std::vector<const Entity*> outEntities;
	for (const Entity& e : mEntities)
	{
		if ((e->pendingGpuDelete<BufferType, T, CCType>(components) || ...))
			outEntities.push_back(e);
	}
	return outEntities;
}



template<typename... T, typename CCType>
std::vector<const Entity*> EntitySet::getRefEntities(const CCType& components, const Entity* entity, EntityRef::Type refType) const
{
	std::vector<const Entity*> ret;
	if (!entity->hasComponents<EntityRef>(components))
		return ret;
	auto eRefs = entity->getComponentArray<EntityRef>(components);
	size_t retSize = 0;
	for (EntityRef eRef : eRefs)
	{
		if (eRef.type == refType)
		{
			const Entity* e = getEntity(eRef.id);
			if constexpr (sizeof...(T) > 0)
			{
				if (e && e->hasComponents<T...>(components))
					++retSize;
			}
			else if (e)
				++retSize;
		}
	}
	ret.reserve(retSize);
	for (EntityRef eRef : eRefs)
	{
		if (eRef.type == refType)
		{
			const Entity* e = getEntity(eRef.id);
			if constexpr (sizeof...(T) > 0)
			{
				if (e && e->hasComponents<T...>(components))
					ret.push_back(e);
			}
			else if (e)
				ret.push_back(e);
		}
	}
	return ret;
}

template<typename... T, typename CCType>
std::vector<Entity*> EntitySet::getRefEntities(CCType& components, const Entity* entity, EntityRef::Type refType)
{
	std::vector<Entity*> ret;
	if (!entity->hasComponents<EntityRef>(components))
		return ret;
	auto eRefs = entity->getComponentArray<EntityRef>(components);
	size_t retSize = 0;
	for (const EntityRef& eRef : eRefs)
	{
		if (eRef.type == refType)
		{
			const Entity* e = getEntity(eRef.id);
			if constexpr (sizeof...(T) > 0)
			{
				if (e && e->hasComponents<T...>(components))
					++retSize;
			}
			else if (e)
				++retSize;
		}
	}
	ret.reserve(retSize);
	for (const EntityRef& eRef : eRefs)
	{
		if (eRef.type == refType)
		{
			Entity* e = getEntity(eRef.id);
			if constexpr (sizeof...(T) > 0)
			{
				if (e && e->hasComponents<T...>(components))
					ret.push_back(e);
			}
			else if (e)
				ret.push_back(e);
		}
	}
	return ret;
}






};