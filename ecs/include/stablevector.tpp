#pragma once

#include "stablevector.h"

#include <algorithm>

namespace ecs
{

// ITERATORS ///////////////////////////

template<typename T>
StableVector<T>::StableVector::Iterator::Iterator(StableVector& vector, size_t chunk, size_t block, size_t chunkOffset)
	: mVector(&vector), mChunk(chunk), mBlock(block), mChunkOffset(chunkOffset)
{}

template<typename T>
StableVector<T>::StableVector::ConstIterator::ConstIterator(const StableVector& vector, size_t chunk, size_t block, size_t chunkOffset)
	: mVector(&vector), mChunk(chunk), mBlock(block), mChunkOffset(chunkOffset)
{}

template<typename T>
bool StableVector<T>::StableVector::Iterator::operator==(const Iterator& rhs) const
{
	return std::make_tuple(mChunk, mChunkOffset) == std::make_tuple(rhs.mChunk, rhs.mChunkOffset);
}

template<typename T>
bool StableVector<T>::StableVector::ConstIterator::operator==(const ConstIterator& rhs) const
{
	return std::make_tuple(mChunk, mChunkOffset) == std::make_tuple(rhs.mChunk, rhs.mChunkOffset);
}

template<typename T>
T& StableVector<T>::Iterator::operator*()
{
	return mVector->mChunks[mChunk].mData[mChunkOffset];
}

template<typename T>
const T& StableVector<T>::ConstIterator::operator*() const
{
	return mVector->mChunks[mChunk].mData[mChunkOffset];
}


template<typename T>
StableVector<T>::Iterator& StableVector<T>::Iterator::operator++()
{
	do
	{
		const StableVector<T>::Chunk& chunk = mVector->mChunks[mChunk];
		const StableVector<T>::Chunk::Block block = chunk.mBlocks[mBlock];
	
		bool lastBlock = (mBlock + 1 == chunk.mBlocks.size());
		size_t blockEnd = (lastBlock ? chunk.size : chunk.mBlocks[mBlock + 1].start);

		if (!block.used)
		{
			mChunkOffset = blockEnd;
			if (lastBlock)
			{
				mChunk++;
				mBlock = 0;
				mChunkOffset = 0;
			}
			else
			{
				mBlock++;
			}
		}
		else
		{
			mChunkOffset++;
			if (mChunkOffset == blockEnd)
			{
				if (lastBlock)
				{
					mChunk++;
					mBlock = 0;
					mChunkOffset = 0;
				}
				else
				{
					mBlock++;
				}
			}
		}
	} while (mChunk < mVector->mChunks.size() && mVector->mChunks[mChunk].mBlocks[mBlock].used == false);
	return *this;
}

template<typename T>
StableVector<T>::ConstIterator::ConstIterator(const StableVector<T>::Iterator& it)
	: mVector(it.mVector), mChunk(it.mChunk), mBlock(it.mBlock), mChunkOffset(it.mChunkOffset)
{}


template<typename T>
StableVector<T>::ConstIterator& StableVector<T>::ConstIterator::operator++()
{
	do
	{
		const StableVector<T>::Chunk& chunk = mVector->mChunks[mChunk];
		const StableVector<T>::Chunk::Block block = chunk.mBlocks[mBlock];
	
		bool lastBlock = (mBlock + 1 == chunk.mBlocks.size());
		size_t blockEnd = (lastBlock ? chunk.size : chunk.mBlocks[mBlock + 1]);

		if (!block.used)
		{
			mChunkOffset += blockEnd;
			if (lastBlock)
			{
				mChunk++;
				mBlock = 0;
				mChunkOffset = 0;
			}
			else
			{
				mBlock++;
			}
		}
		else
		{
			mChunkOffset++;
			if (mChunkOffset == blockEnd)
			{
				if (lastBlock)
				{
					mChunk++;
					mBlock = 0;
					mChunkOffset = 0;
				}
				else
				{
					mBlock++;
				}
			}
		}
	} while (mVector->mChunks[mChunk].mBlocks[mBlock].used == false);
	return *this;
}


template<typename T>
StableVector<T>::Iterator& StableVector<T>::Iterator::operator++(int)
{
	auto ret = *this;
	++*this;
	return ret;
}

template<typename T>
StableVector<T>::ConstIterator& StableVector<T>::ConstIterator::operator++(int)
{
	auto ret = *this;
	++*this;
	return ret;
}

template<typename T>
size_t StableVector<T>::Iterator::getIdx() const
{
	return mVector->mChunks[mChunk].start + mChunkOffset;
}


template<typename T>
size_t StableVector<T>::ConstIterator::getIdx() const
{
	return mVector->mChunks[mChunk].start + mChunkOffset;
}



///////////////////////////////////////////////////////


template<typename T>
StableVector<T>::StableVector::Iterator StableVector<T>::StableVector::begin()
{
	Iterator beginIt(*this, 0, 0, 0);
	if (!mChunks.empty() && !mChunks[0].mBlocks[0].used)
		++beginIt;
	return beginIt;
}

template<typename T>
StableVector<T>::StableVector::Iterator StableVector<T>::StableVector::end()
{
	return Iterator(*this, mChunks.size(), 0, 0);
}


template<typename T>
StableVector<T>::StableVector::ConstIterator StableVector<T>::StableVector::begin() const
{
	ConstIterator beginIt(*this, 0, 0, 0);
	if (!mChunks.empty() && !mChunks[0].mBlocks[0].used)
		++beginIt;
	return beginIt;
}

template<typename T>
StableVector<T>::StableVector::ConstIterator StableVector<T>::StableVector::end() const
{
	return ConstIterator(*this, mChunks.size(), 0, 0);
}

template<typename T>
StableVector<T>::StableVector::Iterator StableVector<T>::StableVector::it(size_t idx)
{
	if (idx == 0)
		return Iterator(*this, 0, 0, 0);
	--idx;
	size_t chunkIdx = findChunk(idx);
	size_t chunkOffset = idx - mChunks[chunkIdx].start;
	size_t blockIdx = mChunks[chunkIdx].findBlock(chunkOffset);
	return ++Iterator(*this, chunkIdx, blockIdx, chunkOffset);
}

template<typename T>
StableVector<T>::StableVector::ConstIterator StableVector<T>::StableVector::it(size_t idx) const
{
	if (idx == 0)
		return Iterator(*this, 0, 0, 0);
	--idx;
	size_t chunkIdx = findChunk(idx);
	size_t chunkOffset = idx - mChunks[chunkIdx].start;
	size_t blockIdx = mChunks[chunkIdx].findBlock(chunkOffset);
	return ++ConstIterator(*this, chunkIdx, blockIdx, chunkOffset);
}



// VECTOR ///////////////////////////////


template<typename T>
StableVector<T>::StableVector(size_t defaultSize)
	: mDefaultChunkSize(defaultSize)
	, mChunks()
	, mFirstFreeChunk(0)
{}

template<typename T>
StableVector<T>::Chunk::Chunk(size_t start, size_t size)
	: start(start)
	, size(size)
	, mBlocks{Block{0, false}}
	, mData(new T[size])
{
}

template<typename T>
T& StableVector<T>::operator[](size_t idx)
{
	size_t chunkIdx = findChunk(idx);
	size_t chunkOffset = idx - mChunks[chunkIdx].start;
	return mChunks[chunkIdx].mData[chunkOffset];
}

template<typename T>
const T& StableVector<T>::operator[](size_t idx) const
{
	size_t chunkIdx = findChunk(idx);
	size_t chunkOffset = idx - mChunks[chunkIdx].start;
	return mChunks[chunkIdx].mData[chunkOffset];
}

template<typename T>
StableVector<T>::Iterator StableVector<T>::insert(const T& val)
{
	for (; mFirstFreeChunk < mChunks.size(); mFirstFreeChunk++)
	{
		Chunk& chunk = mChunks[mFirstFreeChunk];
		if (const size_t chunkOffset = chunk.insert(); chunkOffset < chunk.size)
		{
			chunk.mData[chunkOffset] = val;
			return Iterator(*this, mFirstFreeChunk, 0, chunkOffset);
		}
	}
	// No space left in any chunk. Create a new one
	Chunk newChunk(mChunks.empty() ? 0 : mChunks.back().start + mChunks.back().size, mDefaultChunkSize);
	mChunks.emplace_back(std::move(newChunk));
	size_t chunkOffset = mChunks.back().insert();
	return Iterator(*this, mChunks.size() - 1, 0, chunkOffset);
}

template<typename T>
std::tuple<typename StableVector<T>::Iterator, typename StableVector<T>::Iterator> StableVector<T>::insert(const T& val, size_t n)
{
	for (size_t i = mFirstFreeChunk; i < mChunks.size(); i++)
	{
		Chunk& chunk = mChunks[i];
		if (const auto [blockIdx, chunkOffset] = chunk.insert(n); chunkOffset < chunk.size)
		{
			for (size_t j = 0; j < n; j++)
				chunk.mData[chunkOffset + j] = val;
			return { Iterator(*this, i, blockIdx, chunkOffset), Iterator(*this, i, blockIdx, chunkOffset + n) };
		}
	}
	// No space left in any chunk. Create a new one
	Chunk newChunk(mChunks.empty() ? 0 : mChunks.back().start + mChunks.back().size, std::max<size_t>(mDefaultChunkSize, n));
	mChunks.emplace_back(std::move(newChunk));
	auto [blockIdx, chunkOffset] = mChunks.back().insert(n);
	return
	{
		Iterator(*this, mChunks.size() - 1, blockIdx, chunkOffset)
		, Iterator(*this, mChunks.size() - 1, blockIdx, chunkOffset + n)
	};
}

template<typename T>
std::tuple<typename StableVector<T>::Iterator, typename StableVector<T>::Iterator> StableVector<T>::emplace(T&& val, size_t n)
{
	for (size_t i = mFirstFreeChunk; i < mChunks.size(); i++)
	{
		Chunk& chunk = mChunks[i];
		if (const auto [blockIdx, chunkOffset] = chunk.insert(n); chunkOffset < chunk.size)
		{
			for (size_t j = 0; j < n; j++)
				chunk.mData[chunkOffset + j] = std::move(val);
			return { Iterator(*this, i, blockIdx, chunkOffset), Iterator(*this, i, blockIdx, chunkOffset + n) };
		}
	}
	// No space left in any chunk. Create a new one
	Chunk newChunk(mChunks.empty() ? 0 : mChunks.back().start + mChunks.back().size, std::max<size_t>(mDefaultChunkSize, n));
	mChunks.emplace_back(std::move(newChunk));
	auto [blockIdx, chunkOffset] = mChunks.back().insert(n);
	return 
	{
		Iterator(*this, mChunks.size() - 1, blockIdx, chunkOffset)
		, Iterator(*this, mChunks.size() - 1, blockIdx, chunkOffset + n)
	};
}

template<typename T>
size_t StableVector<T>::freePreceedingSize(const ConstIterator& it)
{
	if (mChunks.empty())
		return 0;

	ConstIterator it2 = it;
	if (it2.mChunk >= mChunks.size())
	{
		--it2.mChunk;
		it2.mBlock = mChunks[it2.mChunk].mBlocks.size() - 1;
		it2.mChunkOffset = mChunks[it2.mChunk].size;
	}

	Chunk& chunk = mChunks[it2.mChunk];
	auto& block = chunk.mBlocks[it2.mBlock];

	if (!block.used)
		return it2.mChunkOffset - block.start;
	if (it2.mBlock == 0 || it2.mChunkOffset > block.start)
		return 0;
	return block.start - chunk.mBlocks[it2.mBlock - 1].start;
}

template<typename T>
bool StableVector<T>::resizeRange(const ConstIterator& startIt, size_t prevSize, size_t newSize)
{
	Chunk& chunk = mChunks[startIt.mChunk];
	typename Chunk::Block& block = chunk.mBlocks[startIt.mBlock];
	typename Chunk::Block* nextBlock = (startIt.mBlock < chunk.mBlocks.size() - 1) ? &chunk.mBlocks[startIt.mBlock] : nullptr;
	typename Chunk::Block* nextNextBlock = (startIt.mBlock < chunk.mBlocks.size() - 2) ? &chunk.mBlocks[startIt.mBlock] : nullptr;
		
	if (!nextBlock || startIt.mChunkOffset + prevSize != nextBlock->start)
		return false;

	size_t nextBlockEnd = (nextNextBlock ? nextNextBlock->start : chunk.size);
	if (newSize > prevSize + nextBlockEnd - nextBlock->start)
		return false;
	nextBlock->start = nextBlock->start + newSize - prevSize;
	for (size_t i = prevSize; i < newSize; i++)
	{
		chunk.mData[startIt.mChunkOffset + i] = T();
	}
	return true;
}


template<typename T>
size_t StableVector<T>::Chunk::insert()
{
	if (!mBlocks[0].used)
	{
		mBlocks[0].used = true;
		if (mBlocks.size() > 1 && mBlocks[1].start == 1)
			mBlocks.erase(std::begin(mBlocks) + 1);
		else if (size > 1)
			mBlocks.emplace(std::begin(mBlocks) + 1, 1, false);
		return 0;
	}
	if (mBlocks.size() == 1)
		return size;

	Block& block = mBlocks[1];
	++block.start; // Shrink free block by 1
	if (block.start == size)
		mBlocks.pop_back();
	else if (mBlocks.size() > 2 && mBlocks[2].start == block.start)
		mBlocks.erase(std::begin(mBlocks) + 1, std::begin(mBlocks) + 3); // Remove empty and merge next into prev
	return block.start - 1;
}

template<typename T>
std::tuple<size_t, size_t> StableVector<T>::Chunk::insert(size_t n)
{
	size_t i = 0;
	if (!mBlocks[i].used)
	{
		Block* nextBlock = (mBlocks.size() > 1 ? &mBlocks[1] : nullptr);
		size_t blockEnd = (nextBlock ? nextBlock->start : size);
		if (n <= blockEnd)
		{
			mBlocks[0].used = true;
			if (nextBlock && n == blockEnd)
				mBlocks.erase(std::begin(mBlocks) + 1);
			else if (size > n)
				mBlocks.emplace(std::begin(mBlocks) + 1, n, false);
			return { 0, 0 };
		}
		++i;
	}

	for (++i; i < mBlocks.size() - 1; i += 2)
	{
		Block& prevBlock = mBlocks[i - 1];
		Block& block = mBlocks[i];
		Block& nextBlock = mBlocks[i + 1];
		size_t blockEnd = nextBlock.start;
		if (n <= blockEnd - block.start)
		{
			size_t pos = block.start;
			block.start += n;
			if (block.start == blockEnd)
				mBlocks.erase(std::begin(mBlocks) + i, std::begin(mBlocks) + i + 2); // Remove empty and merge next into prev
			return { i - 1, pos };
		}
	}

	if (i + 1 == mBlocks.size())
	{
		size_t blockEnd = size;
		Block& block = mBlocks.back();
		if (n <= blockEnd - block.start)
		{
			size_t pos = block.start;
			block.start += n;
			if (block.start == blockEnd)
				mBlocks.pop_back();
			return { i - 1, pos };
		}
	}
	return { mBlocks.size(), size };
}

template<typename T>
void StableVector<T>::erase(size_t idx)
{
	size_t chunkIdx = findChunk(idx);
	if (chunkIdx < mFirstFreeChunk)
		mFirstFreeChunk = chunkIdx;
	mChunks[chunkIdx].erase(idx);
	if (mChunks[chunkIdx].mBlocks.size() == 1)
		mChunks.erase(std::begin(mChunks) + chunkIdx);
}

template<typename T>
StableVector<T>::Iterator StableVector<T>::erase(const Iterator& it)
{
	auto ret = it; ++ret;
	mChunks[it.mChunk].erase(it.mBlock, it.mChunkOffset);
	if (it.mChunk < mFirstFreeChunk)
		mFirstFreeChunk = it.mChunk;
	if (mChunks[it.mChunk].mBlocks.size() == 1)
		mChunks.erase(std::begin(mChunks) + it.mChunk);
	ret.mBlock = mChunks[it.mChunk].findBlock(ret.mChunkOffset);
	return ret;
}

template<typename T>
StableVector<T>::ConstIterator StableVector<T>::erase(const ConstIterator& it)
{
	auto ret = it; ++ret;
	mChunks[it.mChunk].erase(it.mBlock, it.mChunkOffset);
	if (mChunks[it.mChunk].mBlocks.size() == 1)
		mChunks.erase(std::begin(mChunks) + it.mChunk);
	if (it.mChunk < mFirstFreeChunk)
		mFirstFreeChunk = it.mChunk;
	ret.mBlock = mChunks[it.mChunk].findBlock(ret.mChunkOffset);
	return ret;
}


template<typename T>
void StableVector<T>::Chunk::erase(size_t blockIdx, size_t chunkOffset)
{
    // Find block
	Block& block = mBlocks[blockIdx];
	Block* nextBlock = (blockIdx == mBlocks.size() - 1) ? nullptr : &mBlocks[blockIdx + 1];
	
	if (block.start == chunkOffset)
	{
		if (blockIdx == 0)
		{
			block.used = false;
			if (size > 1)
				mBlocks.emplace(std::begin(mBlocks) + 1, Block(1, true));
		}
		else
		{
			++block.start;
			if (nextBlock && nextBlock->start == block.start)
				mBlocks.erase(std::begin(mBlocks) + blockIdx, std::begin(mBlocks) + blockIdx + 2);
		}
	}
	else if (nextBlock && nextBlock->start == chunkOffset + 1)
	{
		nextBlock->start--;
	}
	else if (start + size == chunkOffset + 1)
	{
		mBlocks.emplace_back(Block(chunkOffset, false));
	}
	else
	{
		mBlocks.insert(std::begin(mBlocks) + blockIdx + 1, { Block(chunkOffset, false), Block(chunkOffset + 1, true) });
	}
}

template<typename T>
size_t StableVector<T>::Chunk::findBlock(size_t chunkOffset) const
{
	auto it = std::upper_bound(std::begin(mBlocks), std::end(mBlocks), chunkOffset, [](size_t offset, const Block& block)
	{
		return offset < block.start;
	});
	return it - std::begin(mBlocks) - 1;
}

template<typename T>
void StableVector<T>::Chunk::erase(size_t idx)
{
    // Find block
	size_t chunkOffset = idx - start;
	size_t blockIdx = findBlock(chunkOffset);
	Block& block = mBlocks[blockIdx];
	Block* nextBlock = (blockIdx == mBlocks.size() - 1) ? nullptr : &mBlocks[blockIdx + 1];
	
	if (block.start == chunkOffset)
	{
		block.start++;
		if (blockIdx == 0)
			mBlocks.emplace(std::begin(mBlocks), Block(0, false));
	}
	else if (nextBlock && nextBlock->start == chunkOffset + 1)
	{
		nextBlock->start--;
	}
	else if (start + size == chunkOffset + 1)
	{
		mBlocks.emplace_back(Block(chunkOffset, false));
	}
	else
	{
		mBlocks.insert(std::begin(mBlocks) + blockIdx + 1, { Block(chunkOffset, false), Block(chunkOffset + 1, true) });
	}
}


template<typename T>
size_t StableVector<T>::findChunk(size_t idx) const
{
	auto it = std::upper_bound(std::begin(mChunks), std::end(mChunks), idx, [](size_t idx, const Chunk& chunk)
	{
		return idx < chunk.start;
	});
	return it - std::begin(mChunks) - 1;
}


};