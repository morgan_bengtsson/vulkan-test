#pragma once

#include "gpucomponentcollectionsubset.h"

namespace ecs
{

template<Component... GpuComponentTypeSubset>
template<typename... ComponentTypes>
GpuComponentCollectionSubSet<GpuComponentTypeSubset...>::GpuComponentCollectionSubSet(GpuComponentCollection<ComponentTypes...>& cc)
	:mTypeIdx { cc.template getComponentTypeIndex<GpuComponentTypeSubset>()... }
	, mArrays (
		ComponentArrayPtrs<vulkanutils::UploadBuffer>(&cc.getComponentStorage<GpuComponentTypeSubset, vulkanutils::UploadBuffer>()...),
		ComponentArrayPtrs<vulkanutils::VertexBuffer>(&cc.getComponentStorage<GpuComponentTypeSubset, vulkanutils::VertexBuffer>()...),
		ComponentArrayPtrs<vulkanutils::IndexBuffer>(&cc.getComponentStorage<GpuComponentTypeSubset, vulkanutils::IndexBuffer>()...),
		ComponentArrayPtrs<vulkanutils::UniformBuffer>(&cc.getComponentStorage<GpuComponentTypeSubset, vulkanutils::UniformBuffer>()...),
		ComponentArrayPtrs<vulkanutils::ShaderStorageBuffer>(&cc.getComponentStorage<GpuComponentTypeSubset, vulkanutils::ShaderStorageBuffer>()...),
		ComponentArrayPtrs<vulkanutils::IndirectDrawBuffer>(&cc.getComponentStorage<GpuComponentTypeSubset, vulkanutils::IndirectDrawBuffer>()...)
	)
	, mImagePtrArray{&cc.getImageComponentStorage<GpuComponentTypeSubset>()...}
{}

template<Component... GpuComponentTypeSubset>
template<typename... ComponentTypes>
GpuComponentCollectionSubSet<GpuComponentTypeSubset...>::GpuComponentCollectionSubSet(GpuComponentCollectionSubSet<ComponentTypes...>& cc)
	: mTypeIdx{ cc.template getComponentTypeIndex<GpuComponentTypeSubset>()... }
	, mArrays (
		ComponentArrayPtrs<vulkanutils::UploadBuffer>(&cc.getComponentStorage<GpuComponentTypeSubset, vulkanutils::UploadBuffer>()...),
		ComponentArrayPtrs<vulkanutils::VertexBuffer>(&cc.getComponentStorage<GpuComponentTypeSubset, vulkanutils::VertexBuffer>()...),
		ComponentArrayPtrs<vulkanutils::IndexBuffer>(&cc.getComponentStorage<GpuComponentTypeSubset, vulkanutils::IndexBuffer>()...),
		ComponentArrayPtrs<vulkanutils::UniformBuffer>(&cc.getComponentStorage<GpuComponentTypeSubset, vulkanutils::UniformBuffer>()...),
		ComponentArrayPtrs<vulkanutils::ShaderStorageBuffer>(&cc.getComponentStorage<GpuComponentTypeSubset, vulkanutils::ShaderStorageBuffer>()...),
		ComponentArrayPtrs<vulkanutils::IndirectDrawBuffer>(&cc.getComponentStorage<GpuComponentTypeSubset, vulkanutils::IndirectDrawBuffer>()...)
	)
	, mImagePtrArray{&cc.getImageComponentStorage<GpuComponentTypeSubset>()...}
{}

template<Component... GpuComponentTypeSubset>
constexpr size_t GpuComponentCollectionSubSet<GpuComponentTypeSubset...>::numComponentTypes()
{
	return sizeof...(GpuComponentTypeSubset);
}

template<Component... GpuComponentTypeSubset>
template<typename ComponentType, typename BufferType>
GpuComponentStorage<ComponentType, BufferType>& GpuComponentCollectionSubSet<GpuComponentTypeSubset...>::getComponentStorage()
	requires IsMember<ComponentType>
{
	return *std::get<GpuComponentStorage<ComponentType, BufferType>*>(std::get<ComponentArrayPtrs<BufferType>>(mArrays).mArrayPtrs);
}

template<Component... GpuComponentTypeSubset>
template<typename ComponentType, typename BufferType>
const GpuComponentStorage<ComponentType, BufferType>& GpuComponentCollectionSubSet<GpuComponentTypeSubset...>::getComponentStorage() const
	requires IsMember<ComponentType>
{
	return *std::get<GpuComponentStorage<ComponentType, BufferType>*>(std::get<ComponentArrayPtrs<BufferType>>(mArrays).mArrayPtrs);
}

template<Component... GpuComponentTypeSubset>
template<typename BufferType>
GpuComponentStorage<BufferType>& GpuComponentCollectionSubSet<GpuComponentTypeSubset...>::getComponentStorage(size_t compTypeId)
{
	size_t idx = 0;
	while (mTypeIdx[idx] != compTypeId && idx < mTypeIdx.size()) idx++;
	return *std::get<ComponentArrayPtrs<BufferType>>(mArrays).mPtrArray[idx];
}

template<Component... GpuComponentTypeSubset>
template<typename BufferType>
const GpuComponentStorage<BufferType>& GpuComponentCollectionSubSet<GpuComponentTypeSubset...>::getComponentStorage(size_t compTypeId) const
{
	size_t idx = 0;
	while (mTypeIdx[idx] != compTypeId && idx < mTypeIdx.size()) idx++;
	if (idx == mTypeIdx.size())
		*(int*) NULL = 0;
	return *std::get<ComponentArrayPtrs<BufferType>>(mArrays).mPtrArray[idx];
}



template<Component... GpuComponentTypeSubset>
template<typename ComponentType>
GpuImageComponentStorage& GpuComponentCollectionSubSet<GpuComponentTypeSubset...>::getImageComponentStorage() requires IsMember<ComponentType>
{
	size_t componentId = getComponentTypeIndex<ComponentType>();
	return getImageComponentStorage(componentId);
}

template<Component... GpuComponentTypeSubset>
template<typename ComponentType>
const GpuImageComponentStorage& GpuComponentCollectionSubSet<GpuComponentTypeSubset...>::getImageComponentStorage() const requires IsMember<ComponentType>
{
	size_t componentId = getComponentTypeIndex<ComponentType>();
	return getImageComponentStorage(componentId);
}

template<Component... GpuComponentTypeSubset>
GpuImageComponentStorage& GpuComponentCollectionSubSet<GpuComponentTypeSubset...>::getImageComponentStorage(size_t compTypeId)
{
	size_t idx = 0;
	while (mTypeIdx[idx] != compTypeId && idx < mTypeIdx.size()) idx++;
	if (idx == mTypeIdx.size())
		*(int*) NULL = 0;
	return *mImagePtrArray[idx];
}

template<Component... GpuComponentTypeSubset>
template<typename BufferType>
const GpuImageComponentStorage& GpuComponentCollectionSubSet<GpuComponentTypeSubset...>::getImageComponentStorage(size_t compTypeId) const
{
	size_t idx = 0;
	while (mTypeIdx[idx] != compTypeId && idx < mTypeIdx.size()) idx++;
	if (idx == mTypeIdx.size())
		*(int*) NULL = 0;
	return *mImagePtrArray[idx];
}



template<Component... GpuComponentTypeSubset>
template<typename ComponentType>
const size_t GpuComponentCollectionSubSet<GpuComponentTypeSubset...>::getComponentTypeIndex() const
	requires IsMember<ComponentType>
{
	constexpr size_t idx = getTypeIndex<ComponentType, GpuComponentTypeSubset...>();
	return mTypeIdx[idx];
}

};