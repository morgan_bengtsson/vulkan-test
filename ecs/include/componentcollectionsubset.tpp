#pragma once

#include "componentcollectionsubset.h"

namespace ecs
{

template<Component... ComponentTypeSubset>
template<typename... ComponentTypes>
ComponentCollectionSubSet<ComponentTypeSubset...>::ComponentCollectionSubSet(ComponentCollection<ComponentTypes...>& cc)
	://mTypeIdx { cc.template getComponentTypeIndex<ComponentTypeSubset>()... }
	 mArrayPtrs { &cc.getComponentStorage<ComponentTypeSubset>()... }
{}

template<Component... ComponentTypeSubset>
template<typename... ComponentTypes>
ComponentCollectionSubSet<ComponentTypeSubset...>::ComponentCollectionSubSet(ComponentCollectionSubSet<ComponentTypes...>& cc)
	: //mTypeIdx{ cc.template getComponentTypeIndex<ComponentTypeSubset>()... }
	mArrayPtrs{ &cc.getComponentStorage<ComponentTypeSubset>()... }
{}

template<Component... ComponentTypeSubset>
constexpr size_t ComponentCollectionSubSet<ComponentTypeSubset...>::numComponentTypes()
{
	return sizeof...(ComponentTypeSubset);
}

template<Component... ComponentTypeSubset>
template<typename ComponentType>
ComponentStorage<ComponentType>& ComponentCollectionSubSet<ComponentTypeSubset...>::getComponentStorage()
	requires IsMember<ComponentType>
{
	return *std::get<ComponentStorage<ComponentType>*>(mArrayPtrs);
}

template<Component... ComponentTypeSubset>
template<typename ComponentType>
const ComponentStorage<ComponentType>& ComponentCollectionSubSet<ComponentTypeSubset...>::getComponentStorage() const
	requires IsMember<ComponentType>
{
	return *std::get<ComponentStorage<ComponentType>*>(mArrayPtrs);
}

template<Component... ComponentTypeSubset>
template<typename ComponentType>
const size_t ComponentCollectionSubSet<ComponentTypeSubset...>::getComponentTypeIndex() const
	requires IsMember<ComponentType>
{
	return std::get<ComponentStorage<ComponentType>*>(mArrayPtrs)->typeIdx();
}


};
