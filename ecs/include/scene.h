#pragma once

#include "entity.h"
#include "entityset.h"
#include "componentcollection.h"
#include "gpucomponentcollection.h"

namespace ecs
{

template<typename T>
concept SceneType = requires(T& scene)
{
	{ scene.getEntity() };
	{ scene.getComponentCollection() };
};

template<typename... ComponentTypes>
class Scene : public EntitySet
{
public:
	Scene(vulkanutils::Device* device);

	virtual void teardown() = 0 {};
	virtual void update() = 0 {};
	void postUpdate();

	ComponentCollection<ComponentTypes...>& getComponentCollection();
	const ComponentCollection<ComponentTypes...>& getComponentCollection() const;

	GpuComponentCollection<ComponentTypes...>& getGpuComponentCollection();
	const GpuComponentCollection<ComponentTypes...>& getGpuComponentCollection() const;

	template<typename... T>
	std::vector<const Entity*> getRefEntities(const Entity* entity, EntityRef::Type refType) const;
	template<typename... T>
	std::vector<Entity*> getRefEntities(const Entity* entity, EntityRef::Type refType);


	template<typename... T>
	std::vector<const Entity*> getEntities() const;
	template<typename... T>
	std::vector<Entity*> getEntities();
	template<typename BufferType, typename... T>
	std::vector<Entity*> getGpuEntities();

protected:
	using EntitySet::addEntity;
private:
	ComponentCollection<ComponentTypes...> mComponentCollection;
	GpuComponentCollection<ComponentTypes...> mGpuComponentCollection;
};



};

#include "scene.tpp"
