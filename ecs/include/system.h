#pragma once

#include "entity.h"
#include "tracer.h"

#include <vulkanDevice.h>

namespace ecs
{

template<typename... Ts>
struct ParamPack
{
	template<typename Functor, typename... Args>
	static auto apply(Functor func, Args&... args)
	{
		return func.operator() < Ts... > (args...);
	}
};

template<typename... Ts>
struct ComponentParamPack : ParamPack<Ts...>
{
	template<typename CCType>
	static constexpr size_t componentTypes(const CCType& components)
	{
		return ((size_t(1) << components.template getComponentTypeIndex<Ts>()) | ...);
	}
};


template<typename Derived_>
class System
{
public:
	typedef Derived_ Derived;

	System(vulkanutils::VulkanContext& vc)
		: mVulkanContext(vc)
	{}
	System(System&&) = default;
	System(const System&) = delete;

	template<typename SceneType>
	void run(SceneType& scene)
	{
		Tracer systemRunTrace(Derived::getName());
		static_cast<Derived*>(this)->Derived::runImpl(scene);
	}
protected:
	vulkanutils::VulkanContext& getVulkanContext()
	{
		return mVulkanContext;
	}


private:
	vulkanutils::VulkanContext& mVulkanContext;
};


template<typename SceneType>
class SceneSystem
{
public:
	virtual void run(SceneType& scene) = 0;
};


template<typename SceneType, typename... Systems>
class SceneSystemT : public SceneSystem<SceneType>
{
public:
	SceneSystemT(SceneType& scene, Systems&&... systems)
		: mSystems{std::make_tuple<Systems...>(std::move(systems)...)}
	{}

	virtual void run(SceneType& scene) override
	{
		std::apply([&](auto& ...system)
		{
			(..., system.run(scene));
		}, mSystems);
	}

	template<typename SystemType>
	SystemType& get()
	{
		return std::get<SystemType>(mSystems);
	}

	template<typename SystemType>
	const SystemType& get() const
	{
		return std::get<SystemType>(mSystems);
	}

private:
	std::tuple<Systems...> mSystems;
};

};