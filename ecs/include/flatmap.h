#pragma once

#include <vector>
#include <tuple>


template<typename Key, typename Value>
class FlatMap
{
public:
	typedef Value ValueType;
	typedef Key KeyType;

	FlatMap();

	class Iterator
	{
	public:
		Iterator operator++();
		Iterator operator++(int);
		size_t operator-(Iterator rhs) const;
		bool operator==(Iterator rhs) const;

		std::tuple<Key, Value>& operator*();
		const std::tuple<Key, Value>& operator*() const;


	private:
		Iterator(std::tuple<Key, Value>* ptr);
		friend class FlatMap<Key, Value>;
		std::tuple<Key, Value>* mPtr;
	};

	class ConstIterator
	{
	public:
		ConstIterator operator++();
		ConstIterator operator++(int);
		size_t operator-(ConstIterator rhs) const;
		bool operator==(ConstIterator rhs) const;

		const std::tuple<Key, Value>& operator*() const;

	private:
		ConstIterator(const std::tuple<Key, Value>* ptr);
		friend class FlatMap<Key, Value>;
		const std::tuple<Key, Value>* mPtr;
	};

	Iterator begin();
	Iterator end();
	ConstIterator begin() const;
	ConstIterator end() const;

	Iterator find(const Key& key);
	ConstIterator find(const Key& key) const;

	void erase(Iterator it);
	void erase(ConstIterator it);
	void erase(const Key& key);

	bool insert(const std::tuple<Key, Value>& value);
	template<typename... Args>
	bool emplace(Args... args);

	Value& operator[](const Key& key);
	const Value& operator[](const Key& key) const;

	std::tuple<Key, Value>* data();
	const std::tuple<Key, Value>* data() const;


private:
	std::vector<std::tuple<Key, Value>> mData;
};



#include "flatmap.tpp"
