#pragma once

#include <chrono>
#include <string>
#include <map>
#include <thread>
#include <mutex>



class TimingStats
{
public:
	TimingStats();

	void queue(const char* tag, uint64_t start, uint64_t end);

	struct Stats
	{
		uint64_t minNs;
		uint64_t maxNs;
		uint64_t avgNs;
		uint64_t num;
	};
private:
	template<typename OStream>
	friend OStream& operator<< (OStream& stream, const class TimingTracer& tt);

	std::map<std::string, Stats> mStats;
};


class Tracer
{
public:
	Tracer(const char* tag);
	~Tracer();

	void stop();
private:
	std::chrono::system_clock::time_point mStart;
	const char* mTag;
};


class TimingTracer
{
public:
	static TimingTracer& GetInstance();

private:
	TimingTracer(){}
	TimingTracer(const TimingTracer&) = delete;
	TimingTracer(TimingTracer&&) = delete;

	void queue(const char* tag, uint64_t start, uint64_t end);

	friend class Tracer;
	
	template<typename OStream>
	friend OStream& operator<< (OStream& stream, const TimingTracer& tt);

	mutable std::mutex mLock;
	std::map<size_t, std::unique_ptr<TimingStats> > mStats;
};


template<typename OStream>
OStream& operator<< (OStream& stream, const TimingTracer& tt)
{
	stream << "====Timing Stats====\n";
	std::lock_guard lg(tt.mLock);
	for (const auto& [threadId, timingStats] : tt.mStats)
	{
		stream << "Thread " << threadId << "\n";
		for (auto& [tag, s] : timingStats->mStats)
		{
			stream << tag << "\n"
				<< "    min: " << s.minNs << "\n"
				<< "    max: " << s.maxNs << "\n"
				<< "    avg: " << s.avgNs << "\n"
				<< "    num: " << s.num << "\n";
		}
	}
	stream << "=====================\n";
	return stream;
}
