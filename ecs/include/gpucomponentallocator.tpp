#pragma once

#include "gpucomponentallocator.h"

namespace ecs
{
template<typename BufferType>
GpuComponentAllocator<BufferType>::GpuComponentAllocator(vulkanutils::Device* device
		, size_t chunkSize)
		: vulkanutils::BufferAllocator<BufferType>(device, chunkSize)
{
}

};
