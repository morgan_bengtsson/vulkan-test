#pragma once

#include "gpucomponentallocator.h"

#include <vector>

namespace ecs
{

class GpuImageComponentStorage
{
public:
	GpuImageComponentStorage(vulkanutils::Device* device);
	GpuImageComponentStorage(const GpuImageComponentStorage&) = delete;
	GpuImageComponentStorage& operator=(const GpuImageComponentStorage&) = delete;
	GpuImageComponentStorage(GpuImageComponentStorage&& a);
	GpuImageComponentStorage& operator=(GpuImageComponentStorage&& a);

	size_t size() const;
	template<typename ImageType, typename... Args>
	void addImage(Args... args);
	vulkanutils::ImageAllocation& getAllocation(size_t idx);
	const vulkanutils::ImageAllocation& getAllocation(size_t idx) const;

private:
	GpuImageComponentAllocator mAllocator;
	std::vector<vulkanutils::ImageAllocation> mAllocations;
};


template<typename... T>
class GpuComponentStorage;

template<typename BufferType>
class GpuComponentStorage<BufferType>
{
public:
	GpuComponentStorage(vulkanutils::Device* device);

	GpuComponentStorage(const GpuComponentStorage&) = delete;
	GpuComponentStorage& operator=(const GpuComponentStorage&) = delete;
	GpuComponentStorage(GpuComponentStorage&& a) = default;
	GpuComponentStorage& operator=(GpuComponentStorage&& a) = default;

	size_t size() const;
	vulkanutils::BufferAllocation<BufferType>& addItemArray(size_t size, size_t count);
	vulkanutils::BufferAllocation<BufferType>& getAllocation(size_t idx);
	const vulkanutils::BufferAllocation<BufferType>& getAllocation(size_t idx) const;
	void removeAllocation(size_t idx);

private:
	GpuComponentAllocator<BufferType> mAllocator;
	std::vector<vulkanutils::BufferAllocation<BufferType> > mAllocations;
};

template<typename ComponentType, typename BufferType>
class GpuComponentStorage<ComponentType, BufferType> : public GpuComponentStorage<BufferType>
{
public:
	GpuComponentStorage(vulkanutils::Device* device);

	GpuComponentStorage(const GpuComponentStorage&) = delete;
	GpuComponentStorage& operator=(const GpuComponentStorage&) = delete;
	GpuComponentStorage(GpuComponentStorage&& a) = default;
	GpuComponentStorage& operator=(GpuComponentStorage&& a) = default;
	vulkanutils::BufferAllocation<BufferType>& addItemArray(size_t count);
};

};

#include "gpucomponentstorage.tpp"
