#pragma once

#include <type_traits>
#include <vector>
#include <tuple>

namespace comm
{

template<typename Message>
class Listener;

template<typename... Messages>
class Dispatcher
{
public:
	template<typename Message> requires (std::is_same_v<Message, Messages> || ...)
	void attach(Listener<Message>* l)
	{
		getListeners<Message>().push_back(l);
	}

	template<typename Message>
	void detach(Listener<Message>* l);

	template<typename Message>
	void dispatch(const Message& message) const;

private:
	


	template<typename Message>
	class ListenerList
	{
	public:
		std::vector<Listener<Message>* >& getListeners()
		{
			return mListeners;
		};
		const std::vector<Listener<Message>* >& getListeners() const
		{
			return mListeners;
		};
	private:
		std::vector<Listener<Message>* > mListeners;
	};
	template<typename M>
	std::vector<Listener<M>* >& getListeners() { return std::get<ListenerList<M>>(mListenerLists).getListeners(); };
	template<typename M>
	const std::vector<Listener<M>* >& getListeners() const { return std::get<ListenerList<M>>(mListenerLists).getListeners(); };

	std::tuple<ListenerList<Messages>...> mListenerLists;
};


template<typename... Messages>
template<typename Message>
void Dispatcher<Messages...>::dispatch(const Message& message) const
{
	const auto& llist = getListeners<Message>();
	for (auto listener : llist)
	{
		listener->execute(message);
	}
}


template<typename... Messages>
template<typename Message>
void Dispatcher<Messages...>::detach(Listener<Message>* l)
{
	auto& llist = getListeners<Message>();
	auto it = std::find(llist.begin(), llist.end(), l);
	if (it != llist.end())
		llist.erase(it);
}

}
