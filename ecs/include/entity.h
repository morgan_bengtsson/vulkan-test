#pragma once

#include "component.h"
#include "componentstorage.h"
#include "gpucomponentstorage.h"
#include "vulkanGraphicsPipeline.h"

#include "flatmap.h"

#include <Eigen/Dense>
#include <map>
#include <optional>
#include <vector>
#include <span>
#include <tuple>
#include <set>

namespace ecs
{

struct EntityRef
{
	size_t id;
	enum class Type
	{
		Parent,
		Child,
		Link,
	} type;
};


template<typename ComponentType, typename BufferType>
struct BufferAllocationTypeDef;

template<typename ComponentType, typename BufferType>
struct BufferAllocationTypeDef
{
	typedef vulkanutils::BufferAllocation<BufferType>& type;
};

template<typename ComponentType>
struct BufferAllocationTypeDef<ComponentType, vulkanutils::UploadBuffer>
{
	typedef vulkanutils::TypedBufferAllocation<ComponentType> type;
};

template <typename ComponentType, typename BufferType>
using BufferAllocationType = typename BufferAllocationTypeDef<ComponentType, BufferType>::type;



class Entity
{
public:
	Entity(const char* name = "unnamed");
	Entity(const Entity& e);
	size_t getId() const;

	template<typename ComponentType, typename CCType>
	bool pendingDelete(const CCType& components) const;
	template<typename BufferType, typename ComponentType, typename CCType>
	bool pendingGpuDelete(const CCType& components) const;


	void setName(const char* name);
	const char* getName() const;

	bool hasComponentIds(size_t ids) const;
	template<typename... ComponentTypes, typename CCType>
	bool hasComponents(const CCType& cc) const;

	bool getComponentIds() const;

	template<typename ComponentType, typename CCType>
	ComponentType* addComponentArray(CCType& components, size_t count);
	
	template<typename ComponentType, typename CCType>
	ComponentType& addComponent(CCType& components);
	template<typename ComponentType, typename CCType>
	ComponentType& addComponent(CCType& components, const ComponentType& componentObject);
	template<typename ComponentType, typename CCType>
	ComponentType& addComponent(CCType& components, ComponentType&& componentObject);

	template<typename... ComponentTypes, typename CCType>
	std::tuple<ComponentTypes&...> addComponents(CCType& components);
	template<typename... ComponentTypes, typename CCType>
	std::tuple<ComponentTypes&...> addComponents(CCType& components, const ComponentTypes&... componentObjects);
	template<typename... ComponentTypes, typename CCType>
	std::tuple<ComponentTypes&...> addComponent(CCType& components, ComponentTypes&&... componentObjects);

	template<typename ComponentType, typename CCType>
	std::span<const ComponentType> getComponentArray(CCType& components) const;
	template<typename ComponentType, typename CCType>
	std::span<ComponentType> getComponentArray(CCType& components);

	template<typename ComponentType, typename CCType>
	const ComponentType& getComponent(CCType& components) const;
	template<typename ComponentType, typename CCType>
	ComponentType& getComponent(CCType& components);


	template<typename... ComponentTypes, typename CCType>
	std::tuple<std::span<const ComponentTypes>...> getComponentArrays(CCType& components) const;
	template<typename... ComponentTypes, typename CCType>
	std::tuple<std::span<ComponentTypes>...> getComponentArrays(CCType& components);
	template<typename... ComponentTypes, typename CCType>
	std::tuple<ComponentTypes&...> getComponents(CCType& components);
	template<typename... ComponentTypes, typename CCType>
	std::tuple<const ComponentTypes&...> getComponents(CCType& components) const;



	template<typename BufferType>
	bool hasGpuComponentIds(size_t ids = (size_t) -1) const;
	template<typename BufferType, typename... ComponentTypes, typename CCType>
	bool hasGpuComponents(const CCType& cc) const;

	template<typename BufferType>
	bool getGpuComponentIds() const;

	template<typename ComponentType, typename CCType, typename... Args>
	vulkanutils::ImageAllocation& addImage(CCType& components, Args... args);

	template<typename ComponentType, typename CCType>
	vulkanutils::ImageAllocation& getImage(CCType& components, size_t idx = 0);
	template<typename ComponentType, typename CCType>
	const vulkanutils::ImageAllocation& getImage(const CCType& components, size_t idx = 0) const;

	template<typename ComponentType, typename CCType>
	std::vector<vulkanutils::ImageAllocation*> getImages(CCType& components);
	template<typename ComponentType, typename CCType>
	std::vector<const vulkanutils::ImageAllocation*> getImages(const CCType& components) const;

	bool hasImageComponentIds(size_t ids) const;
	template<typename... ComponentTypes, typename CCType>
	bool hasImageComponents(const CCType& cc) const;




	/*
	template<typename ComponentType, typename... BufferTypes, typename CCType>
	std::tuple<BufferAllocationType<ComponentType, BufferTypes>...> addGpuComponentArrays(CCType& components, size_t count);
	template<typename ComponentType, typename... BufferTypes, typename CCType>
	std::tuple<BufferAllocationType<ComponentType, BufferTypes>...> getGpuComponentArrays(CCType& components);
	*/

	template<typename BufferType, typename ComponentType, typename CCType>
	vulkanutils::BufferAllocation<BufferType>& addGpuComponentArray(CCType& components, size_t count);
	template<typename ComponentType, typename CCType>
	vulkanutils::TypedBufferAllocation<ComponentType> addHostGpuComponentArray(CCType& components, size_t count);

	template<typename BufferType, typename ComponentType, typename CCType>
	std::vector<vulkanutils::BufferAllocation<BufferType>*> addGpuComponentArrays(CCType& components, std::vector<size_t> counts);
	template<typename ComponentType, typename CCType>
	std::vector<vulkanutils::TypedBufferAllocation<ComponentType>> addHostGpuComponentArrays(CCType& components, std::vector<size_t> counts);

	template<typename BufferType, typename ComponentType, typename CCType>
	const vulkanutils::BufferAllocation<BufferType>& getGpuComponentArray(CCType& components, size_t idx = 0) const;
	template<typename BufferType, typename ComponentType, typename CCType>
	vulkanutils::BufferAllocation<BufferType>& getGpuComponentArray(CCType& components, size_t idx = 0);
	template<typename BufferType, typename ComponentType, typename CCType>
	std::vector<vulkanutils::BufferAllocation<BufferType>*> getGpuComponentArrays(CCType& components);

	template<typename ComponentType, typename CCType>
	vulkanutils::TypedBufferAllocation<ComponentType> getHostGpuComponentArray(CCType& components, size_t idx = 0);
	template<typename ComponentType, typename CCType>
	std::vector<vulkanutils::TypedBufferAllocation<ComponentType>> getHostGpuComponentArrays(CCType& components);


	template<typename BufferType, typename CCType>
	const vulkanutils::BufferAllocation<BufferType>& getGpuComponentArray(CCType& components, size_t componentTypeId, size_t idx = 0) const;
	template<typename BufferType, typename CCType>
	vulkanutils::BufferAllocation<BufferType>& getGpuComponentArray(CCType& components, size_t componentTypeId, size_t idx = 0);

	
	template<typename BufferType, typename CCType>
	std::vector<const vulkanutils::BufferAllocation<BufferType>*> getGpuComponentArrays(CCType& components, size_t componentTypeId) const;
	template<typename BufferType, typename CCType>
	std::vector<vulkanutils::BufferAllocation<BufferType>*> getGpuComponentArrays(CCType& components, size_t componentTypeId);

	template<typename CCType>
	void removeAllComponents(CCType& components);
	template<typename CCType>
	void removeAllGpuComponents(CCType& components);

	template<typename CCType>
	void removeComponent(CCType& components, size_t componentId);
	template<typename ComponentType, typename CCType>
	void removeComponent(CCType& components);

	template<typename BufferType, typename CCType>
	void removeGpuComponent(CCType& components, size_t componentId);
	template<typename ComponentType, typename BufferType, typename CCType>
	void removeGpuComponent(CCType& components);


	void queueRemoveComponent(size_t componentId);
	template<typename ComponentType, typename CCType>
	void queueRemoveComponent(CCType& components);

	template<typename BufferType>
	void queueRemoveGpuComponent(size_t componentId);
	template<typename ComponentType, typename BufferType, typename CCType>
	void queueRemoveGpuComponent(CCType& components);

	void queueRemoveAllComponents();
	void queueRemoveAllGpuComponents();

	template<typename CCType>
	void updateComponentDeletion(CCType& components);
	template<typename CCType>
	void updateGpuComponentDeletion(CCType& components);

	void setModified();
	void setUnmodified() const;
	bool isModified() const;

	void debugBreakOn(const char* name) const;
private:
	template<typename BufferType, typename CCType>
	void removeAllGpuComponents(CCType& components);
	template<typename BufferType>
	void queueRemoveAllGpuComponents();

	size_t mId;
	std::string mName;
	mutable bool mModified;
	size_t mComponentIds;

	std::set<size_t> mQueuedComponentDeletions;

	struct ActiveDeletion
	{
		size_t componentId;
		size_t refCount;
	};
	std::vector<ActiveDeletion> mActiveComponentDeletions;

	FlatMap<size_t, std::vector<size_t> > mImageIdx;

	struct ComponentReference
	{
		size_t idx;
		size_t count;
	};
	
	template<typename BufferType>
	struct GpuBufferComponentIds
	{
		size_t componentIds;
		FlatMap<size_t, std::vector<size_t> > idx;
		std::set<size_t> queuedComponentDeletions;
		std::vector<ActiveDeletion> activeComponentDeletions;
	};
	
	std::tuple<GpuBufferComponentIds<vulkanutils::UploadBuffer>,
		GpuBufferComponentIds<vulkanutils::VertexBuffer>,
		GpuBufferComponentIds<vulkanutils::IndexBuffer>,
		GpuBufferComponentIds<vulkanutils::UniformBuffer>,
		GpuBufferComponentIds<vulkanutils::ShaderStorageBuffer>,
		GpuBufferComponentIds<vulkanutils::IndirectDrawBuffer>>
		mGpuComponentIds;
	
	FlatMap<size_t, ComponentReference > mIdx;
	size_t mImageComponentIds;
	static inline size_t NUM_ENTITIES = 0;
};

template <typename CCType>
void cloneComponentsHelper(Entity& dst, const Entity& src, CCType& components);
template <typename CCType, typename ComponentType, typename... CTs>
void cloneComponentsHelper(Entity& dst, const Entity& src, CCType& components);
template <typename... CTs, typename CCType>
void cloneComponents(Entity& dst, const Entity& src, CCType& components);

};

#include "entity.tpp"
