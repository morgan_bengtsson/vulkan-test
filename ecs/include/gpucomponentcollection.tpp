#pragma once

#include "gpucomponentcollection.h"

#include <vulkanBufferAllocator.h>

namespace ecs
{

template<typename T, typename T2>
T2 ArrayInit(T2 t)
{
	return t;
};

template<Component... ComponentTypes>
GpuComponentCollection<ComponentTypes...>::GpuComponentCollection(vulkanutils::Device* device)
	: mComponentArrays(device)
	, mImageStorage{ std::move(ArrayInit<ComponentTypes, decltype(device)>(device))... }
{
}

template<Component... ComponentTypes>
GpuComponentCollection<ComponentTypes...>::~GpuComponentCollection()
{
}

template<Component... ComponentTypes>
constexpr size_t GpuComponentCollection<ComponentTypes...>::numComponentTypes()
{
	return sizeof...(ComponentTypes);
}

template<Component... ComponentTypes>
template<typename ComponentType, typename BufferType>
GpuComponentStorage<ComponentType, BufferType>& GpuComponentCollection<ComponentTypes...>::getComponentStorage()
	requires IsMember<ComponentType>
{
	return mComponentArrays.getArray<ComponentType, BufferType>();
}

template<Component... ComponentTypes>
template<typename ComponentType, typename BufferType>
const GpuComponentStorage<ComponentType, BufferType>& GpuComponentCollection<ComponentTypes...>::getComponentStorage() const
	requires IsMember<ComponentType>
{
	return mComponentArrays.getArray<ComponentType, BufferType>();
}

template<Component... ComponentTypes>
template<typename BufferType>
GpuComponentStorage<BufferType>& GpuComponentCollection<ComponentTypes...>::getComponentStorage(size_t compTypeId)
{
	return mComponentArrays.getArray<BufferType>(compTypeId);
}

template<Component... ComponentTypes>
template<typename BufferType>
const GpuComponentStorage<BufferType>& GpuComponentCollection<ComponentTypes...>::getComponentStorage(size_t compTypeId) const
{
	return mComponentArrays.getArray<BufferType>(compTypeId);
}

template<Component... ComponentTypes>
template<typename ComponentType>
GpuImageComponentStorage& GpuComponentCollection<ComponentTypes...>::getImageComponentStorage() requires IsMember<ComponentType>
{
	return mImageStorage[getComponentTypeIndex<ComponentType>()];
}

template<Component... ComponentTypes>
template<typename ComponentType>
const GpuImageComponentStorage& GpuComponentCollection<ComponentTypes...>::getImageComponentStorage() const requires IsMember<ComponentType>
{
	return mImageStorage[getComponentTypeIndex<ComponentType>()];
}

template<Component... ComponentTypes>
GpuImageComponentStorage& GpuComponentCollection<ComponentTypes...>::getImageComponentStorage(size_t compTypeId)
{
	return mImageStorage[compTypeId];
}

template<Component... ComponentTypes>
template<typename BufferType>
const GpuImageComponentStorage& GpuComponentCollection<ComponentTypes...>::getImageComponentStorage(size_t compTypeId) const
{
	return mImageStorage[compTypeId];
}

template<Component... ComponentTypes>
template<typename ComponentType>
constexpr size_t GpuComponentCollection<ComponentTypes...>::getComponentTypeIndex()
	requires IsMember<ComponentType>
{
	return getTypeIndex<ComponentType, ComponentTypes...>();
}

template<Component... ComponentTypes>
template<typename... BufferTypes>
GpuComponentCollection<ComponentTypes...>::GpuBufferAllocators<BufferTypes...>::GpuBufferAllocators(vulkanutils::Device* device)
	: mAllocators{GpuComponentAllocator<BufferTypes>(device, 65536)...}
{}

template<Component... ComponentTypes>
template<typename... BufferTypes>
template<typename BufferType>
GpuComponentAllocator<BufferType>& GpuComponentCollection<ComponentTypes...>::GpuBufferAllocators<BufferTypes...>::getAllocator()
{
	return std::get<GpuComponentAllocator<BufferType>>(mAllocators);
}

template<Component... ComponentTypes>
template<typename... BufferTypes>
GpuComponentCollection<ComponentTypes...>::ComponentArrays<BufferTypes...>::ComponentArrays(vulkanutils::Device* device)
	: mComponentArrays(std::move(BufferComponentArrays<BufferTypes>(device))...)
{}

template<Component... ComponentTypes>
template<typename... BufferTypes>
template<typename BufferType>
GpuComponentStorage<BufferType>& GpuComponentCollection<ComponentTypes...>::ComponentArrays<BufferTypes...>::getArray(size_t compTypeId)
{
	return std::get<BufferComponentArrays<BufferType>>(mComponentArrays).getArray(compTypeId);
}

template<Component... ComponentTypes>
template<typename... BufferTypes>
template<typename ComponentType, typename BufferType>
GpuComponentStorage<ComponentType, BufferType>& GpuComponentCollection<ComponentTypes...>::ComponentArrays<BufferTypes...>::getArray()
{
	return std::get<BufferComponentArrays<BufferType>>(mComponentArrays).getArray<ComponentType>();
}

template<Component... ComponentTypes>
template<typename... BufferTypes>
template<typename BufferType>
GpuComponentCollection<ComponentTypes...>::ComponentArrays<BufferTypes...>::BufferComponentArrays<BufferType>::BufferComponentArrays(vulkanutils::Device* device)
	: mBufferComponentArrays(GpuComponentStorage<ComponentTypes, BufferType>(device)...)
	, mPtrArray()
{};

template<Component... ComponentTypes>
template<typename... BufferTypes>
template<typename BufferType>
template<typename ComponentType>
GpuComponentStorage<ComponentType, BufferType>& GpuComponentCollection<ComponentTypes...>::ComponentArrays<BufferTypes...>::BufferComponentArrays<BufferType>::getArray()
{
	return std::get<GpuComponentStorage<ComponentType, BufferType>>(mBufferComponentArrays);
};

template<Component... ComponentTypes>
template<typename... BufferTypes>
template<typename BufferType>
GpuComponentStorage<BufferType>& GpuComponentCollection<ComponentTypes...>::ComponentArrays<BufferTypes...>::BufferComponentArrays<BufferType>::getArray(size_t idx)
{
	return *mPtrArray[idx];
}

};