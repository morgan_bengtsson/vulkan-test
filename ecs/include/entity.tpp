#pragma once

#include <iostream>
#include <tuple>

#include "componentstorage.h"

namespace ecs
{

template<typename BufferType>
bool Entity::hasGpuComponentIds(size_t ids) const
{
	const GpuBufferComponentIds<BufferType>& bufferIds = std::get<GpuBufferComponentIds<BufferType>>(mGpuComponentIds);
	return (ids & bufferIds.componentIds) == ids;
}

template<typename... ComponentTypes, typename CCType>
bool Entity::hasComponents(const CCType& cc) const
{
	return hasComponentIds(((size_t(1) << cc.template getComponentTypeIndex<ComponentTypes>()) | ...));
}

template<typename BufferType, typename... ComponentTypes, typename CCType>
bool Entity::hasGpuComponents(const CCType& cc) const
{
	return hasGpuComponentIds<BufferType>(((size_t(1) << cc.template getComponentTypeIndex<ComponentTypes>()) | ...));
}

template<typename BufferType>
bool Entity::getGpuComponentIds() const
{
	const GpuBufferComponentIds<BufferType>& bufferIds = std::get<GpuBufferComponentIds<BufferType>>(mGpuComponentIds);
	return bufferIds.componentIds;
}

template<typename CCType>
void Entity::removeComponent(CCType& components, size_t componentId)
{
	auto it = mIdx.find(componentId);
	components.removeComponent(componentId, std::get<1>(*it).idx, std::get<1>(*it).count);
	mIdx.erase(it);
	mComponentIds = (mComponentIds & ~componentId);
}

template<typename ComponentType, typename CCType>
void Entity::removeComponent(CCType& components)
{
	constexpr size_t componentId = CCType::template getComponentTypeIndex<ComponentType>();
	auto it = mIdx.find(componentId);
	components.removeComponent<ComponentType>(std::get<1>(*it).idx, std::get<1>(*it).count);
	mIdx.erase(it);
	mComponentIds = (mComponentIds & ~componentId);
}

template<typename CCType>
void Entity::removeAllComponents(CCType& components)
{
	for (auto& [componentId, componentReference ] : mIdx)
	{
		components.removeComponent(componentId, componentReference.idx, componentReference.count);
	}
}

template<typename BufferType, typename CCType>
void Entity::removeAllGpuComponents(CCType& components)
{
	auto& bufferIds = std::get<GpuBufferComponentIds<BufferType>>(mGpuComponentIds);
	for (auto& [componentId, offsets ] : bufferIds.idx)
	{
		auto& componentStorage = components.getComponentStorage<BufferType>(componentId);
		for (size_t offset : offsets)
			componentStorage.removeAllocation(offset);
	}
	bufferIds.idx.clear();
	bufferIds.componentIds = 0;
};

template<typename CCType>
void Entity::removeAllGpuComponents(CCType& components)
{
	removeAllGpuComponents<vulkanutils::UploadBuffer>(components);
	removeAllGpuComponents<vulkanutils::VertexBuffer>(components);
	removeAllGpuComponents<vulkanutils::IndexBuffer>(components);
	removeAllGpuComponents<vulkanutils::UniformBuffer>(components);
	removeAllGpuComponents<vulkanutils::ShaderStorageBuffer>(components);
	removeAllGpuComponents<vulkanutils::IndirectDrawBuffer>(components);
}

template<typename ComponentType, typename CCType>
ComponentType* Entity::addComponentArray(CCType& components, size_t count)
{
	const size_t componentId = components.template getComponentTypeIndex<ComponentType>();
	ComponentStorage<ComponentType>& ca = components.template getComponentStorage<ComponentType>();
	mComponentIds |= (size_t(1) << componentId);
	ComponentReference& cRef = mIdx[componentId];
	if (cRef.count > 0)
	{
		if (ca.resizeRange(ca.it(cRef.idx), cRef.count, cRef.count + count))
			return &ca[cRef.idx];
		size_t oldCount = cRef.count;
		auto [startIt, endIt] = ca.emplace(ComponentType(), count + oldCount);
		auto oldStartIt = ca.it(cRef.idx);
		auto oldEndIt = ca.it(cRef.idx + cRef.count);
		std::move(&*oldStartIt, (&*oldStartIt) + oldCount, &*startIt);
		for (auto it = oldStartIt; it != oldEndIt;)
			it = ca.erase(it);
		cRef.idx = startIt.getIdx();
		cRef.count = count + oldCount;
		return &*startIt + oldCount;
	}
	auto [startIt, endIt] = ca.emplace(ComponentType(), count);
	cRef.idx = startIt.getIdx();
	cRef.count = count;
	return &*startIt;
}

template<typename ComponentType, typename CCType, typename... Args>
vulkanutils::ImageAllocation& Entity::addImage(CCType& components, Args... args)
{
	const size_t componentId = components.template getComponentTypeIndex<ComponentType>();
	GpuImageComponentStorage& imageStorage = components.template getImageComponentStorage<ComponentType>();
	size_t idx = imageStorage.size();
	mImageIdx[componentId].push_back(idx);
	imageStorage.addImage<vulkanutils::Image>(args...);
	mImageComponentIds |= (size_t(1) << componentId);
	return imageStorage.getAllocation(idx);
}

template<typename ComponentType, typename CCType>
vulkanutils::ImageAllocation& Entity::getImage(CCType& components, size_t idx)
{
	const size_t componentId = components.template getComponentTypeIndex<ComponentType>();
	GpuImageComponentStorage& imageStorage = components.template getImageComponentStorage<ComponentType>();

	return imageStorage.getAllocation(mImageIdx[componentId][idx]);
}

template<typename ComponentType, typename CCType>
const vulkanutils::ImageAllocation& Entity::getImage(const CCType& components, size_t idx) const
{
	const size_t componentId = components.template getComponentTypeIndex<ComponentType>();
	const GpuImageComponentStorage& imageStorage = components.template getImageComponentStorage<ComponentType>();

	return imageStorage.getAllocation(mImageIdx.find(componentId)->second[idx]);
}

template<typename ComponentType, typename CCType>
std::vector<vulkanutils::ImageAllocation*> Entity::getImages(CCType& components)
{
	const size_t componentId = components.template getComponentTypeIndex<ComponentType>();
	GpuImageComponentStorage& imageStorage = components.template getImageComponentStorage<ComponentType>();

	std::vector<vulkanutils::ImageAllocation*> ret;
	for (size_t idx : mImageIdx[componentId])
		ret.push_back(&imageStorage.getAllocation(idx));
	return ret;
}

template<typename ComponentType, typename CCType>
std::vector<const vulkanutils::ImageAllocation*> Entity::getImages(const CCType& components) const
{
	const size_t componentId = components.template getComponentTypeIndex<ComponentType>();
	const GpuImageComponentStorage& imageStorage = components.template getImageComponentStorage<ComponentType>();

	std::vector<vulkanutils::ImageAllocation*> ret;
	for (size_t idx : mImageIdx.find(componentId)->second)
		ret.push_back(&imageStorage.getAllocation(idx));
	return ret;
}

template<typename... ComponentTypes, typename CCType>
bool Entity::hasImageComponents(const CCType& cc) const
{
	return hasImageComponentIds(((size_t(1) << cc.template getComponentTypeIndex<ComponentTypes>()) | ...));
}


template<typename BufferType, typename ComponentType, typename CCType>
vulkanutils::BufferAllocation<BufferType>& Entity::addGpuComponentArray(CCType& components, size_t count)
{
	const size_t componentId = components.template getComponentTypeIndex<ComponentType>();
	GpuComponentStorage<ComponentType, BufferType>& ca = components.template getComponentStorage<ComponentType, BufferType>();
		
	GpuBufferComponentIds<BufferType>& bufferIds = std::get<GpuBufferComponentIds<BufferType>>(mGpuComponentIds);
	bufferIds.componentIds |= (size_t(1) << componentId);
	size_t idx = ca.size();
	bufferIds.idx[componentId].push_back(idx);
	ca.addItemArray(count);
	return ca.getAllocation(idx);
}

template<typename ComponentType, typename CCType>
vulkanutils::TypedBufferAllocation<ComponentType> Entity::addHostGpuComponentArray(CCType& components, size_t count)
{
	return vulkanutils::TypedBufferAllocation<ComponentType>(addGpuComponentArray<vulkanutils::UploadBuffer, ComponentType>(components, count));
}


template<typename BufferType, typename ComponentType, typename CCType>
std::vector<vulkanutils::BufferAllocation<BufferType>*> Entity::addGpuComponentArrays(CCType& components, std::vector<size_t> counts)
{
	const size_t componentId = components.template getComponentTypeIndex<ComponentType>();
	GpuComponentStorage<ComponentType, BufferType>& ca = components.template getComponentStorage<ComponentType, BufferType>();
	GpuBufferComponentIds<BufferType>& bufferIds = std::get<GpuBufferComponentIds<BufferType>>(mGpuComponentIds);
	bufferIds.componentIds |= (size_t(1) << componentId);
	auto& bufferIdsIdx = bufferIds.idx[componentId];
	bufferIdsIdx.reserve(bufferIdsIdx.size() + counts.size());

	size_t idx = ca.size();
	size_t startIdx = idx;
	for (size_t c : counts)
	{
		bufferIdsIdx.push_back(idx++);
		ca.addItemArray(c);
	}
	std::vector<vulkanutils::BufferAllocation<BufferType>*> ret;
	ret.reserve(counts.size());
	for (size_t i = startIdx; i < ca.size(); i++)
		ret.push_back(&ca.getAllocation(i));
	return ret;
}

template<typename ComponentType, typename CCType>
std::vector<vulkanutils::TypedBufferAllocation<ComponentType>> Entity::addHostGpuComponentArrays(CCType& components, std::vector<size_t> counts)
{
	std::vector<vulkanutils::BufferAllocation<vulkanutils::UploadBuffer>*> arrays = addGpuComponentArrays<vulkanutils::UploadBuffer, ComponentType>(components, counts);
	std::vector<vulkanutils::TypedBufferAllocation<ComponentType>> ret;
	ret.reserve(arrays.size());
	for (auto& a : arrays)
		ret.emplace_back(*a);
	return ret;
}

template<typename ComponentType, typename CCType>
ComponentType& Entity::addComponent(CCType& components)
{
	return *addComponentArray<ComponentType, CCType>(components, 1) = ComponentType();
}

template<typename ComponentType, typename CCType>
ComponentType& Entity::addComponent(CCType& components, const ComponentType& componentObject)
{
	return *addComponentArray<ComponentType, CCType>(components, 1) = componentObject;
}

template<typename ComponentType, typename CCType>
ComponentType& Entity::addComponent(CCType& components, ComponentType&& componentObject)
{
	return *addComponentArray<ComponentType, CCType>(components, 1) = std::move(componentObject);
}


template<typename... ComponentTypes, typename CCType>
std::tuple<ComponentTypes&...> Entity::addComponents(CCType& components)
{
	return { addComponent<ComponentTypes>(components)... };
}

template<typename... ComponentTypes, typename CCType>
std::tuple<ComponentTypes&...> Entity::addComponents(CCType& components, const ComponentTypes&... componentObjects)
{
	return { addComponent<ComponentTypes>(components, componentObjects)... };
}

template<typename... ComponentTypes, typename CCType>
std::tuple<ComponentTypes&...> Entity::addComponent(CCType& components, ComponentTypes&&... componentObjects)
{
	return { addComponent<ComponentTypes>(components, componentObjects)... };
}

template<typename ComponentType, typename CCType>
ComponentType& Entity::getComponent(CCType& components)
{
	ComponentStorage<ComponentType>& componentStorage = components.template getComponentStorage<ComponentType>();
	const FlatMap<size_t, ComponentReference >::Iterator it = mIdx.find(componentStorage.typeIdx());
	return componentStorage[std::get<1>(*it).idx];
}

template<typename ComponentType, typename CCType>
const ComponentType& Entity::getComponent(CCType& components) const
{
	const ComponentStorage<ComponentType>& componentStorage = components.template getComponentStorage<ComponentType>();
	const FlatMap<size_t, ComponentReference >::ConstIterator it = mIdx.find(componentStorage.typeIdx());
	return componentStorage[std::get<1>(*it).idx];
}

template<typename ComponentType, typename CCType>
std::span<const ComponentType> Entity::getComponentArray(CCType& components) const
{
	const ComponentStorage<ComponentType>& componentStorage = components.template getComponentStorage<ComponentType>();
	const FlatMap<size_t, ComponentReference >::ConstIterator it = mIdx.find(componentStorage.typeIdx());
	if (it == mIdx.end())
		return {};
	const ComponentReference& cmpRef = std::get<1>(*it);
	return { &componentStorage[cmpRef.idx], cmpRef.count };
}

template<typename ComponentType, typename CCType>
std::span<ComponentType> Entity::getComponentArray(CCType& components)
{
	ComponentStorage<ComponentType>& componentStorage = components.template getComponentStorage<ComponentType>();
	const FlatMap<size_t, ComponentReference >::Iterator it = mIdx.find(componentStorage.typeIdx());
	if (it == mIdx.end())
		return {};
	const ComponentReference& cmpRef = std::get<1>(*it);
	return { &componentStorage[cmpRef.idx], cmpRef.count };
}

template<typename... ComponentTypes, typename CCType>
std::tuple<std::span<const ComponentTypes>...> Entity::getComponentArrays(CCType& components) const
{
	return { getComponentArray<ComponentTypes>(components)... };
}

template<typename... ComponentTypes, typename CCType>
std::tuple<std::span<ComponentTypes>...> Entity::getComponentArrays(CCType& components)
{
	return { getComponentArray<ComponentTypes>(components)... };
}

template<typename... ComponentTypes, typename CCType>
std::tuple<ComponentTypes&...> Entity::getComponents(CCType& components)
{
	return { getComponent<ComponentTypes>(components)... };
}

template<typename... ComponentTypes, typename CCType>
std::tuple<const ComponentTypes&...> Entity::getComponents(CCType& components) const
{
	return { getComponentArray<ComponentTypes>(components)... };
}

template<typename BufferType, typename ComponentType, typename CCType>
const vulkanutils::BufferAllocation<BufferType>& Entity::getGpuComponentArray(CCType& components, size_t idx) const
{
	const size_t componentId = components.template getComponentTypeIndex<ComponentType>();
	const GpuBufferComponentIds<BufferType>& bufferIds = std::get<GpuBufferComponentIds<BufferType>>(mGpuComponentIds);
	auto it = bufferIds.idx.find(componentId);
	//if (it == bufferIds.idx.end())
	//	*(int*) NULL = 0;
	return components.template getComponentStorage<ComponentType, BufferType>().getAllocation(std::get<1>(*it)[idx]);
}

template<typename BufferType, typename ComponentType, typename CCType>
vulkanutils::BufferAllocation<BufferType>& Entity::getGpuComponentArray(CCType& components, size_t idx)
{
	const size_t componentId = components.template getComponentTypeIndex<ComponentType>();
	const GpuBufferComponentIds<BufferType>& bufferIds = std::get<GpuBufferComponentIds<BufferType>>(mGpuComponentIds);
	auto it = bufferIds.idx.find(componentId);
	//if (it == bufferIds.idx.end())
	//	*(int*) NULL = 0;
	return components.template getComponentStorage<ComponentType, BufferType>().getAllocation(std::get<1>(*it)[idx]);
}

template<typename BufferType, typename ComponentType, typename CCType>
std::vector<vulkanutils::BufferAllocation<BufferType>*> Entity::getGpuComponentArrays(CCType& components)
{
	const size_t componentId = components.template getComponentTypeIndex<ComponentType>();
	const GpuBufferComponentIds<BufferType>& bufferIds = std::get<GpuBufferComponentIds<BufferType>>(mGpuComponentIds);
	auto it = bufferIds.idx.find(componentId);
	//if (it == bufferIds.idx.end())
	//	*(int*) NULL = 0;
	const std::vector<size_t>& indices = std::get<1>(*it);

	std::vector<vulkanutils::BufferAllocation<BufferType>*> ret;
	ret.reserve(indices.size());
	for (size_t i : indices)
		ret.push_back(&components.template getComponentStorage<ComponentType, BufferType>().getAllocation(i));
	return ret;
}

template<typename ComponentType, typename CCType>
vulkanutils::TypedBufferAllocation<ComponentType> Entity::getHostGpuComponentArray(CCType& components, size_t idx)
{
	return vulkanutils::TypedBufferAllocation<ComponentType>(getGpuComponentArray<vulkanutils::UploadBuffer, ComponentType>(components, idx));
}

template<typename ComponentType, typename CCType>
std::vector<vulkanutils::TypedBufferAllocation<ComponentType>> Entity::getHostGpuComponentArrays(CCType& components)
{
	std::vector<vulkanutils::BufferAllocation<vulkanutils::UploadBuffer>*> arrays = getGpuComponentArrays<vulkanutils::UploadBuffer, ComponentType>(components);
	std::vector<vulkanutils::TypedBufferAllocation<ComponentType>> ret;
	ret.reserve(arrays.size());
	for (auto& a : arrays)
		ret.emplace_back(*a);
	return ret;
}

template<typename BufferType, typename CCType>
const vulkanutils::BufferAllocation<BufferType>& Entity::getGpuComponentArray(CCType& components, size_t componentTypeId, size_t idx) const
{
	size_t componentId = 0;
	_BitScanReverse64((unsigned long*)&componentId, componentTypeId);

	const GpuBufferComponentIds<BufferType>& bufferIds = std::get<GpuBufferComponentIds<BufferType>>(mGpuComponentIds);
	auto it = bufferIds.idx.find(componentId);
	//if (it == bufferIds.idx.end())
	//	*(int*) NULL = 0;
	return components.template getComponentStorage<BufferType>(componentId).getAllocation(std::get<1>(*it)[idx]);
}

template<typename BufferType, typename CCType>
vulkanutils::BufferAllocation<BufferType>& Entity::getGpuComponentArray(CCType& components, size_t componentTypeId, size_t idx)
{
	size_t componentId = 0;
	_BitScanReverse64((unsigned long*)&componentId, componentTypeId);

	const GpuBufferComponentIds<BufferType>& bufferIds = std::get<GpuBufferComponentIds<BufferType>>(mGpuComponentIds);
	auto it = bufferIds.idx.find(componentId);
	//if (it == bufferIds.idx.end())
	//	*(int*) NULL = 0;
	return components.template getComponentStorage<BufferType>(componentId).getAllocation(std::get<1>(*it)[idx]);
}





template<typename BufferType, typename CCType>
std::vector<const vulkanutils::BufferAllocation<BufferType>*> Entity::getGpuComponentArrays(CCType& components, size_t componentTypeId) const
{
	size_t componentId = 0;
	_BitScanReverse64((unsigned long*)&componentId, componentTypeId);

	const GpuBufferComponentIds<BufferType>& bufferIds = std::get<GpuBufferComponentIds<BufferType>>(mGpuComponentIds);
	auto it = bufferIds.idx.find(componentId);
	//if (it == bufferIds.idx.end())
	//	*(int*) NULL = 0;

	const std::vector<size_t>& indices = std::get<1>(*it);
	std::vector<const vulkanutils::BufferAllocation<BufferType>*> ret;
	ret.reserve(indices.size());
	for (size_t i : indices)
		ret.push_back(&components.template getComponentStorage<BufferType>(componentId).getAllocation(i));
	return ret;
}

template<typename BufferType, typename CCType>
std::vector<vulkanutils::BufferAllocation<BufferType>*> Entity::getGpuComponentArrays(CCType& components, size_t componentTypeId)
{
	size_t componentId = 0;
	_BitScanReverse64((unsigned long*)&componentId, componentTypeId);

	const GpuBufferComponentIds<BufferType>& bufferIds = std::get<GpuBufferComponentIds<BufferType>>(mGpuComponentIds);
	auto it = bufferIds.idx.find(componentId);
	//if (it == bufferIds.idx.end())
	//	*(int*) NULL = 0;

	const std::vector<size_t>& indices = std::get<1>(*it);
	std::vector<vulkanutils::BufferAllocation<BufferType>*> ret;
	ret.reserve(indices.size());
	for (size_t i : indices)
		ret.push_back(&components.template getComponentStorage<BufferType>(componentId).getAllocation(i));
	return ret;
}




template<typename BufferType, typename CCType>
void Entity::removeGpuComponent(CCType& components, size_t componentId)
{
	GpuBufferComponentIds<BufferType>& bufferIds = std::get<GpuBufferComponentIds<BufferType>>(mGpuComponentIds);
	GpuComponentStorage<BufferType>& componentStorage = components.template getComponentStorage<BufferType>(componentId);

	for (size_t idx : bufferIds.idx[componentId])
		componentStorage.removeAllocation(idx);

	bufferIds.idx.erase(componentId);
	bufferIds.componentIds = (bufferIds.componentIds & ~componentId);
}

template<typename ComponentType, typename BufferType, typename CCType>
void Entity::removeGpuComponent(CCType& components)
{
	const size_t componentId = components.template getComponentTypeIndex<ComponentType>();
	removeGpuComponent<BufferType>(components, componentId);
}


template<typename ComponentType, typename CCType>
bool Entity::pendingDelete(const CCType& components) const
{
	const size_t componentId = components.template getComponentTypeIndex<ComponentType>();
	ComponentStorage<ComponentType>& cs = components.template getComponentStorage<ComponentType>();

	return hasComponentIds(size_t(1) << componentId)
		&& cs.pendingDeletion(mIdx[componentId].idx);
}

template<typename BufferType, typename ComponentType, typename CCType>
bool Entity::pendingGpuDelete(const CCType& components) const
{
	const size_t componentId = components.template getComponentTypeIndex<ComponentType>();
	const GpuBufferComponentIds<BufferType>& bufferIds = std::get<GpuBufferComponentIds<BufferType>>(mGpuComponentIds);
	GpuComponentStorage<BufferType>& componentStorage = components.template getComponentStorage<BufferType>(componentId);
	auto it = bufferIds.idx.find(componentId);
	if (it == bufferIds.idx.end())
		return false;

	if (hasGpuComponentIds<BufferType>(componentId))
	{
		for (size_t idx : bufferIds.idx[componentId])
			if (componentStorage.pendingDeletion(idx))
				return true;
	}
	return false;
}


template <typename CCType>
void cloneComponentsHelper(Entity& dst, const Entity& src, CCType& components)
{}

template <typename CCType, typename ComponentType, typename... CTs>
void cloneComponentsHelper(Entity& dst, const Entity& src, CCType& components)
{
	auto sca = src.getComponentArray<ComponentType>(components);
	ComponentType* dca = dst.addComponentArray<ComponentType>(components, sca.size());
	for (size_t i = 0; i < sca.size(); i++)
		dca[i] = sca[i];
	cloneComponentsHelper<CCType, CTs...>(dst, src, components);
}

template <typename... CTs, typename CCType>
void cloneComponents(Entity& dst, const Entity& src, CCType& components)
{
	cloneComponentsHelper<CCType, CTs...>(dst, src, components);
}

template<typename ComponentType, typename CCType>
void Entity::queueRemoveComponent(CCType& components)
{
	const size_t componentId = components.template getComponentTypeIndex<ComponentType>();
	mQueuedComponentDeletions.insert(componentId);
}

template<typename BufferType>
void Entity::queueRemoveGpuComponent(size_t componentId)
{
	GpuBufferComponentIds<BufferType>& bufferIds = std::get<GpuBufferComponentIds<BufferType>>(mGpuComponentIds);
	bufferIds.queuedComponentDeletions.insert(componentId);
}

template<typename ComponentType, typename BufferType, typename CCType>
void Entity::queueRemoveGpuComponent(CCType& components)
{
	const size_t componentId = components.template getComponentTypeIndex<ComponentType>();
	queueRemoveGpuComponent(componentId);
}

template<typename BufferType>
void Entity::queueRemoveAllGpuComponents()
{
	GpuBufferComponentIds<BufferType>& bufferIds = std::get<GpuBufferComponentIds<BufferType>>(mGpuComponentIds);
	for (auto& [componentId, componentReference] : bufferIds.idx)
	{
		bufferIds.queuedComponentDeletions.insert(componentId);
	}
}

template<typename CCType>
void Entity::updateComponentDeletion(CCType& components)
{
	for (ActiveDeletion& del : mActiveComponentDeletions)
		if (del.refCount == 0)
			removeComponent(components, del.componentId);

	mActiveComponentDeletions.erase(
		std::remove_if(std::begin(mActiveComponentDeletions), std::end(mActiveComponentDeletions),
			[](const ActiveDeletion& ad)
	{
		return ad.refCount == 0;
	}), std::end(mActiveComponentDeletions));

	for (size_t cIdx : mQueuedComponentDeletions)
		mActiveComponentDeletions.emplace_back(cIdx, 0);
	mQueuedComponentDeletions.clear();
}

template<typename CCType>
void Entity::updateGpuComponentDeletion(CCType& components)
{
	auto func = [&]<typename T>(GpuBufferComponentIds<T>& bufferIds)
	{
		for (ActiveDeletion& del : bufferIds.activeComponentDeletions)
			if (del.refCount == 0)
				removeGpuComponent<T, CCType>(components, del.componentId);

		bufferIds.activeComponentDeletions.erase(
			std::remove_if(std::begin(bufferIds.activeComponentDeletions), std::end(bufferIds.activeComponentDeletions),
				[](const ActiveDeletion& ad)
		{
			return ad.refCount == 0;
		}), std::end(bufferIds.activeComponentDeletions));


		for (size_t cIdx : bufferIds.queuedComponentDeletions)
			bufferIds.activeComponentDeletions.emplace_back(cIdx, 0);
		bufferIds.queuedComponentDeletions.clear();
	};
	func(std::get<GpuBufferComponentIds<vulkanutils::UploadBuffer>>(mGpuComponentIds));
	func(std::get<GpuBufferComponentIds<vulkanutils::VertexBuffer>>(mGpuComponentIds));
	func(std::get<GpuBufferComponentIds<vulkanutils::IndexBuffer>>(mGpuComponentIds));
	func(std::get<GpuBufferComponentIds<vulkanutils::UniformBuffer>>(mGpuComponentIds));
	func(std::get<GpuBufferComponentIds<vulkanutils::ShaderStorageBuffer>>(mGpuComponentIds));
	func(std::get<GpuBufferComponentIds<vulkanutils::IndirectDrawBuffer>>(mGpuComponentIds));
}

/*
template<typename ComponentType, typename... BufferTypes, typename CCType>
std::tuple<ecs::BufferAllocationType<ComponentType, BufferTypes>...> Entity::addGpuComponentArrays(CCType& components, size_t count)
{
	return std::tuple<ecs::BufferAllocationType<ComponentType, BufferTypes>...>{ addGpuComponentArray<BufferTypes, ComponentType>(components, count)... };
}

template<typename ComponentType, typename... BufferTypes, typename CCType>
std::tuple<ecs::BufferAllocationType<ComponentType, BufferTypes>...> Entity::getGpuComponentArrays(CCType& components)
{
	return std::tuple<ecs::BufferAllocationType<ComponentType, BufferTypes>...>{ getGpuComponentArray<BufferTypes, ComponentType>(components)... };
}
*/

};
