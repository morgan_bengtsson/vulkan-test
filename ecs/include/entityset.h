#pragma once

#include "entity.h"
#include <vector>

namespace ecs
{

class EntitySet
{
public:
	EntitySet();

	std::vector<Entity>& getAllEntities();
	const std::vector<Entity>& getAllEntities() const;

	Entity* getEntity(size_t entityId);
	const Entity* getEntity(size_t entityId) const;

	template<typename... T, typename CCType>
	std::vector<const Entity*> getRefEntities(const CCType& components, const Entity* entity, EntityRef::Type refType) const;
	template<typename... T, typename CCType>
	std::vector<Entity*> getRefEntities(CCType& components, const Entity* entity, EntityRef::Type refType);


	template<typename... T, typename CCType>
	std::vector<const Entity*> getEntities(const CCType& components) const;
	template<typename... T, typename CCType>
	std::vector<Entity*> getEntities(CCType& components);

	template<typename... T, typename CCType>
	std::vector<const Entity*> getPendingDestructedEntities(const CCType& components) const;
	template<typename... T, typename CCType>
	std::vector<Entity*> getPendingDestructedEntities(CCType& components);

	template<typename BufferType, typename... T, typename CCType>
	std::vector<const Entity*> getGpuPendingDestructedEntities(const CCType& components) const;
	template<typename BufferType, typename... T, typename CCType>
	std::vector<Entity*> getGpuPendingDestructedEntities(CCType& components);

	template<typename BufferType, typename... T, typename CCType>
	std::vector<Entity*> getGpuEntities(CCType& components);

	void addEntity(const Entity& entity);
	void queueRemoveEntity(size_t id);
private:

	std::vector<Entity> mEntities;
};


};



#include "entityset.tpp"