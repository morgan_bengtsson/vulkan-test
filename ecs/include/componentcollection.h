#pragma once

#include "component.h"
#include "componentstorage.h"

#include <tuple>

namespace ecs
{

template<Component... ComponentTypes>
class ComponentCollection
{
	template<typename ComponentType>
	static constexpr bool IsMember = (std::is_same_v<ComponentType, ComponentTypes> || ...);
public:
	ComponentCollection();
	~ComponentCollection();

	ComponentCollection(const ComponentCollection&) = delete;
	ComponentCollection(const ComponentCollection&&) = delete;

	ComponentCollection& operator=(const ComponentCollection&) = delete;
	ComponentCollection& operator=(ComponentCollection&&) = delete;

	static constexpr size_t numComponentTypes();

	template<typename ComponentType>
	ComponentStorage<ComponentType>& getComponentStorage() requires IsMember<ComponentType>;

	template<typename ComponentType>
	const ComponentStorage<ComponentType>& getComponentStorage() const requires IsMember<ComponentType>;

	template<typename ComponentType>
	static constexpr size_t getComponentTypeIndex() requires IsMember<ComponentType>;

	void removeComponent(size_t componentType, size_t idx, size_t n);
	template<typename ComponentType>
	void removeComponent(size_t idx, size_t n);

private:
	std::tuple<ComponentStorage<ComponentTypes>...> mComponentArrays;
};

};

#include "componentcollection.tpp"
