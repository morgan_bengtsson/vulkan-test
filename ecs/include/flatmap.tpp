#pragma once


#include "flatmap.h"

template<typename Key, typename Value>
FlatMap<Key, Value>::FlatMap()
	: mData()
{}

template<typename Key, typename Value>
std::tuple<Key, Value>* FlatMap<Key, Value>::data()
{
	return mData.data();
}


template<typename Key, typename Value>
const std::tuple<Key, Value>* FlatMap<Key, Value>::data() const
{
	return mData.data();
}

template<typename Key, typename Value>
bool FlatMap<Key, Value>::insert(const std::tuple<Key, Value>& value)
{
	auto it = std::lower_bound(std::begin(mData), std::end(mData), std::get<Key>(value),
		[](const std::tuple<Key, Value>& val, const Key& key)
	{
		return std::get<Key>(val) < key;
	});
	if (std::get<Key>(*it) == std::get<Key>(value))
	{
		return false;
	}
	mData.insert(it, value);
	return true;
}


template<typename Key, typename Value>
template<typename... Args>
bool FlatMap<Key, Value>::emplace(Args... args)
{
	std::tuple<Key, Value> obj(args...);
	auto it = std::lower_bound(std::begin(mData), std::end(mData), std::get<Key>(obj),
		[](const std::tuple<Key, Value>& val, const Key& key)
	{
		return std::get<Key>(val) < key;
	});
	if (std::get<Key>(*it) == std::get<Key>(obj))
	{
		return false;
	}
	mData.emplace(it, std::move(obj));
	return true;
}


template<typename Key, typename Value>
Value& FlatMap<Key, Value>::operator[](const Key& key)
{
	auto it = std::lower_bound(std::begin(mData), std::end(mData), key,
		[](const std::tuple<Key, Value>& val, const Key& key)
	{
		return std::get<Key>(val) < key;
	});
	if (it == std::end(mData) || std::get<Key>(*it) != key)
	{
		it = mData.emplace(it, std::make_tuple(key, Value()));
	}
	return std::get<Value>(*it);
}

template<typename Key, typename Value>
const Value& FlatMap<Key, Value>::operator[](const Key& key) const
{
	auto it = std::lower_bound(std::begin(mData), std::end(mData), key,
		[](const std::tuple<Key, Value>& val, const Key& key)
	{
		return std::get<Key>(val) < key;
	});
	if (it == std::end(mData) || std::get<Key>(*it) != key)
		*(int*) NULL = 0;
	return std::get<Value>(*it);
}



template<typename Key, typename Value>
FlatMap<Key, Value>::Iterator::Iterator(std::tuple<Key, Value>* ptr)
	: mPtr(ptr)
{}

template<typename Key, typename Value>
FlatMap<Key, Value>::Iterator FlatMap<Key, Value>::Iterator::operator++()
{
	return ++mPtr;
}

template<typename Key, typename Value>
FlatMap<Key, Value>::Iterator FlatMap<Key, Value>::Iterator::operator++(int)
{
	return mPtr++;
}

template<typename Key, typename Value>
std::tuple<Key, Value>& FlatMap<Key, Value>::Iterator::operator*()
{
	return *mPtr;
}

template<typename Key, typename Value>
const std::tuple<Key, Value>& FlatMap<Key, Value>::Iterator::operator*() const
{
	return *mPtr;
}





template<typename Key, typename Value>
FlatMap<Key, Value>::ConstIterator::ConstIterator(const std::tuple<Key, Value>* ptr)
	: mPtr(ptr)
{}

template<typename Key, typename Value>
FlatMap<Key, Value>::ConstIterator FlatMap<Key, Value>::ConstIterator::operator++()
{
	return ++mPtr;
}

template<typename Key, typename Value>
FlatMap<Key, Value>::ConstIterator FlatMap<Key, Value>::ConstIterator::operator++(int)
{
	return mPtr++;
}

template<typename Key, typename Value>
const std::tuple<Key, Value>& FlatMap<Key, Value>::ConstIterator::operator*() const
{
	return *mPtr;
}

template<typename Key, typename Value>
FlatMap<Key, Value>::Iterator FlatMap<Key, Value>::begin()
{
	return Iterator(mData.data());
}

template<typename Key, typename Value>
FlatMap<Key, Value>::Iterator FlatMap<Key, Value>::end()
{
	return Iterator(mData.data() + mData.size());
}

template<typename Key, typename Value>
FlatMap<Key, Value>::ConstIterator FlatMap<Key, Value>::begin() const
{
	return ConstIterator(mData.data());
}

template<typename Key, typename Value>
FlatMap<Key, Value>::ConstIterator FlatMap<Key, Value>::end() const
{
	return ConstIterator(mData.data() + mData.size());
}

template<typename Key, typename Value>
FlatMap<Key, Value>::Iterator FlatMap<Key, Value>::find(const Key& key)
{
	auto it = std::lower_bound(std::begin(mData), std::end(mData), key,
		[](const std::tuple<Key, Value>& val, const Key& key)
	{
		return std::get<Key>(val) < key;
	});
	if (it == std::end(mData) || std::get<Key>(*it) != key)
		return end();
	return Iterator(mData.data() + (it - std::begin(mData)));
}

template<typename Key, typename Value>
FlatMap<Key, Value>::ConstIterator FlatMap<Key, Value>::find(const Key& key) const
{
	auto it = std::lower_bound(std::begin(mData), std::end(mData), key,
		[](const std::tuple<Key, Value>& val, const Key& key)
	{
		return std::get<Key>(val) < key;
	});
	if (it == std::end(mData) || std::get<Key>(*it) != key)
		return end();
	return ConstIterator(mData.data() + (it - std::begin(mData)));
}

template<typename Key, typename Value>
size_t FlatMap<Key, Value>::Iterator::operator-(FlatMap<Key, Value>::Iterator rhs) const
{
	return mPtr - rhs.mPtr;
}

template<typename Key, typename Value>
size_t FlatMap<Key, Value>::ConstIterator::operator-(FlatMap<Key, Value>::ConstIterator rhs) const
{
	return mPtr - rhs.mPtr;
}

template<typename Key, typename Value>
bool FlatMap<Key, Value>::Iterator::operator==(FlatMap<Key, Value>::Iterator rhs) const
{
	return mPtr == rhs.mPtr;
}

template<typename Key, typename Value>
bool FlatMap<Key, Value>::ConstIterator::operator==(FlatMap<Key, Value>::ConstIterator rhs) const
{
	return mPtr == rhs.mPtr;
}


template<typename Key, typename Value>
void FlatMap<Key, Value>::erase(Iterator it)
{
	mData.erase(mData.begin() + (it - begin()));
}

template<typename Key, typename Value>
void FlatMap<Key, Value>::erase(ConstIterator it)
{
	mData.erase(mData.begin() + (it - begin()));
}

template<typename Key, typename Value>
void FlatMap<Key, Value>::erase(const Key& key)
{
	auto it = find(key);
	erase(it);
}