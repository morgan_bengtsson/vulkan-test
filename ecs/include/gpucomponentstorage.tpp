#pragma once

#include "gpucomponentstorage.h"

namespace ecs
{

template<typename BufferType>
GpuComponentStorage<BufferType>::GpuComponentStorage(vulkanutils::Device* device)
	: mAllocator(device, 65536)
	, mAllocations()
{}

template<typename BufferType>
size_t
GpuComponentStorage<BufferType>::size() const
{
	return mAllocations.size();
}

template<typename BufferType>
vulkanutils::BufferAllocation<BufferType>&
GpuComponentStorage<BufferType>::addItemArray(size_t size, size_t count)
{
	auto allocation = mAllocator.allocate(size * count);
	return mAllocations.emplace_back(std::move(allocation));
}


template<typename BufferType>
vulkanutils::BufferAllocation<BufferType>&
GpuComponentStorage<BufferType>::getAllocation(size_t idx)
{
	return mAllocations[idx];
}


template<typename BufferType>
const vulkanutils::BufferAllocation<BufferType>&
GpuComponentStorage<BufferType>::getAllocation(size_t idx) const
{
	return mAllocations[idx];
}


template<typename BufferType>
void
GpuComponentStorage<BufferType>::removeAllocation(size_t idx)
{
	mAllocations[idx] = vulkanutils::BufferAllocation<BufferType>();
}

template<typename ComponentType, typename BufferType>
GpuComponentStorage<ComponentType, BufferType>::GpuComponentStorage(vulkanutils::Device* device)
	: GpuComponentStorage<BufferType>(device)
{}


template<typename ComponentType, typename BufferType>
vulkanutils::BufferAllocation<BufferType>&
GpuComponentStorage<ComponentType, BufferType>::addItemArray(size_t count)
{
	return GpuComponentStorage<BufferType>::addItemArray(sizeof(ComponentType), count);
}


template<typename ImageType, typename... Args>
void GpuImageComponentStorage::addImage(Args... args)
{
	auto allocation = mAllocator.allocate<ImageType>(args...);
	mAllocations.emplace_back(std::move(allocation));
}

};