#pragma once

#include "component.h"
#include "gpucomponentcollection.h"

namespace ecs
{

template<Component... GpuComponentTypeSubset>
class GpuComponentCollectionSubSet
{
	template<typename ComponentType>
	static constexpr bool IsMember = (std::is_same_v<ComponentType, GpuComponentTypeSubset> || ...);
	//static constexpr bool IsMember = std::disjunction<std::is_same<ComponentType, GpuComponentTypeSubset>...>::value;
public:
	template<typename... ComponentTypes>
	GpuComponentCollectionSubSet(GpuComponentCollection<ComponentTypes...>& cc);

	template<typename... ComponentTypes>
	GpuComponentCollectionSubSet(GpuComponentCollectionSubSet<ComponentTypes...>& cc);

	static constexpr size_t numComponentTypes();


	template<typename ComponentType, typename BufferType>
	GpuComponentStorage<ComponentType, BufferType>& getComponentStorage() requires IsMember<ComponentType>;
	template<typename ComponentType, typename BufferType>
	const GpuComponentStorage<ComponentType, BufferType>& getComponentStorage() const requires IsMember<ComponentType>;

	template<typename BufferType>
	GpuComponentStorage<BufferType>& getComponentStorage(size_t compTypeId);
	template<typename BufferType>
	const GpuComponentStorage<BufferType>& getComponentStorage(size_t compTypeId) const;


	template<typename ComponentType>
	GpuImageComponentStorage& getImageComponentStorage() requires IsMember<ComponentType>;
	template<typename ComponentType>
	const GpuImageComponentStorage& getImageComponentStorage() const requires IsMember<ComponentType>;

	GpuImageComponentStorage& getImageComponentStorage(size_t compTypeId);
	template<typename BufferType>
	const GpuImageComponentStorage& getImageComponentStorage(size_t compTypeId) const;


	template<typename ComponentType>
	const size_t getComponentTypeIndex() const requires IsMember<ComponentType>;
private:
	template<typename BufferType>
	struct ComponentArrayPtrs
	{
		ComponentArrayPtrs(GpuComponentStorage<GpuComponentTypeSubset, BufferType>*... subsets)
			: mArrayPtrs{subsets...}
			, mPtrArray{subsets...}
		{}
		std::tuple<GpuComponentStorage<GpuComponentTypeSubset, BufferType>*...> mArrayPtrs;
		std::array<GpuComponentStorage<BufferType>*, sizeof...(GpuComponentTypeSubset)> mPtrArray;
	};

	const std::array<const size_t, sizeof...(GpuComponentTypeSubset)> mTypeIdx;
	const std::tuple<ComponentArrayPtrs<vulkanutils::UploadBuffer>,
					ComponentArrayPtrs<vulkanutils::VertexBuffer>,
					ComponentArrayPtrs<vulkanutils::IndexBuffer>,
					ComponentArrayPtrs<vulkanutils::UniformBuffer>,
					ComponentArrayPtrs<vulkanutils::ShaderStorageBuffer>,
					ComponentArrayPtrs<vulkanutils::IndirectDrawBuffer>> mArrays;
	std::array<GpuImageComponentStorage*, sizeof...(GpuComponentTypeSubset)> mImagePtrArray;
};

};

#include "gpucomponentcollectionsubset.tpp"