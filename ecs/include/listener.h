#pragma once
#include"Dispatcher.h"

namespace comm
{
   
template<typename... Messages>
class Dispatcher;
   
template<typename Message>
class Listener
{
public:
   Listener(){}
   virtual ~Listener(){}

   virtual void execute(const Message& message) = 0;
  
   template<typename Dispatcher>
   void attachTo(Dispatcher* d)
   {
      d->template attach<Message>(this);
   }
};

template<typename... Message>
class MessageListener : public Listener<Message>...
{
  public:
   template<typename Dispatcher>
      void attachTo(Dispatcher* d)
   {
      attach<Dispatcher, Message...>(d);
   }
   
  private:
   template<typename Dispatcher>
      void attach(Dispatcher* d){}
   
   template<typename Dispatcher, typename M, typename... Ms>
      void attach(Dispatcher* d)
   {
      d->template attach<M>(this);
      attach<Dispatcher, Ms...>(d);
   }
};

};
