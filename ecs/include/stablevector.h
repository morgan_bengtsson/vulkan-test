#pragma once

#include <vector>
#include <memory>

namespace ecs
{

void stable_vector_benchmark();

template<typename T>
class StableVector
{
public:
	StableVector(size_t defaultSize = 2048 / sizeof(T));

	T& operator[](size_t idx);
	const T& operator[](size_t idx) const;

	class Iterator
	{
	public:
		Iterator(const Iterator&) = default;
		Iterator& operator=(const Iterator&) = default;

		T& operator*();
		Iterator& operator++();
		Iterator& operator++(int);
		bool operator==(const Iterator& rhs) const;
		size_t getIdx() const;
	private:
		friend class StableVector<T>;
		friend class ConstIterator;

		Iterator(StableVector& vector, size_t chunk, size_t block, size_t chunkOffset);

		StableVector* mVector;
		size_t mChunk;
		size_t mBlock;
		size_t mChunkOffset;
	};

	class ConstIterator
	{
	public:
		ConstIterator(const ConstIterator&) = default;
		ConstIterator& operator=(const ConstIterator&) = default;
		ConstIterator(const Iterator&);

		const T& operator*() const;
		ConstIterator& operator++();
		ConstIterator& operator++(int);
		bool operator==(const ConstIterator& rhs) const;
		size_t getIdx() const;
	private:
		friend class StableVector<T>;

		ConstIterator(const StableVector& vector, size_t chunk, size_t block, size_t chunkOffset);

		const StableVector* mVector;
		size_t mChunk;
		size_t mBlock;
		size_t mChunkOffset;
	};

	Iterator insert(const T& val);
	std::tuple<Iterator, Iterator> insert(const T& val, size_t n);
	std::tuple<Iterator, Iterator> emplace(T&& val, size_t n);
	bool resizeRange(const ConstIterator& startIt, size_t prevSize, size_t newSize);

	size_t freePreceedingSize(const ConstIterator& it);

	ConstIterator erase(const ConstIterator& it);
	Iterator erase(const Iterator& it);
	void erase(size_t idx);

	Iterator begin();
	Iterator end();
	Iterator it(size_t idx);

	ConstIterator begin() const;
	ConstIterator end() const;
	ConstIterator it(size_t idx) const;

private:
	friend class Iterator;
	friend class ConstIterator;

	size_t findChunk(size_t idx) const;

	struct Chunk
	{
		Chunk(size_t start, size_t size);
		size_t insert();
		std::tuple<size_t, size_t> insert(size_t n);
		void erase(size_t idx);
		void erase(size_t block, size_t chunkOffset);
		size_t findBlock(size_t chunkOffset) const;

		struct Block
		{
			size_t start;
			bool used;
		};
		size_t start;
		size_t size;
		std::vector<Block> mBlocks;
		std::unique_ptr<T[]> mData;
	};

	size_t mFirstFreeChunk;
	size_t mDefaultChunkSize;
	std::vector<Chunk> mChunks;
};

};

#include "stablevector.tpp"
