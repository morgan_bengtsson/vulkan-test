#pragma once

namespace ecs
{

template<typename... ComponentTypes>
Scene<ComponentTypes...>::Scene(vulkanutils::Device* device)
	: mComponentCollection()
	, mGpuComponentCollection(device)
{}

template<typename... ComponentTypes>
void Scene<ComponentTypes...>::postUpdate()
{
	return;
	auto& entities = EntitySet::getAllEntities();
	for (Entity& e : entities)
	{
		e.updateComponentDeletion(getComponentCollection());
		e.updateGpuComponentDeletion(getGpuComponentCollection());
	}
	entities.erase(std::remove_if(
		std::begin(entities), std::end(entities),
		[](Entity& e)
	{
		return !e.getComponentIds()
			&& !e.getGpuComponentIds<vulkanutils::UploadBuffer>()
			&& !e.getGpuComponentIds<vulkanutils::VertexBuffer>()
			&& !e.getGpuComponentIds<vulkanutils::IndexBuffer>()
			&& !e.getGpuComponentIds<vulkanutils::UniformBuffer>()
			&& !e.getGpuComponentIds<vulkanutils::ShaderStorageBuffer>();
	}), std::end(entities));
}

template<typename... ComponentTypes>
ComponentCollection<ComponentTypes...>& Scene<ComponentTypes...>::getComponentCollection()
{
	return mComponentCollection;
}

template<typename... ComponentTypes>
const ComponentCollection<ComponentTypes...>& Scene<ComponentTypes...>::getComponentCollection() const
{
	return mComponentCollection;
}

template<typename... ComponentTypes>
GpuComponentCollection<ComponentTypes...>& Scene<ComponentTypes...>::getGpuComponentCollection()
{
	return mGpuComponentCollection;
}

template<typename... ComponentTypes>
const GpuComponentCollection<ComponentTypes...>& Scene<ComponentTypes...>::getGpuComponentCollection() const
{
	return mGpuComponentCollection;
}

/*
template<typename... ComponentTypes>
GpuImageComponentStorage& Scene<ComponentTypes...>::getImageStorage()
{
	return mImageStorage;
}

template<typename... ComponentTypes>
const GpuImageComponentStorage& Scene<ComponentTypes...>::getImageStorage() const
{
	return mImageStorage;
}
*/

template<typename... ComponentTypes>
template<typename... T>
std::vector<const Entity*> Scene<ComponentTypes...>::getEntities() const
{
	return EntitySet::getEntities<T...>(mComponentCollection);
}

template<typename... ComponentTypes>
template<typename... T>
std::vector<Entity*> Scene<ComponentTypes...>::getEntities()
{
	return EntitySet::getEntities<T...>(mComponentCollection);
}

template<typename... ComponentTypes>
template<typename BufferType, typename... T>
std::vector<Entity*> Scene<ComponentTypes...>::getGpuEntities()
{
	return getGpuEntities<BufferType, T...>(mComponentCollection);
}

template<typename... ComponentTypes>
template<typename... T>
std::vector<const Entity*> Scene<ComponentTypes...>::getRefEntities(const Entity* entity, EntityRef::Type refType) const
{
	return getRefEntities<T...>(mComponentCollection, entity, refType);
}

template<typename... ComponentTypes>
template<typename... T>
std::vector<Entity*> Scene<ComponentTypes...>::getRefEntities(const Entity* entity, EntityRef::Type refType)
{
	return getRefEntities<T...>(mComponentCollection, entity, refType);
}


};