#pragma once

#include "componentcollection.h"

namespace ecs
{

template<size_t N, typename ComponentType, typename T, typename... Ts>
requires std::is_same<ComponentType, T>::value
static constexpr size_t getTypeIndexHelper()
{
	return N;
}

template<size_t N, typename ComponentType, typename T, typename... Ts>
requires !std::is_same<ComponentType, T>::value
static constexpr size_t getTypeIndexHelper()
{
	return getTypeIndexHelper<N + 1, ComponentType, Ts...>();
}

template<typename ComponentType, typename T, typename... Ts>
static constexpr size_t getTypeIndex()
{
	return getTypeIndexHelper<0, ComponentType, T, Ts...>();
}


template<Component... ComponentTypes>
ComponentCollection<ComponentTypes...>::ComponentCollection()
	:mComponentArrays({ getTypeIndex<ComponentTypes, ComponentTypes...>()... })
{
}

template<Component... ComponentTypes>
ComponentCollection<ComponentTypes...>::~ComponentCollection()
{
}

template<Component... ComponentTypes>
constexpr size_t ComponentCollection<ComponentTypes...>::numComponentTypes()
{
	return sizeof...(ComponentTypes);
}

template<Component... ComponentTypes>
template<typename ComponentType>
void ComponentCollection<ComponentTypes...>::removeComponent(size_t idx, size_t n)
{
	ComponentStorage<ComponentType>& cs = std::get<ComponentStorage<ComponentType>>(mComponentArrays);
	auto it = cs.it(idx);
	for (size_t i = 0; i < n; i++)
		it = cs.erase(it);
};

template<Component... ComponentTypes>
void ComponentCollection<ComponentTypes...>::removeComponent(size_t componentType, size_t idx, size_t n)
{
	typedef void (ComponentCollection<ComponentTypes...>::*FuncType)(size_t idx, size_t n);
	std::array<FuncType, sizeof...(ComponentTypes)> funcList { (&ComponentCollection<ComponentTypes...>::removeComponent<ComponentTypes>)... };
	(this->*funcList[componentType])(idx, n);
}

template<Component... ComponentTypes>
template<typename ComponentType>
ComponentStorage<ComponentType>& ComponentCollection<ComponentTypes...>::getComponentStorage()
	requires IsMember<ComponentType>
{
	return std::get<ComponentStorage<ComponentType>>(mComponentArrays);
}

template<Component... ComponentTypes>
template<typename ComponentType>
const ComponentStorage<ComponentType>& ComponentCollection<ComponentTypes...>::getComponentStorage() const
	requires IsMember<ComponentType>
{
	return std::get<ComponentStorage<ComponentType>>(mComponentArrays);
}

template<Component... ComponentTypes>
template<typename ComponentType>
constexpr size_t ComponentCollection<ComponentTypes...>::getComponentTypeIndex()
	requires IsMember<ComponentType>
{
	return getTypeIndex<ComponentType, ComponentTypes...>();
}

};