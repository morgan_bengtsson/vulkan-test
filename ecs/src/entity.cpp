#include "entity.h"

#include <set>

namespace ecs
{

Entity::Entity(const char* name)
	: mId(++Entity::NUM_ENTITIES)
	, mName(name)
	, mModified(true)
	, mComponentIds()
	, mQueuedComponentDeletions()
	, mActiveComponentDeletions()
	, mImageIdx()
	, mGpuComponentIds()
	, mIdx()
	, mImageComponentIds()
{
}

Entity::Entity(const Entity& e)
	: mId(e.mId)
	, mName(e.mName)
	, mModified(e.mModified)
	, mComponentIds(e.mComponentIds)
	, mQueuedComponentDeletions(e.mQueuedComponentDeletions)
	, mActiveComponentDeletions(e.mActiveComponentDeletions)
	, mImageIdx(e.mImageIdx)
	, mGpuComponentIds(e.mGpuComponentIds)
	, mIdx(e.mIdx)
	, mImageComponentIds(e.mImageComponentIds)
{
}

size_t Entity::getId() const
{
	return mId;
}

bool Entity::hasComponentIds(size_t ids) const
{
	return (ids & mComponentIds) == ids;
}



void Entity::setModified()
{
	mModified = true;
}

void Entity::setUnmodified() const
{
	mModified = false;
}

bool Entity::isModified() const
{
	return mModified;
}

void Entity::setName(const char* name)
{
	mName = name;
}

const char* Entity::getName() const
{
	return mName.c_str();
}

bool Entity::getComponentIds() const
{
	return mComponentIds;
}

bool Entity::hasImageComponentIds(size_t ids) const
{
	return (ids & mImageComponentIds) == ids;
}

void Entity::debugBreakOn(const char* name) const
{
	if (std::strcmp(getName(), name) == 0)
		__debugbreak();
}

void Entity::queueRemoveAllComponents()
{
	for (auto& [componentId, componentReference] : mIdx)
	{
		mQueuedComponentDeletions.insert(componentId);
	}
}

void Entity::queueRemoveAllGpuComponents()
{
	queueRemoveAllGpuComponents<vulkanutils::UploadBuffer>();
	queueRemoveAllGpuComponents<vulkanutils::VertexBuffer>();
	queueRemoveAllGpuComponents<vulkanutils::IndexBuffer>();
	queueRemoveAllGpuComponents<vulkanutils::UniformBuffer>();
	queueRemoveAllGpuComponents<vulkanutils::ShaderStorageBuffer>();
	queueRemoveAllGpuComponents<vulkanutils::IndirectDrawBuffer>();
}


void Entity::queueRemoveComponent(size_t componentId)
{
	mQueuedComponentDeletions.insert(componentId);
}


};