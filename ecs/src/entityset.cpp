#include "entityset.h"

namespace ecs
{

EntitySet::EntitySet()
	:mEntities()
{}

std::vector<Entity>& EntitySet::getAllEntities()
{
	return mEntities;
}

const std::vector<Entity>& EntitySet::getAllEntities() const
{
	return mEntities;
}

const Entity* EntitySet::getEntity(size_t entityId) const
{
	auto entityIt = std::lower_bound(std::begin(mEntities), std::end(mEntities), entityId
		, [] (const Entity& e1, const size_t e2)
	{
		return e1.getId() < e2;
	});
	if (entityIt != std::end(mEntities) && entityIt->getId() == entityId)
	{
		return &*entityIt;
	}
	return nullptr;
}

Entity* EntitySet::getEntity(size_t entityId)
{
	auto entityIt = std::lower_bound(std::begin(mEntities), std::end(mEntities), entityId
		, [] (const Entity& e1, const size_t e2)
	{
		return e1.getId() < e2;
	});
	if (entityIt != std::end(mEntities) && entityIt->getId() == entityId)
	{
		return &*entityIt;
	}
	return nullptr;
}

void EntitySet::addEntity(const Entity& entity)
{
	mEntities.insert(std::upper_bound(mEntities.begin(), mEntities.end(), entity, 
		[] (const Entity& e1, const Entity& e2)
	{
		return e1.getId() < e2.getId();
	}), entity);
}


void EntitySet::queueRemoveEntity(size_t entityId)
{
	auto it = std::lower_bound(mEntities.begin(), mEntities.end(), entityId, 
		[] (const Entity& e1, size_t id)
	{
		return e1.getId() < id;
	});

	if (it != mEntities.end() && it->getId() == entityId)
	{
		it->queueRemoveAllComponents();
		it->queueRemoveAllGpuComponents();
	}
}

};
