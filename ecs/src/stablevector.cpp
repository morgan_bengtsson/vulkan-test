#include "stablevector.h"

#include <chrono>
#include <thread>
#include <iostream>
#include <list>
#include <set>

namespace ecs
{

void stable_vector_benchmark()
{
	StableVector<int> sv;
	std::vector<int> v;
	std::list<int> list;
	std::set<int> set;

	constexpr const int N = 1000000;
	{
		auto tp = std::chrono::system_clock::now();
		v.resize(N);
		for (int i = 0; i < N; i++)
			v.push_back(i);
		std::chrono::microseconds duration = std::chrono::duration_cast<std::chrono::microseconds>(std::chrono::system_clock::now() - tp);

		std::cout << "Vector push back: " << duration.count() << " microseconds " << std::endl;
	}

	{
		auto tp = std::chrono::system_clock::now();
		for (int i = 0; i < N; i++)
			sv.insert(i);
		std::chrono::microseconds duration = std::chrono::duration_cast<std::chrono::microseconds>(std::chrono::system_clock::now() - tp);

		std::cout << "Stable Vector insert: " << duration.count() << " microseconds " << std::endl;
	}

	{
		auto tp = std::chrono::system_clock::now();
		for (int i = 0; i < N; i++)
			list.push_back(i);
		std::chrono::microseconds duration = std::chrono::duration_cast<std::chrono::microseconds>(std::chrono::system_clock::now() - tp);

		std::cout << "list insert: " << duration.count() << " microseconds " << std::endl;
	}

	{
		auto tp = std::chrono::system_clock::now();
		for (int i = 0; i < N; i++)
			set.insert(i);
		std::chrono::microseconds duration = std::chrono::duration_cast<std::chrono::microseconds>(std::chrono::system_clock::now() - tp);

		std::cout << "set insert: " << duration.count() << " microseconds " << std::endl;
	}
}



};