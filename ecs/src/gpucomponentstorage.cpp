#include "gpucomponentstorage.h"

namespace ecs
{

GpuImageComponentStorage::GpuImageComponentStorage(vulkanutils::Device* device)
	: mAllocator(device)
	, mAllocations()
{}

size_t GpuImageComponentStorage::size() const
{
	return mAllocations.size();
}

vulkanutils::ImageAllocation& GpuImageComponentStorage::getAllocation(size_t idx)
{
	return mAllocations[idx];
}


const vulkanutils::ImageAllocation& GpuImageComponentStorage::getAllocation(size_t idx) const
{
	return mAllocations[idx];
}

GpuImageComponentStorage::GpuImageComponentStorage(GpuImageComponentStorage&& rhs)
	: mAllocator(std::move(rhs.mAllocator))
	, mAllocations(std::move(rhs.mAllocations))
{
}


GpuImageComponentStorage& GpuImageComponentStorage::operator=(GpuImageComponentStorage&& rhs)
{
	std::swap(mAllocator, rhs.mAllocator);
	std::swap(mAllocations, rhs.mAllocations);
	return *this;
}

};
